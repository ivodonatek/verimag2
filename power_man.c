//******************************************************************************
//  Management nabijeni/uspavani/probouzeni.
//*****************************************************************************/

#include "power_man.h"
#include "LPC23xx.h"
#include "type.h"
#include "ui.h"
#include "buttons.h"
#include "display.h"
#include "menu_tables.h"
#include "maine.h"
#include "rtc.h"
#include "target.h"
#include "step_up.h"
#include "modbus/mb.h"
#include "devices/ADP5063/ADP5063.h"

// ADP5063 hodnoty registru
#define ADP5063_VINx_Pin_Set_REG_VALUE                 (0x0B)    /* VINx pin settings */
#define ADP5063_Termination_Set_REG_VALUE              (0x8D)    /* Termination settings */
#define ADP5063_Volt_Thresholds_REG_VALUE              (0x6c)    /* Voltage thresholds */
#define ADP5063_Func_Set_1_REG_VALUE                   (0x25)    /* Functional Settings 1 */

#define INIT_TIMEOUT         5      //timeout initu nabijecky [s]

extern const unsigned char ucSlaveID[];

// Fw module state
static volatile struct
{
    // Power-down mode
    BOOL powerDownMode;

    // pomoc. pocitadlo pro probuzeni
    unsigned char wakeUpCounter;

    // casovac initu nabijecky
    unsigned long initTimer;

    // stav baterie
    TBatteryStatus batteryStatus;

} state;

//------------------------------------------------------------------------------
//  Volat pro probuzeni.
//------------------------------------------------------------------------------
static void WakeUpTask(void)
{
    if (state.powerDownMode)
    {
        RTC_Task(FALSE);

        if (Buttons_GetScanCode() != BTC_NONE)
        {
            state.wakeUpCounter++;
            if (state.wakeUpCounter >= 5) // do 5s
                state.powerDownMode = FALSE;
        }
        else
        {
            state.wakeUpCounter = 0;
        }
    }
}

//------------------------------------------------------------------------------
//  Cteni stavu ADP5063, po chybe vraci FALSE.
//------------------------------------------------------------------------------
static BOOL ReadState_ADP5063(void)
{
    state.batteryStatus = BATS_NO_BATTERY;

    int16_t status1 = ADP5063_I2CReadReg(ADP5063_Charger_Status_1_REG);
    if (status1 < 0)
        return FALSE;

    int16_t status2 = ADP5063_I2CReadReg(ADP5063_Charger_Status_2_REG);
    if (status2 < 0)
        return FALSE;

    /*
    Charger Status 1

    [7] VIN_OV R
    1 = the voltage at the VINx pins exceeds VVIN_OV.

    [6] VIN_OK R
    1 = the voltage at the VINx pins exceeds VVIN_OK_RISE and VVIN_OK_FALL.

    [5] VIN_ILIM R
    1 = the current into a VINx pin is limited by the high voltage blocking
    FET and the charger is not running at the full programmed ICHG.

    [4] THERM_LIM R
    1 = the charger is not running at the full programmed ICHG but is
    limited by the die temperature.

    [3] CHDONE R
    1 = the end of a charge cycle has been reached. This bit latches on,
    in that it does not reset to low when the VRCH threshold is breached.

    [2:0] CHARGER_STATUS[2:0] R Charger status bus.
    000 = off.
    001 = trickle charge.
    010 = fast charge (CC mode).
    011 = fast charge (CV mode).
    100 = charge complete.
    101 = LDO mode.
    110 = trickle or fast charge timer expired.
    111 = battery detection.
    */
    unsigned char chargerStatus = status1 & 0x07;
    if ((chargerStatus != 0) && (chargerStatus != 0x07))
        state.batteryStatus = BATS_CHARGING;

    /*
    Charger Status 2

    [7:5] THR_STATUS[2:0] R THR pin status.
    000 = off.
    001 = battery cold.
    010 = battery cool.
    011 = battery warm.
    100 = battery hot.
    111 = thermistor OK.

    [4] Not used

    [3] RCH_LIM_INFO R The recharge limit information function is activated when DIS_RCH is
    logic high and CHARGER_STATUS[2:0] = 100 (binary). The
    RCH_LIM_INFO bit informs the system that a recharge cycle is required.
    0 = VBAT_SNS > VRCH
    1 = VBAT_SNS < VRCH

    [2:0] BATTERY_STATUS[2:0] R Battery status bus.
    000 = battery monitor off.
    001 = no battery.
    010 = VBAT_SNS < VTRK_DEAD.
    011 = VTRK_DEAD < VBAT_SNS < VWEAK.
    100 = VBAT_SNS > VWEAK.
    */
    if (chargerStatus == 0)
    {
        unsigned char batteryStatus = status2 & 0x07;
        if (batteryStatus == 2)
        {
            state.batteryStatus = BATS_LOW_BATTERY;
            PowerMan_SwitchOff();
        }
        else if (batteryStatus == 3)
            state.batteryStatus = BATS_MEDIUM_BATTERY;
        else if (batteryStatus == 4)
            state.batteryStatus = BATS_HIGH_BATTERY;
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//  Nastavi ADP5063, po chybe vraci FALSE.
//------------------------------------------------------------------------------
static BOOL Init_ADP5063(void)
{
    if (ADP5063_I2CReadReg(ADP5063_Manuf_Model_ID_REG) < 0)
        return FALSE;

    if (!ADP5063_I2CWriteReadReg(ADP5063_VINx_Pin_Set_REG, ADP5063_VINx_Pin_Set_REG_VALUE))
        return FALSE;

    if (!ADP5063_I2CWriteReadReg(ADP5063_Termination_Set_REG, ADP5063_Termination_Set_REG_VALUE))
        return FALSE;

    if (!ADP5063_I2CWriteReadReg(ADP5063_Volt_Thresholds_REG, ADP5063_Volt_Thresholds_REG_VALUE))
        return FALSE;

    if (!ADP5063_I2CWriteReadReg(ADP5063_Func_Set_1_REG, ADP5063_Func_Set_1_REG_VALUE))
        return FALSE;

    return ReadState_ADP5063();
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//  Mela by byt prvni funkce v mainu.
//------------------------------------------------------------------------------
void PowerMan_Init(void)
{
    PCONP = 0x00;

    state.powerDownMode = FALSE;
    state.initTimer = RTC_GetSysSeconds();
    state.batteryStatus = BATS_NO_BATTERY;

    ADP5063_IOSetup();
    ADP5063_I2CSetup();
    Init_ADP5063();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void PowerMan_Task(void)
{
    if (RTC_IsSysSecondTimeout(state.initTimer, INIT_TIMEOUT))
    {
        state.initTimer = RTC_GetSysSeconds();
        Init_ADP5063();
    }
}

//------------------------------------------------------------------------------
//  Vypnout.
//------------------------------------------------------------------------------
void PowerMan_SwitchOff(void)
{
    DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;
    DisplayOff();
    StepUp_Set(STPUP_OFF);

    state.powerDownMode = TRUE;
    state.wakeUpCounter = 0;
    PCONP = (1<<9);                // only PCRTC power ON

    while (state.powerDownMode)
    {
        WakeUpTask();
        if (state.powerDownMode)
        {
            INTWAKE = (1<<15);  // RTCWAKE
            PCON = (1<<1);
        }
    }

    TargetResetInit();
    InitBoard(FALSE);
}

//------------------------------------------------------------------------------
//  Vrati stav baterie.
//------------------------------------------------------------------------------
TBatteryStatus PowerMan_GetBatteryStatus(void)
{
    return state.batteryStatus;
}
