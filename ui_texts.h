//******************************************************************************
//  Texty uzivatelskeho rozhrani.
//*****************************************************************************/

#ifndef UI_TEXTS_H_INCLUDED
#define UI_TEXTS_H_INCLUDED

// Identifikatory textu
typedef enum
{
    TXTID_TestID = 0,
    TXTID_TestMeas2C1 = 1,
    TXTID_TestMeas2C2 = 2,
    TXTID_TestMeas1E1 = 3,
    TXTID_TestMeas1Temp = 4,
    TXTID_TestCInductance = 5,
    TXTID_TestExcitationIn = 6,
    TXTID_TestFrequency = 7,
    TXTID_TestIout = 8,
    TXTID_TestMagX2Comm = 9,
    TXTID_TestSensorSlave = 10,
    TXTID_TestSensorMaster = 11,
    TXTID_TestModbus = 12,
    TXTID_TestADP5063 = 13,
    TXTID_NA = 14,
    TXTID_CalibrationDate = 15,
    TXTID_ON = 16,
    TXTID_OFF = 17,
    TXTID_Backlight = 18,
    TXTID_UnitNo = 19,
    TXTID_Firmware = 20,
    TXTID_Info = 21,
    TXTID_Date = 22,
    TXTID_Time = 23,
    TXTID_Setting = 24,
    TXTID_PowerOff = 25,
    TXTID_PowerOffQuestion = 26,
    TXTID_Error = 27,
    TXTID_Timeout = 28,
    TXTID_EmptyString = 29,
    TXTID_TestHw = 30,
    TXTID_Debug = 31,
    TXTID_Test = 32,
    TXTID_Progress = 33,
    TXTID_TestComplete = 34,
    TXTID_C1Rezistance = 35,
    TXTID_C2Rezistance = 36,
    TXTID_EInsulation = 37,
    TXTID_TempSensor = 38,
    TXTID_CInductance = 39,
    TXTID_ExcitationLevels = 40,
    TXTID_Frequency = 41,
    TXTID_Iout = 42,
    TXTID_Magx2UnitNo = 43,
    TXTID_Magx2Firmware = 44,
    TXTID_SensorUnitNo = 45,
    TXTID_SensorFirmware = 46,
    TXTID_Diameter = 47,
    TXTID_FlowRange = 48,
    TXTID_ErrorMin = 49,
    TXTID_OkMin = 50,
    TXTID_ErrorCode = 51,
    TXTID_Total = 52,
    TXTID_TotalPlus = 53,
    TXTID_TotalMinus = 54,
    TXTID_Aux = 55,
    TXTID_CalibrationPoint1 = 56,
    TXTID_CalibrationPoint2 = 57,
    TXTID_CalibrationPoint3 = 58,
    TXTID_CalibrationData1 = 59,
    TXTID_CalibrationData2 = 60,
    TXTID_CalibrationData3 = 61,
    TXTID_Totals = 62,
    TXTID_Calibration = 63,
    TXTID_CurrentLoopSetting = 64,
    TXTID_Magx2Passwords = 65,
    TXTID_FrequencyOutputSetting = 66,
    TXTID_Measurement = 67,
    TXTID_Excitation = 68,
    TXTID_FlowSimulationSetting = 69,
    TXTID_Pass = 70,
    TXTID_Fail = 71,
    TXTID_PrepareSector = 72,
    TXTID_WriteData = 73,
    TXTID_CompareData = 74,
    TXTID_Unsupported = 75,
    TXTID_LowFlowCutoff = 76,
    TXTID_StoppingTest = 77,
    TXTID_TestStopped = 78,

} TUITextId;

// pocet textu
#define UI_TEXT_COUNT   (TXTID_TestStopped + 1)

//------------------------------------------------------------------------------
//  Vrati text podle identifikatoru.
//------------------------------------------------------------------------------
const char* UI_GetText(TUITextId id);

#endif // UI_TEXTS_H_INCLUDED
