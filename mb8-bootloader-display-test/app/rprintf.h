#ifndef rprintf_h_
#define rprintf_h_

#define sprintf rprintf

extern void rprintf ( char* target, char const *format, ... );

#endif
