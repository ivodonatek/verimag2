//******************************************************************************
//  Character set test.
//*****************************************************************************/

#ifndef TEST_CHARSET_H_INCLUDED
#define TEST_CHARSET_H_INCLUDED

#include "test.h"

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestCharset_Task(TestFuncParam param);

#endif // TEST_CHARSET_H_INCLUDED
