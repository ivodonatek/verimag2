#include "lpc23xx.h"
#include "spi.h"


void SPI_DisplayInit(void)
{
  IODIR1 |= 0x03700000;
  IOSET1 |= 0x03700000;
  PINSEL3 |= 0x0003CF00; // configure SPI0 pins
  SSP0CR0  = 0x00C7; // CPHA=1, CPOL=1, master mode, MSB first, interrupt enabled
  SSP0CPSR = 0x2;
  SSP0CR1 = 0x0002;
}

void SPI_DisplayTransmit(unsigned char cData)
{
  SSP0DR = 0x0000 + cData;
  while ((SSP0SR & 0x10)) ; // wait for end of transfer
}
