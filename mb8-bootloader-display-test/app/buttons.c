//******************************************************************************
//  Buttons driver implementation.
//*****************************************************************************/

#include "buttons.h"
#include "LPC23xx.h"
#include "delay.h"

#define BTN_DOWN  1
#define BTN_UP    2
#define BTN_LEFT  3
#define BTN_RIGHT 4
#define BTN_ESC   5
#define BTN_ENTER 6

volatile unsigned char pressed_key = 0xff; // kod stisknuteho tlacitka
volatile unsigned char flag_tlacitko = 0;

static unsigned long stisk(void)
{
  unsigned char tlacitko = 0;
  if(IOPIN1 & (1<<8))
    tlacitko |= (1<<0);
  if(IOPIN1 & (1<<9))
    tlacitko |= (1<<1);
  if(IOPIN1 & (1<<10))
    tlacitko |= (1<<2);
  return tlacitko;
}

/*static void Tlacitko(void)
{
  if (IO0_INT_STAT_R & (1<<5)) // rissing interrupt
  {
    flag_tlacitko = 1;
    pressed_key = stisk();
  }

  IO0_INT_CLR |= (1<<5);
  VICVectAddr=0;
}*/

static int adcRead(int button)
{
  AD0CR &= ~(1<<21);

  AD0CR &= ~(1<<0);
  AD0CR &= ~(1<<1);
  AD0CR &= ~(1<<2);
  AD0CR &= ~(1<<3);

  switch(button)
  {
    case BTN_DOWN:
    case BTN_UP:
      AD0CR |= (1<<0);
    break;
    case BTN_LEFT:
    case BTN_RIGHT:
      AD0CR |= (1<<3);
    break;
    case BTN_ESC:
    case BTN_ENTER:
      AD0CR |= (1<<2);
    break;
  }
  AD0CR |= (1<<21);
  // Manually start conversions
  AD0CR |=   (1<<24);
  switch(button)
  {
    case BTN_DOWN:
    case BTN_UP:
      while((AD0DR0 & (1 << 31)) == 0);
      return ((AD0DR0 >> 6) & 0xfff)+10;
    break;
    case BTN_LEFT:
    case BTN_RIGHT:
      // Wait for the conversion to complete
      while((AD0DR3 & (1 << 31)) == 0);
      return (AD0DR3 >> 6) & 0xfff;
    break;
    case BTN_ESC:
    case BTN_ENTER:
      // Wait for the conversion to complete
      while((AD0DR2 & (1 << 31)) == 0);
      return (AD0DR2 >> 6) & 0xfff;
    break;
  }

  return 1024;
}

static void readButtons(void)
{
  // precti stisknute tlacitko
    pressed_key = 0xff;
    int buttons [7] = {0};
    IOSET1 |= (1<<0);
    delay_10ms();

    // i == 1 -> BTN_DOWN, UP, RIGHT, LEFT, ESC, ENTER == 6

    // read DOWN, LEFT, ESC
    for(int i = 1; i < 6; i += 2)
    {
      buttons[i] = adcRead(i);
      buttons[i] = adcRead(i);
      buttons[i] = adcRead(i);
    }

    // switch tranzistors
    IOCLR1 |= (1<<0);
    IOSET1 |= (1<<1);
    delay_10ms();

    // read UP, RIGHT, ENTER
    for(int i = 2; i < 7; i += 2)
    {
      buttons[i] = adcRead(i);
      buttons[i] = adcRead(i);
      buttons[i] = adcRead(i);
    }
    IOCLR1 |= (1<<1);

    // vypocitej jestli nektere tlacitko je o 25% nizsi nez prumer ze vsech tlac
    int sum = 0;
    int min = 1023;
    int minIndex = 0;
    for (int i = 1; i < 7; i++)
    {
      sum += buttons[i];
      if (buttons[i] < min)
      {
        min = buttons[i];
        minIndex = i;
      }
    }
    // od sumy odectu nejnizssi hodnotu a udelam prumer
    double temp = (sum - min) / 5.0;
    // dynamicka zmena urovne pro stisk, dle prumerne hladiny
    if (temp > 700)
      temp = (temp / 100.0) * 65.0;
    else if (temp > 570)
      temp = (temp / 100.0) * 70.0;
    else if (temp > 450)
      temp = (temp / 100.0) * 75.0;
    else if (temp > 350)
      temp = (temp / 100.0) * 78.0;
    else if (temp > 250)
      temp = (temp / 100.0) * 83.0;
    else
    {
      // chyba tlacitek a nic nejde zmacknout
      return;
    }

    // kolik tlacitek ma mensi hodnotu nez 75% z prumeru?
    int count = 0;
    for( int i = 1; i < 7; i++)
    {
      if (buttons[i] < temp)
        count++;
    }

    // je zmacknuto jedno tlacitko
    if (count == 1)
    {
      switch(minIndex)
      {
        case BTN_DOWN:
          pressed_key = 2;
        break;
        case BTN_UP:
          pressed_key = 4;
        break;
        case BTN_LEFT:
          pressed_key = 3;
        break;
        case BTN_RIGHT:
          pressed_key = 1;
        break;
        case BTN_ESC:
          pressed_key = 5;
        break;
        case BTN_ENTER:
          pressed_key = 0;
        break;
      }
      flag_tlacitko = 1;
    }
}

static void pinConfig(void)
{
  IODIR1 |= (1<<1);
  IODIR1 |= (1<<0);

  IOCLR1 |= (1<<0);
  IOCLR1 |= (1<<1);

  // ADC power on
  PCONP |= (1<<12);
  AD0CR &= ~(1<<21);
  // select clock for ADC
  PCLKSEL0 |= (1 << 25);
  PCLKSEL0 |= (1 << 24);
  // AD0.0 AD0.2 and AD0.3
  PINSEL1 = (PINSEL1 & ~(0x3 << 14)) | (0x1 << 14);
  PINSEL1 = (PINSEL1 & ~(0x3 << 16)) | (0x1 << 16);
  PINSEL1 = (PINSEL1 & ~(0x3 << 18)) | (0x1 << 18);
  PINSEL1 = (PINSEL1 & ~(0x3 << 20)) | (0x1 << 20);

  //PINSEL1 |= 0x144000;
  AD0CR &= ~(1<<0);
  AD0CR &= ~(1<<1);
  AD0CR &= ~(1<<2);
  AD0CR &= ~(1<<3);
  AD0CR &= ~(1<<4);
  AD0CR &= ~(1<<5);
  AD0CR &= ~(1<<6);
  AD0CR &= ~(1<<7);

  AD0CR |= (1<<3);
  AD0CR |= (1<<21);
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Buttons_Init(void)
{
    ///tlacitka preruseni
    /*IO0_INT_EN_R |= (1<<5);
    IO0_INT_EN_F |= (1<<5);
    VICVectAddr17  = (unsigned long)Tlacitko;// Set Interrupt Vector
    VICIntEnable  = (1  << 17);*/
}

//------------------------------------------------------------------------------
//  Scan buttons state. Call periodically.
//------------------------------------------------------------------------------
ButtonCode Buttons_Scan(void)
{
    pressed_key = 0xff;
    if (IOPIN1 & (1<<4))
    {
      pressed_key = stisk();
    }

  return (ButtonCode)pressed_key;
}

//------------------------------------------------------------------------------
//  Get button code text representation.
//------------------------------------------------------------------------------
char* Buttons_GetCodeText(ButtonCode buttonCode)
{
    switch (buttonCode)
    {
    case BTC_NONE:
    default:
        return "";

    case BTC_DOWN:
        return "DOWN";

    case BTC_UP:
        return "UP";

    case BTC_LEFT:
        return "LEFT";

    case BTC_RIGHT:
        return "RIGHT";

    case BTC_ESC:
        return "ESC";

    case BTC_ENTER:
        return "ENTER";
    }
}


