#ifndef _INTEGER
#define _INTEGER

#include <stdint.h>
/* modified by Martin Thomas to avoid redefiniton
   for BYTE, WORD and DWORD */

/* These types are assumed as 16-bit or larger integer */
typedef int32_t		INT;
typedef uint32_t	UINT;

/* These types are assumed as 8-bit integer */
typedef int8_t		CHAR;
typedef uint8_t	UCHAR;
#ifndef BYTE_TYPEDEF
#define BYTE_TYPEDEFED
typedef uint8_t	BYTE;
#endif

/* These types are assumed as 16-bit integer */
typedef int16_t	SHORT;
typedef uint16_t	USHORT;
#ifndef WORD_TYPEDEF
#define WORD_TYPEDEF
typedef uint16_t	WORD;
#endif

/* These types are assumed as 32-bit integer */
typedef int32_t		LONG;
typedef uint32_t	ULONG;
#ifndef DWORD_TYPEDEF
#define DWORD_TYPEDEF
typedef uint32_t	DWORD;
#endif

#if 1
// mthomas
#ifndef FALSE
typedef enum { FALSE = 0, TRUE } BOOL;
#endif
#else
/* Boolean type */
typedef enum { FALSE = 0, TRUE } BOOL;
#endif

#endif
