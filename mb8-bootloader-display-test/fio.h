#ifndef FIO_H_INCLUDED
#define FIO_H_INCLUDED

#ifndef EOF
#define EOF -1
#endif

#define SEEK_SET	0
#define SEEK_CUR	stream->fptr
#define SEEK_END	stream->fsize

extern FIL* fopen ( const char * filename, const char * mode );
extern int fclose( FIL *stream );
extern int fputc (char, FIL*);								/* Put a character to the file */
extern int fputs (const char*, FIL*);						/* Put a string to the file */
extern int fprintf (FIL*, const char*, ...);				/* Put a formatted string to the file */
extern char* fgets (char*, int, FIL*);						/* Get a string from the file */
extern int feof ( FIL *stream);
extern void rewind( FIL *stream );
extern int fseek ( FIL * stream, long int offset, int origin );

#endif // FIO_H_INCLUDED
