/*---------------------------------------------------------------*/
/* FAT file system module test program R0.05   (C)ChaN, 2007     */
/* Extended for LPC23xx/24xx MCI Test          (C)Martin Thomas  */
/*---------------------------------------------------------------*/

#define VERSIONSTRING "ChaN 0.06 / NXP 1.4 / mt 0.5"

/* Target specific:                    */
/* Tested with Keil MCB2300 / NXP2368  */
/* Martin Thomas: for LPC23xx MCI demo */
#include "LPC23xx.h"
#include "type.h"
#include "target.h"
#include "rtc.h"
#include "timer.h"
/* end target-spefice includes */

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "rtc.h"
#include "comm.h"
#include "monitor.h"
#include "diskio.h"
#include "tff.h"
#include "fio.h"
#include "sbl_iap.h"
#include "sbl_config.h"
#include "app/maine.h"
#include "app/rtc.h"

#define DBG(...)

static FATFS fatfs;		/* File system object for each logical drive */
static TFwUpdateResult fwUpdateResult = FWUR_NONE;

/*-----------------------------------------------------------------------*/
/* Main                                                                  */
int main (void)
{
    IntEnable();

    RTC_init();         //inicializace RTC
    RTC_set_interrupt();//nastaveni preruseni od RTC

//	uart_init();		/* Initialize UART driver */
	DBG("\nBootloader test for LPC2368 "__TIME__"\r\n");
/*
	FIL *file = fopen ("/Arkon2/read.txt","r");
	fputs( "pokus123456 789456", file );
	fclose( file );

	file = fopen ("/Arkon2/write.txt","w");
	fputs( "pokus123456 789456", file );
	fclose( file );

	file = fopen ("/Arkon2/append2.txt","a");
	fputs( "pokus123456 789456\r\n", file );
	fclose( file );

	file = fopen ("//A/r/k////o/n//append2.txt","a");
	fputs( "pokus123456 789456\r\n", file );
	fclose( file );
*/
    unsigned char buff[256];
    do
    {
        if( disk_initialize( 0 ) != RES_OK )
        {
            DBG("Unknown SD card\r\n");
        }
        else if( f_mount( 0, &fatfs) != FR_OK )
        {
            DBG("Unknown FAT\r\n");
        }
        else
        {
            FIL *file = fopen ("/Update/app.bin","rb");
            if( file )
            {
                rewind(file);

                DBG( "!New firmware on SD card detected\r\n" );
                DBG( "Erase user flash:" );
                unsigned long address = USER_FLASH_START;

                if(erase_user_flash())
                {
                    DBG( "OK\r\n" );
                    DBG( "Flashing firmware" );
                    while( !feof(file))
                    {
                        unsigned int rc = 0;
                        unsigned int err;
                        if (f_read(file, buff, sizeof(buff), &rc) != FR_OK)
                        {
                            fwUpdateResult = FWUR_READ_FILE_ERROR;
                            break;
                        }
                        if ((err = write_flash( address, buff, rc)) != 0)
                        {
                            fwUpdateResult = FWUR_WRITE_PROGRAM_ERROR;
                            DBG("error %d\r\n", err);
                            break;
                        }
                        address+=sizeof(buff);
                        DBG(".");
                    }

                    fwUpdateResult = FWUR_DONE;
                    DBG("done\r\n");
                }
                else
                {
                    fwUpdateResult = FWUR_ERASE_FAILED;
                    DBG( "failed\r\n" );
                }
            }
            else
            {
                DBG( "No new firmware\r\n" );
            }
            fclose( file );
        }
    }
    while( 0 );
    //while( !user_code_present());

/*	xputs( "Main Memory:\n");
	for( unsigned int j=0; j<10; j++)
	{
        for( unsigned int i=0; i<16; i++)
        {
            xprintf("%02X ", *((char*)(i+j*16)));
        }
        xputs( "\n" );
	}

	xputs( "\nUser Memory:\n");
	for( unsigned int j=0; j<10; j++)
	{
        for( unsigned int i=0; i<16; i++)
        {
            xprintf("%02X ", *((char*)(USER_FLASH_START+i+j*16)));
        }
        xputs( "\n" );
	}
*/

    if (fwUpdateResult != FWUR_NONE)
        app_fw_update_info(fwUpdateResult);

    if (user_code_present())
    {
        DBG( "Execute_user_code...\r\n" );
        execute_user_code();
    }
    else
    {
        app_product_test();
    }

	while(1);
}

DWORD get_fattime (void)
{
	RTCTime t;
	DWORD res;

//	RTCGetTime( &t );
	/*res = (
		(( t.RTC_Sec/2 )       << 0UL  ) |
		(( t.RTC_Min )         << 5UL  ) |
		(( t.RTC_Hour )        << 11UL ) |
		(( t.RTC_Mday )        << 16UL ) |
		(( t.RTC_Mon )         << 21UL ) |
		(( t.RTC_Year-1980 )   << 25UL )
	);*/

	res = (
		(( 0/*t.RTC_Sec/2*/ )       << 0UL  ) |
		(( 33/*t.RTC_Min*/ )         << 5UL  ) |
		(( 16/*t.RTC_Hour*/ )        << 11UL ) |
		(( 11/*t.RTC_Mday*/ )        << 16UL ) |
		(( 4/*t.RTC_Mon*/ )         << 21UL ) |
		(( 2010/*t.RTC_Year*/-1980 )   << 25UL )
	);

	return res;
}
