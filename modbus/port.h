/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: port.h,v 1.1 2007/04/24 23:15:18 wolti Exp $
 */

#ifndef _PORT_H
#define _PORT_H

#include <assert.h>
#include <inttypes.h>
#include "type.h"

//#define	INLINE
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#define ENTER_CRITICAL_SECTION( )		EnterCriticalSection( )
#define EXIT_CRITICAL_SECTION( )    ExitCriticalSection( )

#define CCLK	57594579L
#define PCLK	CCLK/2

void EnterCriticalSection( void );
void ExitCriticalSection( void );

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

typedef unsigned char UCHAR;
//typedef char    CHAR;

#ifndef USHORT_TYPEDEF
#define USHORT_TYPEDEF
typedef uint16_t USHORT;
#endif

#ifndef SHORT_TYPEDEF
#define SHORT_TYPEDEF
typedef int16_t SHORT;
#endif

#ifndef ULONG_TYPEDEF
#define ULONG_TYPEDEF
typedef uint32_t	ULONG;
#endif

#ifndef ULONG_TYPEDEF
#define ULONG_TYPEDEF
typedef uint32_t	ULONG;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif


/* ----------------------- RS485 specifics ----------------------------------*/

//#define RTS_ENABLE 1

#ifdef  RTS_ENABLE

#define RTS_IODIR IODIR0
#define RTS_IOSET IOSET0
#define RTS_IOCLR IOCLR0
#define RTS_IOPIN (1<<1)

#define RTS_INIT  {RTS_IODIR |= RTS_IOPIN;}
#define RTS_OUT   {RTS_IOSET |= RTS_IOPIN;}
#define RTS_IN    {RTS_IOCLR |= RTS_IOPIN;}
#endif

#endif
