/*
  * FreeModbus Libary: LPC214X Port
  * Copyright (C) 2007 Tiago Prado Lone <tiago@maxwellbohr.com.br>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2007/04/24 23:15:18 wolti Exp $
 */

#include <LPC23xx.h>
#include <string.h>
#include "port.h"
#include "irq.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "VCOM.h"
#include "menu_tables.h"
#include "maine.h"
#include "rprintf.h"
#include "display.h"

// ----------------------- static functions ---------------------------------/
static void sio_irq( void ) __irq;
static void uart1_irq( void ) __irq;

static void     prvvUARTTxReadyISR( void );
static void     prvvUARTRxISR( void );

// ----------------------- Start implementation -----------------------------/
void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    if (xTxEnable)
    {
        prvvUARTTxReadyISR();
    }
}


void vMBPortClose( void )
{
};

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
    BOOL            bInitialized = TRUE;
    return bInitialized;

    USHORT          cfg = 0;
    ULONG           reload = ( ( PCLK / ulBaudRate ) / 16UL );
    volatile char   dummy;

#ifdef RTS_ENABLE
    RTS_INIT;
    RTS_IN
#endif

    ( void )ucPORT;


	//PINSEL0 = 0x0000000A;       /* TxD3 RXD3 P0.0 P0.1*/

            //USB,RS232/485
            //if(!USBconnected())      //pokud je pripojene USB
            {
                PINSEL0 |= (1<<20) | (1<<22);       /* TxD2 RxD2 P0.10 P0.11*/
                PCONP |= (1<<24);      //set power on UART2
            }
            //TCP/IP
            PINSEL4 |= (1<<1) | (1<<3);       //TxD1 RxD1 P2.0 P2.1
            PCONP |= (1<<4);     //set power on UART1


    switch ( ucDataBits )
    {
    case 5:
        break;

    case 6:
        cfg |= 0x00000001;
        break;

    case 7:
        cfg |= 0x00000002;
        break;

    case 8:
        cfg |= 0x00000003;
        break;

    default:
        bInitialized = FALSE;
    }

    switch ( eParity )
    {
    case MB_PAR_NONE:
        break;

    case MB_PAR_ODD:
        cfg |= 0x00000008;
        break;

    case MB_PAR_EVEN:
        cfg |= 0x00000018;
        break;
    case MB_PAR_NONE_TWO_STOPBITS:
        cfg |= 0x00000004;
        break;
    }

    //FIO2DIR |= (1<<3);   //vystupni pin  pro reset TCP/IP modulu
    //FIO2SET |= (1<<3);      //reset - TCP/IP pin P2.3 do "1"
    if( bInitialized )
    {
      // RS232/485
      {
        U2LCR = cfg;            /* Configure Data Bits and Parity */
        U2IER = 0;              /* Disable UART1 Interrupts */

        U2LCR |= 0x80;          /* Set DLAB */
        U2DLL = reload;         /* Set Baud     */
        U2DLM = reload >> 8;    /* Set Baud */
        U2LCR &= ~0x80;         /* Clear DLAB */

        // Configure UART2 Interrupt
        VICVectAddr28 = ( unsigned long )sio_irq;
        VICVectCntl28 = (0x20 | 28);
        VICIntEnable |= (1 << 28);  // Enable UART2 Interrupt
      }

      // TCP/IP, Bluetooth, GPRS, WiFi
      U1LCR = 0;            // Configure Data Bits and Parity
      U1LCR |= 0x00000003;    //none parity, 8 bits
      U1IER = 0;              // Disable UART1 Interrupts
      U1LCR |= 0x80;          // Set DLAB
      ULONG rychlost = ( ( PCLK / 19200 ) / 16UL );
      U1DLL = rychlost;         // Set Baud
      U1DLM = rychlost >> 8;    // Set Baud
      U1LCR &= ~0x80;         // Clear DLAB

      // Configure UART1 Interrupt
      VICVectAddr7 = ( unsigned long )uart1_irq;
      VICVectCntl7 = (0x20 | 7);
      VICIntEnable |= (1 << 7);  // Enable UART1 Interrupt

      dummy = U1IIR;          // Required to Get Interrupts Started
    }
    return bInitialized;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    if(USBconnected())
    {
        VCOM_putchar(ucByte);
    }
    else
    {
    }

    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
  if(USBconnected())
  {
    *pucByte = VCOM_getchar();
  }
  else
  {
  }
  return TRUE;
}


void
sio_irq( void )
    __irq
{
    volatile char   dummy;
    volatile char   IIR;

    ///tady asi staci jen nastavit flag komunikacni modul, zbytecne se nemusi kontrolovat

    while( ( ( IIR = U2IIR ) & 0x01 ) == 0 )
    {
        switch ( IIR & 0x0E )
        {
        case 0x06:             /* Receive Line Status */
            dummy = U2LSR;      /* Just clear the interrupt source */
            break;

        case 0x04:             /* Receive Data Available */
        case 0x0C:             /* Character Time-Out */
            prvvUARTRxISR(  );
            break;

        case 0x02:             /* THRE Interrupt */
            prvvUARTTxReadyISR(  );
            break;

        case 0x00:             /* Modem Interrupt */
            //dummy = U1MSR;      /* Just clear the interrupt source */
            break;

        default:
            break;
        }
    }

    VICVectAddr = 0xFF;         /* Acknowledge Interrupt */
}

void uart1_irq( void ) __irq
{
      volatile char   dummy;
      volatile char   IIR;

       while( ( ( IIR = U1IIR ) & 0x01 ) == 0 )
       {
          switch ( IIR & 0x0E )
          {
          case 0x06:             /* Receive Line Status */
              dummy = U1LSR;      /* Just clear the interrupt source */
              break;

          case 0x04:             /* Receive Data Available */
          case 0x0C:             /* Character Time-Out */
              prvvUARTRxISR(  );
              //unsigned char hnup=U1RBR;
              break;

          case 0x02:             /* THRE Interrupt */
              prvvUARTTxReadyISR(  );
              break;

          case 0x00:             /* Modem Interrupt */
              //dummy = U1MSR;      /* Just clear the interrupt source */
              break;

          default:
              break;
          }
      }

  VICVectAddr = 0xFF;         /* Acknowledge Interrupt */
}

/*
 * Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call
 * xMBPortSerialPutByte( ) to send the character.
 */
static void
prvvUARTTxReadyISR( void )
{
    pxMBFrameCBTransmitterEmpty(  );
}

/*
 * Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
static void
prvvUARTRxISR( void )
{
    pxMBFrameCBByteReceived(  );
}
