#include "LPC23xx.h"
#include "maine.h"
#include "target.h"
#include "display.h"
#include "spi.h"
#include "images.h"
#include "rprintf.h"
#include "menu_tables.h"
#include "string.h"
#include "font.h"
#include "rtc.h"
#include "VCOM.h"
#include <stdlib.h>
#include "menu.h"
#include "mb.h"
#include "type.h"
#include "swi.h"
#include "target.h"
#include "uart.h"
#include "iout.h"
#include "delay.h"
#include "sensor.h"
#include "timer.h"
#include "calibration.h"
#include "modbus_bin.h"
#include "modbus.h"
#include "sbl_iap.h"
#include "sbl_config.h"
#include "conversion.h"
#include "fw_check.h"
#include "extmeas/temperature/exttemp.h"
#include "extmeas/pressure/extpress.h"
#include "extmeas/inductance/extind.h"
#include "extmeas/iout/extiout.h"
#include "screen.h"
#include "buttons.h"
#include "ui.h"
#include "switch.h"
#include "ad.h"
#include "task_man.h"
#include "test.h"
#include "step_up.h"
#include "sensor_master.h"
#include "sensor_slave.h"
#include "magx2_comm.h"
#include "store_man.h"
#include "power_man.h"

///SD KARTA
#include <string.h>
#include <stdlib.h>
#include "comm.h"
#include "monitor.h"
#include "diskio.h"
#include "tff.h"
#include "fio.h"

FATFS fatfs;		// File system object for each logical drive

const unsigned char ucSlaveID[] =
{
    0x01, 'A','r','k','o','n',' ',' ',' ',' ',' '
    ,'V','e','r','i','M','A','G',0,0,0,0,0,0,0,1,10
};

unsigned long RAM_Read_Data_Long(unsigned long Address)
{
	return(*(unsigned long *)Address);
}

void RAM_Write_Data_Long(unsigned long data, unsigned long Address)
{
  *(unsigned long *)Address = data;
}

float RAM_Read_Data_Float(unsigned long Address)
{
	return(*(float *)Address);
}

void RAM_Write_Data_Float(float data, unsigned long Address)
{
  *(float *)Address = data;
}

void RAM_Write_Data(unsigned long data, unsigned long Address)
{
  *(unsigned long *)Address &= data;
  *(unsigned long *)Address |= data;
}

unsigned char key_lock = 0; // promenna pro zamykani klaves
unsigned char last_key = 0; // posledni stistena klavesa
unsigned char key_dummy = 0; // pro vynechani klavesy enter pri odemceni klavesnice
unsigned char key_ESC_time_sec = 0; // sekund kdy se stiskla klavesa ESC
volatile unsigned char pressed_key = 0xff; // kod stisknuteho tlacitka
volatile static unsigned long empty_pipe = 0; // namerena hodnota z empty pipe
volatile static long flow_avg[SAMPLES_MAX + 1] = {};   //flow_avg[0] vysledny prumer

struct TSensor
{
  long mp1;
  long mp2;
  long mp3;
  long cp1;
  long cp2; // kalibracni konstanty a pointy
  long cp3; // kalibracni konstanty a pointy
}sensor;

/**
    Záznam data loggeru
*/
typedef struct DataloggerRecord
{
    // datum a čas
    unsigned char dt_minute;
    unsigned char dt_hour;
    unsigned char dt_day;
    unsigned char dt_month;
    unsigned int  dt_year;

    // měřené veličiny
    long long meas_total;
    long long meas_flow_pos;
    long long meas_flow_neg;
    long long meas_aux;
    float     meas_temp;
    float     meas_ext_temp;    // if present
    float     meas_ext_press;   // if present

    // stavy
    unsigned int firmware;      // FW_VERSION
    unsigned char measurement;  // Run=1/Stop=0

    // chybové informace
    unsigned long error_value;
    unsigned long error_minute;
    unsigned long ok_minute;

} TDataloggerRecord;

// max. počet záznamů v cache data loggeru
#define MAX_DATALOGGER_CACHE_RECORDS    (10)

void co_sekunda(void);
float Convert_Flow(float in, unsigned char typ);
long long Convert_Volume(long long in, unsigned char typ);
unsigned long long Unit_Volume_Conversion(unsigned long in_hi, unsigned long in_lo);
unsigned long Volume_Conversion(unsigned long in_hi, unsigned long in_lo, unsigned char typ); // prevod spodniho pocitadla do spravne jednotky

///debugovani
#define ERR_LINE    1
#define SHOW_ANS    2
#define ACT_ERROR   5
#define INFO        7

//#define SERVICE
#define TOTALISER

///flagy
unsigned int flag_cisteni_cas = 0;
unsigned int flag_zapsat_flash = SEC_TO_WRITE_FLASH;
unsigned char flag_zapis_prutok = 0;      // flag pro vyvolani fce zapisu prutoku do pole
unsigned char flag_kalibrace_send = 0;
unsigned char flag_kalibrace_recv = 0;    // jednotlive biti informuji o stavu dane komunikace
unsigned char flag_cekam = 1;             // neco jako timeoutu v zadosti o data
unsigned char flag_start = 10;
unsigned char flag_outputs_modules = 0;
unsigned char flag_datalogger = 0;
unsigned char flag_cisteni = 0;
unsigned char flag_sensor_zadosti = 0;
unsigned char flag_sensor7 = 0;
unsigned char flag_sensor8 = 0;
unsigned char flag_key_lock_time = 0;
unsigned char flag_totaliser_cycling = 0;
unsigned char flag_reset_after_load_default = 0;
unsigned char flag_second_graph = 0;
unsigned char flag_timer1;
unsigned char flag_wifi_test;
volatile unsigned char flag_RTC = 0;
volatile unsigned char flag_receive = 0;  // dosla a zpracovala se data
volatile unsigned char flag_aktivni_zadost = 0;    //pokud se nevraci spravna odpoved, pomoci tohoto flagu se odstavi zadost na data
volatile unsigned char flag_tlacitko = 0;
volatile unsigned char flag_sms_zastaveny = 0;
volatile unsigned char flag_gsm_dokoncil_prijem = 0;
volatile unsigned char flag_timeout_timer2 = 0;
volatile unsigned char flag_timeout_timer3 = 0;
volatile unsigned char flag_usb_modul_insert = 0;

volatile unsigned int zaloha_sum_pozice_pole = 0;
volatile unsigned char zaloha_pozice_pole = 0;
volatile unsigned char flag_data_correct = 0;
volatile unsigned char flag_posli_datalogger_do_gsm = 0;
volatile unsigned char flag_gsm_datalogger_first_send = 0;
volatile unsigned char flag_gsm_potvrdit_nastaveni = 0;
volatile char odesilaci_buffer[160];
volatile long act_adc[SAMPLES_MAX];

int down, left, esc, up, right, enter;
int timer1_running = 0;
unsigned int zaloha_pocet_odeslani = 0;
unsigned int zaloha_datalogger_counter = 1;
unsigned int second_to_datalogger = 0;
unsigned int frekvence_site = 0;
unsigned char pocet_znaku_gsm = 149;
unsigned char aktualni_cislo_pro_odeslani = 1;
unsigned char aktualni_cislo_interval = 1;
unsigned char flag_chci_odeslat_event = 0;
unsigned char flag_chci_odeslat_interval = 0;
unsigned char flag_probiha_prijem_od_gsm = 0;
unsigned char backlight_second;
unsigned char error_sec = 0;
unsigned char ok_sec = 0;
unsigned char pocet_spatnych_prijmu = 0;
unsigned char pocet_pokusu_send = 0;
unsigned char pocet_pokusu_recv = 0;
unsigned char pocet_pokusu_zadost_sensor = 0;
unsigned char aktivni_cisteni = 0;
unsigned char SDread = 0;
unsigned long flag_aktualni_event = 0;
//unsigned long debug_code = 0;
unsigned long actual_error = 0;
unsigned long actual_error_datalogger = 0;
unsigned long ep_avg;
unsigned long error_min = 0;
unsigned long ok_min = 0;
unsigned long sensor_fw_version = 0; // fw verze senzoru
unsigned long last_sending_error_gsm = 0;
long adc_average;
long teplota_avg;

volatile long pole_teplota[VELIKOST_POLE_TEPLOTA_EP]=
{
  8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,
};
volatile long pole_ep[VELIKOST_POLE_TEPLOTA_EP];
volatile TDateTime DateTime;

// aktualni stranka servisnich informaci
unsigned char serviceInfoPage = SERVICE_MAIN_PAGE;

unsigned char inicializace_senzoru(void);   //pro nacteni calibracnich dat atd
void odesli_buffer(unsigned char velikost);
unsigned char check_gsm_modul(void);

void InitBoard(BOOL start)
{
  //IntDisable();

  PowerMan_Init();

  StepUp_Init();
  StepUp_Set(STPUP_ON);

  TaskMan_Init();

  SPI_DisplayInit();
  Init_Display();
  DisplaySetContrast(60);
  if (RAM_Read_Data_Long(BACKLIGHT))
    DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
  else
    DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;

  AD_Init();
  RTC_init();
  Screen_Init();
  Buttons_Init();
  UI_Init();
  Switch_Init();
  ExtInd_Init();
  ExtIout_Init();
  SensorMaster_Init();
  SensorSlave_Init();
  MagX2Comm_Init();
  Test_Init();

  // modbus
  InitializationVCOM();
  IntEnable();
  Modbus_init();      //inicializace modusu
  eMBSetSlaveID( 27, TRUE, ucSlaveID, 27 );		// ulozeni slave id do modbusu
  // modbus end

  if (start)
  {
    Screen_ShowIntroduction();
    delay_seconds(3);

    StoreMan_Init();
  }

  UI_SetMenu(MENUID_Main, 0);
}

///**************************************************************************//
///                                 MAIN                                     //
///**************************************************************************//
int main(void)
{
  InitBoard(TRUE);

  while (1)
  {
      ( void )eMBPoll(); // modbus pool
      PowerMan_Task();
      RTC_Task(TRUE);
      Buttons_Task();
      SensorMaster_Task();
      SensorSlave_Task();
      MagX2Comm_Task();
      TaskMan_Task();
      StoreMan_Task();
  }

  return 0;
}
