#include "timer.h"
#include "LPC23xx.h"
#include "target.h"
#include "maine.h"
#include "menu.h"
#include "display.h"

extern unsigned char flag_timer1;
extern volatile unsigned char pressed_key;

void init_timer1()      /// casovac pro rolovani tlacitek
{
  flag_timer1 = 0;
  T1MR0         = Fcclk/4;                   // 1s pri 57.6MHz
  T1MCR         = 3;                         // Interrupt and Reset on MR0
  VICVectAddr5  = (unsigned long)timer1_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 5);                 // Enable Timer0 Interrupt
}

void disable_timer1()
{
  T1TCR          = 0;
  T1MR0          = Fcclk/4; // 0.5 pri 57.6MHz
  T1TC           = 0;       //reset timer
}

void enable_timer1()
{
  T1TCR |= (1<<0);    //counter enable
}

void timer1_overflow()
{
  T1IR = 1;			/* clear interrupt flag */
  T1MR0 = Fcclk/8;                   // 0.25 pri 57.6MHz

  pressed_key = 0xff;
  if (IOPIN0 & (1<<26))
  {
    flag_timer1=1;
    pressed_key = stisk();
  }
  else
    disable_timer1();

  VICVectAddr = 0;		/* Acknowledge Interrupt */
}

void timer2_overflow()
{
  disable_timer2();  //vypnuti temru
  flag_timeout_timer2=1;
  flag_aktivni_zadost=0;
  T2IR |= (1<<2);    //clear interrupt flag on MC2
  VICVectAddr = 0;   //vynulovani registru preruseni
}

void init_timer2()      ///casovac pro timeout pri komunikaci se sens8
{
  PCONP |= (1<<22);        //power on
  T2MR2          = Fcclk/8;                   // 140ms
  T2MCR         |= (1<<6) | (1<<7);            // Interrupt and Reset on MR0
  VICVectAddr26 = (unsigned long)timer2_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 26);                 // Enable Timer0 Interrupt
}

void disable_timer2()
{
  T2TCR &= ~(1<<0);
  T2TC = 0;    //reset timer
}

void enable_timer2()
{
  T2TC = 0;    //reset timer
  T2TCR |= (1<<0);    //counter enable
}

void init_timer3()  /// cawsovac co cca 300ms pro zadost na sens8
{
  PCONP |= (1<<23);        //power on
  T3MR2          = Fcclk/6;                   // cca 333,333333ms
  T3MCR         |= (1<<6) | (1<<7);            // Interrupt and Reset on MR0
  VICVectAddr27 = (unsigned long)timer3_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 27);                 // Enable Timer0 Interrupt
}

void disable_timer3()
{
  T3TCR = 0;
  T3TC = 0;
}

void enable_timer3()
{
  T3TCR |= (1<<0);
}

void timer3_overflow()
{
  flag_timeout_timer3=1;
  T3IR |= (1<<2);    //clear interrupt flag on MC2
  VICVectAddr = 0;   //vynulovani registru preruseni
}
