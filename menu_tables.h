#ifndef menu_tables
#define menu_tables

#define OFFSET = ;

extern const unsigned int P_edit[];
extern const unsigned int P_menu[];  // count back n*[forward text edit]
extern const unsigned char P_texty[];
extern const unsigned long P_values[];
extern unsigned int sizeof_P_values;

/* Tabulka jednotek */
#define UNIT_SIZE 8            // velikost jednotek
extern const unsigned char P_unit[];      // Jednotky. Prvni cislo pocet radku, druhe cislo pocet sloupcu

/* Jazykove konstanty */
extern  const unsigned int LANGUAGE_PAGE_SIZE;
extern  const unsigned int LANGUAGES_COUNT;


/* Konstanty textovych hlasek + indexy do textu */
extern  const unsigned int mess_EraseMemoryModul;
extern  const unsigned int mess_EraseAux;
extern  const unsigned int mess_RestoreDefault;
extern  const unsigned int mess_EraseErro;
extern  const unsigned int mess_EraseOk;
extern  const unsigned int mess_SavingSetup;
extern  const unsigned int mess_DeletingNegativeVolume;
extern  const unsigned int mess_DeletingPositiveVolume;
extern  const unsigned int mess_DeletingTotalVolume;
extern  const unsigned int mess_MinAllowedValueIs;
extern  const unsigned int mess_MaxAllowedValueIs;
extern  const unsigned int mess_MinAllowedValueIsFwin;
extern  const unsigned int mess_MaxAllowedValueIsFwin;
extern  const unsigned int mess_WrongPasword;
extern  const unsigned int mess_LoadingDefaultSettings;
extern  const unsigned int mess_PaswordChanged;
extern  const unsigned int mess_SavingFactorySettings;
extern  const unsigned int mess_AirDetectorNotResponding;
extern  const unsigned int mess_MemoryModuleNotRes;
extern  const unsigned int mess_RTCmoduleNotResponding;
extern  const unsigned int mess_SensorNotResponding;
extern  const unsigned int mess_FBmoduleNotResponding;
extern  const unsigned int mess_RS232notResponding;
extern  const unsigned int mess_RS485notResponding;
extern  const unsigned int mess_USBnotResponding;
extern  const unsigned int mess_TCPnotResponding;
extern  const unsigned int mess_WiFinotResponding;
extern  const unsigned int mess_BadDate;
extern  const unsigned int mess_BadTime;
extern  const unsigned int mess_UnknownOperation;
extern  const unsigned int mess_RamDataLost;


/* Nazvy konstant + adresy */

// Tato hodnota bude ulozena na adrese CONTROL_POINTER pokud jiz byl procesor s timto software nekdy pusteny
#define IS_NOT_FIRST       (0x28AF603B)
//#define VALUES_OFFSET      (0x428)      // Na teto adrese budou ve flash ulozene values;
#define VALUES_OFFSET      (0xE0084000)      // Na teto adrese budou v zalohovane ram ulozene values;

// Na teto adrese je longint hodnota, urcujici, jestli se jedna o prvni zpusteni procesoru
#define CONTROL_POINTER       (VALUES_OFFSET)

#define VALUES_SIZE           (VALUES_OFFSET + 0x0000)
#define UNIT_NO               (VALUES_OFFSET + 0x0004)
#define FIRMWARE              (VALUES_OFFSET + 0x0008)
#define CONTRAST              (VALUES_OFFSET + 0x000C)
#define BACKLIGHT             (VALUES_OFFSET + 0x0010)
#define DATE                  (VALUES_OFFSET + 0x0014)
#define TIME                  (VALUES_OFFSET + 0x0018)
#define SERVICE_MODE          (VALUES_OFFSET + 0x001C)
#define CALIB_DATE            (VALUES_OFFSET + 0x0020)
#define TEST_COUNTER          (VALUES_OFFSET + 0x0024)
#define TEST_SIZE             (VALUES_OFFSET + 0x0028)
#define TEST_ID               (VALUES_OFFSET + 0x002C)
#define TEST_ERASE            (VALUES_OFFSET + 0x0030)

#define ACTUAL_ERROR          (VALUES_OFFSET + 0x0034)
#define POWER_FREKVENCY       (VALUES_OFFSET + 0x0038)
#define TEMPERATURE           (VALUES_OFFSET + 0x003C)
#define POINT_UP              (VALUES_OFFSET + 0x0040)
#define LOAD_DEFAULT_SETTING  (VALUES_OFFSET + 0x0044)
#define DATE_SET              (VALUES_OFFSET + 0x0048)
#define TIME_SETTING          (VALUES_OFFSET + 0x004C)
#define MODBUS_SLAVE_ADDRESS  (VALUES_OFFSET + 0x0050)
#define MODBUS_BAUDRATE       (VALUES_OFFSET + 0x0054)
#define MODBUS_PARITY         (VALUES_OFFSET + 0x0058)
#define TOTALIZER_CYCLING     (VALUES_OFFSET + 0x005C)

#define PASSWORD_FACTORY      (VALUES_OFFSET + 0x0060)
#define SAVE_SETTINGS         (VALUES_OFFSET + 0x0064)
#define PASSWORD_AUTHORIZE    (VALUES_OFFSET + 0x0068)
#define SLAVE_UNIT_NO         (VALUES_OFFSET + 0x006C)

#define F_UNIT_NO         (FLASH_FACTORY_START)
#define F_FIRMWARE        (FLASH_FACTORY_START + 0x04)
#define F_CONTRAST        (FLASH_FACTORY_START + 0x08)
#define F_BACKLIGHT       (FLASH_FACTORY_START + 0x0C)
#define F_DATE            (FLASH_FACTORY_START + 0x10)
#define F_TIME            (FLASH_FACTORY_START + 0x14)
#define F_SERVICE_MODE    (FLASH_FACTORY_START + 0x18)
#define F_CALIB_DATE      (FLASH_FACTORY_START + 0x1C)
#define F_TEST_COUNTER    (FLASH_FACTORY_START + 0x20) // pocet testu v pameti, pri vycteni se nuluje
#define F_TEST_SIZE       (FLASH_FACTORY_START + 0x24)
#define F_TEST_ID         (FLASH_FACTORY_START + 0x28) // cislo testu porad se zvysuje
#define F_TEST_ERASE      (FLASH_FACTORY_START + 0x2C) // priznak podle ktereho se mazou testy z Flash




#define F_END1             0x7FFFB
#define F_END2             0x7F7FF




#endif //menu_tables
