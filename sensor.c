/**********************************************************************************************************
** FW pro komunikaci mezi uPC a senzorem pres UART3, zaroven RS485, rizeni toku, celou dobu je nastaveno **
** na prijemm jen pri odesilani je smer toku prepnut, po odeslani se                                     **
** zase prepne na prijem (mozny error s prerusenim, kdyz neco prijde a nevyprazni se se buffer).         **
**                                                                                                       **
**                                                                                                       **
** VYTVORENO 11.5.2010  POSLEDNI UPRAVY 11.5.2010                                                        **
**********************************************************************************************************/

#include "LPC23xx.h"
#include "type.h"
#include "display.h"
#include "rprintf.h"
#include "sensor.h"
#include "menu_tables.h"
#include "maine.h"
#include "mbcrc.h"
#include "uart.h"

unsigned char recv_buffer[80];
unsigned char pozice_bufferu=0;
unsigned char size_recv=0;
unsigned int yes=0;

void send_calibration(unsigned char pos)
{
  if( flag_sensor7 )
    send_calibration_sensor7(pos);
  else if (flag_sensor8)
    send_calibration_sensor8();
  else
    ;
}
void sensor_cisteni(unsigned char clean)
{
  if( flag_sensor7 )
    sensor_cisteni_sensor7(clean);
  else if ( flag_sensor8)
    send_cisteni_sensor8(clean);
  else
    ;
}

void sensor_buzeni(unsigned char power)
{
  if( flag_sensor7 )
    sensor_buzeni_sensor7(power);
  else if ( flag_sensor8)
    nastav_buzeni_sensor8();    //zapne jak frekvenci tak power
  else
    ;
}
void sensor_frekvence(unsigned char frequenc)
{
  if( flag_sensor7 )
    sensor_frekvence_sensor7(frequenc);
  else if ( flag_sensor8)
    nastav_buzeni_sensor8();    //zapne jak frekvenci tak power
  else
    ;
}

void sensor_stav_frekvence()
{
  if( flag_sensor7 )
    sensor_stav_frekvence_sensor7();
  else if ( flag_sensor8)
    ;
  else
    ;
}

void sensor_stav_buzeni()
{
  if( flag_sensor7 )
    sensor_stav_buzeni_sensor7();
  else if ( flag_sensor8)
    sensor_stav_buzeni_sensor8();
  else
    ;
}

void request_data()
{
  if( flag_sensor7 )
    request_data_sensor7();
  else if ( flag_sensor8)
    request_data_sensor8();
  else
    ;
}


void recv_calibration(unsigned char id)
{
  if( flag_sensor7 )
    recv_calibration_sensor7(id);
  else if ( flag_sensor8)
    sensor_stav_buzeni_sensor8();
  else
    ;
}

unsigned char zpracovat_data()
{
  if( flag_sensor7 )
    zpracovat_data_sensor7();
  else if ( flag_sensor8)
    zpracovat_data_sensor8();
  else
    ;

  return 1;
}

void recv_ser_numb(void)
{
  if( flag_sensor7 )
    recv_firmware_sensor7();
  else if ( flag_sensor8)
    recv_firmware_sensor8();
  else
    ;
}

void send_zero_constant(void)
{
  if( flag_sensor7 )
    send_zero_constant_sensor7();
  else if ( flag_sensor8)
    ;//send_zero_constant_sensor8();
  else
    ;
}

void recv_zero_constant(void)
{
  if( flag_sensor7 )
    recv_zero_constant_sensor7();
  else if ( flag_sensor8)
    recv_zero_constant_sensor8();
  else
    ;
}


unsigned char sensor_cisteni_sensor7(unsigned char clean)
{
  //#ifdef SENSOR7
  pozice_bufferu=0;
  unsigned char buff[7];
   //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x07;

  //funkce
  buff[5]=ZAPNI_CISTENI;

  if(clean == SENSOR_OFF)  //Sensor OFF = 0x00
  {
    buff[6]=SENSOR_OFF;
    aktivni_cisteni=0;
  }
  else        //sensor ON = 0xff
  {
    buff[6]=SENSOR_ON;
    aktivni_cisteni=1;
  }
  Send_sensor(buff,7);
  //ocekavana velikost dat odpovedi
  size_recv=0x07;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis cisteni OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;

  flag_sensor_zadosti |= AKTIVNI_ZADOST | ZADOST_ZAPIS_CISTENI;
  /*#elif SENSOR8

  #else
  #error neni definovana verze sensoru
  #endif*/
  return 1;
}

unsigned char sensor_buzeni_sensor7(unsigned char power)
{
  //#ifdef SENSOR7
  unsigned char buff[7];
   //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x07;

  //funkce
  buff[5]=ZAPNI_BUZENI;

  buff[6]=power;

  Send_sensor(buff,7);
  //ocekavana velikost dat odpovedi
  size_recv=0x07;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis buzeni OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;

  flag_sensor_zadosti |= AKTIVNI_ZADOST | ZADOST_ZAPIS_BUZENI;

  return 1;
}


unsigned char sensor_frekvence_sensor7(unsigned char frequenc)
{
  //#ifdef SENSOR7
  unsigned char buff[7];
   //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x07;

  //funkce
  buff[5]=NASTAV_FREKVENCI;

  buff[6]=frequenc;

  Send_sensor(buff,7);
  //ocekavana velikost dat odpovedi
  size_recv=0x07;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis frekvence OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;

  flag_sensor_zadosti |= AKTIVNI_ZADOST | ZADOST_ZAPIS_FREKVENCE;
  return 1;
}

unsigned char sensor_stav_buzeni_sensor7()
{
  //#ifdef SENSOR7
  unsigned char buff[6];
  //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x07;

  //funkce
  buff[5]=ZJISTI_STAV_BUZENI;

  Send_sensor(buff,6);
  //ocekavana velikost dat odpovedi
  size_recv=0x07;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis zadosti buzeni OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;

  flag_sensor_zadosti |= AKTIVNI_ZADOST | ZADOST_STAV_BUZENI;
  return 1;
}

unsigned char sensor_stav_frekvence_sensor7()
{
  //#ifdef SENSOR7
  unsigned char buff[7];
  //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x07;

  //funkce
  buff[5]=ZJISTI_FREKVENCI;

  Send_sensor(buff,6);
  //ocekavana velikost dat odpovedi
  size_recv=0x07;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis zadosti frek OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;

  flag_sensor_zadosti |= AKTIVNI_ZADOST | ZADOST_STAV_FREKVENCE;
  return 1;

}

void send_calibration_sensor7(unsigned char id)
{
  pozice_bufferu=0;
  unsigned char funkce=0;
  unsigned long calib_data=0;
  unsigned long meas_data=0;
  //#ifdef SENSOR7
  unsigned char buff[14];

  switch(id)
  {
    case 1: //point 1
        funkce=KALIBRACE_VBODE1;
        flag_kalibrace_send |= (1<<0);
        calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_ONE);
        meas_data=RAM_Read_Data_Long(MEASURED_POINT_ONE);
        break;
    case 2: //point 2
        funkce=KALIBRACE_VBODE2;
        flag_kalibrace_send |= (1<<1);
        calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_TWO);
        meas_data=RAM_Read_Data_Long(MEASURED_POINT_TWO);
        break;
    case 3: //point 3
        funkce=KALIBRACE_VBODE3;
        flag_kalibrace_send |= (1<<2);
        calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_THREE);
        meas_data=RAM_Read_Data_Long(MEASURED_POINT_THREE);
        break;
  }
  //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x0E;

  //funkce
  buff[5]=funkce;

  //calibracni data
  buff[6]=(unsigned char)(calib_data>>24);
  buff[7]=(unsigned char)(calib_data>>16);
  buff[8]=(unsigned char)(calib_data>>8);
  buff[9]=(unsigned char)(calib_data);

  //measure data
  buff[10]=(unsigned char)(meas_data>>24);
  buff[11]=(unsigned char)(meas_data>>16);
  buff[12]=(unsigned char)(meas_data>>8);
  buff[13]=(unsigned char)(meas_data);

  //odeslani

  Send_sensor(buff,0x0E);

  size_recv=0x0E; //velikost prijatych dat bude ...

  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Odeslani kalibrace OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu=0;
}

unsigned char request_data_sensor7()
{
  pozice_bufferu=0;
  unsigned char buff[6];

  //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x06;

  //funkce
  buff[5]=DATA_ZE_SENZORU;

  //velikost
  Send_sensor(buff,6);
  //ocekavana velikost odpovedi
  size_recv=0x12;

  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zadost o data OK");
    ShowLine(INFO);
  #endif

  flag_cekam = 1;
  return 1;
}

unsigned char recv_calibration_sensor7(unsigned char id)
{
  pozice_bufferu=0;
  unsigned char buff[6];

   //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4] = 0x06;
  unsigned char funkce;

  switch(id)      //naplneni fukce podle typu kalibracniho bodu
  {
    case 1:
        funkce=KALIBRACNI_DATA1;
        flag_kalibrace_recv |= (1<<0);
    break;

    case 2:
        funkce=KALIBRACNI_DATA2;
        flag_kalibrace_recv |= (1<<1);
    break;

    case 3:
        funkce=KALIBRACNI_DATA3;
        flag_kalibrace_recv |= (1<<2);
    break;

    default:
      return 0;
  }

  //funkce
  buff[5]=funkce;

  //velikost
  Send_sensor(buff,6);

  //ocekavana velikost odpovedi
  size_recv=0x0E;

  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Prijata kalibrace OK");
    ShowLine(INFO);
  #endif

  pozice_bufferu=0;
  return 1;
}

unsigned char send_ser_numb_sensor7(unsigned long serial)
{
  pozice_bufferu=0;
  unsigned char buff[10];

   //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x0A;

  //funkce
  buff[5]=ZAPIS_SERIOVEHO_CISLA;

  buff[6]=(unsigned char)(serial>>24);
  buff[7]=(unsigned char)(serial>>16);
  buff[8]=(unsigned char)(serial>>8);
  buff[9]=(unsigned char)(serial);

  Send_sensor(buff,10);
  //ocekavana velikost dat odpovedi
  size_recv=0x06;
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Zapis serial OK");
    ShowLine(INFO);
  #endif
  pozice_bufferu = 0;
  return 1;
}

unsigned char recv_ser_numb_sensor7()
{
  pozice_bufferu = 0;
  unsigned char buff[6];

  //id zarizeni
  buff[0]=(unsigned char)(DEVICE_ID>>24);
  buff[1]=(unsigned char)(DEVICE_ID>>16);
  buff[2]=(unsigned char)(DEVICE_ID>>8);
  buff[3]=(unsigned char)(DEVICE_ID);

  //size
  buff[4]=0x06;

  //funkce
  buff[5]=CTENI_SERIOVEHO_CISLA;

  Send_sensor(buff,6);
  //ocekavana velikost odpovedi
  size_recv=0x0A;

  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Prijem serial OK");
    ShowLine(INFO);
  #endif

  pozice_bufferu = 0;
  return 1;       //vraceni serioveho cisla
}

unsigned char recv_firmware_sensor7()
{
  return 1;
}

void send_zero_constant_sensor7()
{
}

void recv_zero_constant_sensor7()
{
}

unsigned char zpracovat_data_sensor7()
{
  unsigned long cdata,mdata;
  unsigned long recv_id;
  ///vypnuti casovace kvuli timeoutu;
  recv_id=0;
  recv_id  = ((unsigned long)recv_buffer[0])<<24;
  recv_id += ((unsigned long)recv_buffer[1])<<16;
  recv_id += ((unsigned long)recv_buffer[2])<<8;
  recv_id += (unsigned long)recv_buffer[3];

  if(recv_id != DEVICE_ID)        //pokud nesouhlasi ID
  {
    #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"spatne prijate id");
    ShowLine(2);
    #endif
    return 0;
  }
    switch(recv_buffer[5])
    {
        case DATA_ZE_SENZORU:
            prutok=0;
            prutok  = ((long)recv_buffer[6])<<24;
            prutok += ((long)recv_buffer[7])<<16;
            prutok += ((long)recv_buffer[8])<<8;
            prutok += (long)recv_buffer[9];

            teplota=0;
            teplota  = ((unsigned long)recv_buffer[10])<<24;
            teplota += ((unsigned long)recv_buffer[11])<<16;
            teplota += ((unsigned long)recv_buffer[12])<<8;
            teplota += (unsigned long)recv_buffer[13];

            ep=0;
            ep  = ((unsigned long)recv_buffer[14])<<24;
            ep += ((unsigned long)recv_buffer[15])<<16;
            ep += ((unsigned long)recv_buffer[16])<<8;
            ep += (unsigned long)recv_buffer[17];
        break;

        case KALIBRACNI_DATA1:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);
            flag_kalibrace_recv &= ~(1<<0);

            RAM_Write_Data_Long(cdata,CALIBRATION_POINT_ONE);
            RAM_Write_Data_Long(mdata,MEASURED_POINT_ONE);
        break;

        case KALIBRACNI_DATA2:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);
            flag_kalibrace_recv &= ~(1<<1);

            RAM_Write_Data_Long(cdata,CALIBRATION_POINT_TWO);
            RAM_Write_Data_Long(mdata,MEASURED_POINT_TWO);
        break;

        case KALIBRACNI_DATA3:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);
            flag_kalibrace_recv &= ~(1<<2);

            RAM_Write_Data_Long(cdata,CALIBRATION_POINT_THREE);
            RAM_Write_Data_Long(mdata,MEASURED_POINT_THREE);
        break;

        case KALIBRACE_VBODE1:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);

            //if(( mdata1 != set_mdata1 ))
            if(( mdata != RAM_Read_Data_Long( MEASURED_POINT_ONE)) || (cdata != RAM_Read_Data_Long(CALIBRATION_POINT_ONE)))
                {
                    #ifdef DEBUGGER
                    EraseLineBuffer();
                    WriteLine(0,"Prijata calib data1 nesouhlasi");
                    ShowLine(ERR_LINE);
                    #endif

                    return 0;
                }
            else
                {
                 flag_kalibrace_send &= ~(1<<0);
                }
       break;

        case KALIBRACE_VBODE2:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);

            //if( ( cdata2 != set_cdata2 ) || ( mdata2 != set_mdata2 ))
            if( ( cdata != RAM_Read_Data_Long( CALIBRATION_POINT_TWO) ) || ( mdata != RAM_Read_Data_Long( MEASURED_POINT_TWO) ))
                {
                    #ifdef DEBUGGER
                    EraseLineBuffer();
                    WriteLine(0,"Prijata calib data2 nesouhlasi");
                    ShowLine(ERR_LINE);
                    #endif
                    return 0;
                }
            else
                {
                 flag_kalibrace_send &= ~(1<<1);
                }
        break;

        case KALIBRACE_VBODE3:
            cdata=0;
            cdata =((unsigned long)recv_buffer[6])<<24;
            cdata +=((unsigned long)recv_buffer[7])<<16;
            cdata +=((unsigned long)recv_buffer[8])<<8;
            cdata +=((unsigned long)recv_buffer[9]);

            mdata=0;
            mdata =((unsigned long)recv_buffer[10])<<24;
            mdata +=((unsigned long)recv_buffer[11])<<16;
            mdata +=((unsigned long)recv_buffer[12])<<8;
            mdata +=((unsigned long)recv_buffer[13]);

            //if( ( cdata3 != set_cdata3 ) || ( mdata3 != set_mdata3 ))
            if( ( cdata != RAM_Read_Data_Long(CALIBRATION_POINT_THREE) ) || ( mdata != RAM_Read_Data_Long( MEASURED_POINT_THREE)))
                {
                    #ifdef DEBUGGER
                    EraseLineBuffer();
                    WriteLine(0,"Prijata calib data3 nesouhlasi");
                    ShowLine(ERR_LINE);
                    #endif
                    return 0;
                }
            else
                {
                 flag_kalibrace_send &= ~(1<<2);
                }
        break;

        case ZAPIS_SERIOVEHO_CISLA:
        case CTENI_SERIOVEHO_CISLA:
        break;

        case ZAPNI_CISTENI:
            if( aktivni_cisteni == 0 )  //necisti
            {
                if( recv_buffer[6] != SENSOR_OFF)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_CISTENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_CISTENI & ~AKTIVNI_ZADOST;
            }
            else                        //cisti
            {
                if( recv_buffer[6] != SENSOR_ON)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_CISTENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_CISTENI & ~AKTIVNI_ZADOST;
            }
        break;
        case NASTAV_FREKVENCI:
            if( RAM_Read_Data_Long(EXCITATION_FREQUENCY) == 0)  //3.125
            {
                if( recv_buffer[6] != FREKVENCE_3HZ)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_FREKVENCE;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_FREKVENCE & ~AKTIVNI_ZADOST;
            }
            else                                                //6.25
            {
                if( recv_buffer[6] != FREKVENCE_6HZ)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_FREKVENCE;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_FREKVENCE & ~AKTIVNI_ZADOST;
            }
        break;

        case ZAPNI_BUZENI:
            if( RAM_Read_Data_Long(EXCITATION) == 0)    //ON
            {
                if(recv_buffer[6] != SENSOR_ON)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_BUZENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_BUZENI & ~AKTIVNI_ZADOST;
            }
            else                                        //OFF
            {
                if(recv_buffer[6] != SENSOR_OFF)
                    flag_sensor_zadosti |= ZADOST_ZAPIS_BUZENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_ZAPIS_BUZENI & ~AKTIVNI_ZADOST;
            }
        break;

        case ZJISTI_FREKVENCI:
        if(RAM_Read_Data_Long(EXCITATION_FREQUENCY) == 0)    //3.125
        {
            if(recv_buffer[6] != FREKVENCE_3HZ)
                flag_sensor_zadosti |= ZADOST_STAV_FREKVENCE;
            else
                flag_sensor_zadosti &= ~ZADOST_STAV_FREKVENCE & ~AKTIVNI_ZADOST;
        }
        else                                                //6.25
        {
            if(recv_buffer[6] != FREKVENCE_6HZ)
                flag_sensor_zadosti |= ZADOST_STAV_FREKVENCE;
            else
                flag_sensor_zadosti &= ~ZADOST_STAV_FREKVENCE & ~AKTIVNI_ZADOST;
        }
        break;

        case ZJISTI_STAV_BUZENI:
            if(RAM_Read_Data_Long(EXCITATION) == 0)         //ON
            {
                if(recv_buffer[6] != SENSOR_ON)
                    flag_sensor_zadosti |= ZADOST_STAV_BUZENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_STAV_BUZENI & ~AKTIVNI_ZADOST;
            }
            else                                            //OFF
            {
                if(recv_buffer[6] != SENSOR_OFF)
                    flag_sensor_zadosti |= ZADOST_STAV_BUZENI;
                else
                    flag_sensor_zadosti &= ~ZADOST_STAV_BUZENI & ~AKTIVNI_ZADOST;
            }
        break;

        default:
            #ifdef DEBUGGER
            EraseLineBuffer();
            WriteLine(0,"Prijata spatna funkce");
            ShowLine(ERR_LINE);
            #endif
            return 0;

        break;
  }
  #ifdef DEBUGGER
    EraseLineBuffer();
    WriteLine(0,"Data prijata OK");
    ShowLine(SHOW_ANS);
  #endif
  size_recv=0;
  flag_receive=0;
  return 1;
}

void sensor_communication()      //preurseni od UART3
{
  recv_buffer[pozice_bufferu]=Read_sensor();
  pozice_bufferu++;
  if( flag_sensor7)
  {
      if(pozice_bufferu==recv_buffer[4])
          {
          flag_receive=1;
          pozice_bufferu=0;
          }
  }

  if( flag_sensor8)
  {
    switch(recv_buffer[1])
    {
      case WRITE_MULTIPLE_REGISTER:
        if(pozice_bufferu>=8)
        {
          flag_receive=1;
          pozice_bufferu=0;
        }
      break;
      case READ_HOLDING_REGISTER:
        if(pozice_bufferu==recv_buffer[2]+5)
        {
          flag_receive=1;
          pozice_bufferu=0;
        }
      break;
      default:
      break;
    }
  }
  VICVectAddr=0;
}
