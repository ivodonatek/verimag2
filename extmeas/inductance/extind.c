//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of inductance.
//*****************************************************************************/

#include <stdint.h>
#include <math.h>
#include "extind.h"
#include "LDC1000_register_settings.h"
#include "../../devices/LDC1000/LDC1000.h"

// frequency of the external clock
#define LDC1000_FREQ_EXT    8000000

// parallel sensor capacitance
#define LDC1000_PAR_SEN_CAP (0.0000001)

#define PI                  (3.1415926535897932384626433832795)

// Error code type
typedef enum ErrorCode
{
    ERRC_NONE,              // No error
    ERRC_CLOSED,            // fw module closed
    ERRC_CFG_DEVICE,        // Unsuccessful configuration of the measuring device
    ERRC_NO_DATA,           // ADC Data Not Available
    ERRC_OVER_RANGE,        // ADC value over range
    ERRC_EMPTY_QUEUE,       // empty queue of ADC samples

} TErrorCode;

// Fw module state
static struct
{
    // open state
    BOOL isOpen;

    // measuring device configuration state
    BOOL isMeasureDeviceConfigured;

    // measured inductance mean value in H
    float measuredMeanValue;

    // measurement queue of ADC samples for calculating the mean value
    uint32_t measurementQueue[EXTIND_MEASUREMENT_QUEUE_LENGTH];

    // number of valid values in measurement queue
    uint8_t valuesInQueue;

    // index of next value in measurement queue
    uint8_t indexInQueue;

    // AD Device state
    uint8_t adcState;

    // raw adc output data
    uint32_t adcData;

    // error code
    TErrorCode errorCode;

} state;

//------------------------------------------------------------------------------
//  Convert error code to text.
//------------------------------------------------------------------------------
static char* ConvertErrorCodeToText(TErrorCode errorCode)
{
    switch (errorCode)
    {
    case ERRC_NONE:
        return "OK";

    case ERRC_CLOSED:
        return "Closed";

    case ERRC_CFG_DEVICE:
        return "Config Device";

    case ERRC_NO_DATA:
        return "Data Not Available";

    case ERRC_OVER_RANGE:
        return "ADC Over Range";

    case ERRC_EMPTY_QUEUE:
        return "Empty Queue";

    default:
        return "Unknown error code";
    }
}

//------------------------------------------------------------------------------
//  Clear all measured inductances.
//------------------------------------------------------------------------------
static void ClearAllMeasurements(void)
{
    state.measuredMeanValue = UNKNOWN_INDUCTANCE;
    state.valuesInQueue = state.indexInQueue = 0;
    state.adcData = UNKNOWN_ADC_INDUCTANCE;
    state.adcState = 0xFF;
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device communication interface.
//------------------------------------------------------------------------------
static void ConfigureMeasureDeviceCommIface(void)
{
    LDC1000_SPISetup();
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device. Returns TRUE if success or FALSE
//  if error.
//------------------------------------------------------------------------------
static BOOL ConfigureMeasureDevice(void)
{
    // Standby mode
    if (!LDC1000_SPIWriteReadReg(LDC1000_Power_Config_REG, LDC1000_Power_Config_Standby_REG_VALUE))
        return FALSE;

    // LDC Configuration
    if (!LDC1000_SPIWriteReadReg(LDC1000_LDC_Config_REG, LDC1000_LDC_Config_REG_VALUE))
        return FALSE;

    // Crystal drive enabled
    if (!LDC1000_SPIWriteReadReg(LDC1000_Clock_Config_REG, LDC1000_Clock_Config_Enabled_REG_VALUE))
        return FALSE;

    // Active mode
    if (!LDC1000_SPIWriteReadReg(LDC1000_Power_Config_REG, LDC1000_Power_Config_Active_REG_VALUE))
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Push new measured ADC value into queue.
//------------------------------------------------------------------------------
static void PushMeasuredValueIntoQueue(uint32_t value)
{
    state.measurementQueue[state.indexInQueue] = value;

    if (state.valuesInQueue < EXTIND_MEASUREMENT_QUEUE_LENGTH)
        state.valuesInQueue++;

    state.indexInQueue++;
    if (state.indexInQueue >= EXTIND_MEASUREMENT_QUEUE_LENGTH)
        state.indexInQueue = 0;
}

//------------------------------------------------------------------------------
//  Calculate and returns mean sample in queue.
//------------------------------------------------------------------------------
static uint32_t CalculateMeanADCSampleInQueue(void)
{
    if (state.valuesInQueue == 0)
        return 0;

    uint32_t sum = 0;
    for (uint8_t i = 0; i < state.valuesInQueue; i++)
        sum += state.measurementQueue[i];

    return (uint32_t)((((float)sum) / state.valuesInQueue) + 0.5f); // round
}

//------------------------------------------------------------------------------
//  Calculate and returns measured mean value in queue.
//------------------------------------------------------------------------------
static float CalculateMeasuredMeanValueInQueue(void)
{
    if (state.valuesInQueue == 0)
        return UNKNOWN_INDUCTANCE;

    uint32_t meanSample = CalculateMeanADCSampleInQueue();
    if (meanSample == 0)
        return UNKNOWN_INDUCTANCE;

    float fSensor = (LDC1000_FREQ_EXT * LDC1000_LDC_CONFIG_RESPONSE_TIME) / (3.0 * meanSample);
    float omega = 2 * PI * fSensor;
    float L = 1 / (LDC1000_PAR_SEN_CAP * omega * omega);

    return L;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtInd_Init(void)
{
    state.isOpen = state.isMeasureDeviceConfigured = FALSE;
    state.errorCode = ERRC_NONE;
    ClearAllMeasurements();
}

//------------------------------------------------------------------------------
//  Allocation of measurement resources. Call only once before measurement.
//------------------------------------------------------------------------------
void ExtInd_Open(void)
{
    ExtInd_Init();

    state.isOpen = TRUE;

    ConfigureMeasureDeviceCommIface();
}

//------------------------------------------------------------------------------
//  Free measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtInd_Close(void)
{
    if (!state.isOpen)
        return;

    if (state.isMeasureDeviceConfigured)
    {
        // Standby mode
        LDC1000_SPIWriteReg(LDC1000_Power_Config_REG, LDC1000_Power_Config_Standby_REG_VALUE);

        // Crystal drive disabled
        LDC1000_SPIWriteReg(LDC1000_Clock_Config_REG, LDC1000_Clock_Config_Disabled_REG_VALUE);
    }

    ExtInd_Init();
}

//------------------------------------------------------------------------------
//  Inductance measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtInd_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtInd_Measure(void)
{
    if (!state.isOpen)
    {
        state.errorCode = ERRC_CLOSED;
        return FALSE;
    }

    if (!state.isMeasureDeviceConfigured)
    {
        state.isMeasureDeviceConfigured = ConfigureMeasureDevice();
        if (!state.isMeasureDeviceConfigured)
        {
            state.errorCode = ERRC_CFG_DEVICE;
            return FALSE;
        }
    }

    state.adcState = LDC1000_SPIReadReg(LDC1000_Status_REG);
    /*if (state.adcState & LDC1000_Status_REG_Data_Ready_BIT)
        return TRUE;*/

    state.adcData = 0;
    LDC1000_SPIReadRegs(LDC1000_FCOUNT_LSB_REG, &state.adcData, 3);

    // save sample value
    PushMeasuredValueIntoQueue(state.adcData);

    // Calculate mean value
    state.measuredMeanValue = CalculateMeasuredMeanValueInQueue();

    state.errorCode = ERRC_NONE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Get the measured value in H or UNKNOWN_INDUCTANCE if no
//  inductance value available.
//------------------------------------------------------------------------------
float ExtInd_GetMeasuredValue(void)
{
    return state.measuredMeanValue;
}

//------------------------------------------------------------------------------
//  Get the raw adc output data.
//------------------------------------------------------------------------------
uint32_t ExtInd_GetAdcData(void)
{
    return state.adcData;
}

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//------------------------------------------------------------------------------
uint8_t ExtInd_GetValuesInQueue(void)
{
    return state.valuesInQueue;
}

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtInd_GetErrorText(void)
{
    return ConvertErrorCodeToText(state.errorCode);
}

//------------------------------------------------------------------------------
//  Get measurement device (LDC1000) status
//------------------------------------------------------------------------------
uint8_t ExtInd_GetMeasDeviceStatus(void)
{
    return state.adcState;
}
