//----------------------------------------------------------------------------
//  This file contains values for the LDC1000 registers.
//----------------------------------------------------------------------------

#ifndef LDC1000_REGISTER_SETTINGS_H_INCLUDED
#define LDC1000_REGISTER_SETTINGS_H_INCLUDED

/* Power Configuration
0:Standby mode: LDC1000 is in a lower power mode but not actively
converting. It is recommended to configure the LDC1000 while in this
mode.
1:Active Mode. Conversion is Enabled
*/
#define LDC1000_Power_Config_Standby_REG_VALUE                  (0x00)
#define LDC1000_Power_Config_Active_REG_VALUE                   (0x01)

/* LDC Configuration
4:3 Amplitude
Sets the oscillation amplitude
00:1V
01:2V
10:4V
11:Reserved

2:0 RESPONSE_TIME
000: Reserved
001: Reserved
010: 192
011: 384
100: 768
101: 1536
110: 3072
111: 6144
*/
#define LDC1000_LDC_Config_REG_VALUE                            (0x0F)
#define LDC1000_LDC_CONFIG_RESPONSE_TIME                        6144

/* Clock Configuration
1 CLK_SEL
Select Clock input type for L measurements.
0: Clock input on XIN pin
1: Crystal connected across XIN/XOUT pins.

0 CLK_PD
Crystal Power Down.
0: Crystal drive enabled.
1: Crystal drive is disabled. Use this setting to reduce power
consumption with a crystal input when device is in Standby mode.
Use this setting for clock input.
*/
#define LDC1000_Clock_Config_Enabled_REG_VALUE                  (0x02)
#define LDC1000_Clock_Config_Disabled_REG_VALUE                 (0x03)

#endif // LDC1000_REGISTER_SETTINGS_H_INCLUDED
