//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of inductance.
//*****************************************************************************/

#ifndef EXTIND_H_INCLUDED
#define EXTIND_H_INCLUDED

#include <stdint.h>

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// The measurement queue length for calculating the mean value.
#define EXTIND_MEASUREMENT_QUEUE_LENGTH    10

#define UNKNOWN_INDUCTANCE     (-1.0f)
#define UNKNOWN_ADC_INDUCTANCE (0xFFFFFFFF)

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtInd_Init(void);

//------------------------------------------------------------------------------
//  Allocation of measurement resources. Call only once before measurement.
//------------------------------------------------------------------------------
void ExtInd_Open(void);

//------------------------------------------------------------------------------
//  Free measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtInd_Close(void);

//------------------------------------------------------------------------------
//  Inductance measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtInd_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtInd_Measure(void);

//------------------------------------------------------------------------------
//  Get the measured value in H or UNKNOWN_INDUCTANCE if no
//  inductance value available.
//------------------------------------------------------------------------------
float ExtInd_GetMeasuredValue(void);

//------------------------------------------------------------------------------
//  Get the raw adc output data.
//------------------------------------------------------------------------------
uint32_t ExtInd_GetAdcData(void);

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//  (0 .. EXTIND_MEASUREMENT_QUEUE_LENGTH)
//------------------------------------------------------------------------------
uint8_t ExtInd_GetValuesInQueue(void);

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtInd_GetErrorText(void);

//------------------------------------------------------------------------------
//  Get measurement device (LDC1000) status
//------------------------------------------------------------------------------
uint8_t ExtInd_GetMeasDeviceStatus(void);

#endif // EXTIND_H_INCLUDED
