//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of temperature.
//*****************************************************************************/

#ifndef EXTTEMP_H_INCLUDED
#define EXTTEMP_H_INCLUDED

#include <stdint.h>

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#define UNKNOWN_TEMPERATURE     (-1.0f)
#define UNKNOWN_ADC_TEMPERATURE (0xFFFFFFFF)

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtTemp_Init(void);

//------------------------------------------------------------------------------
//  Allocation of temperature measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtTemp_Open(void);

//------------------------------------------------------------------------------
//  Free temperature measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtTemp_Close(void);

//------------------------------------------------------------------------------
//  Temperature measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtTemp_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtTemp_Measure(void);

//------------------------------------------------------------------------------
//  Get the measured value in degrees Kelvin or UNKNOWN_TEMPERATURE if no
//  temperature value available.
//------------------------------------------------------------------------------
float ExtTemp_GetMeasuredValue(void);

//------------------------------------------------------------------------------
//  Calibrate temperature measurement. Input parameter is expected temperature.
//------------------------------------------------------------------------------
void ExtTemp_CalibrateMeasurement(float temperature);

//------------------------------------------------------------------------------
//  Call after temperature sensor type changed.
//------------------------------------------------------------------------------
void ExtTemp_OnSensorTypeChanged(void);

//------------------------------------------------------------------------------
//  Call after temperature sensor connection changed.
//------------------------------------------------------------------------------
void ExtTemp_OnSensorConnectionChanged(void);

//------------------------------------------------------------------------------
//  Get the raw adc output data.
//------------------------------------------------------------------------------
uint32_t ExtTemp_GetAdcData(void);

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//------------------------------------------------------------------------------
uint8_t ExtTemp_GetValuesInQueue(void);

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtTemp_GetErrorText(void);

//------------------------------------------------------------------------------
// Get ADC input circuit reference electrical resistance [Ohm].
//------------------------------------------------------------------------------
float ExtTemp_GetADCRefResistance(void);

#endif // EXTTEMP_H_INCLUDED
