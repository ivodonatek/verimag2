//----------------------------------------------------------------------------
//  This file contains values for the LMP90100 registers.
//----------------------------------------------------------------------------

#ifndef HEADER_FILE_TI_LMP90100_REGISTER_SETTINGS_H
#define HEADER_FILE_TI_LMP90100_REGISTER_SETTINGS_H

/************************************************************
* TI LMP90100 REGISTER SET INITIALIZATION VALUES
************************************************************/

/* Register and Conversion Reset */
#define TI_LMP90100_RESETCN_REG_VALUE                  (0xC3)

/* SPI Normal Streaming mode */
#define TI_LMP90100_SPI_STREAMCN_REG_VALUE             (0x00)

/* Power Mode Control and Status
    - Active Mode */
#define TI_LMP90100_PWRCN_REG_VALUE                    (0x00)

/* Background Calibration Control
    - BgcalMode0: Background Calibration OFF */
#define TI_LMP90100_BGCALCN_REG_VALUE                  (0x00)

/* ADC Auxiliary Control
    - "External Clock Detection" is operational
    - Selects external clock
    - RTD Current 1000 μA */
#define TI_LMP90100_ADC_AUXCN_REG_VALUE                (0x1A)

/* GPIO Direction
    - D0 - D5: Output
    - D6: default (Input, not used) */
#define TI_LMP90100_GPIO_DIRCN_REG_VALUE               (0x3F)

/* GPIO Data used
    - D0 - D5: Used GPIOs */
#define TI_LMP90100_GPIO_DAT_REG_VALUE_USED_MASK       (0x3F)

/* Channel Scan Mode for 2 wire RTD connection
    - Starting channel for conversion: CH0
    - Last channel for conversion: CH0
    - ScanMode2: One or more channels Continuous Scan */
#define TI_LMP90100_CH_SCAN_2WIRE_REG_VALUE            (0x80)

/* Channel Scan Mode for 3/4 wire RTD connection
    - Starting channel for conversion: CH0
    - Last channel for conversion: CH1
    - ScanMode2: One or more channels Continuous Scan */
#define TI_LMP90100_CH_SCAN_34WIRE_REG_VALUE           (0x88)

/* CH0 Input Control for 2 wire RTD connection
    - Disable Sensor Diagnostics current injection for this Channel
    - Select the reference: VREFP2 and VREFN2
    - Positive input select: VIN0
    - Negative input select: VIN6 */
#define TI_LMP90100_CH0_INPUTCN_2WIRE_REG_VALUE        (0x46)

/* CH0 Input Control for 3/4 wire RTD connection
    - Disable Sensor Diagnostics current injection for this Channel
    - Select the reference: VREFP2 and VREFN2
    - Positive input select: VIN0
    - Negative input select: VIN1 */
#define TI_LMP90100_CH0_INPUTCN_34WIRE_REG_VALUE       (0x41)

/* CH1 Input Control for 3/4 wire RTD connection
    - Disable Sensor Diagnostics current injection for this Channel
    - Select the reference: VREFP2 and VREFN2
    - Positive input select: VIN1
    - Negative input select: VIN6 */
#define TI_LMP90100_CH1_INPUTCN_34WIRE_REG_VALUE       (0x4E)

/* CHx Configuration
    - ODR Select: 214.65 SPS
    - Gain Select: FGA OFF
    - Include the buffer in the signal path */
#define TI_LMP90100_CHX_CONFIG_REG_VALUE               (0x70)

/************************************************************
* TI LMP90100 REGISTER VALUES
************************************************************/

/* The largest positive ADC Conversion Data value */
#define TI_LMP90100_ADC_DOUT_MAX_POS_VALUE             (0x7FFFFF)

/* The half ADC Conversion Data value range */
#define TI_LMP90100_ADC_DOUT_HALF_VALUE_RANGE          (0x800000)

/* Data Not Available – indicates if new conversion data is not available */
#define TI_LMP90100_ADC_DONE_NOT_AVAILABLE_VALUE       (0xFF)

#endif   // HEADER_FILE_TI_LMP90100_REGISTER_SETTINGS_H
