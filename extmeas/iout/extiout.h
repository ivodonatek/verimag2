//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of current loop (IOut).
//*****************************************************************************/

#ifndef EXTIOUT_H_INCLUDED
#define EXTIOUT_H_INCLUDED

#include <stdint.h>

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// The measurement queue length for calculating the mean value.
#define EXTIOUT_MEASUREMENT_QUEUE_LENGTH    10

#define UNKNOWN_IOUT     (-1.0f)
#define UNKNOWN_ADC_IOUT (0xFFFF)

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtIout_Init(void);

//------------------------------------------------------------------------------
//  Allocation of measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtIout_Open(void);

//------------------------------------------------------------------------------
//  Free measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtIout_Close(void);

//------------------------------------------------------------------------------
//  Iout measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtIout_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtIout_Measure(void);

//------------------------------------------------------------------------------
//  Get the measured value in A or UNKNOWN_IOUT if no current value available.
//------------------------------------------------------------------------------
float ExtIout_GetMeasuredValue(void);

//------------------------------------------------------------------------------
//  Get the raw adc output data or UNKNOWN_ADC_IOUT.
//------------------------------------------------------------------------------
uint16_t ExtIout_GetAdcData(void);

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//  (0 .. EXTIOUT_MEASUREMENT_QUEUE_LENGTH)
//------------------------------------------------------------------------------
uint8_t ExtIout_GetValuesInQueue(void);

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtIout_GetErrorText(void);

#endif // EXTIOUT_H_INCLUDED
