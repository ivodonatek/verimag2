//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of current loop (IOut).
//*****************************************************************************/

#include <stdint.h>
#include <limits.h>
#include "extiout.h"
#include "../../devices/ADS7822/ADS7822.h"

// ADC sample values
#define MIN_ADC_VALUE    	0
#define MAX_ADC_VALUE       4095

// specific ADC sample values
#define IOUT_ADC_4mA_VALUE    	3334
#define IOUT_ADC_20mA_VALUE     557

// Error code type
typedef enum ErrorCode
{
    ERRC_NONE,              // No error
    ERRC_CLOSED,            // fw module closed
    ERRC_SAMPLING           // sample error

} TErrorCode;

// Fw module state
static struct
{
    // open state
    BOOL isOpen;

    // measured current mean value in A
    float measuredMeanValue;

    // measurement queue for calculating the mean value
    uint16_t measurementQueue[EXTIOUT_MEASUREMENT_QUEUE_LENGTH];

    // number of valid values in measurement queue
    uint8_t valuesInQueue;

    // index of next value in measurement queue
    uint8_t indexInQueue;

    // raw adc output data
    uint16_t adcData;

    // error code
    TErrorCode errorCode;

} state;

//------------------------------------------------------------------------------
//  Convert error code to text.
//------------------------------------------------------------------------------
static char* ConvertErrorCodeToText(TErrorCode errorCode)
{
    switch (errorCode)
    {
    case ERRC_NONE:
        return "OK";

    case ERRC_CLOSED:
        return "Closed";

    case ERRC_SAMPLING:
        return "Sampling";

    default:
        return "Unknown error code";
    }
}

// -----------------------------------------------------------------------------
// Prevod hodnoty ADC vzorku na proud [A]
// -----------------------------------------------------------------------------
static float ConvertADCSampleToCurrent(uint16_t sample)
{
	float k;

	k = ((float)(0.004f - 0.020f)) / ((float)(IOUT_ADC_4mA_VALUE - IOUT_ADC_20mA_VALUE));

	return (float)(k * (sample - IOUT_ADC_20mA_VALUE) + 0.020f);
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device communication interface.
//------------------------------------------------------------------------------
static void ConfigureMeasureDeviceCommIface(void)
{
    ADS7822_Setup();
}

//------------------------------------------------------------------------------
//  Push new measured value into queue.
//------------------------------------------------------------------------------
static void PushMeasuredValueIntoQueue(uint16_t value)
{
    state.measurementQueue[state.indexInQueue] = value;

    if (state.valuesInQueue < EXTIOUT_MEASUREMENT_QUEUE_LENGTH)
        state.valuesInQueue++;

    state.indexInQueue++;
    if (state.indexInQueue >= EXTIOUT_MEASUREMENT_QUEUE_LENGTH)
        state.indexInQueue = 0;
}

//------------------------------------------------------------------------------
//  Calculate and returns measured mean value in queue.
//------------------------------------------------------------------------------
static float CalculateMeasuredMeanValueInQueue(void)
{
    if (state.valuesInQueue == 0)
        return UNKNOWN_IOUT;

    // sample sum
    uint32_t sum = 0;
    for (uint8_t i = 0; i < state.valuesInQueue; i++)
        sum += state.measurementQueue[i];

    // mean sample
    uint16_t meanSample = (uint16_t)((((float)sum) / state.valuesInQueue) /*+0.5f*/); // round

    // mean current
    float meanCurrent = ConvertADCSampleToCurrent(meanSample);

    return meanCurrent;
}

//------------------------------------------------------------------------------
//  Clear all measured values.
//------------------------------------------------------------------------------
static void ClearAllMeasurements(void)
{
    state.measuredMeanValue = UNKNOWN_IOUT;
    state.valuesInQueue = state.indexInQueue = 0;
    state.adcData = UNKNOWN_ADC_IOUT;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtIout_Init(void)
{
    state.isOpen = FALSE;
    state.errorCode = ERRC_NONE;
    ClearAllMeasurements();
}

//------------------------------------------------------------------------------
//  Allocation of measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtIout_Open(void)
{
    ExtIout_Init();

    state.isOpen = TRUE;

    ConfigureMeasureDeviceCommIface();
}

//------------------------------------------------------------------------------
//  Free measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtIout_Close(void)
{
    ExtIout_Init();
}

//------------------------------------------------------------------------------
//  Iout measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtIout_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtIout_Measure(void)
{
    if (!state.isOpen)
    {
        state.errorCode = ERRC_CLOSED;
        return FALSE;
    }

    // Get data sample
    state.adcData = ADS7822_GetSample();
    if (state.adcData < 0)
    {
        state.errorCode = ERRC_SAMPLING;
        return FALSE; // sample error
    }

    // save sample value
    PushMeasuredValueIntoQueue(state.adcData);

    // Calculate mean value
    state.measuredMeanValue = CalculateMeasuredMeanValueInQueue();

    state.errorCode = ERRC_NONE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Get the measured value in A or UNKNOWN_IOUT if no current value available.
//------------------------------------------------------------------------------
float ExtIout_GetMeasuredValue(void)
{
    return state.measuredMeanValue;
}

//------------------------------------------------------------------------------
//  Get the raw adc output data or UNKNOWN_ADC_IOUT.
//------------------------------------------------------------------------------
uint16_t ExtIout_GetAdcData(void)
{
    return state.adcData;
}

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//  (0 .. EXTIOUT_MEASUREMENT_QUEUE_LENGTH)
//------------------------------------------------------------------------------
uint8_t ExtIout_GetValuesInQueue(void)
{
    return state.valuesInQueue;
}

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtIout_GetErrorText(void)
{
    return ConvertErrorCodeToText(state.errorCode);
}
