//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of pressure.
//*****************************************************************************/

#include <stdint.h>
#include <limits.h>
#include "extpress.h"
#include "../../maine.h"
#include "../../menu_tables.h"
#include "../../conversion.h"
#include "../../devices/ADS7822/ADS7822.h"

// The measurement queue length for calculating the mean value.
#define MEASUREMENT_QUEUE_LENGTH    10

// Converter Pressure [Pa]
#define MIN_CONVERTER_PRESSURE       0
#define MAX_CONVERTER_PRESSURE       INT_MAX

// Converter Current [mA]
#define MIN_CONVERTER_CURRENT        (4.0f)
#define MAX_CONVERTER_CURRENT        (20.0f)

// ADC sample values
#define MIN_ADC_VALUE    	0
#define MAX_ADC_VALUE       4095

// specific ADC sample values
#define ADC_4mA_VALUE    	3375
#define ADC_20mA_VALUE      757

// Error code type
typedef enum ErrorCode
{
    ERRC_NONE,              // No error
    ERRC_CLOSED,            // fw module closed
    ERRC_SAMPLING           // sample error

} TErrorCode;

// Fw module state
static struct
{
    // open state
    BOOL isOpen;

    // measured pressure mean value in Pa
    int32_t measuredMeanValue;

    // measurement queue for calculating the mean value
    uint16_t measurementQueue[MEASUREMENT_QUEUE_LENGTH];

    // number of valid values in measurement queue
    uint8_t valuesInQueue;

    // index of next value in measurement queue
    uint8_t indexInQueue;

    // raw adc output data
    int16_t adcData;

    // error code
    TErrorCode errorCode;

} state;

//------------------------------------------------------------------------------
//  Convert error code to text.
//------------------------------------------------------------------------------
static char* ConvertErrorCodeToText(TErrorCode errorCode)
{
    switch (errorCode)
    {
    case ERRC_NONE:
        return "OK";

    case ERRC_CLOSED:
        return "Closed";

    case ERRC_SAMPLING:
        return "Sampling";

    default:
        return "Unknown error code";
    }
}

//------------------------------------------------------------------------------
//  Get converter low pressure in Pa (pascal).
//------------------------------------------------------------------------------
static uint32_t ExtPress_GetConverterLowPressure(void)
{
    return Convert_BarToPascal(RAM_Read_Data_Long(I_EXT_PRESS_MIN) / 1000.0);
}

//------------------------------------------------------------------------------
//  Get converter high pressure in Pa (pascal).
//------------------------------------------------------------------------------
static uint32_t ExtPress_GetConverterHighPressure(void)
{
    return Convert_BarToPascal(RAM_Read_Data_Long(I_EXT_PRESS_MAX) / 1000.0);
}

//------------------------------------------------------------------------------
//  Get converter low current in mA.
//------------------------------------------------------------------------------
static float ExtPress_GetConverterLowCurrent(void)
{
    return RAM_Read_Data_Long(EXT_PRESS_CURRENT_MIN) / 1.0;
}

//------------------------------------------------------------------------------
//  Get converter high current in mA.
//------------------------------------------------------------------------------
static float ExtPress_GetConverterHighCurrent(void)
{
    return RAM_Read_Data_Long(EXT_PRESS_CURRENT_MAX) / 1.0;
}

// -----------------------------------------------------------------------------
// Prevod hodnoty ADC vzorku na proud [mA]
// -----------------------------------------------------------------------------
static float ConvertADCSampleToCurrent(uint16_t sample)
{
	float k;

	if (sample <= ADC_20mA_VALUE)
		return MAX_CONVERTER_CURRENT;

	if (sample >= ADC_4mA_VALUE)
		return MIN_CONVERTER_CURRENT;

	k = ((float)(MIN_CONVERTER_CURRENT - MAX_CONVERTER_CURRENT)) / ((float)(ADC_4mA_VALUE - ADC_20mA_VALUE));

	return (float)(k * (sample - ADC_20mA_VALUE) + MAX_CONVERTER_CURRENT);
}

// -----------------------------------------------------------------------------
// Prevod proudu [mA] na tlak [Pa]. Po chybe vraci UNKNOWN_PRESSURE
// -----------------------------------------------------------------------------
static int32_t ConvertCurrentToPressure(float current)
{
	float currentLow = ExtPress_GetConverterLowCurrent();
	float currentHigh = ExtPress_GetConverterHighCurrent();

    if (currentLow > currentHigh)
		return UNKNOWN_PRESSURE;

    uint32_t pressureLow = ExtPress_GetConverterLowPressure();
	uint32_t pressureHigh = ExtPress_GetConverterHighPressure();

    if (pressureLow > pressureHigh)
		return UNKNOWN_PRESSURE;

    float currentDiff = currentHigh - currentLow;
	if (currentDiff == 0)
		return UNKNOWN_PRESSURE;

    float pressureDiff = pressureHigh - pressureLow;

    // vypocet podle primky y = k*x + q
	float k = pressureDiff / currentDiff;
	float q = pressureLow - k * currentLow;
	float pressure = k * current + q;

	if (pressure < MIN_CONVERTER_PRESSURE)
        return MIN_CONVERTER_PRESSURE;

    if (pressure > MAX_CONVERTER_PRESSURE)
        return MAX_CONVERTER_PRESSURE;

    return (int32_t)pressure;
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device communication interface.
//------------------------------------------------------------------------------
static void ConfigureMeasureDeviceCommIface(void)
{
    ADS7822_Setup();
}

//------------------------------------------------------------------------------
//  Push new measured value into queue.
//------------------------------------------------------------------------------
static void PushMeasuredValueIntoQueue(uint16_t value)
{
    state.measurementQueue[state.indexInQueue] = value;

    if (state.valuesInQueue < MEASUREMENT_QUEUE_LENGTH)
        state.valuesInQueue++;

    state.indexInQueue++;
    if (state.indexInQueue >= MEASUREMENT_QUEUE_LENGTH)
        state.indexInQueue = 0;
}

//------------------------------------------------------------------------------
//  Calculate and returns measured mean value in queue.
//------------------------------------------------------------------------------
static int32_t CalculateMeasuredMeanValueInQueue(void)
{
    if (state.valuesInQueue == 0)
        return UNKNOWN_PRESSURE;

    // sample sum
    uint32_t sum = 0;
    for (uint8_t i = 0; i < state.valuesInQueue; i++)
        sum += state.measurementQueue[i];

    // mean sample
    uint16_t meanSample = (uint16_t)((((float)sum) / state.valuesInQueue) + 0.5f); // round

    // mean current
    float meanCurrent = ConvertADCSampleToCurrent(meanSample);

    // mean pressure
    return ConvertCurrentToPressure(meanCurrent);
}

//------------------------------------------------------------------------------
//  Clear all measured values.
//------------------------------------------------------------------------------
static void ClearAllMeasurements(void)
{
    state.measuredMeanValue = UNKNOWN_PRESSURE;
    state.valuesInQueue = state.indexInQueue = 0;
    state.adcData = UNKNOWN_PRESSURE;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtPress_Init(void)
{
    state.isOpen = FALSE;
    state.errorCode = ERRC_NONE;
    ClearAllMeasurements();
}

//------------------------------------------------------------------------------
//  Allocation of pressure measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtPress_Open(void)
{
    ExtPress_Init();

    state.isOpen = TRUE;

    ConfigureMeasureDeviceCommIface();
}

//------------------------------------------------------------------------------
//  Free pressure measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtPress_Close(void)
{
    ExtPress_Init();
}

//------------------------------------------------------------------------------
//  Pressure measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtPress_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtPress_Measure(void)
{
    if (!state.isOpen)
    {
        state.errorCode = ERRC_CLOSED;
        return FALSE;
    }

    // Get data sample
    state.adcData = ADS7822_GetSample();
    if (state.adcData < 0)
    {
        state.errorCode = ERRC_SAMPLING;
        return FALSE; // sample error
    }

    // save sample value
    PushMeasuredValueIntoQueue(state.adcData);

    // Calculate mean value
    state.measuredMeanValue = CalculateMeasuredMeanValueInQueue();

    state.errorCode = ERRC_NONE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Get the measured value in Pa (pascal) or UNKNOWN_PRESSURE if no
//  pressure value available.
//------------------------------------------------------------------------------
int32_t ExtPress_GetMeasuredValue(void)
{
    return state.measuredMeanValue;
}

//------------------------------------------------------------------------------
//  Call after current/pressure transform parameters changed.
//------------------------------------------------------------------------------
void ExtPress_OnCurrentToPressParametersChanged(void)
{
    ClearAllMeasurements();
}

//------------------------------------------------------------------------------
//  Get the raw adc output data or UNKNOWN_PRESSURE.
//------------------------------------------------------------------------------
int16_t ExtPress_GetAdcData(void)
{
    return state.adcData;
}

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//------------------------------------------------------------------------------
uint8_t ExtPress_GetValuesInQueue(void)
{
    return state.valuesInQueue;
}

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtPress_GetErrorText(void)
{
    return ConvertErrorCodeToText(state.errorCode);
}
