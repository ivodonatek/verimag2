
typedef struct DateTime
{
  float msecond;
  unsigned char second;   //enter the current time, date, month, and year
  unsigned char minute;
  unsigned char hour;
  unsigned char day;
  unsigned char month;
  unsigned int year;
}TDateTime;

void RTC_init(void);
void RTC_set(unsigned char hodina,unsigned char minuta, unsigned char sekunda, unsigned char den, unsigned char mesic, unsigned int rok);
void RTC_set_year(unsigned int year);
void RTC_set_month(unsigned char month);
void RTC_set_day(unsigned char day);
void RTC_set_hour(unsigned char hour);
void RTC_set_minute(unsigned char minute);
void RTC_set_second(unsigned char second);
unsigned long RTC_GetSysSeconds(void);
unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds);

//------------------------------------------------------------------------------
//  Hlavni uloha RTC, volat opakovane.
//------------------------------------------------------------------------------
void RTC_Task(BOOL screenTask);

//------------------------------------------------------------------------------
//  Pristup k datu a casu.
//------------------------------------------------------------------------------
const TDateTime* RTC_GetDateTime(void);

//------------------------------------------------------------------------------
// funkce vraci datum ve formatu BCD uint32_t 14.05.2014 = 20140514
//------------------------------------------------------------------------------
unsigned long RTC_GetDateBCD(void);

//------------------------------------------------------------------------------
// funkce vraci polozky datumu a casu ve formatu BCD
//------------------------------------------------------------------------------
unsigned int RTC_GetYearFromBCD(unsigned long date);
unsigned int RTC_GetMonthFromBCD(unsigned long date);
unsigned int RTC_GetDayFromBCD(unsigned long date);
unsigned int RTC_GetHourFromBCD(unsigned long time);
unsigned int RTC_GetMinuteFromBCD(unsigned long time);
unsigned int RTC_GetSecondFromBCD(unsigned long time);

extern volatile TDateTime DateTime;
extern volatile unsigned char flag_RTC;
