//******************************************************************************
//  Nastaveni a prepinani mereni pomoci 4052
//  (Dual 4-channel analog multiplexer/demultiplexer).
//*****************************************************************************/

#include "switch.h"
#include "LPC23xx.h"

// konfigurace portu
#define SWITCH_IODIR FIO2DIR
#define SWITCH_IOSET FIO2SET
#define SWITCH_IOCLR FIO2CLR

#define SWITCH1_EN_IOPIN (1<<8)
#define SWITCH1_A_IOPIN (1<<7)
#define SWITCH1_B_IOPIN (1<<6)

#define SWITCH2_EN_IOPIN (1<<5)
#define SWITCH2_A_IOPIN (1<<4)
#define SWITCH2_B_IOPIN (1<<3)

#define SWITCH_INIT  {SWITCH_IODIR |= SWITCH1_EN_IOPIN | SWITCH1_A_IOPIN | SWITCH1_B_IOPIN | SWITCH2_EN_IOPIN | SWITCH2_A_IOPIN | SWITCH2_B_IOPIN;}

#define SWITCH1_EN_H {SWITCH_IOSET |= SWITCH1_EN_IOPIN;}
#define SWITCH1_EN_L {SWITCH_IOCLR |= SWITCH1_EN_IOPIN;}

#define SWITCH1_A_H {SWITCH_IOSET |= SWITCH1_A_IOPIN;}
#define SWITCH1_A_L {SWITCH_IOCLR |= SWITCH1_A_IOPIN;}

#define SWITCH1_B_H {SWITCH_IOSET |= SWITCH1_B_IOPIN;}
#define SWITCH1_B_L {SWITCH_IOCLR |= SWITCH1_B_IOPIN;}

#define SWITCH2_EN_H {SWITCH_IOSET |= SWITCH2_EN_IOPIN;}
#define SWITCH2_EN_L {SWITCH_IOCLR |= SWITCH2_EN_IOPIN;}

#define SWITCH2_A_H {SWITCH_IOSET |= SWITCH2_A_IOPIN;}
#define SWITCH2_A_L {SWITCH_IOCLR |= SWITCH2_A_IOPIN;}

#define SWITCH2_B_H {SWITCH_IOSET |= SWITCH2_B_IOPIN;}
#define SWITCH2_B_L {SWITCH_IOCLR |= SWITCH2_B_IOPIN;}

//------------------------------------------------------------------------------
//  Nastaveni mereni.
//------------------------------------------------------------------------------
static void SetSwitch1(TSwitch1Setup setup)
{
    SWITCH1_EN_H;

    switch (setup)
    {
    case S1_Q0:
        SWITCH1_A_L;
        SWITCH1_B_L;
        SWITCH1_EN_L;
        break;
    case S1_Q1:
        SWITCH1_A_H;
        SWITCH1_B_L;
        SWITCH1_EN_L;
        break;
    case S1_Q2:
        SWITCH1_A_L;
        SWITCH1_B_H;
        SWITCH1_EN_L;
        break;
    case S1_Q3:
        SWITCH1_A_H;
        SWITCH1_B_H;
        SWITCH1_EN_L;
        break;
    }
}

//------------------------------------------------------------------------------
//  Prepinani mereni.
//------------------------------------------------------------------------------
static void SetSwitch2(TSwitch2Setup setup)
{
    SWITCH2_EN_H;

    switch (setup)
    {
    case S2_Q0:
        SWITCH2_A_L;
        SWITCH2_B_L;
        SWITCH2_EN_L;
        break;
    case S2_Q1:
        SWITCH2_A_H;
        SWITCH2_B_L;
        SWITCH2_EN_L;
        break;
    case S2_Q2:
        SWITCH2_A_L;
        SWITCH2_B_H;
        SWITCH2_EN_L;
        break;
    case S2_Q3:
        SWITCH2_A_H;
        SWITCH2_B_H;
        SWITCH2_EN_L;
        break;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Switch_Init(void)
{
    SWITCH_INIT;
    Switch_Disconnect();
}

//------------------------------------------------------------------------------
//  Nastaveni testu.
//------------------------------------------------------------------------------
void Switch_SetTest(TSwitchTest test)
{
    switch (test)
    {
    case SW_TEST1:
        SetSwitch1(S1_Q0);
        SetSwitch2(S2_Q2);
        break;
    case SW_TEST2:
        SetSwitch1(S1_Q1);
        SetSwitch2(S2_Q3);
        break;
    case SW_TEST3:
        SetSwitch1(S1_Q2);
        SetSwitch2(S2_Q2);
        break;
    case SW_TEST4:
        SetSwitch1(S1_Q3);
        SetSwitch2(S2_Q1);
        break;
    }
}

//------------------------------------------------------------------------------
//  Odpojeni mericich obvodu.
//------------------------------------------------------------------------------
void Switch_Disconnect(void)
{
    SWITCH1_EN_H;
    SWITCH2_EN_H;
}
