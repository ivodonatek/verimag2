//******************************************************************************
//  Komunikace se senzorem - VeriMag je v pozici mastera na sbernici.
//*****************************************************************************/

#include "sensor_master.h"
#include "LPC23xx.h"
#include "target.h"
#include "type.h"
#include "rtc.h"
#include "sensor.h"
#include "mbcrc.h"

#define BAUD_RATE           9600   //baud rate
#define RCV_BUFFER_LENGTH   80     //delka prijimaciho buferu
#define RCV_TIMEOUT         2      //timeout prijmu [s]

#define DATA_RECEIVE        IOCLR0|=(1<<4)
#define DATA_SEND           IOSET0|=(1<<4)

// Fw module state
static struct
{
    // stav komunikace
    TCommStatus commStatus;

    // prijimaci bufer
    unsigned char rcvBuffer[RCV_BUFFER_LENGTH];

    // pocet prijatych bytu v prijimacim buferu
    unsigned char rcvBufferByteCount;

    // prijimaci casovac
    unsigned long rcvTimer;

    // funkce, ktera je volana po prijmu odpovedi na dotaz
    TSensorMasterDataHandler sensorMasterDataHandler;

} state;

//------------------------------------------------------------------------------
//  Start prijimaciho casovace
//------------------------------------------------------------------------------
static void StartRcvTimer(void)
{
    state.rcvTimer = RTC_GetSysSeconds();
}

//------------------------------------------------------------------------------
//  Zjisteni prijimaciho timeoutu
//------------------------------------------------------------------------------
static unsigned char IsRcvTimeout(void)
{
    return RTC_IsSysSecondTimeout(state.rcvTimer, RCV_TIMEOUT);
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prijmu
//------------------------------------------------------------------------------
static void ProcessRcvTimeout(void)
{
    state.commStatus = CMSTA_NONE;
    if (state.sensorMasterDataHandler != NO_SENSOR_MASTER_DATA_HANDLER)
    {
        TCommData commData;
        commData.commResult = CRES_TMR;
        commData.buffer = (unsigned char *)0;
        commData.bufferSize = 0;

        state.sensorMasterDataHandler(&commData);
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
static void ProcessRcvData(void)
{
    if (state.rcvBufferByteCount < 2)
        return;

    switch (state.rcvBuffer[1])
    {
        case WRITE_MULTIPLE_REGISTER:
            if (state.rcvBufferByteCount >= 8)
            {
                state.commStatus = CMSTA_NONE;
                if (state.sensorMasterDataHandler != NO_SENSOR_MASTER_DATA_HANDLER)
                {
                    TCommData commData;
                    commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                    commData.buffer = (unsigned char *)0;
                    commData.bufferSize = 0;

                    state.sensorMasterDataHandler(&commData);
                }
            }
            break;

        case READ_HOLDING_REGISTER:
            if (state.rcvBufferByteCount >= 3)
            {
                if (state.rcvBufferByteCount == (state.rcvBuffer[2] + 5))
                {
                    state.commStatus = CMSTA_NONE;
                    if (state.sensorMasterDataHandler != NO_SENSOR_MASTER_DATA_HANDLER)
                    {
                        TCommData commData;
                        commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                        commData.buffer = &state.rcvBuffer[3];
                        commData.bufferSize = state.rcvBuffer[2];

                        state.sensorMasterDataHandler(&commData);
                    }
                }
            }
            break;

        default:
            break;
    }
}

//------------------------------------------------------------------------------
//  Vyzvednuti bytu z UART3
//------------------------------------------------------------------------------
static unsigned char ReadUartByte(void)
{
    return U3RBR;
}

//------------------------------------------------------------------------------
//  Obsluha preruseni od UART3
//------------------------------------------------------------------------------
static void OnRxUartHandler(void)
{
    unsigned char rxByte = ReadUartByte();

    if (state.commStatus == CMSTA_RX)
    {
        if (state.rcvBufferByteCount < RCV_BUFFER_LENGTH)
        {
            state.rcvBuffer[state.rcvBufferByteCount] = rxByte;
            state.rcvBufferByteCount++;
        }
    }

    VICVectAddr = 0;
}

//------------------------------------------------------------------------------
//  Cteni holding DWORDu sensoru.
//------------------------------------------------------------------------------
static void ReadHoldingDWord(unsigned int address, TSensorMasterDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(address >> 8);
    buff[3]=(unsigned char)(address);

    buff[4]=0x00;
    buff[5]=0x02;       //zadost musi byt delitelna dvema

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SensorMaster_SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Inicializace hw komunikacniho rozhrani.
//------------------------------------------------------------------------------
static void SensorCommHwInit(void)
{
    unsigned long Fdiv;
    PCONP |= (1<<25);      //set power on UART3
    PINSEL9 |= (1<<24) | (1<<25) | (1<<26) | (1<<27);   //nastaveni TxD3 a RxD3 na piny 4.28,4.29
    U3LCR = 0x83;   //8bits,no Parity,1 stop, !!! DLAB=0 !!!!

    Fdiv = ( Fpclk / 16 ) / BAUD_RATE;	//baud rate
    U3FCR = 0x07;   //FIFO Enable and restart tx and rx

    U3DLM = Fdiv / 256;
    U3DLL = Fdiv % 256;

    IODIR0 |= (1<<4);    //P0.4 jako vystupni pro ovladani smeru dat 485
    DATA_RECEIVE;

    // preruseni
    VICVectAddr29 = (unsigned long) OnRxUartHandler;   //set interrupt vector
    VICIntEnable |= (1<<29);            // UART3 enable
    U3LCR = 0x03;                       //DLAB=0
    U3IER = 1;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void SensorMaster_Init(void)
{
    state.commStatus = CMSTA_NONE;
    state.rcvBufferByteCount = 0;
    state.sensorMasterDataHandler = NO_SENSOR_MASTER_DATA_HANDLER;
    SensorCommHwInit();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void SensorMaster_Task(void)
{
    if (state.commStatus == CMSTA_RX)
    {
        if (IsRcvTimeout())
            ProcessRcvTimeout();
        else
            ProcessRcvData();
    }
}

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TCommStatus SensorMaster_GetCommStatus(void)
{
    return state.commStatus;
}

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* SensorMaster_GetRcvFrame(void)
{
    return state.rcvBuffer;
}

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int SensorMaster_GetRcvFrameByteCount(void)
{
    return state.rcvBufferByteCount;
}

//------------------------------------------------------------------------------
//  Odeslani dat na sbernici
//------------------------------------------------------------------------------
void SensorMaster_SendData(const unsigned char *dataBuffer, unsigned int dataCount, TSensorMasterDataHandler handler)
{
    state.commStatus = CMSTA_TX;

    DATA_SEND; // prepnuti smeru RS485

    while (dataCount != 0)
    {
        while (!(U3LSR & (1<<5))); // Wait until Transmitter Holding Register is not Empty
        U3THR = *dataBuffer;
        dataBuffer++;
        dataCount--;
    }

    while (!(U3LSR & (1<<6)));

    DATA_RECEIVE; // prepnuti smeru RS485

    state.sensorMasterDataHandler = handler;
    state.rcvBufferByteCount = 0;
    StartRcvTimer();
    state.commStatus = CMSTA_RX;
}

//------------------------------------------------------------------------------
//  Parsing typu unsigned long senzoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseUlong(const unsigned char *buffer)
{
    unsigned char pos=0;
    unsigned long help=0;

    help=(unsigned long)buffer[pos++]<<8;
    help+=(unsigned long)buffer[pos++];
    help+=(unsigned long)buffer[pos++]<<24;
    help+=(unsigned long)buffer[pos++]<<16;

    return help;
}

//------------------------------------------------------------------------------
//  Zapis hesel sensoru.
//------------------------------------------------------------------------------
void SensorMaster_WritePasswords(TSensorMasterDataHandler handler)
{
    unsigned char buff[21];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)( MODBUS_HESLA >> 8);
    buff[3]=(unsigned char)( MODBUS_HESLA);

    buff[4]=0x00;   //pocet registru 2B
    buff[5]=6;

    buff[6]=12;   //pocet bytu 4

    unsigned int heslo=1111;        //user

    buff[7]=(unsigned char)(heslo >> 8);
    buff[8]=(unsigned char)(heslo);
    buff[9]=(unsigned char)(heslo >> 24);
    buff[10]=(unsigned char)(heslo >> 16);

    heslo=5831;                     //service

    buff[11]=(unsigned char)(heslo >> 8);
    buff[12]=(unsigned char)(heslo);
    buff[13]=(unsigned char)(heslo >> 24);
    buff[14]=(unsigned char)(heslo >> 16);

    heslo=7367;

    buff[15]=(unsigned char)(heslo >> 8);
    buff[16]=(unsigned char)(heslo);
    buff[17]=(unsigned char)(heslo >> 24);
    buff[18]=(unsigned char)(heslo >> 16);

    unsigned int crcecko=usMBCRC16(buff,19);;   //kontrolni soucet
    buff[19]=(unsigned char)(crcecko);
    buff[20]=(unsigned char)(crcecko>>8);

    SensorMaster_SendData(buff, 21, handler);
}

//------------------------------------------------------------------------------
//  Cteni firmware sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadFirmware(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_FIRM_WARE, handler);
}

//------------------------------------------------------------------------------
//  Parsing firmware sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseFirmware(const unsigned char *buffer)
{
    unsigned long sensor_fw_version = 0; // fw verze senzoru
    sensor_fw_version=(unsigned long)buffer[1];
    sensor_fw_version+=(unsigned int)buffer[0]<<8;
    return sensor_fw_version;
}

//------------------------------------------------------------------------------
//  Cteni kalibracnich dat sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadCalibration(TSensorMasterDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MODBUS_KALIB_1 >> 8);
    buff[3]=(unsigned char)(MODBUS_KALIB_1);

    buff[4]=0x00;   //pocet 4 registry, 1 reg je zapsan jako 2-byty
    buff[5]=12;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SensorMaster_SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing kalibracnich dat sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ParseCalibration(const unsigned char *buffer, TSensorCalibration *sensorCalibration)
{
    unsigned char pos=0;
    unsigned long cdata, mdata;

    cdata=0;
    cdata=(unsigned long)buffer[pos++]<<8;
    cdata+=(unsigned long)buffer[pos++];
    cdata+=(unsigned long)buffer[pos++]<<24;
    cdata+=(unsigned long)buffer[pos++]<<16;

    mdata=0;
    mdata=(unsigned long)buffer[pos++]<<8;
    mdata+=(unsigned long)buffer[pos++];
    mdata+=(unsigned long)buffer[pos++]<<24;
    mdata+=(unsigned long)buffer[pos++]<<16;

    sensorCalibration->cp1 = cdata;
    sensorCalibration->mp1 = mdata;

    cdata=0;
    cdata=(unsigned long)buffer[pos++]<<8;
    cdata+=(unsigned long)buffer[pos++];
    cdata+=(unsigned long)buffer[pos++]<<24;
    cdata+=(unsigned long)buffer[pos++]<<16;

    mdata=0;
    mdata=(unsigned long)buffer[pos++]<<8;
    mdata+=(unsigned long)buffer[pos++];
    mdata+=(unsigned long)buffer[pos++]<<24;
    mdata+=(unsigned long)buffer[pos++]<<16;

    sensorCalibration->cp2 = cdata;
    sensorCalibration->mp2 = mdata;

    cdata=0;
    cdata=(unsigned long)buffer[pos++]<<8;
    cdata+=(unsigned long)buffer[pos++];
    cdata+=(unsigned long)buffer[pos++]<<24;
    cdata+=(unsigned long)buffer[pos++]<<16;

    mdata=0;
    mdata=(unsigned long)buffer[pos++]<<8;
    mdata+=(unsigned long)buffer[pos++];
    mdata+=(unsigned long)buffer[pos++]<<24;
    mdata+=(unsigned long)buffer[pos++]<<16;

    sensorCalibration->cp3 = cdata;
    sensorCalibration->mp3 = mdata;
}

//------------------------------------------------------------------------------
//  Cteni diameteru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadDiameter(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_DIAMETER, handler);
}

//------------------------------------------------------------------------------
//  Parsing diameteru sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseDiameter(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni cisla sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadUnitNo(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_UNIT_NO, handler);
}

//------------------------------------------------------------------------------
//  Parsing cisla sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseUnitNo(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni totalizeru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadTotals(TSensorMasterDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MODBUS_TOTAL2 >> 8);
    buff[3]=(unsigned char)(MODBUS_TOTAL2);

    buff[4]=0x00;
    buff[5]=16;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SensorMaster_SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing totalizeru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ParseTotals(const unsigned char *buffer, TSensorTotals *sensorTotals)
{
    unsigned long pomocna;
    unsigned char pos=0;

    pomocna=0;      //total dig
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->total=pomocna;
    sensorTotals->total*=1000.0;

    pomocna=0;      //total dec
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->total+=(pomocna);

    pomocna=0;      //total + dig
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->flow_pos=pomocna;
    sensorTotals->flow_pos*=1000.0;

    pomocna=0;      //total + deg
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->flow_pos+=(pomocna);

    pomocna=0;      //total - dig
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->flow_neg=pomocna;
    sensorTotals->flow_neg*=1000.0;

    pomocna=0;      //total - deg
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->flow_neg+=(pomocna);

    pomocna=0;      //aux dig
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->aux=pomocna;
    sensorTotals->aux*=1000.0;

    pomocna=0;      //aux deg
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    sensorTotals->aux+=(pomocna);
}

//------------------------------------------------------------------------------
//  Cteni rozsahu sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadFlowRange(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_FLOW_RANGE, handler);
}

//------------------------------------------------------------------------------
//  Parsing rozsahu sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseFlowRange(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni chyboveho kodu sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadErrorCode(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_ERROR_CODE, handler);
}

//------------------------------------------------------------------------------
//  Parsing chyboveho kodu sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseErrorCode(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni chybovych minut sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadErrorMin(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_ERROR_MIN, handler);
}

//------------------------------------------------------------------------------
//  Parsing chybovych minut sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseErrorMin(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni ok minut sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadOkMin(TSensorMasterDataHandler handler)
{
    ReadHoldingDWord(MODBUS_OK_MIN, handler);
}

//------------------------------------------------------------------------------
//  Parsing ok minut sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseOkMin(const unsigned char *buffer)
{
    return SensorMaster_ParseUlong(buffer);
}
