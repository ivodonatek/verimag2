//******************************************************************************
//  Komunikace se senzorem - VeriMag je v pozici mastera na sbernici.
//*****************************************************************************/

#ifndef SENSOR_MASTER_H_INCLUDED
#define SENSOR_MASTER_H_INCLUDED

// Vysledek komunikace
typedef enum
{
    CRES_OK, // bez chyb
    CRES_TMR, // timeout
    CRES_ERR // chyba

} TCommResult;

// stav komunikace
typedef enum
{
    CMSTA_NONE, // nic se nedeje
    CMSTA_TX, // vysilani
    CMSTA_RX // prijem

} TCommStatus;

// Data komunikace
typedef struct
{
    // vysledek komunikace
    TCommResult commResult;

    // bufer
    unsigned char *buffer;

    // pocet bytu v buferu
    unsigned int bufferSize;

} TCommData;

// Kalibracni data senzoru
typedef struct
{
    // calibration points
    unsigned long cp1, cp2, cp3;

    // measured points
    unsigned long mp1, mp2, mp3;

} TSensorCalibration;

// Totalizery senzoru
typedef struct
{
    long long total;
    long long aux;
    long long flow_pos;
    long long flow_neg;

} TSensorTotals;

// Typ - prototyp funkce, ktera je volana po prijmu odpovedi na dotaz.
typedef void (*TSensorMasterDataHandler) (const TCommData *commData);

// zadna funkce po prijmu odpovedi
#define NO_SENSOR_MASTER_DATA_HANDLER   ((TSensorMasterDataHandler)0)

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void SensorMaster_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void SensorMaster_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TCommStatus SensorMaster_GetCommStatus(void);

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* SensorMaster_GetRcvFrame(void);

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int SensorMaster_GetRcvFrameByteCount(void);

//------------------------------------------------------------------------------
//  Odeslani dat na sbernici
//------------------------------------------------------------------------------
void SensorMaster_SendData(const unsigned char *dataBuffer, unsigned int dataCount, TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing typu unsigned long senzoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseUlong(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis hesel sensoru.
//------------------------------------------------------------------------------
void SensorMaster_WritePasswords(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Cteni firmware sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadFirmware(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing firmware sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseFirmware(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni kalibracnich dat sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadCalibration(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing kalibracnich dat sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ParseCalibration(const unsigned char *buffer, TSensorCalibration *sensorCalibration);

//------------------------------------------------------------------------------
//  Cteni diameteru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadDiameter(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing diameteru sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseDiameter(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni cisla sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadUnitNo(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing cisla sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseUnitNo(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni totalizeru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadTotals(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing totalizeru sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ParseTotals(const unsigned char *buffer, TSensorTotals *sensorTotals);

//------------------------------------------------------------------------------
//  Cteni rozsahu sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadFlowRange(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing rozsahu sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseFlowRange(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni chyboveho kodu sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadErrorCode(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing chyboveho kodu sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseErrorCode(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni chybovych minut sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadErrorMin(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing chybovych minut sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseErrorMin(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni ok minut sensoru.
//------------------------------------------------------------------------------
void SensorMaster_ReadOkMin(TSensorMasterDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing ok minut sensoru.
//------------------------------------------------------------------------------
unsigned long SensorMaster_ParseOkMin(const unsigned char *buffer);

#endif // SENSOR_MASTER_H_INCLUDED
