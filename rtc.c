#include "LPC23xx.h"
#include "type.h"
#include "rtc.h"
#include "maine.h"
#include "display.h"
#include "screen.h"
#include "rprintf.h"
#include "power_man.h"

// Fw module state
typedef struct RTCState
{
    unsigned long secondTick; // pocitani sekund od startu systemu
    BOOL nextSecond; // ubehla dalsi sekunda

} TRTCState;

static volatile TRTCState state;

static void RTC_read(void)
{
  DateTime.second=RTC_SEC;
  DateTime.minute=RTC_MIN;
  DateTime.hour=RTC_HOUR;
  DateTime.day=RTC_DOM;
  DateTime.month=RTC_MONTH;
  DateTime.year=RTC_YEAR;
}

static void RTC_interrupt(void)
{
  RTC_ILR  |= (1<<0);

  state.nextSecond = TRUE;
  state.secondTick++;

  VICVectAddr=0;
}

void RTC_init()
{
  // stav
  state.secondTick = 0;
  state.nextSecond = FALSE;

  ///na zacatku se musi vetsina povypinat a nasledne se muze povolit
  PCONP |= (1<<9);                //PCRTC power ON
  RTC_CCR=0;                      //vypnuti RTc
  RTC_CISS =  0;
  RTC_CIIR=0;
  RTC_ILR=0;
  RTC_AMR=0;
  RTC_PREFRAC=0;
  RTC_PREINT=0;

  RTC_CCR |= (1<<4) | (1<<0); //zapnuti extereniho 32kHz krzstalu

  ///preruseni
  RTC_ILR  |= (1<<0);                 //generovani preruseni od casu
  RTC_CIIR |= (1<<0);     //inkrementace sekundy s kazdym prerusenim
  VICVectAddr13=(unsigned long)RTC_interrupt;
  VICIntEnable |= (1<<13);
}

void RTC_set(unsigned char hodina,unsigned char minuta, unsigned char sekunda, unsigned char den, unsigned char mesic, unsigned int rok)
{
  RTC_SEC=sekunda;
  RTC_MIN=minuta;
  RTC_HOUR=hodina;
  RTC_DOM=den;
  RTC_MONTH=mesic;
  RTC_YEAR=rok;

  DateTime.second=sekunda;
  DateTime.minute=minuta;
  DateTime.hour=hodina;
  DateTime.day=den;
  DateTime.month=mesic;
  DateTime.year=rok;
}

void RTC_set_year(unsigned int year)
{
  RTC_YEAR      =year;
  DateTime.year =year;
}

void RTC_set_month(unsigned char month)
{
  RTC_MONTH      =month;
  DateTime.month =month;
}

void RTC_set_day(unsigned char day)
{
  RTC_DOM       =day;
  DateTime.day  =day;
}

void RTC_set_hour(unsigned char hour)
{
  RTC_HOUR      =hour;
  DateTime.hour =hour;
}

void RTC_set_minute(unsigned char minute)
{
  RTC_MIN         =minute;
  DateTime.minute =minute;
}

void RTC_set_second(unsigned char second)
{
  RTC_SEC         =second;
  DateTime.second =second;
}

unsigned long RTC_GetSysSeconds(void)
{
    volatile unsigned long ss;
    ss = state.secondTick;
    return ss;
}

unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds)
{
    if ((RTC_GetSysSeconds() - userSysSeconds) >= delaySeconds)
        return 1;
    else
        return 0;
}

//------------------------------------------------------------------------------
//  Hlavni uloha RTC, volat opakovane.
//------------------------------------------------------------------------------
void RTC_Task(BOOL screenTask)
{
    if (state.nextSecond)
    {
        state.nextSecond = FALSE;
        RTC_read();
        Screen_Task();
    }
}

//------------------------------------------------------------------------------
//  Pristup k datu a casu.
//------------------------------------------------------------------------------
const TDateTime* RTC_GetDateTime(void)
{
    return &DateTime;
}

//------------------------------------------------------------------------------
// funkce vraci datum ve formatu BCD uint32_t 14.05.2014 = 20140514
//------------------------------------------------------------------------------
unsigned long RTC_GetDateBCD(void)
{
    unsigned long result;

    result = (DateTime.year)*10000;
    result += (DateTime.month)*100;
    result +=  DateTime.day;

    return result;
}

//------------------------------------------------------------------------------
// funkce vraci polozky datumu ve formatu BCD
//------------------------------------------------------------------------------
unsigned int RTC_GetYearFromBCD(unsigned long date)
{
    return date / 10000;
}

unsigned int RTC_GetMonthFromBCD(unsigned long date)
{
    unsigned long year = RTC_GetYearFromBCD(date);
    unsigned long monthDay = date - (year * 10000);
    return monthDay / 100;
}

unsigned int RTC_GetDayFromBCD(unsigned long date)
{
    unsigned long year = RTC_GetYearFromBCD(date);
    unsigned long monthDay = date - (year * 10000);
    return monthDay % 100;
}

unsigned int RTC_GetHourFromBCD(unsigned long time)
{
    return time / 100;
}

unsigned int RTC_GetMinuteFromBCD(unsigned long time)
{
    return time % 100;
}

unsigned int RTC_GetSecondFromBCD(unsigned long time)
{
    return 0;
}

