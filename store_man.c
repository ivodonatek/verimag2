//******************************************************************************
//  Management uloziste dat.
//*****************************************************************************/

#include "type.h"
#include "test.h"
#include "store_man.h"
#include "sbl_config.h"
#include "sbl_iap.h"
#include "menu_tables.h"
#include "maine.h"
#include "rtc.h"

#define TEST_BUFFER_BYTE_COUNT  120
#define MAX_WRITE_FLASH_COUNT   1

// Fw module state
static struct
{
    // Ulozi nastaveni
    BOOL saveSetting;

} state;

//------------------------------------------------------------------------------
//  Zapise factory data do flash.
//------------------------------------------------------------------------------
static void WriteFactoryFlash(void)
{
  state.saveSetting = FALSE;

  erase_user_flash(FLASH_FACTORY_START, FLASH_FACTORY_END, 0);

  int i = 0;
  static char buffer[FLASH_BUF_SIZE];

  buffer[i++]=*(unsigned char *)UNIT_NO;
  buffer[i++]=*(unsigned char *)(UNIT_NO+1);
  buffer[i++]=*(unsigned char *)(UNIT_NO+2);
  buffer[i++]=*(unsigned char *)(UNIT_NO+3);

  buffer[i++]=*(unsigned char *)FIRMWARE;
  buffer[i++]=*(unsigned char *)(FIRMWARE+1);
  buffer[i++]=*(unsigned char *)(FIRMWARE+2);
  buffer[i++]=*(unsigned char *)(FIRMWARE+3);

  buffer[i++]=*(unsigned char *)CONTRAST;
  buffer[i++]=*(unsigned char *)(CONTRAST+1);
  buffer[i++]=*(unsigned char *)(CONTRAST+2);
  buffer[i++]=*(unsigned char *)(CONTRAST+3);
  //backlight
  buffer[i++]=*(unsigned char *)(BACKLIGHT);
  buffer[i++]=*(unsigned char *)(BACKLIGHT+1);
  buffer[i++]=*(unsigned char *)(BACKLIGHT+2);
  buffer[i++]=*(unsigned char *)(BACKLIGHT+3);
  // date
  buffer[i++]=*(unsigned char *)(DATE);
  buffer[i++]=*(unsigned char *)(DATE+1);
  buffer[i++]=*(unsigned char *)(DATE+2);
  buffer[i++]=*(unsigned char *)(DATE+3);
  // time
  buffer[i++]=*(unsigned char *)(TIME);
  buffer[i++]=*(unsigned char *)(TIME+1);
  buffer[i++]=*(unsigned char *)(TIME+2);
  buffer[i++]=*(unsigned char *)(TIME+3);
  // service mode
  buffer[i++]=*(unsigned char *)(SERVICE_MODE);
  buffer[i++]=*(unsigned char *)(SERVICE_MODE+1);
  buffer[i++]=*(unsigned char *)(SERVICE_MODE+2);
  buffer[i++]=*(unsigned char *)(SERVICE_MODE+3);
  // calibration date
  buffer[i++]=*(unsigned char *)(CALIB_DATE);
  buffer[i++]=*(unsigned char *)(CALIB_DATE+1);
  buffer[i++]=*(unsigned char *)(CALIB_DATE+2);
  buffer[i++]=*(unsigned char *)(CALIB_DATE+3);
  // test counter
  buffer[i++]=*(unsigned char *)(TEST_COUNTER);
  buffer[i++]=*(unsigned char *)(TEST_COUNTER+1);
  buffer[i++]=*(unsigned char *)(TEST_COUNTER+2);
  buffer[i++]=*(unsigned char *)(TEST_COUNTER+3);
  // test size
  buffer[i++]=*(unsigned char *)(TEST_SIZE);
  buffer[i++]=*(unsigned char *)(TEST_SIZE+1);
  buffer[i++]=*(unsigned char *)(TEST_SIZE+2);
  buffer[i++]=*(unsigned char *)(TEST_SIZE+3);
  // test id
  buffer[i++]=*(unsigned char *)(TEST_ID);
  buffer[i++]=*(unsigned char *)(TEST_ID+1);
  buffer[i++]=*(unsigned char *)(TEST_ID+2);
  buffer[i++]=*(unsigned char *)(TEST_ID+3);
  // test erase
  buffer[i++]=*(unsigned char *)(TEST_ERASE);
  buffer[i++]=*(unsigned char *)(TEST_ERASE+1);
  buffer[i++]=*(unsigned char *)(TEST_ERASE+2);
  buffer[i++]=*(unsigned char *)(TEST_ERASE+3);
  // vyplneni reserved bytu
  for(int j=i; j<80;j++)
  {
    buffer[j] = 0;
  }

  // doplneni bufferu
  for(int j = 80; j < FLASH_BUF_SIZE; j++)
  {
    buffer[j] = 0xff;
  }

  for (int i = 0; i < MAX_WRITE_FLASH_COUNT; i++)
  {
      if (write_flash((unsigned *)FLASH_FACTORY_START,buffer,(unsigned int)sizeof(buffer)))
        continue;
      else
        break;
  }
}

//------------------------------------------------------------------------------
//  Vymaze data testu ve flash.
//------------------------------------------------------------------------------
static void EraseTestsFlash(void)
{
  erase_user_flash(FLASH_TESTS_START, FLASH_TESTS_END, 1);
}

//------------------------------------------------------------------------------
//  Konverze float do unsigned long.
//------------------------------------------------------------------------------
static unsigned long FloatToUlong(float value)
{
    unsigned long result;
    unsigned char i;
    char *pS, *pD;

    pS = (char *)&value;
    pD = (char *)&result;

    for (i = 0; i < 4; i++)
    {
        *pD = *pS;
        pS++;
        pD++;
    }

    return result;
}

//------------------------------------------------------------------------------
//  Inicializace promennych, pokud je neco ulozeno ve FLASH
//------------------------------------------------------------------------------
static void FirstRun(void)
{
  if (RAM_Read_Data_Long(FLASH_FACTORY_START) != 0xffffffff)
  {
    // nactu z FLASH do RAM
    int ram = UNIT_NO;
    // 11*4 viz menu_tables.h definice pameti v ram jsou za sebou
    for(int i = FLASH_FACTORY_START; i <= FLASH_FACTORY_START+11*4; i+=4)
    {
      RAM_Write_Data_Long(RAM_Read_Data_Long(i),ram);
      ram += 4;
    }
  }
  else
  {
    RAM_Write_Data_Long(201701,  UNIT_NO);
    RAM_Write_Data_Long(FW_VERSION, FIRMWARE);
    RAM_Write_Data_Long(60,      CONTRAST);
    RAM_Write_Data_Long(0,       BACKLIGHT);
    RAM_Write_Data_Long(20171212,DATE);
    RAM_Write_Data_Long(1200,    TIME);
    RAM_Write_Data_Long(20171212,CALIB_DATE);
    RAM_Write_Data_Long(0,       SERVICE_MODE);
    RAM_Write_Data_Long(0,       TEST_COUNTER);
    RAM_Write_Data_Long(30,      TEST_SIZE);
    RAM_Write_Data_Long(1,       TEST_ID);
    RAM_Write_Data_Long(0,       TEST_ERASE);

    WriteFactoryFlash();
  }

  unsigned long date = RAM_Read_Data_Long(DATE);
  unsigned long time = RAM_Read_Data_Long(TIME);
  RTC_set(RTC_GetHourFromBCD(time), RTC_GetMinuteFromBCD(time), RTC_GetSecondFromBCD(time), RTC_GetDayFromBCD(date), RTC_GetMonthFromBCD(date), RTC_GetYearFromBCD(date));
}

//------------------------------------------------------------------------------
//  Naplni bufer (TEST_BUFFER_BYTE_COUNT byty) daty testu.
//------------------------------------------------------------------------------
static void FillBufferWithTest(char *buffer, const TMainTestData *test)
{
    unsigned int i = 0;

    /// 1. test id
    buffer[i++]=test->id;
    buffer[i++]=test->id>>8;
    buffer[i++]=test->id>>16;
    buffer[i++]=test->id>>24;

    /// 2. test date
    buffer[i++]=test->date;
    buffer[i++]=test->date>>8;
    buffer[i++]=test->date>>16;
    buffer[i++]=test->date>>24;

    /// 3. UNIT NO
    buffer[i++]=test->magx2UnitNo;
    buffer[i++]=test->magx2UnitNo>>8;
    buffer[i++]=test->magx2UnitNo>>16;
    buffer[i++]=test->magx2UnitNo>>24;

    /// 4. FIRMWARE + DIAMETR
    buffer[i++]=test->magx2Firmware;
    buffer[i++]=test->magx2Firmware>>8;
    buffer[i++]=test->diameter;
    buffer[i++]=test->diameter>>8;

    /// 5. testResults + sensorFirmware
    buffer[i++]=test->testResult;
    buffer[i++]=test->errorCodeTestResult;
    buffer[i++]=test->sensorFirmware;
    buffer[i++]=test->sensorFirmware>>8;

    /// 6. sensorUnitNo
    buffer[i++]=test->sensorUnitNo;
    buffer[i++]=test->sensorUnitNo>>8;
    buffer[i++]=test->sensorUnitNo>>16;
    buffer[i++]=test->sensorUnitNo>>24;

    /// 7. ERROR_MIN
    buffer[i++]=test->errorMin;
    buffer[i++]=test->errorMin>>8;
    buffer[i++]=test->errorMin>>16;
    buffer[i++]=test->errorMin>>24;

    /// 8. OK_MIN
    buffer[i++]=test->okMin;
    buffer[i++]=test->okMin>>8;
    buffer[i++]=test->okMin>>16;
    buffer[i++]=test->okMin>>24;

    /// 9. errorCode
    buffer[i++]=test->errorCode;
    buffer[i++]=test->errorCode>>8;
    buffer[i++]=test->errorCode>>16;
    buffer[i++]=test->errorCode>>24;

    /// 10. flowRange
    buffer[i++]=test->flowRange;
    buffer[i++]=test->flowRange>>8;
    buffer[i++]=0;//rezerva
    buffer[i++]=0;//rezerva

    /// 11. CALIBRATION POINT 1
    buffer[i++]=test->calibrationPoint1;
    buffer[i++]=test->calibrationPoint1>>8;
    buffer[i++]=test->calibrationPoint1>>16;
    buffer[i++]=test->calibrationPoint1>>24;

    /// 12. CALIBRATION POINT 2
    buffer[i++]=test->calibrationPoint2;
    buffer[i++]=test->calibrationPoint2>>8;
    buffer[i++]=test->calibrationPoint2>>16;
    buffer[i++]=test->calibrationPoint2>>24;

    /// 13. CALIBRATION POINT 3
    buffer[i++]=test->calibrationPoint3;
    buffer[i++]=test->calibrationPoint3>>8;
    buffer[i++]=test->calibrationPoint3>>16;
    buffer[i++]=test->calibrationPoint3>>24;

    /// 14. CALIBRATION DATA 1
    buffer[i++]=test->calibrationData1;
    buffer[i++]=test->calibrationData1>>8;
    buffer[i++]=test->calibrationData1>>16;
    buffer[i++]=test->calibrationData1>>24;

    /// 15. CALIBRATION DATA 2
    buffer[i++]=test->calibrationData2;
    buffer[i++]=test->calibrationData2>>8;
    buffer[i++]=test->calibrationData2>>16;
    buffer[i++]=test->calibrationData2>>24;

    /// 16. CALIBRATION DATA 3
    buffer[i++]=test->calibrationData3;
    buffer[i++]=test->calibrationData3>>8;
    buffer[i++]=test->calibrationData3>>16;
    buffer[i++]=test->calibrationData3>>24;

    /// 17. TOTAL
    buffer[i++]=FloatToUlong(test->total);
    buffer[i++]=FloatToUlong(test->total)>>8;
    buffer[i++]=FloatToUlong(test->total)>>16;
    buffer[i++]=FloatToUlong(test->total)>>24;

    /// 18. TOTAL+
    buffer[i++]=FloatToUlong(test->totalPlus);
    buffer[i++]=FloatToUlong(test->totalPlus)>>8;
    buffer[i++]=FloatToUlong(test->totalPlus)>>16;
    buffer[i++]=FloatToUlong(test->totalPlus)>>24;

    /// 19. TOTAL-
    buffer[i++]=FloatToUlong(test->totalMinus);
    buffer[i++]=FloatToUlong(test->totalMinus)>>8;
    buffer[i++]=FloatToUlong(test->totalMinus)>>16;
    buffer[i++]=FloatToUlong(test->totalMinus)>>24;

    /// 20. AUX
    buffer[i++]=FloatToUlong(test->aux);
    buffer[i++]=FloatToUlong(test->aux)>>8;
    buffer[i++]=FloatToUlong(test->aux)>>16;
    buffer[i++]=FloatToUlong(test->aux)>>24;

    /// 21. testFlows
    buffer[i++]=test->testFlows[0];
    buffer[i++]=test->testFlows[0]>>8;
    buffer[i++]=test->testFlows[1];
    buffer[i++]=test->testFlows[1]>>8;

    /// 22. testFlows + frequency
    buffer[i++]=test->testFlows[2];
    buffer[i++]=test->testFlows[2]>>8;
    buffer[i++]=test->frequency[0];
    buffer[i++]=test->frequency[0]>>8;

    /// 23. frequency
    buffer[i++]=test->frequency[1];
    buffer[i++]=test->frequency[1]>>8;
    buffer[i++]=test->frequency[2];
    buffer[i++]=test->frequency[2]>>8;

    /// 24. iOut
    buffer[i++]=test->iOut[0];
    buffer[i++]=test->iOut[0]>>8;
    buffer[i++]=test->iOut[1];
    buffer[i++]=test->iOut[1]>>8;

    /// 25. iOut + test results
    buffer[i++]=test->iOut[2];
    buffer[i++]=test->iOut[2]>>8;
    buffer[i++]=test->frequencyTestResult;
    buffer[i++]=test->iOutTestResult;

    // reserved

    /// 26. reserved 1
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;

    /// 27. reserved 2
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;

    /// 28. reserved 3
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;

    /// 29. reserved 4
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;

    /// 30. reserved 5
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
    buffer[i++]=0;
}

//------------------------------------------------------------------------------
//  Naplni bufer daty z flash.
//------------------------------------------------------------------------------
static void FillBufferWithFlash(char *buffer, unsigned long flashAddress, unsigned int charCount)
{
    for (unsigned int i = 0; i < charCount; i++)
    {
        buffer[i] = 0xFF;// *((unsigned char *)(flashAddress + i));
    }
}

//------------------------------------------------------------------------------
//  Porovna bufer dat s daty ve flash. Pokud se rovnaji, vraci TRUE.
//------------------------------------------------------------------------------
static BOOL CompareBufferWithFlash(const char *buffer, unsigned long flashAddress, unsigned int charCount)
{
    for (unsigned int i = 0; i < charCount; i++)
    {
        if (buffer[i] != *((unsigned char *)(flashAddress + i)))
            return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void StoreMan_Init(void)
{
    state.saveSetting = FALSE;
    FirstRun();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne.
//------------------------------------------------------------------------------
void StoreMan_Task(void)
{
    if (RAM_Read_Data_Long(TEST_ERASE))
    {
        EraseTestsFlash();

        RAM_Write_Data_Long(0,TEST_COUNTER);
        RAM_Write_Data_Long(0,TEST_ERASE);

        state.saveSetting = TRUE;
    }

    if (state.saveSetting)
    {
        WriteFactoryFlash();
    }
}

//------------------------------------------------------------------------------
//  Ulozi test.
//  Vraci:
//  0 ... OK
//  1 ... prepare_sector error
//  2 ... write_data error
//  3 ... compare_data error
//  4 ... TestMemoryFull
//------------------------------------------------------------------------------
unsigned StoreMan_SaveTest(const TMainTestData *test)
{
    unsigned result = 0;

    if (!StoreMan_IsTestMemoryFull())
    {
        static char buffer[TEST_BUFFER_BYTE_COUNT];
        FillBufferWithTest(buffer, test);
        unsigned long bufferCounter = RAM_Read_Data_Long(TEST_COUNTER);

        static char flashBuffer[FLASH_BUF_SIZE];
        unsigned long flashBufferCounter = (bufferCounter*TEST_BUFFER_BYTE_COUNT)/FLASH_BUF_SIZE;

        FillBufferWithFlash(flashBuffer, FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE, FLASH_BUF_SIZE);

        unsigned int freeBytes = ((flashBufferCounter+1)*FLASH_BUF_SIZE) - (bufferCounter*TEST_BUFFER_BYTE_COUNT);
        unsigned int flashBufferIdx = FLASH_BUF_SIZE - freeBytes;

        unsigned int bufCount = TEST_BUFFER_BYTE_COUNT;
        if (freeBytes < TEST_BUFFER_BYTE_COUNT)
            bufCount = freeBytes;

        for (int i = 0; i < bufCount; i++)
        {
            flashBuffer[flashBufferIdx + i] = buffer[i];
        }

        for (int i = 0; i < MAX_WRITE_FLASH_COUNT; i++)
        {
            result = write_flash((unsigned *)(FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE),flashBuffer,FLASH_BUF_SIZE);
            if (result == 0)
                break;
        }

        if (result == 0)
        {
            if (!CompareBufferWithFlash(&flashBuffer[flashBufferIdx], FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE+flashBufferIdx, bufCount))
                result = 3;
        }

        if (freeBytes < TEST_BUFFER_BYTE_COUNT)
        {
            flashBufferCounter++;
            FillBufferWithFlash(flashBuffer, FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE, FLASH_BUF_SIZE);

            bufCount = TEST_BUFFER_BYTE_COUNT - freeBytes;
            for (int i = 0; i < bufCount; i++)
            {
                flashBuffer[i] = buffer[freeBytes + i];
            }

            unsigned result2 = 0;
            for (int i = 0; i < MAX_WRITE_FLASH_COUNT; i++)
            {
                result2 = write_flash((unsigned *)(FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE),flashBuffer,FLASH_BUF_SIZE);
                if (result2 == 0)
                    break;
            }

            if (result == 0)
                result = result2;

            if (result == 0)
            {
                if (!CompareBufferWithFlash(flashBuffer, FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE, bufCount))
                    result = 3;
            }
        }

        /*if (result==3)
        {
            int compIndex = -1;
            int bufCell = -1;
            int flashCell = -1;
            unsigned long flashAddress = FLASH_TESTS_START+flashBufferCounter*FLASH_BUF_SIZE;
            for (int i = 0; i < FLASH_BUF_SIZE; i++)
            {
                if (flashBuffer[i] != *((unsigned char *)(flashAddress + i)))
                {
                    compIndex = i;
                    bufCell = flashBuffer[i];
                    flashCell = *((unsigned char *)(flashAddress + i));
                    break;
                }
            }

            Screen_RefreshEnable(FALSE);
            Screen_ShowFlashErrors(compIndex, bufCell, flashCell, bufferCounter, flashBufferCounter);
        }*/

        RAM_Write_Data_Long(bufferCounter + 1, TEST_COUNTER);
    }
    else
        result = 4;

    RAM_Write_Data_Long((RAM_Read_Data_Long(TEST_ID)) + 1, TEST_ID);

    WriteFactoryFlash();

    return result;
}

//------------------------------------------------------------------------------
//  Ulozi nastaveni.
//------------------------------------------------------------------------------
void StoreMan_SaveSetting(void)
{
    state.saveSetting = TRUE;
}

//------------------------------------------------------------------------------
//  Vrati stav zaplneni uloziste testu.
//------------------------------------------------------------------------------
BOOL StoreMan_IsTestMemoryFull(void)
{
    if ((RAM_Read_Data_Long(TEST_COUNTER)) > 270)
        return TRUE;
    else
        return FALSE;
}
