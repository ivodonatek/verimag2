//******************************************************************************
//  Testy.
//*****************************************************************************/

#include "type.h"
#include "LPC23xx.h"
#include "target.h"
#include "maine.h"
#include "sensor_master.h"
#include "test.h"
#include "task_man.h"
#include "switch.h"
#include "step_up.h"
#include "ad.h"
#include "rtc.h"
#include "ui.h"
#include "ui_menu.h"
#include "buttons.h"
#include "modbus.h"
#include "sensor_slave.h"
#include "magx2_comm.h"
#include "store_man.h"
#include "extmeas/inductance/extind.h"
#include "extmeas/iout/extiout.h"
#include "devices/ADP5063/ADP5063.h"

// min. podpora firmware
#define MIN_MAGX2_FIRMWARE      2139
#define MIN_SENSOR_FIRMWARE     3003

// pocet vzorku mereni v AD testech
#define AD_TEST_SAMPLE_COUNT   1000

// max pocet mericich cyklu v testu indukcnosti
#define MAX_INDUCTANCE_MEASURE_COUNT   1000

// mereni v testu buzeni
#define EXCITE_IN_TEST_ALL_SAMPLE_COUNT    100000
#define EXCITE_IN_TEST_L_SAMPLE_COUNT      1000
#define EXCITE_IN_TEST_L1_MAX          400
#define EXCITE_IN_TEST_L3_MIN          600

// mereni v testu frekvence
#define FREQUENCY_TEST_TIME_COUNT   100
#define FREQUENCY_TEST_TIMEOUT_SEC  5

// mereni v testu Iout
#define IOUT_TEST_TIMEOUT_SEC  5

// max pocet mericich cyklu v testu Iout
#define MAX_IOUT_MEASURE_COUNT   1000

// max pocet timeoutu cteni hodnot
#define MAX_READ_VALUE_TIMEOUT_COUNT    5

// max pocet timeoutu zapisu hodnot
#define MAX_WRITE_VALUE_TIMEOUT_COUNT    MAX_READ_VALUE_TIMEOUT_COUNT

// prodleva mezi testy
#define DELAY_BETWEEN_TESTS_SEC  2

// prodleva mezi nastavenimi
#define DELAY_BETWEEN_SETTINGS_SEC  2

//------------------------------------------------------------------------------
//  Parametry testu.
//------------------------------------------------------------------------------

// stav demo
#define TEST_PARAM_DEMO    0

// stav mereni
#define TEST_PARAM_MEASUREMENT    1

// stav potlaceni nizkeho prutoku
#define TEST_PARAM_LOW_FLOW_CUTOFF    5

// stav buzeni
#define TEST_PARAM_EXCITATION     1

// parametry proudove smycky
#define TEST_PARAM_CL_CURRENT_SET       5
#define TEST_PARAM_CL_FLOW_MIN          0
#define TEST_PARAM_CL_FLOW_MAX          10000
#define TEST_PARAM_CL_CURRENT_MIN       4
#define TEST_PARAM_CL_CURRENT_MAX       20

// parametry frekvencniho vystupu
#define TEST_PARAM_FO_FREQUENCY_SET     6
#define TEST_PARAM_FO_FLOW_MIN          0
#define TEST_PARAM_FO_FLOW_MAX          10000
#define TEST_PARAM_FO_FREQUENCY_MIN     0
#define TEST_PARAM_FO_FREQUENCY_MAX     1000
#define TEST_PARAM_FO_DUTY_CYCLE        50

// testovaci prutoky
const unsigned long TEST_FLOWS[TEST_FLOW_COUNT] =
{
    1000, 5000, 9000
};

// ocekavane proudy [mA] na testovaci prutoky
const float EXPECTED_CURRENTS[TEST_FLOW_COUNT] =
{
    5.6, 12.0, 18.4
};

// ocekavane frekvence [Hz] na testovaci prutoky
const float EXPECTED_FREQUENCIES[TEST_FLOW_COUNT] =
{
    100, 500, 900
};

// povolene odchylky uspesnych testu
#define TEST_CL_PASS_RATIO      (0.01) // proudove smycky
#define TEST_FO_PASS_RATIO      (0.02) // frekvencniho vystupu

// Info o dilcim testu hlavniho testu
typedef struct
{
    // absolutni prispevek k celkovemu trvani hlavniho testu
    unsigned int progressStep;

    // dalsi dilci test po zadosti o zastaveni hlavniho testu
    TMainSubTestId subTestAfterStopRequest;

} TMainSubTestInfo;

// stavy testu
static TMainTestState mainState;
static TADTestState meas2C1State;
static TADTestState meas2C2State;
static TADTestState meas1E1State;
static TADTestState meas1TempState;
static TCInductanceTestState cInductanceState;
static TExciteInTestState exciteInState;
static TFrequencyTestState frequencyState;
static TIoutTestState iOutState;
static TModbusTestState magX2CommState;
static TModbusTestState sensorSlaveState;
static TModbusTestState sensorMasterState;
static TModbusTestState modbusState;
static TADP5063TestState adp5063State;
static TWriteValueCommTestState writeMagx2PasswordsState;
static TReadValueCommTestState readMagx2UnitNoState;
static TReadValueCommTestState readMagx2FirmwareState;
static TValueCommTestState readMagx2MeasurementState;
static TValueCommTestState readMagx2LowFlowCutoffState;
static TValueCommTestState readMagx2ExcitationState;
static TCurrentLoopSettingCommTestState readMagx2CurrentLoopSettingState;
static TFrequencyOutputSettingCommTestState readMagx2FrequencyOutputSettingState;
static TFlowSimulationSettingCommTestState readMagx2FlowSimulationSettingState;
static TReadValueCommTestState readSensorUnitNoState;
static TReadValueCommTestState readSensorFirmwareState;
static TReadValueCommTestState readSensorDiameterState;
static TReadValueCommTestState readSensorFlowRangeState;
static TReadValueCommTestState readSensorErrorMinState;
static TReadValueCommTestState readSensorOkMinState;
static TReadValueCommTestState readSensorErrorCodeState;
static TReadTotalsCommTestState readSensorTotalsState;
static TReadCalibrationCommTestState readSensorCalibrationState;
static TValueCommTestState setTestMagx2MeasurementState;
static TValueCommTestState setTestMagx2LowFlowCutoffState;
static TValueCommTestState setTestMagx2ExcitationState;
static TCurrentLoopSettingCommTestState setTestMagx2CurrentLoopSettingState;
static TFrequencyOutputSettingCommTestState setTestMagx2FrequencyOutputSettingState;
static TValueCommTestState setOriginalMagx2MeasurementState;
static TValueCommTestState setOriginalMagx2LowFlowCutoffState;
static TValueCommTestState setOriginalMagx2ExcitationState;
static TCurrentLoopSettingCommTestState setOriginalMagx2CurrentLoopSettingState;
static TFrequencyOutputSettingCommTestState setOriginalMagx2FrequencyOutputSettingState;
static TFlowSimulationSettingCommTestState setOriginalMagx2FlowSimulationSettingState;

static void Test_InitSubTestStates(void);

//------------------------------------------------------------------------------
//  Init stavu AD testu.
//------------------------------------------------------------------------------
static void InitADTestState(TADTestState *adTestState)
{
    adTestState->testState = TEST_STOPPED;
    adTestState->testType = TEST_TYPE_UNLIMITED;
    adTestState->adLastSample = 0;
    adTestState->adSampleCount = 0;
    adTestState->adSampleSum = 0;
    adTestState->adSampleAvg = 0;
}

//------------------------------------------------------------------------------
//  Uloha AD testu.
//------------------------------------------------------------------------------
static void ADTestTask(TADTestState *adTestState, unsigned int adSample)
{
    if (adTestState->testState == TEST_RUNNING)
    {
        adTestState->adLastSample = adSample;

        if (adTestState->testType == TEST_TYPE_UNLIMITED)
        {
            if (adTestState->adSampleSum > (0xFFFFFFFF - 0xFFFF))
            {
                adTestState->adSampleSum = 0;
                adTestState->adSampleCount = 0;
            }
        }

        adTestState->adSampleCount++;
        adTestState->adSampleSum += adTestState->adLastSample;
        adTestState->adSampleAvg = adTestState->adSampleSum / adTestState->adSampleCount;
        if ((adTestState->testType == TEST_TYPE_LIMITED) && (adTestState->adSampleCount >= AD_TEST_SAMPLE_COUNT))
            adTestState->testState = TEST_COMPLETE;
    }
}

//------------------------------------------------------------------------------
//  Init stavu Modbus testu.
//------------------------------------------------------------------------------
static void InitModbusTestState(TModbusTestState *modbusTestState)
{
    modbusTestState->txFrameCount = 0;
    modbusTestState->rxFrameCount = 0;
    modbusTestState->rxByteCount = 0;
    modbusTestState->rxTimeoutCount = 0;
    modbusTestState->address = 0;
    modbusTestState->data = 0;
}

//------------------------------------------------------------------------------
//  Init stavu ADP5063 testu.
//------------------------------------------------------------------------------
static void InitADP5063TestState(TADP5063TestState *state)
{
    state->errCount = 0;
    state->regData_ChargerStatus1 = 0;
    state->regData_ChargerStatus2 = 0;
    state->sysEnLevel = FALSE;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni hodnot pres kom. port.
//------------------------------------------------------------------------------
static void InitReadValueCommTestState(TReadValueCommTestState *readValueCommTestState)
{
    readValueCommTestState->testState = TEST_STOPPED;
    readValueCommTestState->readValue = 0;
    readValueCommTestState->timeoutCount = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni/zapisu hodnot pres kom. port.
//------------------------------------------------------------------------------
static void InitValueCommTestState(TValueCommTestState *state, unsigned long value)
{
    state->testState = TEST_STOPPED;
    state->value = value;
    state->timeoutCount = 0;
}

//------------------------------------------------------------------------------
//  Init stavu zapisu hodnot pres kom. port.
//------------------------------------------------------------------------------
static void InitWriteValueCommTestState(TWriteValueCommTestState *writeValueCommTestState)
{
    writeValueCommTestState->testState = TEST_STOPPED;
    writeValueCommTestState->timeoutCount = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni totalizeru pres kom. port.
//------------------------------------------------------------------------------
static void InitReadTotalsCommTestState(TReadTotalsCommTestState *readTotalsCommTestState)
{
    readTotalsCommTestState->testState = TEST_STOPPED;
    readTotalsCommTestState->timeoutCount = 0;
    readTotalsCommTestState->readTotals.total = 0;
    readTotalsCommTestState->readTotals.flow_pos = 0;
    readTotalsCommTestState->readTotals.flow_neg = 0;
    readTotalsCommTestState->readTotals.aux = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni kalibrace pres kom. port.
//------------------------------------------------------------------------------
static void InitReadCalibrationCommTestState(TReadCalibrationCommTestState *readCalibrationCommTestState)
{
    readCalibrationCommTestState->testState = TEST_STOPPED;
    readCalibrationCommTestState->timeoutCount = 0;
    readCalibrationCommTestState->readCalibration.cp1 = 0;
    readCalibrationCommTestState->readCalibration.cp2 = 0;
    readCalibrationCommTestState->readCalibration.cp3 = 0;
    readCalibrationCommTestState->readCalibration.mp1 = 0;
    readCalibrationCommTestState->readCalibration.mp2 = 0;
    readCalibrationCommTestState->readCalibration.mp3 = 0;
}

//------------------------------------------------------------------------------
//  Init nastaveni proudove smycky.
//------------------------------------------------------------------------------
static void InitCurrentLoopSetting(TCurrentLoopSetting *currentLoopSetting)
{
    currentLoopSetting->currentSet = 0;
    currentLoopSetting->flowMin = 0;
    currentLoopSetting->flowMax = 0;
    currentLoopSetting->currentMin = 0;
    currentLoopSetting->currentMax = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni nastaveni proudove smycky pres kom. port.
//------------------------------------------------------------------------------
static void InitReadCurrentLoopSettingCommTestState(TCurrentLoopSettingCommTestState *state)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;
    state->sysTime = 0;
    InitCurrentLoopSetting(&state->setting);
}

//------------------------------------------------------------------------------
//  Init stavu zapisu nastaveni proudove smycky pres kom. port.
//------------------------------------------------------------------------------
static void InitWriteCurrentLoopSettingCommTestState(TCurrentLoopSettingCommTestState *state,
                                                     const TCurrentLoopSetting *setting)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;
    state->sysTime = 0;

    state->setting.currentSet = setting->currentSet;
    state->setting.flowMin = setting->flowMin;
    state->setting.flowMax = setting->flowMax;
    state->setting.currentMin = setting->currentMin;
    state->setting.currentMax = setting->currentMax;
}

//------------------------------------------------------------------------------
//  Init nastaveni frekvencniho vystupu.
//------------------------------------------------------------------------------
static void InitFrequencyOutputSetting(TFrequencyOutputSetting *setting)
{
    setting->frequencySet = 0;
    setting->flowMin = 0;
    setting->flowMax = 0;
    setting->frequencyMin = 0;
    setting->frequencyMax = 0;
    setting->dutyCycle = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni nastaveni frekvencniho vystupu pres kom. port.
//------------------------------------------------------------------------------
static void InitReadFrequencyOutputSettingCommTestState(TFrequencyOutputSettingCommTestState *state)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;
    state->sysTime = 0;
    InitFrequencyOutputSetting(&state->setting);
}

//------------------------------------------------------------------------------
//  Init stavu zapisu nastaveni frekvencniho vystupu pres kom. port.
//------------------------------------------------------------------------------
static void InitWriteFrequencyOutputSettingCommTestState(TFrequencyOutputSettingCommTestState *state,
                                                     const TFrequencyOutputSetting *setting)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;
    state->sysTime = 0;

    state->setting.frequencySet = setting->frequencySet;
    state->setting.flowMin = setting->flowMin;
    state->setting.flowMax = setting->flowMax;
    state->setting.frequencyMin = setting->frequencyMin;
    state->setting.frequencyMax = setting->frequencyMax;
    state->setting.dutyCycle = setting->dutyCycle;
}

//------------------------------------------------------------------------------
//  Init nastaveni simulovaneho toku.
//------------------------------------------------------------------------------
static void InitFlowSimulationSetting(TFlowSimulationSetting *setting)
{
    setting->demo = 0;
    setting->simulatedFlow = 0;
}

//------------------------------------------------------------------------------
//  Init stavu vycteni nastaveni simulovaneho toku pres kom. port.
//------------------------------------------------------------------------------
static void InitReadFlowSimulationSettingCommTestState(TFlowSimulationSettingCommTestState *state)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;
    InitFlowSimulationSetting(&state->setting);
}

//------------------------------------------------------------------------------
//  Init stavu zapisu nastaveni simulovaneho toku pres kom. port.
//------------------------------------------------------------------------------
static void InitWriteFlowSimulationSettingCommTestState(TFlowSimulationSettingCommTestState *state,
                                                     const TFlowSimulationSetting *setting)
{
    state->testState = TEST_STOPPED;
    state->timeoutCount = 0;

    state->setting.demo = setting->demo;
    state->setting.simulatedFlow = setting->simulatedFlow;
}

//------------------------------------------------------------------------------
//  Test Meas2 C1.
//------------------------------------------------------------------------------
static void Test_Meas2C1_Task(void)
{
    ADTestTask(&meas2C1State, AD_Read(ADCH_MEAS_2));
    if (meas2C1State.testState == TEST_COMPLETE)
        Test_Meas2C1_Stop();
}

void Test_Meas2C1_Start(TTestType testType)
{
    InitADTestState(&meas2C1State);
    meas2C1State.testType = testType;
    meas2C1State.testState = TEST_RUNNING;
    Switch_SetTest(SW_TEST1);
    TaskMan_Register(Test_Meas2C1_Task);
}

void Test_Meas2C1_Stop(void)
{
    if (meas2C1State.testState == TEST_RUNNING)
        meas2C1State.testState = TEST_STOPPED;

    Switch_Disconnect();
    TaskMan_Unregister(Test_Meas2C1_Task);
}

const TADTestState* Test_Meas2C1_GetState(void)
{
    return &meas2C1State;
}

//------------------------------------------------------------------------------
//  Test Meas2 C2.
//------------------------------------------------------------------------------
static void Test_Meas2C2_Task(void)
{
    ADTestTask(&meas2C2State, AD_Read(ADCH_MEAS_2));
    if (meas2C2State.testState == TEST_COMPLETE)
        Test_Meas2C2_Stop();
}

void Test_Meas2C2_Start(TTestType testType)
{
    InitADTestState(&meas2C2State);
    meas2C2State.testType = testType;
    meas2C2State.testState = TEST_RUNNING;
    Switch_SetTest(SW_TEST2);
    TaskMan_Register(Test_Meas2C2_Task);
}

void Test_Meas2C2_Stop(void)
{
    if (meas2C2State.testState == TEST_RUNNING)
        meas2C2State.testState = TEST_STOPPED;

    Switch_Disconnect();
    TaskMan_Unregister(Test_Meas2C2_Task);
}

const TADTestState* Test_Meas2C2_GetState(void)
{
    return &meas2C2State;
}

//------------------------------------------------------------------------------
//  Test Meas1 E1.
//------------------------------------------------------------------------------
static void Test_Meas1E1_Task(void)
{
    ADTestTask(&meas1E1State, AD_Read(ADCH_MEAS_1));
    if (meas1E1State.testState == TEST_COMPLETE)
        Test_Meas1E1_Stop();
}

void Test_Meas1E1_Start(TTestType testType)
{
    InitADTestState(&meas1E1State);
    meas1E1State.testType = testType;
    meas1E1State.testState = TEST_RUNNING;
    Switch_SetTest(SW_TEST3);
    TaskMan_Register(Test_Meas1E1_Task);
}

void Test_Meas1E1_Stop(void)
{
    if (meas1E1State.testState == TEST_RUNNING)
        meas1E1State.testState = TEST_STOPPED;

    Switch_Disconnect();
    TaskMan_Unregister(Test_Meas1E1_Task);
}

const TADTestState* Test_Meas1E1_GetState(void)
{
    return &meas1E1State;
}

//------------------------------------------------------------------------------
//  Test Meas1 Temp.
//------------------------------------------------------------------------------
static void Test_Meas1Temp_Task(void)
{
    ADTestTask(&meas1TempState, AD_Read(ADCH_MEAS_1));
    if (meas1TempState.testState == TEST_COMPLETE)
        Test_Meas1Temp_Stop();
}

void Test_Meas1Temp_Start(TTestType testType)
{
    InitADTestState(&meas1TempState);
    meas1TempState.testType = testType;
    meas1TempState.testState = TEST_RUNNING;
    Switch_SetTest(SW_TEST4);
    TaskMan_Register(Test_Meas1Temp_Task);
}

void Test_Meas1Temp_Stop(void)
{
    if (meas1TempState.testState == TEST_RUNNING)
        meas1TempState.testState = TEST_STOPPED;

    Switch_Disconnect();
    TaskMan_Unregister(Test_Meas1Temp_Task);
}

const TADTestState* Test_Meas1Temp_GetState(void)
{
    return &meas1TempState;
}

//------------------------------------------------------------------------------
//  Test mereni indukcnosti civek.
//------------------------------------------------------------------------------
static void Test_CInductance_Task(void)
{
    ExtInd_Measure();

    cInductanceState.adState = ExtInd_GetMeasDeviceStatus();
    cInductanceState.errText = ExtInd_GetErrorText();
    cInductanceState.adLastSample = ExtInd_GetAdcData();
    cInductanceState.adSampleCount = ExtInd_GetValuesInQueue();
    cInductanceState.measureCount++;
    cInductanceState.inductanceInHenry = ExtInd_GetMeasuredValue();

    if (cInductanceState.testType == TEST_TYPE_LIMITED)
    {
        if ((cInductanceState.adSampleCount >= EXTIND_MEASUREMENT_QUEUE_LENGTH) || (cInductanceState.measureCount >= MAX_INDUCTANCE_MEASURE_COUNT))
        {
            cInductanceState.testState = TEST_COMPLETE;
            Test_CInductance_Stop();
        }
    }
}

static void Test_InitCInductanceTestState(void)
{
    cInductanceState.testState = TEST_STOPPED;
    cInductanceState.testType = TEST_TYPE_UNLIMITED;
    cInductanceState.adState = 0;
    cInductanceState.errText = UI_GetText(TXTID_EmptyString);
    cInductanceState.adLastSample = UNKNOWN_ADC_INDUCTANCE;
    cInductanceState.adSampleCount = 0;
    cInductanceState.measureCount = 0;
    cInductanceState.inductanceInHenry = UNKNOWN_INDUCTANCE;
}

void Test_CInductance_Start(TTestType testType)
{
    Test_InitCInductanceTestState();
    cInductanceState.testType = testType;
    cInductanceState.testState = TEST_RUNNING;
    ExtInd_Open();
    TaskMan_Register(Test_CInductance_Task);
}

void Test_CInductance_Stop(void)
{
    if (cInductanceState.testState == TEST_RUNNING)
        cInductanceState.testState = TEST_STOPPED;

    ExtInd_Close();
    TaskMan_Unregister(Test_CInductance_Task);
}

const TCInductanceTestState* Test_CInductance_GetState(void)
{
    return &cInductanceState;
}

//------------------------------------------------------------------------------
//  Test mereni urovni buzeni.
//------------------------------------------------------------------------------
static void Test_ExcitationIn_PushSample(unsigned int sample, TADSampleState *sampleState)
{
    sampleState->adSampleCount++;
    sampleState->adSampleSum += sample;
    sampleState->adSampleAvg = sampleState->adSampleSum / sampleState->adSampleCount;
}

static void Test_ExcitationIn_ClearSamples(TADSampleState *sampleState)
{
    sampleState->adSampleCount = 0;
    sampleState->adSampleSum = 0;
    sampleState->adSampleAvg = 0;
}

static void Test_ExcitationIn_Task(void)
{
    unsigned int sample = AD_Read(ADCH_EXCITATION_IN);

    if (sample <= EXCITE_IN_TEST_L1_MAX)
        Test_ExcitationIn_PushSample(sample, &exciteInState.L1SampleState);
    else if (sample < EXCITE_IN_TEST_L3_MIN)
        Test_ExcitationIn_PushSample(sample, &exciteInState.L2SampleState);
    else
        Test_ExcitationIn_PushSample(sample, &exciteInState.L3SampleState);

    unsigned long allSampleCount = exciteInState.L1SampleState.adSampleCount +
        exciteInState.L2SampleState.adSampleCount + exciteInState.L3SampleState.adSampleCount;

    if (exciteInState.testType == TEST_TYPE_UNLIMITED)
    {
        if (allSampleCount > (0xFFFFFFFF - 0xFFFF))
        {
            Test_ExcitationIn_ClearSamples(&exciteInState.L1SampleState);
            Test_ExcitationIn_ClearSamples(&exciteInState.L2SampleState);
            Test_ExcitationIn_ClearSamples(&exciteInState.L3SampleState);
        }
    }

    if ((exciteInState.testType == TEST_TYPE_LIMITED) && (allSampleCount >= EXCITE_IN_TEST_ALL_SAMPLE_COUNT))
    {
        exciteInState.testState = TEST_COMPLETE;

        if (exciteInState.L1SampleState.adSampleCount < EXCITE_IN_TEST_L_SAMPLE_COUNT)
            Test_ExcitationIn_ClearSamples(&exciteInState.L1SampleState);
        if (exciteInState.L2SampleState.adSampleCount < EXCITE_IN_TEST_L_SAMPLE_COUNT)
            Test_ExcitationIn_ClearSamples(&exciteInState.L2SampleState);
        if (exciteInState.L3SampleState.adSampleCount < EXCITE_IN_TEST_L_SAMPLE_COUNT)
            Test_ExcitationIn_ClearSamples(&exciteInState.L3SampleState);

        Test_ExcitationIn_Stop();
    }
}

static void Test_InitExcitationInTestState(void)
{
    exciteInState.testState = TEST_STOPPED;
    exciteInState.testType = TEST_TYPE_UNLIMITED;
    Test_ExcitationIn_ClearSamples(&exciteInState.L1SampleState);
    Test_ExcitationIn_ClearSamples(&exciteInState.L2SampleState);
    Test_ExcitationIn_ClearSamples(&exciteInState.L3SampleState);
}

void Test_ExcitationIn_Start(TTestType testType)
{
    Test_InitExcitationInTestState();
    exciteInState.testType = testType;
    exciteInState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ExcitationIn_Task);
}

void Test_ExcitationIn_Stop(void)
{
    if (exciteInState.testState == TEST_RUNNING)
        exciteInState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ExcitationIn_Task);
}

const TExciteInTestState* Test_ExcitationIn_GetState(void)
{
    return &exciteInState;
}

//------------------------------------------------------------------------------
//  Test mereni frekvence.
//------------------------------------------------------------------------------

static void Test_Frequency_SetFlowSimulationSetting_Demo(void);
static void Test_Frequency_SetFlowSimulationSetting_Flow(void);
static float Test_Frequency_HwTimerGetIncrementTime(void);
static unsigned long Test_Frequency_HwTimerGetEventSample(void);

static void Timer_interrupt(void)
{
  T1IR = 0xFF;

  unsigned long timerSample = Test_Frequency_HwTimerGetEventSample();

  if (frequencyState.timerSnapNew != timerSample)
  {
        frequencyState.lastEventSysTime = RTC_GetSysSeconds();

        frequencyState.timeCount++;

        frequencyState.timerSnapOld = frequencyState.timerSnapNew;
        frequencyState.timerSnapNew = timerSample;

        if (frequencyState.timeCount > 2)
        {
            frequencyState.timeSum += (frequencyState.timerSnapNew - frequencyState.timerSnapOld) * Test_Frequency_HwTimerGetIncrementTime();
            frequencyState.timeAvg = frequencyState.timeSum / (frequencyState.timeCount - 2);
            if (frequencyState.timeAvg != 0)
                frequencyState.frequency[frequencyState.testFlowIndex] = (unsigned long)((1 / (frequencyState.timeAvg/* * 2*/)) + 0.5);
            else
                frequencyState.frequency[frequencyState.testFlowIndex] = 0xFFFFFFFF;
        }
  }

  VICVectAddr=0;
}

static void Test_Frequency_HwTimerInit(void)
{
    // Timer/Counter 1, CCLK/4
    PCONP |= (1 << 2);
    PCLKSEL0 &= ~((1 << 5) | (1 << 4));
    PINSEL3 |= (1 << 7) | (1 << 6) ;
    T1CCR = (1 << 3) /*| (1 << 4)*/ | (1 << 5);
    T1TCR = (1 << 0);
    //T1TCR &= ~(1 << 1);
    T1CTCR = 0;
    T1PR = 0;
    VICVectAddr5=(unsigned long)Timer_interrupt;
    VICIntEnable |= (1<<5);
}

static void Test_Frequency_HwTimerRelease(void)
{
    VICIntEnable &= ~(1<<5);
    // Timer/Counter 1
    PCONP &= ~(1 << 2);
    //T1TCR &= ~(1 << 0);
    T1TCR = 0;
}

static unsigned long Test_Frequency_HwTimerGetEventSample(void)
{
    // Timer/Counter 1
    return T1CR1;
}

static float Test_Frequency_HwTimerGetIncrementTime(void)
{
    // Timer/Counter 1
    return 1 / (Fcclk / 4.0);
}

static void Test_Frequency_InitTimeMeasuring(void)
{
    frequencyState.timerSnapOld = 0;
    frequencyState.timerSnapNew = 0;
    frequencyState.timeSum = 0;
    frequencyState.timeCount = 0;
    frequencyState.timeAvg = 0;
    frequencyState.lastEventSysTime = RTC_GetSysSeconds();
    frequencyState.timeoutCount = 0;
}

static void Test_Frequency_Task(void)
{
    if (frequencyState.testType == TEST_TYPE_UNLIMITED)
    {
        if (frequencyState.timeCount > (0xFFFFFFFF - 0xFFFF))
            Test_Frequency_InitTimeMeasuring();
    }

    if ((frequencyState.testType == TEST_TYPE_LIMITED) &&
        ((frequencyState.timeCount >= FREQUENCY_TEST_TIME_COUNT) ||
         (RTC_IsSysSecondTimeout(frequencyState.lastEventSysTime, FREQUENCY_TEST_TIMEOUT_SEC))))
    {
        if (frequencyState.timeCount < FREQUENCY_TEST_TIME_COUNT)
        {
            frequencyState.testState = TEST_COMPLETE;
            frequencyState.testResult = TRES_UNKNOWN;
            Test_Frequency_Stop();
            return;
        }

        if ((frequencyState.testFlowIndex + 1) < TEST_FLOW_COUNT)
        {
            Test_Frequency_HwTimerRelease();
            frequencyState.testFlowIndex++;
            Test_Frequency_InitTimeMeasuring();
            TaskMan_Unregister(Test_Frequency_Task);
            Test_Frequency_SetFlowSimulationSetting_Demo();
        }
        else
        {
            frequencyState.testState = TEST_COMPLETE;
            Test_Frequency_Stop();
        }
    }
}

static void Test_Frequency_DelayAfterSetting_Task(void)
{
    if (RTC_IsSysSecondTimeout(frequencyState.lastEventSysTime, FREQUENCY_TEST_TIMEOUT_SEC))
    {
        TaskMan_Unregister(Test_Frequency_DelayAfterSetting_Task);
        frequencyState.lastEventSysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_Frequency_Task);
        Test_Frequency_HwTimerInit();
    }
}

static void Test_Frequency_Flow_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        frequencyState.lastEventSysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_Frequency_DelayAfterSetting_Task);
    }
    else
    {
        frequencyState.timeoutCount++;
        if (frequencyState.timeoutCount <= MAX_WRITE_VALUE_TIMEOUT_COUNT)
            Test_Frequency_SetFlowSimulationSetting_Flow();
        else
        {
            frequencyState.testState = TEST_COMPLETE;
            frequencyState.testResult = TRES_TIMEOUT;
            Test_Frequency_Stop();
        }
    }
}

static void Test_Frequency_SetFlowSimulationSetting_Flow(void)
{
    TFlowSimulationSetting setting;
    setting.demo = TEST_PARAM_DEMO;
    setting.simulatedFlow = TEST_FLOWS[frequencyState.testFlowIndex];

    MagX2Comm_WriteFlowSimulationSetting_Flow(Test_Frequency_Flow_DataHandler, &setting);
}

static void Test_Frequency_DelayAfterSettingDemo_Task(void)
{
    if (RTC_IsSysSecondTimeout(frequencyState.lastEventSysTime, DELAY_BETWEEN_SETTINGS_SEC))
    {
        TaskMan_Unregister(Test_Frequency_DelayAfterSettingDemo_Task);
        Test_Frequency_SetFlowSimulationSetting_Flow();
    }
}

static void Test_Frequency_Demo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        frequencyState.lastEventSysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_Frequency_DelayAfterSettingDemo_Task);
    }
    else
    {
        frequencyState.timeoutCount++;
        if (frequencyState.timeoutCount <= MAX_WRITE_VALUE_TIMEOUT_COUNT)
            Test_Frequency_SetFlowSimulationSetting_Demo();
        else
        {
            frequencyState.testState = TEST_COMPLETE;
            frequencyState.testResult = TRES_TIMEOUT;
            Test_Frequency_Stop();
        }
    }
}

static void Test_Frequency_SetFlowSimulationSetting_Demo(void)
{
    TFlowSimulationSetting setting;
    setting.demo = TEST_PARAM_DEMO;
    setting.simulatedFlow = TEST_FLOWS[frequencyState.testFlowIndex];

    MagX2Comm_WriteFlowSimulationSetting_Demo(Test_Frequency_Demo_DataHandler, &setting);
}

static void Test_InitFrequencyTestState(void)
{
    frequencyState.testState = TEST_STOPPED;
    frequencyState.testType = TEST_TYPE_UNLIMITED;
    frequencyState.testResult = TRES_OK;

    frequencyState.testFlowIndex = 0;
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        frequencyState.frequency[i] = 0;

    Test_Frequency_InitTimeMeasuring();
}

void Test_Frequency_Start(TTestType testType)
{
    Test_InitFrequencyTestState();
    frequencyState.testType = testType;
    frequencyState.testState = TEST_RUNNING;
    if (testType == TEST_TYPE_UNLIMITED)
    {
        Test_Frequency_HwTimerInit();
        TaskMan_Register(Test_Frequency_Task);
    }
    else
        Test_Frequency_SetFlowSimulationSetting_Demo();
}

void Test_Frequency_Stop(void)
{
    if (frequencyState.testState == TEST_RUNNING)
        frequencyState.testState = TEST_STOPPED;

    Test_Frequency_HwTimerRelease();
    TaskMan_Unregister(Test_Frequency_Task);
    TaskMan_Unregister(Test_Frequency_DelayAfterSetting_Task);
    TaskMan_Unregister(Test_Frequency_DelayAfterSettingDemo_Task);
    MagX2Comm_StopReceiving();
}

const TFrequencyTestState* Test_Frequency_GetState(void)
{
    return &frequencyState;
}

//------------------------------------------------------------------------------
//  Test mereni Iout.
//------------------------------------------------------------------------------

static void Test_Iout_SetFlowSimulationSetting_Demo(void);
static void Test_Iout_SetFlowSimulationSetting_Flow(void);

static void Test_Iout_Task(void)
{
    ExtIout_Measure();

    iOutState.errText = ExtIout_GetErrorText();
    iOutState.adLastSample = ExtIout_GetAdcData();
    iOutState.adSampleCount = ExtIout_GetValuesInQueue();
    iOutState.measureCount++;
    iOutState.currentInAmpere[iOutState.testFlowIndex] = ExtIout_GetMeasuredValue();

    if (iOutState.testType == TEST_TYPE_LIMITED)
    {
        if (iOutState.adSampleCount >= EXTIOUT_MEASUREMENT_QUEUE_LENGTH)
        {
            if ((iOutState.testFlowIndex + 1) < TEST_FLOW_COUNT)
            {
                iOutState.testFlowIndex++;
                ExtIout_Close();
                TaskMan_Unregister(Test_Iout_Task);
                Test_Iout_SetFlowSimulationSetting_Demo();
            }
            else
            {
                for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
                {
                    if (iOutState.currentInAmpere[i] > 0.02)
                        iOutState.testResult = TRES_UNKNOWN;
                }

                iOutState.testState = TEST_COMPLETE;
                Test_Iout_Stop();
            }
        }
        else if (iOutState.measureCount >= MAX_IOUT_MEASURE_COUNT)
        {
            iOutState.testState = TEST_COMPLETE;
            iOutState.testResult = TRES_UNKNOWN;
            Test_Iout_Stop();
        }
    }
}

static void Test_Iout_DelayAfterSetting_Task(void)
{
    if (RTC_IsSysSecondTimeout(iOutState.sysTime, IOUT_TEST_TIMEOUT_SEC))
    {
        TaskMan_Unregister(Test_Iout_DelayAfterSetting_Task);
        ExtIout_Open();
        TaskMan_Register(Test_Iout_Task);
    }
}

static void Test_Iout_Flow_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        iOutState.sysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_Iout_DelayAfterSetting_Task);
    }
    else
    {
        iOutState.timeoutCount++;
        if (iOutState.timeoutCount <= MAX_WRITE_VALUE_TIMEOUT_COUNT)
            Test_Iout_SetFlowSimulationSetting_Flow();
        else
        {
            iOutState.testState = TEST_COMPLETE;
            iOutState.testResult = TRES_TIMEOUT;
            Test_Iout_Stop();
        }
    }
}

static void Test_Iout_SetFlowSimulationSetting_Flow(void)
{
    TFlowSimulationSetting setting;
    setting.demo = TEST_PARAM_DEMO;
    setting.simulatedFlow = TEST_FLOWS[iOutState.testFlowIndex];

    MagX2Comm_WriteFlowSimulationSetting_Flow(Test_Iout_Flow_DataHandler, &setting);
}

static void Test_Iout_DelayAfterSettingDemo_Task(void)
{
    if (RTC_IsSysSecondTimeout(iOutState.sysTime, DELAY_BETWEEN_SETTINGS_SEC))
    {
        TaskMan_Unregister(Test_Iout_DelayAfterSettingDemo_Task);
        Test_Iout_SetFlowSimulationSetting_Flow();
    }
}

static void Test_Iout_Demo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        iOutState.sysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_Iout_DelayAfterSettingDemo_Task);
    }
    else
    {
        iOutState.timeoutCount++;
        if (iOutState.timeoutCount <= MAX_WRITE_VALUE_TIMEOUT_COUNT)
            Test_Iout_SetFlowSimulationSetting_Demo();
        else
        {
            iOutState.testState = TEST_COMPLETE;
            iOutState.testResult = TRES_TIMEOUT;
            Test_Iout_Stop();
        }
    }
}

static void Test_Iout_SetFlowSimulationSetting_Demo(void)
{
    TFlowSimulationSetting setting;
    setting.demo = TEST_PARAM_DEMO;
    setting.simulatedFlow = TEST_FLOWS[iOutState.testFlowIndex];

    MagX2Comm_WriteFlowSimulationSetting_Demo(Test_Iout_Demo_DataHandler, &setting);
}

static void Test_InitIoutTestState(void)
{
    iOutState.testState = TEST_STOPPED;
    iOutState.testType = TEST_TYPE_UNLIMITED;
    iOutState.errText = UI_GetText(TXTID_EmptyString);
    iOutState.adLastSample = UNKNOWN_ADC_IOUT;
    iOutState.adSampleCount = 0;
    iOutState.measureCount = 0;
    iOutState.sysTime = RTC_GetSysSeconds();
    iOutState.timeoutCount = 0;
    iOutState.testResult = TRES_OK;
    iOutState.testFlowIndex = 0;
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        iOutState.currentInAmpere[i] = UNKNOWN_IOUT;
}

void Test_Iout_Start(TTestType testType)
{
    Test_InitIoutTestState();
    iOutState.testType = testType;
    iOutState.testState = TEST_RUNNING;
    if (testType == TEST_TYPE_UNLIMITED)
    {
        ExtIout_Open();
        TaskMan_Register(Test_Iout_Task);
    }
    else
        Test_Iout_SetFlowSimulationSetting_Demo();
}

void Test_Iout_Stop(void)
{
    if (iOutState.testState == TEST_RUNNING)
        iOutState.testState = TEST_STOPPED;

    ExtIout_Close();
    TaskMan_Unregister(Test_Iout_Task);
    TaskMan_Unregister(Test_Iout_DelayAfterSetting_Task);
    TaskMan_Unregister(Test_Iout_DelayAfterSettingDemo_Task);
    MagX2Comm_StopReceiving();
}

const TIoutTestState* Test_Iout_GetState(void)
{
    return &iOutState;
}

//------------------------------------------------------------------------------
//  Test MagX2 komunikace.
//------------------------------------------------------------------------------
static void Test_MagX2Comm_DataHandler(const TCommData *commData)
{
    magX2CommState.txFrameCount++;
    magX2CommState.rxByteCount += MagX2Comm_GetRcvFrameByteCount();

    if (commData->commResult == CRES_OK)
        magX2CommState.rxFrameCount++;
    else if (commData->commResult == CRES_TMR)
        magX2CommState.rxTimeoutCount++;

    MagX2Comm_ReadUnitNo(Test_MagX2Comm_DataHandler);
}

void Test_MagX2Comm_Start(void)
{
    InitModbusTestState(&magX2CommState);
    MagX2Comm_ReadUnitNo(Test_MagX2Comm_DataHandler);
}

void Test_MagX2Comm_Stop(void)
{
    MagX2Comm_StopReceiving();
}

const TModbusTestState* Test_MagX2Comm_GetState(void)
{
    return &magX2CommState;
}

//------------------------------------------------------------------------------
//  Test Sensor Slave komunikace.
//------------------------------------------------------------------------------
static void Test_SensorSlave_DataRxHandler(const TCommData *commData)
{
    sensorSlaveState.rxByteCount += SensorSlave_GetRcvFrameByteCount();

    if (commData->commResult == CRES_OK)
        sensorSlaveState.rxFrameCount++;
    else if (commData->commResult == CRES_TMR)
        sensorSlaveState.rxTimeoutCount++;
}

static void Test_SensorSlave_DataTxHandler(const TCommData *commData)
{
    sensorSlaveState.txFrameCount++;
}

void Test_SensorSlave_Start(void)
{
    InitModbusTestState(&sensorSlaveState);
    SensorSlave_SetRxHandler(Test_SensorSlave_DataRxHandler);
    SensorSlave_SetTxHandler(Test_SensorSlave_DataTxHandler);
}

void Test_SensorSlave_Stop(void)
{
    SensorSlave_SetRxHandler(NO_SENSOR_SLAVE_DATA_HANDLER);
    SensorSlave_SetTxHandler(NO_SENSOR_SLAVE_DATA_HANDLER);
}

const TModbusTestState* Test_SensorSlave_GetState(void)
{
    return &sensorSlaveState;
}

//------------------------------------------------------------------------------
//  Test Sensor Master komunikace.
//------------------------------------------------------------------------------
static void Test_SensorMaster_DataHandler(const TCommData *commData)
{
    sensorMasterState.rxByteCount += SensorMaster_GetRcvFrameByteCount();

    if (commData->commResult == CRES_OK)
        sensorMasterState.rxFrameCount++;
    else if (commData->commResult == CRES_TMR)
        sensorMasterState.rxTimeoutCount++;
}

static void Test_SensorMaster_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        SensorMaster_ReadUnitNo(Test_SensorMaster_DataHandler);
        sensorMasterState.txFrameCount++;
    }
}

void Test_SensorMaster_Start(void)
{
    InitModbusTestState(&sensorMasterState);
    TaskMan_Register(Test_SensorMaster_Task);
}

void Test_SensorMaster_Stop(void)
{
    TaskMan_Unregister(Test_SensorMaster_Task);
}

const TModbusTestState* Test_SensorMaster_GetState(void)
{
    return &sensorMasterState;
}

//------------------------------------------------------------------------------
//  Test Modbus komunikace.
//------------------------------------------------------------------------------
static void Test_Modbus_DataHandler(const TModbusCommData *commData)
{
    modbusState.address = commData->address;
    modbusState.data = commData->data;
    modbusState.rxFrameCount++;
}

void Test_Modbus_Start(void)
{
    InitModbusTestState(&modbusState);
    Modbus_SetCommDataHandler(Test_Modbus_DataHandler);
}

void Test_Modbus_Stop(void)
{
    Modbus_SetCommDataHandler(NO_MODBUS_COMM_DATA_HANDLER);
}

const TModbusTestState* Test_Modbus_GetState(void)
{
    return &modbusState;
}

//------------------------------------------------------------------------------
//  Test ADP5063 komunikace.
//------------------------------------------------------------------------------
static void Test_ADP5063_Task(void)
{
    adp5063State.sysEnLevel = ADP5063_GetSysEnLevel();

    int16_t readResult = ADP5063_I2CReadReg(ADP5063_Charger_Status_1_REG);
    if (readResult >= 0)
    {
        adp5063State.regData_ChargerStatus1 = (unsigned char)readResult;
    }
    else
    {
        adp5063State.regData_ChargerStatus1 = 0xFF;
        adp5063State.errCount++;
    }

    readResult = ADP5063_I2CReadReg(ADP5063_Charger_Status_2_REG);
    if (readResult >= 0)
    {
        adp5063State.regData_ChargerStatus2 = (unsigned char)readResult;
    }
    else
    {
        adp5063State.regData_ChargerStatus2 = 0xFF;
        adp5063State.errCount++;
    }
}

void Test_ADP5063_Start(void)
{
    InitADP5063TestState(&adp5063State);
    TaskMan_Register(Test_ADP5063_Task);
}

void Test_ADP5063_Stop(void)
{
    TaskMan_Unregister(Test_ADP5063_Task);
}

const TADP5063TestState* Test_ADP5063_GetState(void)
{
    return &adp5063State;
}

//------------------------------------------------------------------------------
//  Zapis hesel magx2.
//------------------------------------------------------------------------------
static void Test_WriteMagx2Passwords_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        writeMagx2PasswordsState.testState = TEST_COMPLETE;
        Test_WriteMagx2Passwords_Stop();
    }
    else
    {
        writeMagx2PasswordsState.timeoutCount++;
        if (writeMagx2PasswordsState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WritePasswords(Test_WriteMagx2Passwords_DataHandler);
        else
        {
            writeMagx2PasswordsState.testState = TEST_COMPLETE;
            Test_WriteMagx2Passwords_Stop();
        }
    }
}

void Test_WriteMagx2Passwords_Start(void)
{
    InitWriteValueCommTestState(&writeMagx2PasswordsState);
    writeMagx2PasswordsState.testState = TEST_RUNNING;
    MagX2Comm_WritePasswords(Test_WriteMagx2Passwords_DataHandler);
}

void Test_WriteMagx2Passwords_Stop(void)
{
    if (writeMagx2PasswordsState.testState == TEST_RUNNING)
        writeMagx2PasswordsState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TWriteValueCommTestState* Test_WriteMagx2Passwords_GetState(void)
{
    return &writeMagx2PasswordsState;
}

//------------------------------------------------------------------------------
//  Vycteni magx2UnitNo.
//------------------------------------------------------------------------------
static void Test_ReadMagx2UnitNo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readMagx2UnitNoState.readValue = MagX2Comm_ParseUnitNo(commData->buffer);
        readMagx2UnitNoState.testState = TEST_COMPLETE;
        Test_ReadMagx2UnitNo_Stop();
    }
    else
    {
        readMagx2UnitNoState.timeoutCount++;
        if (readMagx2UnitNoState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadUnitNo(Test_ReadMagx2UnitNo_DataHandler);
        else
        {
            readMagx2UnitNoState.testState = TEST_COMPLETE;
            Test_ReadMagx2UnitNo_Stop();
        }
    }
}

void Test_ReadMagx2UnitNo_Start(void)
{
    InitReadValueCommTestState(&readMagx2UnitNoState);
    readMagx2UnitNoState.testState = TEST_RUNNING;
    MagX2Comm_ReadUnitNo(Test_ReadMagx2UnitNo_DataHandler);
}

void Test_ReadMagx2UnitNo_Stop(void)
{
    if (readMagx2UnitNoState.testState == TEST_RUNNING)
        readMagx2UnitNoState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TReadValueCommTestState* Test_ReadMagx2UnitNo_GetState(void)
{
    return &readMagx2UnitNoState;
}

//------------------------------------------------------------------------------
//  Vycteni magx2Firmware.
//------------------------------------------------------------------------------
static void Test_ReadMagx2Firmware_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readMagx2FirmwareState.readValue = MagX2Comm_ParseFirmware(commData->buffer);
        readMagx2FirmwareState.testState = TEST_COMPLETE;
        Test_ReadMagx2Firmware_Stop();
    }
    else
    {
        readMagx2FirmwareState.timeoutCount++;
        if (readMagx2FirmwareState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadFirmware(Test_ReadMagx2Firmware_DataHandler);
        else
        {
            readMagx2FirmwareState.testState = TEST_COMPLETE;
            Test_ReadMagx2Firmware_Stop();
        }
    }
}

void Test_ReadMagx2Firmware_Start(void)
{
    InitReadValueCommTestState(&readMagx2FirmwareState);
    readMagx2FirmwareState.testState = TEST_RUNNING;
    MagX2Comm_ReadFirmware(Test_ReadMagx2Firmware_DataHandler);
}

void Test_ReadMagx2Firmware_Stop(void)
{
    if (readMagx2FirmwareState.testState == TEST_RUNNING)
        readMagx2FirmwareState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TReadValueCommTestState* Test_ReadMagx2Firmware_GetState(void)
{
    return &readMagx2FirmwareState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu mereni.
//------------------------------------------------------------------------------
static void Test_ReadMagx2Measurement_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readMagx2MeasurementState.value = MagX2Comm_ParseMeasurement(commData->buffer);
        readMagx2MeasurementState.testState = TEST_COMPLETE;
        Test_ReadMagx2Measurement_Stop();
    }
    else
    {
        readMagx2MeasurementState.timeoutCount++;
        if (readMagx2MeasurementState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadMeasurement(Test_ReadMagx2Measurement_DataHandler);
        else
        {
            readMagx2MeasurementState.testState = TEST_COMPLETE;
            Test_ReadMagx2Measurement_Stop();
        }
    }
}

void Test_ReadMagx2Measurement_Start(void)
{
    InitValueCommTestState(&readMagx2MeasurementState, 0);
    readMagx2MeasurementState.testState = TEST_RUNNING;
    MagX2Comm_ReadMeasurement(Test_ReadMagx2Measurement_DataHandler);
}

void Test_ReadMagx2Measurement_Stop(void)
{
    if (readMagx2MeasurementState.testState == TEST_RUNNING)
        readMagx2MeasurementState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_ReadMagx2Measurement_GetState(void)
{
    return &readMagx2MeasurementState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
static void Test_ReadMagx2LowFlowCutoff_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readMagx2LowFlowCutoffState.value = MagX2Comm_ParseLowFlowCutoff(commData->buffer);
        readMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
        Test_ReadMagx2LowFlowCutoff_Stop();
    }
    else
    {
        readMagx2LowFlowCutoffState.timeoutCount++;
        if (readMagx2LowFlowCutoffState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadLowFlowCutoff(Test_ReadMagx2LowFlowCutoff_DataHandler);
        else
        {
            readMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
            Test_ReadMagx2LowFlowCutoff_Stop();
        }
    }
}

void Test_ReadMagx2LowFlowCutoff_Start(void)
{
    InitValueCommTestState(&readMagx2LowFlowCutoffState, 0);
    readMagx2LowFlowCutoffState.testState = TEST_RUNNING;
    MagX2Comm_ReadLowFlowCutoff(Test_ReadMagx2LowFlowCutoff_DataHandler);
}

void Test_ReadMagx2LowFlowCutoff_Stop(void)
{
    if (readMagx2LowFlowCutoffState.testState == TEST_RUNNING)
        readMagx2LowFlowCutoffState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_ReadMagx2LowFlowCutoff_GetState(void)
{
    return &readMagx2LowFlowCutoffState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu buzeni.
//------------------------------------------------------------------------------
static void Test_ReadMagx2Excitation_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readMagx2ExcitationState.value = MagX2Comm_ParseExcitation(commData->buffer);
        readMagx2ExcitationState.testState = TEST_COMPLETE;
        Test_ReadMagx2Excitation_Stop();
    }
    else
    {
        readMagx2ExcitationState.timeoutCount++;
        if (readMagx2ExcitationState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadExcitation(Test_ReadMagx2Excitation_DataHandler);
        else
        {
            readMagx2ExcitationState.testState = TEST_COMPLETE;
            Test_ReadMagx2Excitation_Stop();
        }
    }
}

void Test_ReadMagx2Excitation_Start(void)
{
    InitValueCommTestState(&readMagx2ExcitationState, 0);
    readMagx2ExcitationState.testState = TEST_RUNNING;
    MagX2Comm_ReadExcitation(Test_ReadMagx2Excitation_DataHandler);
}

void Test_ReadMagx2Excitation_Stop(void)
{
    if (readMagx2ExcitationState.testState == TEST_RUNNING)
        readMagx2ExcitationState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_ReadMagx2Excitation_GetState(void)
{
    return &readMagx2ExcitationState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni proudove smycky.
//------------------------------------------------------------------------------
static void Test_ReadMagx2CurrentLoopSetting_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_ParseCurrentLoopSetting(commData->buffer, &readMagx2CurrentLoopSettingState.setting);
        readMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
        Test_ReadMagx2CurrentLoopSetting_Stop();
    }
    else
    {
        readMagx2CurrentLoopSettingState.timeoutCount++;
        if (readMagx2CurrentLoopSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadCurrentLoopSetting(Test_ReadMagx2CurrentLoopSetting_DataHandler);
        else
        {
            readMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
            Test_ReadMagx2CurrentLoopSetting_Stop();
        }
    }
}

void Test_ReadMagx2CurrentLoopSetting_Start(void)
{
    InitReadCurrentLoopSettingCommTestState(&readMagx2CurrentLoopSettingState);
    readMagx2CurrentLoopSettingState.testState = TEST_RUNNING;
    MagX2Comm_ReadCurrentLoopSetting(Test_ReadMagx2CurrentLoopSetting_DataHandler);
}

void Test_ReadMagx2CurrentLoopSetting_Stop(void)
{
    if (readMagx2CurrentLoopSettingState.testState == TEST_RUNNING)
        readMagx2CurrentLoopSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TCurrentLoopSettingCommTestState* Test_ReadMagx2CurrentLoopSetting_GetState(void)
{
    return &readMagx2CurrentLoopSettingState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni frekvencniho vystupu.
//------------------------------------------------------------------------------
static void Test_ReadMagx2FrequencyOutputSetting_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_ParseFrequencyOutputSetting(commData->buffer, &readMagx2FrequencyOutputSettingState.setting);
        readMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
        Test_ReadMagx2FrequencyOutputSetting_Stop();
    }
    else
    {
        readMagx2FrequencyOutputSettingState.timeoutCount++;
        if (readMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadFrequencyOutputSetting(Test_ReadMagx2FrequencyOutputSetting_DataHandler);
        else
        {
            readMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_ReadMagx2FrequencyOutputSetting_Stop();
        }
    }
}

void Test_ReadMagx2FrequencyOutputSetting_Start(void)
{
    InitReadFrequencyOutputSettingCommTestState(&readMagx2FrequencyOutputSettingState);
    readMagx2FrequencyOutputSettingState.testState = TEST_RUNNING;
    MagX2Comm_ReadFrequencyOutputSetting(Test_ReadMagx2FrequencyOutputSetting_DataHandler);
}

void Test_ReadMagx2FrequencyOutputSetting_Stop(void)
{
    if (readMagx2FrequencyOutputSettingState.testState == TEST_RUNNING)
        readMagx2FrequencyOutputSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TFrequencyOutputSettingCommTestState* Test_ReadMagx2FrequencyOutputSetting_GetState(void)
{
    return &readMagx2FrequencyOutputSettingState;
}

//------------------------------------------------------------------------------
//  Vycteni nastaveni simulovaneho toku.
//------------------------------------------------------------------------------
static void Test_ReadMagx2FlowSimulationSetting_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_ParseFlowSimulationSetting(commData->buffer, &readMagx2FlowSimulationSettingState.setting);
        readMagx2FlowSimulationSettingState.testState = TEST_COMPLETE;
        Test_ReadMagx2FlowSimulationSetting_Stop();
    }
    else
    {
        readMagx2FlowSimulationSettingState.timeoutCount++;
        if (readMagx2FlowSimulationSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadFlowSimulationSetting(Test_ReadMagx2FlowSimulationSetting_DataHandler);
        else
        {
            readMagx2FlowSimulationSettingState.testState = TEST_COMPLETE;
            Test_ReadMagx2FlowSimulationSetting_Stop();
        }
    }
}

void Test_ReadMagx2FlowSimulationSetting_Start(void)
{
    InitReadFlowSimulationSettingCommTestState(&readMagx2FlowSimulationSettingState);
    readMagx2FlowSimulationSettingState.testState = TEST_RUNNING;
    MagX2Comm_ReadFlowSimulationSetting(Test_ReadMagx2FlowSimulationSetting_DataHandler);
}

void Test_ReadMagx2FlowSimulationSetting_Stop(void)
{
    if (readMagx2FlowSimulationSettingState.testState == TEST_RUNNING)
        readMagx2FlowSimulationSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TFlowSimulationSettingCommTestState* Test_ReadMagx2FlowSimulationSetting_GetState(void)
{
    return &readMagx2FlowSimulationSettingState;
}

//------------------------------------------------------------------------------
//  Vycteni sensorUnitNo.
//------------------------------------------------------------------------------
static void Test_ReadSensorUnitNo_Task(void);

static void Test_ReadSensorUnitNo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorUnitNoState.readValue = SensorMaster_ParseUnitNo(commData->buffer);
        readSensorUnitNoState.testState = TEST_COMPLETE;
        Test_ReadSensorUnitNo_Stop();
    }
    else
    {
        readSensorUnitNoState.timeoutCount++;
        if (readSensorUnitNoState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorUnitNo_Task);
        else
        {
            readSensorUnitNoState.testState = TEST_COMPLETE;
            Test_ReadSensorUnitNo_Stop();
        }
    }
}

static void Test_ReadSensorUnitNo_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorUnitNo_Task);
        SensorMaster_ReadUnitNo(Test_ReadSensorUnitNo_DataHandler);
    }
}

void Test_ReadSensorUnitNo_Start(void)
{
    InitReadValueCommTestState(&readSensorUnitNoState);
    readSensorUnitNoState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorUnitNo_Task);
}

void Test_ReadSensorUnitNo_Stop(void)
{
    if (readSensorUnitNoState.testState == TEST_RUNNING)
        readSensorUnitNoState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorUnitNo_Task);
}

const TReadValueCommTestState* Test_ReadSensorUnitNo_GetState(void)
{
    return &readSensorUnitNoState;
}

//------------------------------------------------------------------------------
//  Vycteni sensorFirmware.
//------------------------------------------------------------------------------
static void Test_ReadSensorFirmware_Task(void);

static void Test_ReadSensorFirmware_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorFirmwareState.readValue = SensorMaster_ParseFirmware(commData->buffer);
        readSensorFirmwareState.testState = TEST_COMPLETE;
        Test_ReadSensorFirmware_Stop();
    }
    else
    {
        readSensorFirmwareState.timeoutCount++;
        if (readSensorFirmwareState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorFirmware_Task);
        else
        {
            readSensorFirmwareState.testState = TEST_COMPLETE;
            Test_ReadSensorFirmware_Stop();
        }
    }
}

static void Test_ReadSensorFirmware_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorFirmware_Task);
        SensorMaster_ReadFirmware(Test_ReadSensorFirmware_DataHandler);
    }
}

void Test_ReadSensorFirmware_Start(void)
{
    InitReadValueCommTestState(&readSensorFirmwareState);
    readSensorFirmwareState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorFirmware_Task);
}

void Test_ReadSensorFirmware_Stop(void)
{
    if (readSensorFirmwareState.testState == TEST_RUNNING)
        readSensorFirmwareState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorFirmware_Task);
}

const TReadValueCommTestState* Test_ReadSensorFirmware_GetState(void)
{
    return &readSensorFirmwareState;
}

//------------------------------------------------------------------------------
//  Vycteni diameter.
//------------------------------------------------------------------------------
static void Test_ReadSensorDiameter_Task(void);

static void Test_ReadSensorDiameter_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorDiameterState.readValue = SensorMaster_ParseDiameter(commData->buffer);
        readSensorDiameterState.testState = TEST_COMPLETE;
        Test_ReadSensorDiameter_Stop();
    }
    else
    {
        readSensorDiameterState.timeoutCount++;
        if (readSensorDiameterState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorDiameter_Task);
        else
        {
            readSensorDiameterState.testState = TEST_COMPLETE;
            Test_ReadSensorDiameter_Stop();
        }
    }
}

static void Test_ReadSensorDiameter_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorDiameter_Task);
        SensorMaster_ReadDiameter(Test_ReadSensorDiameter_DataHandler);
    }
}

void Test_ReadSensorDiameter_Start(void)
{
    InitReadValueCommTestState(&readSensorDiameterState);
    readSensorDiameterState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorDiameter_Task);
}

void Test_ReadSensorDiameter_Stop(void)
{
    if (readSensorDiameterState.testState == TEST_RUNNING)
        readSensorDiameterState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorDiameter_Task);
}

const TReadValueCommTestState* Test_ReadSensorDiameter_GetState(void)
{
    return &readSensorDiameterState;
}

//------------------------------------------------------------------------------
//  Vycteni flowRange.
//------------------------------------------------------------------------------
static void Test_ReadSensorFlowRange_Task(void);

static void Test_ReadSensorFlowRange_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorFlowRangeState.readValue = SensorMaster_ParseFlowRange(commData->buffer);
        readSensorFlowRangeState.testState = TEST_COMPLETE;
        Test_ReadSensorFlowRange_Stop();
    }
    else
    {
        readSensorFlowRangeState.timeoutCount++;
        if (readSensorFlowRangeState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorFlowRange_Task);
        else
        {
            readSensorFlowRangeState.testState = TEST_COMPLETE;
            Test_ReadSensorFlowRange_Stop();
        }
    }
}

static void Test_ReadSensorFlowRange_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorFlowRange_Task);
        SensorMaster_ReadFlowRange(Test_ReadSensorFlowRange_DataHandler);
    }
}

void Test_ReadSensorFlowRange_Start(void)
{
    InitReadValueCommTestState(&readSensorFlowRangeState);
    readSensorFlowRangeState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorFlowRange_Task);
}

void Test_ReadSensorFlowRange_Stop(void)
{
    if (readSensorFlowRangeState.testState == TEST_RUNNING)
        readSensorFlowRangeState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorFlowRange_Task);
}

const TReadValueCommTestState* Test_ReadSensorFlowRange_GetState(void)
{
    return &readSensorFlowRangeState;
}

//------------------------------------------------------------------------------
//  Vycteni errorMin.
//------------------------------------------------------------------------------
static void Test_ReadSensorErrorMin_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorErrorMinState.readValue = MagX2Comm_ParseErrorMin(commData->buffer);
        readSensorErrorMinState.testState = TEST_COMPLETE;
        Test_ReadSensorErrorMin_Stop();
    }
    else
    {
        readSensorErrorMinState.timeoutCount++;
        if (readSensorErrorMinState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadErrorMin(Test_ReadSensorErrorMin_DataHandler);
        else
        {
            readSensorErrorMinState.testState = TEST_COMPLETE;
            Test_ReadSensorErrorMin_Stop();
        }
    }
}

void Test_ReadSensorErrorMin_Start(void)
{
    InitReadValueCommTestState(&readSensorErrorMinState);
    readSensorErrorMinState.testState = TEST_RUNNING;
    MagX2Comm_ReadErrorMin(Test_ReadSensorErrorMin_DataHandler);
}

void Test_ReadSensorErrorMin_Stop(void)
{
    if (readSensorErrorMinState.testState == TEST_RUNNING)
        readSensorErrorMinState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TReadValueCommTestState* Test_ReadSensorErrorMin_GetState(void)
{
    return &readSensorErrorMinState;
}

//------------------------------------------------------------------------------
//  Vycteni okMin.
//------------------------------------------------------------------------------
static void Test_ReadSensorOkMin_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorOkMinState.readValue = MagX2Comm_ParseOkMin(commData->buffer);
        readSensorOkMinState.testState = TEST_COMPLETE;
        Test_ReadSensorOkMin_Stop();
    }
    else
    {
        readSensorOkMinState.timeoutCount++;
        if (readSensorOkMinState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_ReadOkMin(Test_ReadSensorOkMin_DataHandler);
        else
        {
            readSensorOkMinState.testState = TEST_COMPLETE;
            Test_ReadSensorOkMin_Stop();
        }
    }
}

void Test_ReadSensorOkMin_Start(void)
{
    InitReadValueCommTestState(&readSensorOkMinState);
    readSensorOkMinState.testState = TEST_RUNNING;
    MagX2Comm_ReadOkMin(Test_ReadSensorOkMin_DataHandler);
}

void Test_ReadSensorOkMin_Stop(void)
{
    if (readSensorOkMinState.testState == TEST_RUNNING)
        readSensorOkMinState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TReadValueCommTestState* Test_ReadSensorOkMin_GetState(void)
{
    return &readSensorOkMinState;
}

//------------------------------------------------------------------------------
//  Vycteni errorCode.
//------------------------------------------------------------------------------
static void Test_ReadSensorErrorCode_Task(void);

static void Test_ReadSensorErrorCode_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        readSensorErrorCodeState.readValue = SensorMaster_ParseErrorCode(commData->buffer);
        readSensorErrorCodeState.testState = TEST_COMPLETE;
        Test_ReadSensorErrorCode_Stop();
    }
    else
    {
        readSensorErrorCodeState.timeoutCount++;
        if (readSensorErrorCodeState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorErrorCode_Task);
        else
        {
            readSensorErrorCodeState.testState = TEST_COMPLETE;
            Test_ReadSensorErrorCode_Stop();
        }
    }
}

static void Test_ReadSensorErrorCode_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorErrorCode_Task);
        SensorMaster_ReadErrorCode(Test_ReadSensorErrorCode_DataHandler);
    }
}

void Test_ReadSensorErrorCode_Start(void)
{
    InitReadValueCommTestState(&readSensorErrorCodeState);
    readSensorErrorCodeState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorErrorCode_Task);
}

void Test_ReadSensorErrorCode_Stop(void)
{
    if (readSensorErrorCodeState.testState == TEST_RUNNING)
        readSensorErrorCodeState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorErrorCode_Task);
}

const TReadValueCommTestState* Test_ReadSensorErrorCode_GetState(void)
{
    return &readSensorErrorCodeState;
}

//------------------------------------------------------------------------------
//  Vycteni totals.
//------------------------------------------------------------------------------
static void Test_ReadSensorTotals_Task(void);

static void Test_ReadSensorTotals_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        SensorMaster_ParseTotals(commData->buffer, &readSensorTotalsState.readTotals);
        readSensorTotalsState.testState = TEST_COMPLETE;
        Test_ReadSensorTotals_Stop();
    }
    else
    {
        readSensorTotalsState.timeoutCount++;
        if (readSensorTotalsState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorTotals_Task);
        else
        {
            readSensorTotalsState.testState = TEST_COMPLETE;
            Test_ReadSensorTotals_Stop();
        }
    }
}

static void Test_ReadSensorTotals_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorTotals_Task);
        SensorMaster_ReadTotals(Test_ReadSensorTotals_DataHandler);
    }
}

void Test_ReadSensorTotals_Start(void)
{
    InitReadTotalsCommTestState(&readSensorTotalsState);
    readSensorTotalsState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorTotals_Task);
}

void Test_ReadSensorTotals_Stop(void)
{
    if (readSensorTotalsState.testState == TEST_RUNNING)
        readSensorTotalsState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorTotals_Task);
}

const TReadTotalsCommTestState* Test_ReadSensorTotals_GetState(void)
{
    return &readSensorTotalsState;
}

//------------------------------------------------------------------------------
//  Vycteni calibration.
//------------------------------------------------------------------------------
static void Test_ReadSensorCalibration_Task(void);

static void Test_ReadSensorCalibration_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        SensorMaster_ParseCalibration(commData->buffer, &readSensorCalibrationState.readCalibration);
        readSensorCalibrationState.testState = TEST_COMPLETE;
        Test_ReadSensorCalibration_Stop();
    }
    else
    {
        readSensorCalibrationState.timeoutCount++;
        if (readSensorCalibrationState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            TaskMan_Register(Test_ReadSensorCalibration_Task);
        else
        {
            readSensorCalibrationState.testState = TEST_COMPLETE;
            Test_ReadSensorCalibration_Stop();
        }
    }
}

static void Test_ReadSensorCalibration_Task(void)
{
    if (SensorMaster_GetCommStatus() == CMSTA_NONE)
    {
        TaskMan_Unregister(Test_ReadSensorCalibration_Task);
        SensorMaster_ReadCalibration(Test_ReadSensorCalibration_DataHandler);
    }
}

void Test_ReadSensorCalibration_Start(void)
{
    InitReadCalibrationCommTestState(&readSensorCalibrationState);
    readSensorCalibrationState.testState = TEST_RUNNING;
    TaskMan_Register(Test_ReadSensorCalibration_Task);
}

void Test_ReadSensorCalibration_Stop(void)
{
    if (readSensorCalibrationState.testState == TEST_RUNNING)
        readSensorCalibrationState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_ReadSensorCalibration_Task);
}

const TReadCalibrationCommTestState* Test_ReadSensorCalibration_GetState(void)
{
    return &readSensorCalibrationState;
}

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu mereni.
//------------------------------------------------------------------------------
static void Test_SetTestMagx2Measurement_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2MeasurementState.testState = TEST_COMPLETE;
        Test_SetTestMagx2Measurement_Stop();
    }
    else
    {
        setTestMagx2MeasurementState.timeoutCount++;
        if (setTestMagx2MeasurementState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteMeasurement(Test_SetTestMagx2Measurement_DataHandler, setTestMagx2MeasurementState.value);
        else
        {
            setTestMagx2MeasurementState.testState = TEST_COMPLETE;
            Test_SetTestMagx2Measurement_Stop();
        }
    }
}

void Test_SetTestMagx2Measurement_Start(unsigned long setting)
{
    InitValueCommTestState(&setTestMagx2MeasurementState, setting);
    setTestMagx2MeasurementState.testState = TEST_RUNNING;
    MagX2Comm_WriteMeasurement(Test_SetTestMagx2Measurement_DataHandler, setTestMagx2MeasurementState.value);
}

void Test_SetTestMagx2Measurement_Stop(void)
{
    if (setTestMagx2MeasurementState.testState == TEST_RUNNING)
        setTestMagx2MeasurementState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetTestMagx2Measurement_GetState(void)
{
    return &setTestMagx2MeasurementState;
}

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
static void Test_SetTestMagx2LowFlowCutoff_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
        Test_SetTestMagx2LowFlowCutoff_Stop();
    }
    else
    {
        setTestMagx2LowFlowCutoffState.timeoutCount++;
        if (setTestMagx2LowFlowCutoffState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteLowFlowCutoff(Test_SetTestMagx2LowFlowCutoff_DataHandler, setTestMagx2LowFlowCutoffState.value);
        else
        {
            setTestMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
            Test_SetTestMagx2LowFlowCutoff_Stop();
        }
    }
}

void Test_SetTestMagx2LowFlowCutoff_Start(unsigned long setting)
{
    InitValueCommTestState(&setTestMagx2LowFlowCutoffState, setting);
    setTestMagx2LowFlowCutoffState.testState = TEST_RUNNING;
    MagX2Comm_WriteLowFlowCutoff(Test_SetTestMagx2LowFlowCutoff_DataHandler, setTestMagx2LowFlowCutoffState.value);
}

void Test_SetTestMagx2LowFlowCutoff_Stop(void)
{
    if (setTestMagx2LowFlowCutoffState.testState == TEST_RUNNING)
        setTestMagx2LowFlowCutoffState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetTestMagx2LowFlowCutoff_GetState(void)
{
    return &setTestMagx2LowFlowCutoffState;
}

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu buzeni.
//------------------------------------------------------------------------------
static void Test_SetTestMagx2Excitation_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2ExcitationState.testState = TEST_COMPLETE;
        Test_SetTestMagx2Excitation_Stop();
    }
    else
    {
        setTestMagx2ExcitationState.timeoutCount++;
        if (setTestMagx2ExcitationState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteExcitation(Test_SetTestMagx2Excitation_DataHandler, setTestMagx2ExcitationState.value);
        else
        {
            setTestMagx2ExcitationState.testState = TEST_COMPLETE;
            Test_SetTestMagx2Excitation_Stop();
        }
    }
}

void Test_SetTestMagx2Excitation_Start(unsigned long setting)
{
    InitValueCommTestState(&setTestMagx2ExcitationState, setting);
    setTestMagx2ExcitationState.testState = TEST_RUNNING;
    MagX2Comm_WriteExcitation(Test_SetTestMagx2Excitation_DataHandler, setTestMagx2ExcitationState.value);
}

void Test_SetTestMagx2Excitation_Stop(void)
{
    if (setTestMagx2ExcitationState.testState == TEST_RUNNING)
        setTestMagx2ExcitationState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetTestMagx2Excitation_GetState(void)
{
    return &setTestMagx2ExcitationState;
}

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru proudove smycky.
//------------------------------------------------------------------------------
static void Test_SetTestMagx2CurrentLoopSetting_MinMax_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
        Test_SetTestMagx2CurrentLoopSetting_Stop();
    }
    else
    {
        setTestMagx2CurrentLoopSettingState.timeoutCount++;
        if (setTestMagx2CurrentLoopSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteCurrentLoopSetting_MinMax(Test_SetTestMagx2CurrentLoopSetting_MinMax_DataHandler, &setTestMagx2CurrentLoopSettingState.setting);
        else
        {
            setTestMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
            Test_SetTestMagx2CurrentLoopSetting_Stop();
        }
    }
}

static void Test_SetTestMagx2CurrentLoopSetting_DelayAfterSet_Task(void)
{
    if (RTC_IsSysSecondTimeout(setTestMagx2CurrentLoopSettingState.sysTime, DELAY_BETWEEN_SETTINGS_SEC))
    {
        TaskMan_Unregister(Test_SetTestMagx2CurrentLoopSetting_DelayAfterSet_Task);
        MagX2Comm_WriteCurrentLoopSetting_MinMax(Test_SetTestMagx2CurrentLoopSetting_MinMax_DataHandler, &setTestMagx2CurrentLoopSettingState.setting);
    }
}

static void Test_SetTestMagx2CurrentLoopSetting_Set_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2CurrentLoopSettingState.sysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_SetTestMagx2CurrentLoopSetting_DelayAfterSet_Task);
    }
    else
    {
        setTestMagx2CurrentLoopSettingState.timeoutCount++;
        if (setTestMagx2CurrentLoopSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteCurrentLoopSetting_Set(Test_SetTestMagx2CurrentLoopSetting_Set_DataHandler, &setTestMagx2CurrentLoopSettingState.setting);
        else
        {
            setTestMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
            Test_SetTestMagx2CurrentLoopSetting_Stop();
        }
    }
}

void Test_SetTestMagx2CurrentLoopSetting_Start(const TCurrentLoopSetting *setting)
{
    InitWriteCurrentLoopSettingCommTestState(&setTestMagx2CurrentLoopSettingState, setting);
    setTestMagx2CurrentLoopSettingState.testState = TEST_RUNNING;
    MagX2Comm_WriteCurrentLoopSetting_Set(Test_SetTestMagx2CurrentLoopSetting_Set_DataHandler, &setTestMagx2CurrentLoopSettingState.setting);
}

void Test_SetTestMagx2CurrentLoopSetting_Stop(void)
{
    if (setTestMagx2CurrentLoopSettingState.testState == TEST_RUNNING)
        setTestMagx2CurrentLoopSettingState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_SetTestMagx2CurrentLoopSetting_DelayAfterSet_Task);
    MagX2Comm_StopReceiving();
}

const TCurrentLoopSettingCommTestState* Test_SetTestMagx2CurrentLoopSetting_GetState(void)
{
    return &setTestMagx2CurrentLoopSettingState;
}

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
static void Test_SetTestMagx2FrequencyOutputSetting_DutyCycle_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
        Test_SetTestMagx2FrequencyOutputSetting_Stop();
    }
    else
    {
        setTestMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setTestMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(Test_SetTestMagx2FrequencyOutputSetting_DutyCycle_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
        else
        {
            setTestMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetTestMagx2FrequencyOutputSetting_Stop();
        }
    }
}

static void Test_SetTestMagx2FrequencyOutputSetting_DelayAfterMinMax_Task(void)
{
    if (RTC_IsSysSecondTimeout(setTestMagx2FrequencyOutputSettingState.sysTime, DELAY_BETWEEN_SETTINGS_SEC))
    {
        TaskMan_Unregister(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterMinMax_Task);
        MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(Test_SetTestMagx2FrequencyOutputSetting_DutyCycle_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
    }
}

static void Test_SetTestMagx2FrequencyOutputSetting_MinMax_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2FrequencyOutputSettingState.sysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterMinMax_Task);
    }
    else
    {
        setTestMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setTestMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_MinMax(Test_SetTestMagx2FrequencyOutputSetting_MinMax_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
        else
        {
            setTestMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetTestMagx2FrequencyOutputSetting_Stop();
        }
    }
}

static void Test_SetTestMagx2FrequencyOutputSetting_DelayAfterSet_Task(void)
{
    if (RTC_IsSysSecondTimeout(setTestMagx2FrequencyOutputSettingState.sysTime, DELAY_BETWEEN_SETTINGS_SEC))
    {
        TaskMan_Unregister(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterSet_Task);
        MagX2Comm_WriteFrequencyOutputSetting_MinMax(Test_SetTestMagx2FrequencyOutputSetting_MinMax_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
    }
}

static void Test_SetTestMagx2FrequencyOutputSetting_Set_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setTestMagx2FrequencyOutputSettingState.sysTime = RTC_GetSysSeconds();
        TaskMan_Register(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterSet_Task);
    }
    else
    {
        setTestMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setTestMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_Set(Test_SetTestMagx2FrequencyOutputSetting_Set_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
        else
        {
            setTestMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetTestMagx2FrequencyOutputSetting_Stop();
        }
    }
}

void Test_SetTestMagx2FrequencyOutputSetting_Start(const TFrequencyOutputSetting *setting)
{
    InitWriteFrequencyOutputSettingCommTestState(&setTestMagx2FrequencyOutputSettingState, setting);
    setTestMagx2FrequencyOutputSettingState.testState = TEST_RUNNING;
    MagX2Comm_WriteFrequencyOutputSetting_Set(Test_SetTestMagx2FrequencyOutputSetting_Set_DataHandler, &setTestMagx2FrequencyOutputSettingState.setting);
}

void Test_SetTestMagx2FrequencyOutputSetting_Stop(void)
{
    if (setTestMagx2FrequencyOutputSettingState.testState == TEST_RUNNING)
        setTestMagx2FrequencyOutputSettingState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterSet_Task);
    TaskMan_Unregister(Test_SetTestMagx2FrequencyOutputSetting_DelayAfterMinMax_Task);
    MagX2Comm_StopReceiving();
}

const TFrequencyOutputSettingCommTestState* Test_SetTestMagx2FrequencyOutputSetting_GetState(void)
{
    return &setTestMagx2FrequencyOutputSettingState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu mereni.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2Measurement_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2MeasurementState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2Measurement_Stop();
    }
    else
    {
        setOriginalMagx2MeasurementState.timeoutCount++;
        if (setOriginalMagx2MeasurementState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteMeasurement(Test_SetOriginalMagx2Measurement_DataHandler, setOriginalMagx2MeasurementState.value);
        else
        {
            setOriginalMagx2MeasurementState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2Measurement_Stop();
        }
    }
}

void Test_SetOriginalMagx2Measurement_Start(unsigned long setting)
{
    InitValueCommTestState(&setOriginalMagx2MeasurementState, setting);
    setOriginalMagx2MeasurementState.testState = TEST_RUNNING;
    MagX2Comm_WriteMeasurement(Test_SetOriginalMagx2Measurement_DataHandler, setOriginalMagx2MeasurementState.value);
}

void Test_SetOriginalMagx2Measurement_Stop(void)
{
    if (setOriginalMagx2MeasurementState.testState == TEST_RUNNING)
        setOriginalMagx2MeasurementState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetOriginalMagx2Measurement_GetState(void)
{
    return &setOriginalMagx2MeasurementState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2LowFlowCutoff_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2LowFlowCutoff_Stop();
    }
    else
    {
        setOriginalMagx2LowFlowCutoffState.timeoutCount++;
        if (setOriginalMagx2LowFlowCutoffState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteLowFlowCutoff(Test_SetOriginalMagx2LowFlowCutoff_DataHandler, setOriginalMagx2LowFlowCutoffState.value);
        else
        {
            setOriginalMagx2LowFlowCutoffState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2LowFlowCutoff_Stop();
        }
    }
}

void Test_SetOriginalMagx2LowFlowCutoff_Start(unsigned long setting)
{
    InitValueCommTestState(&setOriginalMagx2LowFlowCutoffState, setting);
    setOriginalMagx2LowFlowCutoffState.testState = TEST_RUNNING;
    MagX2Comm_WriteLowFlowCutoff(Test_SetOriginalMagx2LowFlowCutoff_DataHandler, setOriginalMagx2LowFlowCutoffState.value);
}

void Test_SetOriginalMagx2LowFlowCutoff_Stop(void)
{
    if (setOriginalMagx2LowFlowCutoffState.testState == TEST_RUNNING)
        setOriginalMagx2LowFlowCutoffState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetOriginalMagx2LowFlowCutoff_GetState(void)
{
    return &setOriginalMagx2LowFlowCutoffState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu buzeni.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2Excitation_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2ExcitationState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2Excitation_Stop();
    }
    else
    {
        setOriginalMagx2ExcitationState.timeoutCount++;
        if (setOriginalMagx2ExcitationState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteExcitation(Test_SetOriginalMagx2Excitation_DataHandler, setOriginalMagx2ExcitationState.value);
        else
        {
            setOriginalMagx2ExcitationState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2Excitation_Stop();
        }
    }
}

void Test_SetOriginalMagx2Excitation_Start(unsigned long setting)
{
    InitValueCommTestState(&setOriginalMagx2ExcitationState, setting);
    setOriginalMagx2ExcitationState.testState = TEST_RUNNING;
    MagX2Comm_WriteExcitation(Test_SetOriginalMagx2Excitation_DataHandler, setOriginalMagx2ExcitationState.value);
}

void Test_SetOriginalMagx2Excitation_Stop(void)
{
    if (setOriginalMagx2ExcitationState.testState == TEST_RUNNING)
        setOriginalMagx2ExcitationState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TValueCommTestState* Test_SetOriginalMagx2Excitation_GetState(void)
{
    return &setOriginalMagx2ExcitationState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru proudove smycky.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2CurrentLoopSetting_MinMax_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2CurrentLoopSetting_Stop();
    }
    else
    {
        setOriginalMagx2CurrentLoopSettingState.timeoutCount++;
        if (setOriginalMagx2CurrentLoopSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteCurrentLoopSetting_MinMax(Test_SetOriginalMagx2CurrentLoopSetting_MinMax_DataHandler, &setOriginalMagx2CurrentLoopSettingState.setting);
        else
        {
            setOriginalMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2CurrentLoopSetting_Stop();
        }
    }
}

static void Test_SetOriginalMagx2CurrentLoopSetting_Set_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_WriteCurrentLoopSetting_MinMax(Test_SetOriginalMagx2CurrentLoopSetting_MinMax_DataHandler, &setOriginalMagx2CurrentLoopSettingState.setting);
    }
    else
    {
        setOriginalMagx2CurrentLoopSettingState.timeoutCount++;
        if (setOriginalMagx2CurrentLoopSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteCurrentLoopSetting_Set(Test_SetOriginalMagx2CurrentLoopSetting_Set_DataHandler, &setOriginalMagx2CurrentLoopSettingState.setting);
        else
        {
            setOriginalMagx2CurrentLoopSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2CurrentLoopSetting_Stop();
        }
    }
}

void Test_SetOriginalMagx2CurrentLoopSetting_Start(const TCurrentLoopSetting *setting)
{
    InitWriteCurrentLoopSettingCommTestState(&setOriginalMagx2CurrentLoopSettingState, setting);
    setOriginalMagx2CurrentLoopSettingState.testState = TEST_RUNNING;
    MagX2Comm_WriteCurrentLoopSetting_Set(Test_SetOriginalMagx2CurrentLoopSetting_Set_DataHandler, &setOriginalMagx2CurrentLoopSettingState.setting);
}

void Test_SetOriginalMagx2CurrentLoopSetting_Stop(void)
{
    if (setOriginalMagx2CurrentLoopSettingState.testState == TEST_RUNNING)
        setOriginalMagx2CurrentLoopSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TCurrentLoopSettingCommTestState* Test_SetOriginalMagx2CurrentLoopSetting_GetState(void)
{
    return &setOriginalMagx2CurrentLoopSettingState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2FrequencyOutputSetting_DutyCycle_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2FrequencyOutputSetting_Stop();
    }
    else
    {
        setOriginalMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setOriginalMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(Test_SetOriginalMagx2FrequencyOutputSetting_DutyCycle_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
        else
        {
            setOriginalMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2FrequencyOutputSetting_Stop();
        }
    }
}

static void Test_SetOriginalMagx2FrequencyOutputSetting_MinMax_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(Test_SetOriginalMagx2FrequencyOutputSetting_DutyCycle_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
    }
    else
    {
        setOriginalMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setOriginalMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_MinMax(Test_SetOriginalMagx2FrequencyOutputSetting_MinMax_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
        else
        {
            setOriginalMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2FrequencyOutputSetting_Stop();
        }
    }
}

static void Test_SetOriginalMagx2FrequencyOutputSetting_Set_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_WriteFrequencyOutputSetting_MinMax(Test_SetOriginalMagx2FrequencyOutputSetting_MinMax_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
    }
    else
    {
        setOriginalMagx2FrequencyOutputSettingState.timeoutCount++;
        if (setOriginalMagx2FrequencyOutputSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFrequencyOutputSetting_Set(Test_SetOriginalMagx2FrequencyOutputSetting_Set_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
        else
        {
            setOriginalMagx2FrequencyOutputSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2FrequencyOutputSetting_Stop();
        }
    }
}

void Test_SetOriginalMagx2FrequencyOutputSetting_Start(const TFrequencyOutputSetting *setting)
{
    InitWriteFrequencyOutputSettingCommTestState(&setOriginalMagx2FrequencyOutputSettingState, setting);
    setOriginalMagx2FrequencyOutputSettingState.testState = TEST_RUNNING;
    MagX2Comm_WriteFrequencyOutputSetting_Set(Test_SetOriginalMagx2FrequencyOutputSetting_Set_DataHandler, &setOriginalMagx2FrequencyOutputSettingState.setting);
}

void Test_SetOriginalMagx2FrequencyOutputSetting_Stop(void)
{
    if (setOriginalMagx2FrequencyOutputSettingState.testState == TEST_RUNNING)
        setOriginalMagx2FrequencyOutputSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TFrequencyOutputSettingCommTestState* Test_SetOriginalMagx2FrequencyOutputSetting_GetState(void)
{
    return &setOriginalMagx2FrequencyOutputSettingState;
}

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru simulovaneho toku.
//------------------------------------------------------------------------------
static void Test_SetOriginalMagx2FlowSimulationSetting_Flow_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        setOriginalMagx2FlowSimulationSettingState.testState = TEST_COMPLETE;
        Test_SetOriginalMagx2FlowSimulationSetting_Stop();
    }
    else
    {
        setOriginalMagx2FlowSimulationSettingState.timeoutCount++;
        if (setOriginalMagx2FlowSimulationSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFlowSimulationSetting_Flow(Test_SetOriginalMagx2FlowSimulationSetting_Flow_DataHandler, &setOriginalMagx2FlowSimulationSettingState.setting);
        else
        {
            setOriginalMagx2FlowSimulationSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2FlowSimulationSetting_Stop();
        }
    }
}

static void Test_SetOriginalMagx2FlowSimulationSetting_Demo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        MagX2Comm_WriteFlowSimulationSetting_Flow(Test_SetOriginalMagx2FlowSimulationSetting_Flow_DataHandler, &setOriginalMagx2FlowSimulationSettingState.setting);
    }
    else
    {
        setOriginalMagx2FlowSimulationSettingState.timeoutCount++;
        if (setOriginalMagx2FlowSimulationSettingState.timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            MagX2Comm_WriteFlowSimulationSetting_Demo(Test_SetOriginalMagx2FlowSimulationSetting_Demo_DataHandler, &setOriginalMagx2FlowSimulationSettingState.setting);
        else
        {
            setOriginalMagx2FlowSimulationSettingState.testState = TEST_COMPLETE;
            Test_SetOriginalMagx2FlowSimulationSetting_Stop();
        }
    }
}

void Test_SetOriginalMagx2FlowSimulationSetting_Start(const TFlowSimulationSetting *setting)
{
    InitWriteFlowSimulationSettingCommTestState(&setOriginalMagx2FlowSimulationSettingState, setting);
    setOriginalMagx2FlowSimulationSettingState.testState = TEST_RUNNING;
    MagX2Comm_WriteFlowSimulationSetting_Demo(Test_SetOriginalMagx2FlowSimulationSetting_Demo_DataHandler, &setOriginalMagx2FlowSimulationSettingState.setting);
}

void Test_SetOriginalMagx2FlowSimulationSetting_Stop(void)
{
    if (setOriginalMagx2FlowSimulationSettingState.testState == TEST_RUNNING)
        setOriginalMagx2FlowSimulationSettingState.testState = TEST_STOPPED;

    MagX2Comm_StopReceiving();
}

const TFlowSimulationSettingCommTestState* Test_SetOriginalMagx2FlowSimulationSetting_GetState(void)
{
    return &setOriginalMagx2FlowSimulationSettingState;
}

//------------------------------------------------------------------------------
//  Vrati text vysledku testu.
//------------------------------------------------------------------------------
const char* Test_GetResultText(TTestResult testResult)
{
    switch (testResult)
    {
    case TRES_OK:
        return UI_GetText(TXTID_Pass);
    case TRES_FAIL:
        return UI_GetText(TXTID_Fail);
    default:
        return UI_GetText(TXTID_NA);
    }
}

//------------------------------------------------------------------------------
//  Hlavni test.
//------------------------------------------------------------------------------

static void Test_Main_Stop(void);

const TMainSubTestInfo SUB_TEST_INFOS[] =
{
    /*{ 1 }, // SUBTID_C1_REZISTANCE
    { 1 }, // SUBTID_C2_REZISTANCE
    { 1 }, // SUBTID_E_INSULATION
    { 1 }, // SUBTID_TEMP_SENSOR
    { 1 }, // SUBTID_C_INDUCTANCE
    { 1 }, // SUBTID_EXCITATION_LEVELS*/
    { 1, SUBTID_End }, // SUBTID_Magx2Passwords
    { 1, SUBTID_End }, // SUBTID_Magx2UnitNo
    { 1, SUBTID_End }, // SUBTID_Magx2Firmware
    { 1, SUBTID_End }, // SUBTID_Magx2Measurement
    { 1, SUBTID_End }, // SUBTID_Magx2LowFlowCutoff
    { 1, SUBTID_End }, // SUBTID_Magx2Excitation
    { 1, SUBTID_End }, // SUBTID_Magx2CurrentLoop
    { 1, SUBTID_End }, // SUBTID_Magx2FrequencyOutput
    { 1, SUBTID_End }, // SUBTID_Magx2FlowSimulation
    { 1, SUBTID_End }, // SUBTID_SensorUnitNo
    { 1, SUBTID_End }, // SUBTID_SensorFirmware
    { 1, SUBTID_End }, // SUBTID_SensorDiameter
    { 1, SUBTID_End }, // SUBTID_SensorFlowRange
    { 1, SUBTID_End }, // SUBTID_SensorErrorMin
    { 1, SUBTID_End }, // SUBTID_SensorOkMin
    { 1, SUBTID_End }, // SUBTID_SensorErrorCode
    { 1, SUBTID_End }, // SUBTID_SensorTotals
    { 1, SUBTID_End }, // SUBTID_SensorCalibration
    { 1, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_SetTestMagx2Measurement
    { 1, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_SetTestMagx2LowFlowCutoff
    { 1, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_SetTestMagx2Excitation
    { 1, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_SetTestMagx2CurrentLoop
    { 1, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_SetTestMagx2FrequencyOutput
    { 2 * TEST_FLOW_COUNT, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_FREQUENCY
    { 2 * TEST_FLOW_COUNT, SUBTID_SetOriginalMagx2Measurement }, // SUBTID_IOUT
    { 1, SUBTID_SetOriginalMagx2LowFlowCutoff }, // SUBTID_SetOriginalMagx2Measurement
    { 1, SUBTID_SetOriginalMagx2Excitation }, // SUBTID_SetOriginalMagx2LowFlowCutoff
    { 1, SUBTID_SetOriginalMagx2CurrentLoop }, // SUBTID_SetOriginalMagx2Excitation
    { 1, SUBTID_SetOriginalMagx2FrequencyOutput }, // SUBTID_SetOriginalMagx2CurrentLoop
    { 1, SUBTID_SetOriginalMagx2FlowSimulation }, // SUBTID_SetOriginalMagx2FrequencyOutput
    { 1, SUBTID_End }, // SUBTID_SetOriginalMagx2FlowSimulation
};

static void OnMainTestButtonESC(void)
{
    UI_SetMainMenuWithMainTestItem();
}

static void OnMainTestButtonENTER(void)
{
    UI_SetMenu(MENUID_MainTestData, 0);
}

static void Test_Main_SetCompleteUI(void)
{
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);
    UI_SetDisplayButton(UI_BTN_ENTER, TRUE);

    Buttons_SetHandler(BTN_ESC, OnMainTestButtonESC);
    Buttons_SetHandler(BTN_ENTER, OnMainTestButtonENTER);
}

static void Test_Main_UpdateProgress(void)
{
    mainState.actualProgressSteps += SUB_TEST_INFOS[mainState.subTestId].progressStep;
    mainState.progressPercent = mainState.actualProgressSteps * 100 / mainState.allProgressSteps;
}

static BOOL IsTestValuePass(float expectedValue, float realValue, float passRatio)
{
    float diff = expectedValue - realValue;

    if (diff < 0)
        diff = -diff;

    if (expectedValue < 0)
        expectedValue = -expectedValue;

    if ((diff / expectedValue) <= passRatio)
        return TRUE;
    else
        return FALSE;
}

static TTestResult CalculateErrorCodeTestResult(const TMainTestData *data)
{
    if (data->errorCode > 1) // ERR_EMPTY_PIPE  (1<<0) je jeste ok
    {
        return TRES_FAIL;
    }

    return TRES_OK;
}

static TTestResult CalculateFrequencyTestResult(const TMainTestData *data)
{
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
    {
        if (!IsTestValuePass(EXPECTED_FREQUENCIES[i], data->frequency[i], TEST_FO_PASS_RATIO))
            return TRES_FAIL;
    }

    return TRES_OK;
}

static TTestResult CalculateIOutTestResult(const TMainTestData *data)
{
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
    {
        if (!IsTestValuePass(EXPECTED_CURRENTS[i], data->iOut[i] / 1000.0, TEST_CL_PASS_RATIO))
            return TRES_FAIL;
    }

    return TRES_OK;
}

static void CalculateTestResult(TMainTestData *data)
{
    data->testResult = TRES_OK;

    if (data->errorCodeTestResult == TRES_OK)
    {
        data->errorCodeTestResult = CalculateErrorCodeTestResult(data);
        if (data->errorCodeTestResult != TRES_OK)
            data->testResult = TRES_FAIL;
    }

    if (data->frequencyTestResult == TRES_OK)
    {
        data->frequencyTestResult = CalculateFrequencyTestResult(data);
        if (data->frequencyTestResult != TRES_OK)
            data->testResult = TRES_FAIL;
    }

    if (data->iOutTestResult == TRES_OK)
    {
        data->iOutTestResult = CalculateIOutTestResult(data);
        if (data->iOutTestResult != TRES_OK)
            data->testResult = TRES_FAIL;
    }
}

static BOOL Test_Main_IsRequestToStop(void)
{
    return mainState.requestToStop;
}

// Nastaveni dalsiho dilciho testu po zadosti o zastaveni hlavniho testu
static void Test_Main_SetSubTestAfterStopRequest(void)
{
    mainState.subTestId = SUB_TEST_INFOS[mainState.subTestId].subTestAfterStopRequest;
}

// Nastaveni dalsiho dilciho testu v zavislosti na stavu zadosti o zastaveni hlavniho testu
static void Test_Main_SetNextSubTest(void)
{
    if (Test_Main_IsRequestToStop())
        Test_Main_SetSubTestAfterStopRequest();
    else
        mainState.subTestId++;
}

static void Test_Main_Task(void)
{
    if (!RTC_IsSysSecondTimeout(mainState.lastSysTime, DELAY_BETWEEN_TESTS_SEC))
        return;

    /*if (mainState.subTestId == SUBTID_C1_REZISTANCE)
    {
        const TADTestState* pState = Test_Meas2C1_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Meas2C1_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            mainState.data.c1Rezistance = (unsigned int)pState->adSampleAvg;
            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }

    if (mainState.subTestId == SUBTID_C2_REZISTANCE)
    {
        const TADTestState* pState = Test_Meas2C2_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Meas2C2_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            mainState.data.c2Rezistance = (unsigned int)pState->adSampleAvg;
            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }

    if (mainState.subTestId == SUBTID_E_INSULATION)
    {
        const TADTestState* pState = Test_Meas1E1_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Meas1E1_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            mainState.data.eInsulation = (unsigned int)pState->adSampleAvg;
            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }

    if (mainState.subTestId == SUBTID_TEMP_SENSOR)
    {
        const TADTestState* pState = Test_Meas1Temp_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Meas1Temp_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            mainState.data.tempSensor = (unsigned int)pState->adSampleAvg;
            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }

    if (mainState.subTestId == SUBTID_C_INDUCTANCE)
    {
        const TCInductanceTestState* pState = Test_CInductance_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_CInductance_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            // prevod na mikrohenry
            mainState.data.cInductance = pState->inductanceInHenry > 0 ?
            (unsigned long)(pState->inductanceInHenry * 1000000) : 0;

            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }

    if (mainState.subTestId == SUBTID_EXCITATION_LEVELS)
    {
        const TExciteInTestState* pState = Test_ExcitationIn_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ExcitationIn_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            mainState.data.L1 = (unsigned int)pState->L1SampleState.adSampleAvg;
            mainState.data.L2 = (unsigned int)pState->L2SampleState.adSampleAvg;
            mainState.data.L3 = (unsigned int)pState->L3SampleState.adSampleAvg;

            Test_Main_UpdateProgress(mainState.subTestId);
            mainState.subTestId++;
        }

        return;
    }*/

    if (mainState.subTestId == SUBTID_Magx2Passwords)
    {
        const TWriteValueCommTestState* pState = Test_WriteMagx2Passwords_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_WriteMagx2Passwords_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Magx2Passwords);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2UnitNo)
    {
        const TReadValueCommTestState* pState = Test_ReadMagx2UnitNo_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2UnitNo_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.magx2UnitNo = pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Magx2UnitNo);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2Firmware)
    {
        const TReadValueCommTestState* pState = Test_ReadMagx2Firmware_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2Firmware_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.magx2Firmware = (unsigned int)pState->readValue;

                if (mainState.data.magx2Firmware >= MIN_MAGX2_FIRMWARE)
                {
                    Test_Main_UpdateProgress();
                    Test_Main_SetNextSubTest();
                }
                else
                {
                    mainState.testResult = TRES_UNSUPPORTED;
                    mainState.errText = UI_GetText(TXTID_Magx2Firmware);
                    Test_Main_SetSubTestAfterStopRequest();
                }
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Magx2Firmware);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2Measurement)
    {
        const TValueCommTestState* pState = Test_ReadMagx2Measurement_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2Measurement_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.measurement = pState->value;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Measurement);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2LowFlowCutoff)
    {
        const TValueCommTestState* pState = Test_ReadMagx2LowFlowCutoff_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2LowFlowCutoff_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.lowFlowCutoff = pState->value;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_LowFlowCutoff);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2Excitation)
    {
        const TValueCommTestState* pState = Test_ReadMagx2Excitation_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2Excitation_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.excitation = pState->value;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Excitation);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2CurrentLoop)
    {
        const TCurrentLoopSettingCommTestState* pState = Test_ReadMagx2CurrentLoopSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2CurrentLoopSetting_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.currentLoopSetting.currentSet = pState->setting.currentSet;
                mainState.data.originalTestSetting.currentLoopSetting.flowMin = pState->setting.flowMin;
                mainState.data.originalTestSetting.currentLoopSetting.flowMax = pState->setting.flowMax;
                mainState.data.originalTestSetting.currentLoopSetting.currentMin = pState->setting.currentMin;
                mainState.data.originalTestSetting.currentLoopSetting.currentMax = pState->setting.currentMax;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_CurrentLoopSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2FrequencyOutput)
    {
        const TFrequencyOutputSettingCommTestState* pState = Test_ReadMagx2FrequencyOutputSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2FrequencyOutputSetting_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.frequencyOutputSetting.frequencySet = pState->setting.frequencySet;
                mainState.data.originalTestSetting.frequencyOutputSetting.flowMin = pState->setting.flowMin;
                mainState.data.originalTestSetting.frequencyOutputSetting.flowMax = pState->setting.flowMax;
                mainState.data.originalTestSetting.frequencyOutputSetting.frequencyMin = pState->setting.frequencyMin;
                mainState.data.originalTestSetting.frequencyOutputSetting.frequencyMax = pState->setting.frequencyMax;
                mainState.data.originalTestSetting.frequencyOutputSetting.dutyCycle = pState->setting.dutyCycle;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FrequencyOutputSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_Magx2FlowSimulation)
    {
        const TFlowSimulationSettingCommTestState* pState = Test_ReadMagx2FlowSimulationSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadMagx2FlowSimulationSetting_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.originalTestSetting.flowSimulationSetting.demo = pState->setting.demo;
                mainState.data.originalTestSetting.flowSimulationSetting.simulatedFlow = pState->setting.simulatedFlow;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FlowSimulationSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorUnitNo)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorUnitNo_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorUnitNo_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.sensorUnitNo = pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_SensorUnitNo);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorFirmware)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorFirmware_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorFirmware_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.sensorFirmware = (unsigned int)pState->readValue;

                if (mainState.data.sensorFirmware >= MIN_SENSOR_FIRMWARE)
                {
                    Test_Main_UpdateProgress();
                    Test_Main_SetNextSubTest();
                }
                else
                {
                    mainState.testResult = TRES_UNSUPPORTED;
                    mainState.errText = UI_GetText(TXTID_SensorFirmware);
                    Test_Main_SetSubTestAfterStopRequest();
                }
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_SensorFirmware);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorDiameter)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorDiameter_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorDiameter_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.diameter = (unsigned int)pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Diameter);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorFlowRange)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorFlowRange_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorFlowRange_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.flowRange = (unsigned int)pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FlowRange);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorErrorMin)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorErrorMin_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorErrorMin_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.errorMin = pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_ErrorMin);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorOkMin)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorOkMin_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorOkMin_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.okMin = pState->readValue;
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_OkMin);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorErrorCode)
    {
        const TReadValueCommTestState* pState = Test_ReadSensorErrorCode_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorErrorCode_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.errorCode = pState->readValue;
                mainState.data.errorCodeTestResult = TRES_OK;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_ErrorCode);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorTotals)
    {
        const TReadTotalsCommTestState* pState = Test_ReadSensorTotals_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorTotals_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.total = pState->readTotals.total / 1000000.0;
                mainState.data.totalPlus = pState->readTotals.flow_pos / 1000000.0;
                mainState.data.totalMinus = pState->readTotals.flow_neg / 1000000.0;
                mainState.data.aux = pState->readTotals.aux / 1000000.0;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Totals);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SensorCalibration)
    {
        const TReadCalibrationCommTestState* pState = Test_ReadSensorCalibration_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_ReadSensorCalibration_Start();
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                mainState.data.calibrationPoint1 = pState->readCalibration.cp1;
                mainState.data.calibrationPoint2 = pState->readCalibration.cp2;
                mainState.data.calibrationPoint3 = pState->readCalibration.cp3;
                mainState.data.calibrationData1 = pState->readCalibration.mp1;
                mainState.data.calibrationData2 = pState->readCalibration.mp2;
                mainState.data.calibrationData3 = pState->readCalibration.mp3;

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Calibration);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetTestMagx2Measurement)
    {
        const TValueCommTestState* pState = Test_SetTestMagx2Measurement_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetTestMagx2Measurement_Start(TEST_PARAM_MEASUREMENT);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Measurement);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetTestMagx2LowFlowCutoff)
    {
        const TValueCommTestState* pState = Test_SetTestMagx2LowFlowCutoff_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetTestMagx2LowFlowCutoff_Start(TEST_PARAM_LOW_FLOW_CUTOFF);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_LowFlowCutoff);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetTestMagx2Excitation)
    {
        const TValueCommTestState* pState = Test_SetTestMagx2Excitation_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetTestMagx2Excitation_Start(TEST_PARAM_EXCITATION);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Excitation);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetTestMagx2CurrentLoop)
    {
        const TCurrentLoopSettingCommTestState* pState = Test_SetTestMagx2CurrentLoopSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            TCurrentLoopSetting setting;
            setting.currentSet = TEST_PARAM_CL_CURRENT_SET;
            setting.flowMin = TEST_PARAM_CL_FLOW_MIN;
            setting.flowMax = TEST_PARAM_CL_FLOW_MAX;
            setting.currentMin = TEST_PARAM_CL_CURRENT_MIN;
            setting.currentMax = TEST_PARAM_CL_CURRENT_MAX;

            Test_SetTestMagx2CurrentLoopSetting_Start(&setting);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_CurrentLoopSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetTestMagx2FrequencyOutput)
    {
        const TFrequencyOutputSettingCommTestState* pState = Test_SetTestMagx2FrequencyOutputSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            TFrequencyOutputSetting setting;
            setting.frequencySet = TEST_PARAM_FO_FREQUENCY_SET;
            setting.flowMin = TEST_PARAM_FO_FLOW_MIN;
            setting.flowMax = TEST_PARAM_FO_FLOW_MAX;
            setting.frequencyMin = TEST_PARAM_FO_FREQUENCY_MIN;
            setting.frequencyMax = TEST_PARAM_FO_FREQUENCY_MAX;
            setting.dutyCycle = TEST_PARAM_FO_DUTY_CYCLE;

            Test_SetTestMagx2FrequencyOutputSetting_Start(&setting);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FrequencyOutputSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_FREQUENCY)
    {
        const TFrequencyTestState* pState = Test_Frequency_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Frequency_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if ((pState->testResult == TRES_OK) || ((pState->testResult == TRES_UNKNOWN)))
            {
                mainState.data.frequencyTestResult = pState->testResult;
                if (pState->testResult == TRES_OK)
                {
                    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
                        mainState.data.frequency[i] = (unsigned int)pState->frequency[i];
                }

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Frequency);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_IOUT)
    {
        const TIoutTestState* pState = Test_Iout_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_Iout_Start(TEST_TYPE_LIMITED);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if ((pState->testResult == TRES_OK) || ((pState->testResult == TRES_UNKNOWN)))
            {
                mainState.data.iOutTestResult = pState->testResult;
                if (pState->testResult == TRES_OK)
                {
                    // prevod na mA
                    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
                        mainState.data.iOut[i] = pState->currentInAmpere[i] > 0 ?
                            (unsigned int)(pState->currentInAmpere[i] * 1000000) : 0;
                }

                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Iout);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2Measurement)
    {
        const TValueCommTestState* pState = Test_SetOriginalMagx2Measurement_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2Measurement_Start(mainState.data.originalTestSetting.measurement);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Measurement);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2LowFlowCutoff)
    {
        const TValueCommTestState* pState = Test_SetOriginalMagx2LowFlowCutoff_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2LowFlowCutoff_Start(mainState.data.originalTestSetting.lowFlowCutoff);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_LowFlowCutoff);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2Excitation)
    {
        const TValueCommTestState* pState = Test_SetOriginalMagx2Excitation_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2Excitation_Start(mainState.data.originalTestSetting.excitation);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_Excitation);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2CurrentLoop)
    {
        const TCurrentLoopSettingCommTestState* pState = Test_SetOriginalMagx2CurrentLoopSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2CurrentLoopSetting_Start(&mainState.data.originalTestSetting.currentLoopSetting);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_CurrentLoopSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2FrequencyOutput)
    {
        const TFrequencyOutputSettingCommTestState* pState = Test_SetOriginalMagx2FrequencyOutputSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2FrequencyOutputSetting_Start(&mainState.data.originalTestSetting.frequencyOutputSetting);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FrequencyOutputSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == SUBTID_SetOriginalMagx2FlowSimulation)
    {
        const TFlowSimulationSettingCommTestState* pState = Test_SetOriginalMagx2FlowSimulationSetting_GetState();
        if (pState->testState == TEST_STOPPED)
        {
            Test_SetOriginalMagx2FlowSimulationSetting_Start(&mainState.data.originalTestSetting.flowSimulationSetting);
        }
        else if (pState->testState == TEST_COMPLETE)
        {
            if (pState->timeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            {
                Test_Main_UpdateProgress();
                Test_Main_SetNextSubTest();
                mainState.lastSysTime = RTC_GetSysSeconds();
            }
            else
            {
                mainState.testResult = TRES_TIMEOUT;
                mainState.errText = UI_GetText(TXTID_FlowSimulationSetting);
                Test_Main_SetSubTestAfterStopRequest();
            }
        }

        return;
    }

    if (mainState.subTestId == MAIN_SUB_TEST_COUNT)
    {
        mainState.data.id = RAM_Read_Data_Long(TEST_ID);
        mainState.data.date = RTC_GetDateBCD();

        if (!Test_Main_IsRequestToStop()) // zda nebylo vyzadano preruseni testu
        {
            if (mainState.testResult == TRES_OK)
            {
                CalculateTestResult(&mainState.data);
                unsigned saveTestResult = StoreMan_SaveTest(&mainState.data);
                if (saveTestResult != 0)
                {
                    if (saveTestResult == 1)
                    {
                        mainState.testResult = TRES_ERROR;
                        mainState.errText = UI_GetText(TXTID_PrepareSector);
                    }
                    else if (saveTestResult == 2)
                    {
                        mainState.testResult = TRES_ERROR;
                        mainState.errText = UI_GetText(TXTID_WriteData);
                    }
                    else if (saveTestResult == 3)
                    {
                        mainState.testResult = TRES_ERROR;
                        mainState.errText = UI_GetText(TXTID_CompareData);
                    }
                }
            }
        }

        mainState.testState = TEST_COMPLETE;
        Test_Main_Stop();
        Test_Main_SetCompleteUI();
    }
}

static unsigned long CalculateAllProgressSteps(void)
{
    unsigned long steps = 0;

    for (unsigned int i = 0; i < MAIN_SUB_TEST_COUNT; i++)
    {
        steps += SUB_TEST_INFOS[i].progressStep;
    }

    return steps;
}

static void InitTestSetting(TTestSetting *testSetting)
{
    testSetting->measurement = 0;
    testSetting->lowFlowCutoff = 0;
    testSetting->excitation = 0;
    InitCurrentLoopSetting(&testSetting->currentLoopSetting);
    InitFrequencyOutputSetting(&testSetting->frequencyOutputSetting);
    InitFlowSimulationSetting(&testSetting->flowSimulationSetting);
}

static void Test_InitMainTestData(TMainTestData *data)
{
    data->testResult = TRES_OK;

    data->magx2UnitNo = 0;
    data->magx2Firmware = 0;

    data->sensorUnitNo = 0;
    data->sensorFirmware = 0;
    data->diameter = 0;
    data->flowRange = 0;
    data->errorMin = 0;
    data->okMin = 0;
    data->errorCode = 0;

    // errorCode test result
    data->errorCodeTestResult = TRES_OK;

    data->total = 0;
    data->totalPlus = 0;
    data->totalMinus = 0;
    data->aux = 0;

    data->calibrationPoint1 = 0;
    data->calibrationPoint2 = 0;
    data->calibrationPoint3 = 0;
    data->calibrationData1 = 0;
    data->calibrationData2 = 0;
    data->calibrationData3 = 0;

    // rezistance C1
    data->c1Rezistance = 0;

    // rezistance C2
    data->c2Rezistance = 0;

    // Electrodes insulation
    data->eInsulation = 0;

    // temp sensor
    data->tempSensor = 0;

    // coil inductance
    data->cInductance = 0;

    // excitation levels
    data->L1 = 0;
    data->L2 = 0;
    data->L3 = 0;

    // testovaci prutoky
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        data->testFlows[i] = TEST_FLOWS[i];

    // frequency
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        data->frequency[i] = 0;

    // frequency test result
    data->frequencyTestResult = TRES_OK;

    // Iout
    for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        data->iOut[i] = 0;

    // Iout test result
    data->iOutTestResult = TRES_OK;

    // puvodni nastaveni pred testem
    InitTestSetting(&data->originalTestSetting);
}

static void Test_InitMainTestState(void)
{
    mainState.testState = TEST_STOPPED;
    mainState.testResult = TRES_OK;
    mainState.requestToStop = FALSE;
    mainState.errText = UI_GetText(TXTID_EmptyString);
    mainState.subTestId = 0;
    mainState.progressPercent = 0;
    mainState.actualProgressSteps = 0;
    mainState.allProgressSteps = CalculateAllProgressSteps();
    mainState.lastSysTime = RTC_GetSysSeconds();
    Test_InitMainTestData(&mainState.data);
}

void Test_Main_Start(void)
{
    Test_InitMainTestState();
    Test_InitSubTestStates();
    mainState.testState = TEST_RUNNING;
    TaskMan_Register(Test_Main_Task);
}

void Test_Main_WantStop(void)
{
    mainState.requestToStop = TRUE;
}

static void Test_Main_Stop(void)
{
    if (mainState.testState == TEST_RUNNING)
        mainState.testState = TEST_STOPPED;

    TaskMan_Unregister(Test_Main_Task);
}

const TMainTestState* Test_Main_GetState(void)
{
    return &mainState;
}

//------------------------------------------------------------------------------
//  Inicializace dilcich testu.
//------------------------------------------------------------------------------
static void Test_InitSubTestStates(void)
{
    InitADTestState(&meas2C1State);
    InitADTestState(&meas2C2State);
    InitADTestState(&meas1E1State);
    InitADTestState(&meas1TempState);
    Test_InitCInductanceTestState();
    Test_InitExcitationInTestState();
    Test_InitFrequencyTestState();
    Test_InitIoutTestState();
    InitWriteValueCommTestState(&writeMagx2PasswordsState);
    InitReadValueCommTestState(&readMagx2UnitNoState);
    InitReadValueCommTestState(&readMagx2FirmwareState);
    InitValueCommTestState(&readMagx2MeasurementState, 0);
    InitValueCommTestState(&readMagx2LowFlowCutoffState, 0);
    InitValueCommTestState(&readMagx2ExcitationState, 0);
    InitReadCurrentLoopSettingCommTestState(&readMagx2CurrentLoopSettingState);
    InitReadFrequencyOutputSettingCommTestState(&readMagx2FrequencyOutputSettingState);
    InitReadFlowSimulationSettingCommTestState(&readMagx2FlowSimulationSettingState);
    InitReadValueCommTestState(&readSensorUnitNoState);
    InitReadValueCommTestState(&readSensorFirmwareState);
    InitReadValueCommTestState(&readSensorDiameterState);
    InitReadValueCommTestState(&readSensorFlowRangeState);
    InitReadValueCommTestState(&readSensorErrorMinState);
    InitReadValueCommTestState(&readSensorOkMinState);
    InitReadValueCommTestState(&readSensorErrorCodeState);
    InitReadTotalsCommTestState(&readSensorTotalsState);
    InitReadCalibrationCommTestState(&readSensorCalibrationState);
    InitValueCommTestState(&setTestMagx2MeasurementState, readMagx2MeasurementState.value);
    InitValueCommTestState(&setTestMagx2LowFlowCutoffState, readMagx2LowFlowCutoffState.value);
    InitValueCommTestState(&setTestMagx2ExcitationState, readMagx2ExcitationState.value);
    InitWriteCurrentLoopSettingCommTestState(&setTestMagx2CurrentLoopSettingState, &readMagx2CurrentLoopSettingState.setting);
    InitWriteFrequencyOutputSettingCommTestState(&setTestMagx2FrequencyOutputSettingState, &readMagx2FrequencyOutputSettingState.setting);
    InitValueCommTestState(&setOriginalMagx2MeasurementState, readMagx2MeasurementState.value);
    InitValueCommTestState(&setOriginalMagx2LowFlowCutoffState, readMagx2LowFlowCutoffState.value);
    InitValueCommTestState(&setOriginalMagx2ExcitationState, readMagx2ExcitationState.value);
    InitWriteCurrentLoopSettingCommTestState(&setOriginalMagx2CurrentLoopSettingState, &readMagx2CurrentLoopSettingState.setting);
    InitWriteFrequencyOutputSettingCommTestState(&setOriginalMagx2FrequencyOutputSettingState, &readMagx2FrequencyOutputSettingState.setting);
    InitWriteFlowSimulationSettingCommTestState(&setOriginalMagx2FlowSimulationSettingState, &readMagx2FlowSimulationSettingState.setting);
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Test_Init(void)
{
    Test_InitMainTestState();
    Test_InitSubTestStates();
}
