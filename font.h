#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#define FULL_CHARACTER_WIDTH         8

unsigned char GetCharacterWidth(unsigned int c);
unsigned char GetFont(unsigned int c);
unsigned char GetSpecialCharacterWidth(unsigned int c);
unsigned char GetSpecialFont(unsigned int c);

#endif // FONT_H_INCLUDED
