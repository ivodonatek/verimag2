//******************************************************************************
//  Management tlacitek.
//*****************************************************************************/

#include "buttons.h"
#include "screen.h"
#include "LPC23xx.h"

// Fw module state
static struct
{
    // posledni scan kod tlacitek
    TButtonScanCode lastScanCode;

    // funkce pro volani po stisku kazdeho tlacitka.
    TButtonHandlers buttonHandlers;

} state;

//------------------------------------------------------------------------------
//  Prevod scan kodu na tlacitka.
//------------------------------------------------------------------------------
const TButton BUTTON_SCAN_CODE_TABLE[] =
{
    BTN_ENTER, BTN_RIGHT, BTN_DOWN, BTN_LEFT, BTN_UP, BTN_ESC
};

//------------------------------------------------------------------------------
//  Scan buttons state.
//------------------------------------------------------------------------------
static TButtonScanCode Buttons_Scan(void)
{
    TButtonScanCode scanCode = BTC_NONE;

    if (IOPIN1 & (1<<4))
    {
        scanCode = 0;

        if (IOPIN1 & (1<<8))
            scanCode |= (1<<0);
        if (IOPIN1 & (1<<9))
            scanCode |= (1<<1);
        if (IOPIN1 & (1<<10))
            scanCode |= (1<<2);

        if (scanCode > BUTTON_SCAN_CODE_MAX)
            scanCode = BTC_NONE;
    }

    return scanCode;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Buttons_Init(void)
{
    state.lastScanCode = BTC_NONE;
    Buttons_InitHandlers();
}

//------------------------------------------------------------------------------
//  Hlavni uloha tlacitek, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Buttons_Task(void)
{
    TButtonScanCode scanCode = Buttons_Scan();
    if (scanCode != state.lastScanCode)
    {
        state.lastScanCode = scanCode;
        if (scanCode != BTC_NONE)
        {
            TButton button = BUTTON_SCAN_CODE_TABLE[scanCode];
            TButtonHandler handler = state.buttonHandlers[button];
            if (handler != NO_BUTTON_HANDLER)
                handler();
        }
    }
}

//------------------------------------------------------------------------------
//  Inicializace (vymazani) vsech obsluznych rutin stisku tlacitek.
//------------------------------------------------------------------------------
void Buttons_InitHandlers(void)
{
    for (unsigned int i = 0; i < BUTTONS_COUNT; i++)
    {
        state.buttonHandlers[i] = NO_BUTTON_HANDLER;
    }
}

//------------------------------------------------------------------------------
//  Nastaveni obsluzne rutiny stisku tlacitka.
//------------------------------------------------------------------------------
void Buttons_SetHandler(TButton button, TButtonHandler handler)
{
    state.buttonHandlers[button] = handler;
}

//------------------------------------------------------------------------------
//  Nastaveni obsluznych rutin stisku tlacitek.
//------------------------------------------------------------------------------
void Buttons_SetHandlers(const TButtonHandlers handlers)
{
    for (unsigned int i = 0; i < BUTTONS_COUNT; i++)
    {
        state.buttonHandlers[i] = (TButtonHandler)handlers[i];
    }
}

//------------------------------------------------------------------------------
//  Vrati, zda je nastavena nejaka obsluzna rutina pro dane tlacitko.
//------------------------------------------------------------------------------
BOOL Buttons_IsHandlerSet(TButton button)
{
    if (state.buttonHandlers[button] != NO_BUTTON_HANDLER)
        return TRUE;
    else
        return FALSE;
}

//------------------------------------------------------------------------------
//  Vrati scan kod tlacitek.
//------------------------------------------------------------------------------
TButtonScanCode Buttons_GetScanCode(void)
{
    return Buttons_Scan();
}
