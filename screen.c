//******************************************************************************
//  Management obrazovek.
//*****************************************************************************/

#include "screen.h"
#include "maine.h"
#include "display.h"
#include "font.h"
#include "images.h"
#include "rtc.h"
#include "rprintf.h"
#include "buttons.h"
#include "ui.h"
#include "ui_texts.h"
#include "sensor_master.h"
#include "test.h"
#include "menu_tables.h"
#include "sbl_config.h"
#include "power_man.h"

#define PROGRESS_BAR_MARGIN_LEFT    8
#define PROGRESS_BAR_MARGIN_RIGHT   PROGRESS_BAR_MARGIN_LEFT
#define PROGRESS_BAR_WIDTH          (NUMBER_OF_COLUMNS - PROGRESS_BAR_MARGIN_LEFT - PROGRESS_BAR_MARGIN_RIGHT)

#define MAIN_TEST_DATA_TITLE_LINE   3
#define MAIN_TEST_DATA_VALUE_LINE   (MAIN_TEST_DATA_TITLE_LINE + 1)

// Typ - prototyp funkce, ktera je volana pro vykreslovani aktualni obrazovky.
typedef void (*TScreenProcedure) (void);

// zadna procedura vykreslovani aktualni obrazovky
#define NO_SCREEN_PROCEDURE   ((TScreenProcedure)0)

// Fw module state
static struct
{
    // povoleni obnovy aktualni obrazovky
    BOOL refreshEnabled;

    // procedura vykreslovani aktualni obrazovky
    TScreenProcedure screenProcedure;

} state;

//------------------------------------------------------------------------------
//  Nastaveni aktualni obrazovky.
//------------------------------------------------------------------------------
static void SetActualScreen(TScreenProcedure actualScreenProc)
{
    state.screenProcedure = actualScreenProc;
}

//------------------------------------------------------------------------------
//  Zapsani data a casu k vykresleni
//------------------------------------------------------------------------------
static void WriteDateTime(unsigned char x, unsigned char line)
{
    const TDateTime *pDateTime = RTC_GetDateTime();
    char str[20];

    sprintf(str, "%02d:%02d:%02d", pDateTime->hour, pDateTime->minute, pDateTime->second);
    Write(x, line, str);
}

//------------------------------------------------------------------------------
//  Zapsani stavu baterie k vykresleni
//------------------------------------------------------------------------------
static void WriteBattery(unsigned char x, unsigned char line)
{
    char str[3];

    TBatteryStatus bs = PowerMan_GetBatteryStatus();
    if (bs == BATS_LOW_BATTERY)
    {
        str[0] = 5;
        str[1] = 10;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_MEDIUM_BATTERY)
    {
        str[0] = 5;
        str[1] = 9;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_HIGH_BATTERY)
    {
        str[0] = 5;
        str[1] = 8;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_CHARGING)
    {
        str[0] = 27;
        str[1] = 0;
        Write(x, line, str);
    }
}

//------------------------------------------------------------------------------
//  Zapsani progress baru k vykresleni
//------------------------------------------------------------------------------
static void WriteProgressBar(unsigned char line, unsigned char progressPercent)
{
    char str[2];
    str[0] = 17;
    str[1] = 0;

    unsigned char progressColumns = PROGRESS_BAR_WIDTH * progressPercent / 100;

    for (unsigned i = 0; i < progressColumns; i++)
    {
        Write(PROGRESS_BAR_MARGIN_LEFT + i, line, str);
    }
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka MENU k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonMENU(void)
{
    char str[2];
    str[0] = 1;
    str[1] = 0;
    Write(LAST_COLUMN - SizeInPixel(str), LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka ENTER k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonENTER(void)
{
    char str[2];
    str[0] = 2;
    str[1] = 0;
    Write(LAST_COLUMN - SizeInPixel(str), LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka ESC k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonESC(void)
{
    char str[2];
    str[0] = 3;
    str[1] = 0;
    Write(0, LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka UP k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonUP(void)
{
    char str[2];
    str[0] = 19;
    str[1] = 0;
    Write(55, LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka DOWN k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonDOWN(void)
{
    char str[2];
    str[0] = 20;
    str[1] = 0;
    Write(63, LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka LEFT k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonLEFT(void)
{
    char str[2];
    str[0] = 18;
    str[1] = 0;
    Write(28, LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani tlacitka RIGHT k vykresleni
//------------------------------------------------------------------------------
static void WriteButtonRIGHT(void)
{
    char str[2];
    str[0] = 15;
    str[1] = 0;
    Write(92, LAST_ROW, str);
}

//------------------------------------------------------------------------------
//  Zapsani hlavni horni stavove listy k vykresleni
//------------------------------------------------------------------------------
static void WriteMainTopBar(void)
{
    WriteBattery(0, 0);
    WriteCenter(0, UI_GetText(TXTID_TestID));
    WriteDateTime(86, 0);
}

//------------------------------------------------------------------------------
//  Zapsani druhe hlavni horni stavove listy k vykresleni
//------------------------------------------------------------------------------
static void WriteMainSecondTopBar(void)
{
    char str[20];

    sprintf(str, "%ld", RAM_Read_Data_Long(TEST_ID));
    WriteCenter(1, str);
}

//------------------------------------------------------------------------------
//  Zapsani horni stavove listy testu k vykresleni
//------------------------------------------------------------------------------
static void WriteTestTopBar(void)
{
    WriteBattery(0, 0);
    WriteDateTime(86, 0);
}

//------------------------------------------------------------------------------
//  Zapsani hlavni dolni stavove listy (tlacitka) k vykresleni
//------------------------------------------------------------------------------
static void WriteMainBottomBar(void)
{
    const TUIState *uiState = UI_GetState();

    if (uiState->displayButtons[UI_BTN_ESC])
        WriteButtonESC();

    if (uiState->displayButtons[UI_BTN_LEFT])
        WriteButtonLEFT();

    if (uiState->displayButtons[UI_BTN_UP])
        WriteButtonUP();

    if (uiState->displayButtons[UI_BTN_DOWN])
        WriteButtonDOWN();

    if (uiState->displayButtons[UI_BTN_RIGHT])
        WriteButtonRIGHT();

    if (uiState->displayButtons[UI_BTN_ENTER])
        WriteButtonENTER();

    if (uiState->displayButtons[UI_BTN_MENU])
        WriteButtonMENU();
}

//------------------------------------------------------------------------------
//  Zapsani menu polozek k vykresleni
//------------------------------------------------------------------------------
static void WriteMenuItems(void)
{
    const TUIState *uiState = UI_GetState();
    unsigned int firstMenuItem = uiState->menuState.firstMenuItem;
    unsigned int itemCount = uiState->menuState.menu->itemCount;
    if (itemCount > SCREEN_MENU_ITEM_COUNT)
        itemCount = SCREEN_MENU_ITEM_COUNT;

    for (unsigned int i = 0; i < itemCount; i++)
    {
        Write(0, SCREEN_MENU_ITEM_FIRST_ROW + i, UI_GetText(uiState->menuState.menu->items[firstMenuItem + i].textId));
    }

    unsigned int selectedLine = uiState->menuState.selectedMenuItem - firstMenuItem + SCREEN_MENU_ITEM_FIRST_ROW;
    InvertLine( 0, (unsigned char)selectedLine, NUMBER_OF_COLUMNS );
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Screen_Init(void)
{
    state.refreshEnabled = TRUE;
    state.screenProcedure = NO_SCREEN_PROCEDURE;
}

//------------------------------------------------------------------------------
//  Hlavni uloha obrazovek, volat pravidelne. Po volani se prekresli aktualni
//  obrazovka (pokud je nejaka nastavena a pokud je prekreslovani povoleno).
//------------------------------------------------------------------------------
void Screen_Task(void)
{
    if (!state.refreshEnabled)
        return;

    if (state.screenProcedure != NO_SCREEN_PROCEDURE)
        state.screenProcedure();
}

//------------------------------------------------------------------------------
//  Povoleni/zakaz obnovovani obrazovky, defaultne je povoleno.
//------------------------------------------------------------------------------
void Screen_RefreshEnable(BOOL enable)
{
    state.refreshEnabled = enable;
}

/*
void Screen_ShowFlashErrors(int idx, int bufCell, int flashCell, unsigned long bufferCounter, unsigned long flashBufferCounter)
{
    WriteErase();

    char str[30];

    sprintf(str, "IDX: %d", idx);
    Write(0, 2, str);

    sprintf(str, "BUFF: %d", bufCell);
    Write(0, 3, str);

    sprintf(str, "FLASH: %d", flashCell);
    Write(0, 4, str);

    sprintf(str, "BUFCNT: %ld", bufferCounter);
    Write(0, 5, str);

    sprintf(str, "FLASHCNT: %ld", flashBufferCounter);
    Write(0, 6, str);

    WriteNow();
}
*/

//------------------------------------------------------------------------------
//  Zobrazeni debug obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowDebug(void)
{
    SetActualScreen(Screen_ShowDebug);

    WriteErase();

    char str[10];

    sprintf(str, "RSIR: %08x", RSIR);
    Write(0, 0, str);

    sprintf(str, "IOPIN0: %08x", IOPIN0);
    Write(0, 1, str);

    sprintf(str, "IOPIN1: %08x", IOPIN1);
    Write(0, 2, str);

    sprintf(str, "FIO2PIN: %08x", FIO2PIN);
    Write(0, 3, str);

    sprintf(str, "FIO3PIN: %08x", FIO3PIN);
    Write(0, 4, str);

    sprintf(str, "FIO4PIN: %08x", FIO4PIN);
    Write(0, 5, str);

    sprintf(str, "PCONP: %08x", PCONP);
    Write(0, 6, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky pro vypnuti.
//------------------------------------------------------------------------------
void Screen_ShowPowerOff(void)
{
    SetActualScreen(Screen_ShowPowerOff);

    WriteErase();

    WriteCenter(3, UI_GetText(TXTID_PowerOffQuestion));

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky hlavniho testu.
//------------------------------------------------------------------------------
void Screen_ShowMainTest(void)
{
    SetActualScreen(Screen_ShowMainTest);

    WriteErase();

    WriteMainTopBar();
    WriteMainSecondTopBar();

    const TMainTestState *pState = Test_Main_GetState();

    if (pState->requestToStop) // prerusen test?
    {
        if (pState->testState != TEST_COMPLETE)
            WriteCenter(2, UI_GetText(TXTID_StoppingTest));
        else
            WriteCenter(2, UI_GetText(TXTID_TestStopped));
    }
    else if (pState->testResult == TRES_OK)
    {
        if (pState->testState != TEST_COMPLETE)
        {
            WriteCenter(3, UI_GetText(TXTID_Progress));
            WriteProgressBar(4, pState->progressPercent);

            char str[10];
            sprintf(str, "%d%%", pState->progressPercent);
            WriteCenter(5, str);
        }
        else
        {
            WriteCenter(2, UI_GetText(TXTID_TestComplete));

            char str[30];
            sprintf(str, "%s: %s", UI_GetText(TXTID_ErrorCode), Test_GetResultText(pState->data.errorCodeTestResult));
            Write(0, 3, str);
            sprintf(str, "%s: %s", UI_GetText(TXTID_Frequency), Test_GetResultText(pState->data.frequencyTestResult));
            Write(0, 4, str);
            sprintf(str, "%s: %s", UI_GetText(TXTID_Iout), Test_GetResultText(pState->data.iOutTestResult));
            Write(0, 5, str);
        }
    }

    if (pState->testResult != TRES_OK)
    {
        TUITextId uiTextId = TXTID_Error;
        if (pState->testResult == TRES_TIMEOUT)
            uiTextId = TXTID_Timeout;
        else if (pState->testResult == TRES_UNSUPPORTED)
            uiTextId = TXTID_Unsupported;

        WriteCenter(3, UI_GetText(uiTextId));
        WriteCenter(4, pState->errText);
    }

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky dat hlavniho testu.
//------------------------------------------------------------------------------
void Screen_ShowMainTestData(void)
{
    SetActualScreen(Screen_ShowMainTestData);

    WriteErase();

    WriteMainTopBar();
    WriteMainSecondTopBar();

    char str[40];
    const TUIState *pUIState = UI_GetState();
    const TMainTestState *pMainTestState = Test_Main_GetState();

    /*if (pUIState->dataMainSubTestId == SUBTID_C1_REZISTANCE)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_C1Rezistance));

        sprintf(str, "%d", pMainTestState->data.c1Rezistance);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_C2_REZISTANCE)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_C2Rezistance));

        sprintf(str, "%d", pMainTestState->data.c2Rezistance);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_E_INSULATION)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_EInsulation));

        sprintf(str, "%d", pMainTestState->data.eInsulation);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_TEMP_SENSOR)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_TempSensor));

        sprintf(str, "%d", pMainTestState->data.tempSensor);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_C_INDUCTANCE)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_CInductance));

        sprintf(str, "%ld", pMainTestState->data.cInductance);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_EXCITATION_LEVELS)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_ExcitationLevels));

        sprintf(str, "L1: %ld", pMainTestState->data.L1);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "L2: %ld", pMainTestState->data.L2);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "L3: %ld", pMainTestState->data.L3);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE + 2, str);
    }
    else*/ if (pUIState->dataMainSubTestId == SUBTID_Magx2UnitNo)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Magx2UnitNo));

        sprintf(str, "%ld", pMainTestState->data.magx2UnitNo);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2Firmware)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Magx2Firmware));

        sprintf(str, "%02d.%02d", pMainTestState->data.magx2Firmware / 100, pMainTestState->data.magx2Firmware % 100);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2Measurement)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Measurement));

        sprintf(str, "%ld", pMainTestState->data.originalTestSetting.measurement);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2LowFlowCutoff)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_LowFlowCutoff));

        sprintf(str, "%ld", pMainTestState->data.originalTestSetting.lowFlowCutoff);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2Excitation)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Excitation));

        sprintf(str, "%ld", pMainTestState->data.originalTestSetting.excitation);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2CurrentLoop)
    {
        sprintf(str, "%s", UI_GetText(TXTID_CurrentLoopSetting));
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, str);

        sprintf(str, "Set:%ld", pMainTestState->data.originalTestSetting.currentLoopSetting.currentSet);
        Write(0, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "Fmin:%ld", pMainTestState->data.originalTestSetting.currentLoopSetting.flowMin / 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "Fmax:%ld", pMainTestState->data.originalTestSetting.currentLoopSetting.flowMax / 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 2, str);

        sprintf(str, "Imin:%ld", pMainTestState->data.originalTestSetting.currentLoopSetting.currentMin);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "Imax:%ld", pMainTestState->data.originalTestSetting.currentLoopSetting.currentMax);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 2, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2FrequencyOutput)
    {
        sprintf(str, "%s", UI_GetText(TXTID_FrequencyOutputSetting));
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, str);

        sprintf(str, "Set:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.frequencySet);
        Write(0, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "FwMin:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.flowMin / 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "FwMax:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.flowMax / 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 2, str);

        sprintf(str, "DutyC:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.dutyCycle);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "FqMin:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.frequencyMin);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "FqMax:%ld", pMainTestState->data.originalTestSetting.frequencyOutputSetting.frequencyMax);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 2, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_Magx2FlowSimulation)
    {
        sprintf(str, "%s", UI_GetText(TXTID_FlowSimulationSetting));
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, str);

        sprintf(str, "Demo:%ld", pMainTestState->data.originalTestSetting.flowSimulationSetting.demo);
        Write(0, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "Flow:%ld", pMainTestState->data.originalTestSetting.flowSimulationSetting.simulatedFlow / 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 1, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorUnitNo)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_SensorUnitNo));

        sprintf(str, "%ld", pMainTestState->data.sensorUnitNo);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorFirmware)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_SensorFirmware));

        sprintf(str, "%02d.%02d", pMainTestState->data.sensorFirmware / 100, pMainTestState->data.sensorFirmware % 100);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorDiameter)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Diameter));

        sprintf(str, "%d mm", pMainTestState->data.diameter);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorFlowRange)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_FlowRange));

        sprintf(str, "%d.%d m3/h", pMainTestState->data.flowRange / 1000, pMainTestState->data.flowRange % 1000);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorErrorMin)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_ErrorMin));

        sprintf(str, "%ld min", pMainTestState->data.errorMin);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorOkMin)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_OkMin));

        sprintf(str, "%ld min", pMainTestState->data.okMin);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorErrorCode)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_ErrorCode));

        sprintf(str, "%08x", pMainTestState->data.errorCode);
        WriteCenter(MAIN_TEST_DATA_VALUE_LINE, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorTotals)
    {
        sprintf(str, "%s [m3]", UI_GetText(TXTID_Totals));
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_Total), (long)pMainTestState->data.total);
        Write(0, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_Aux), (long)pMainTestState->data.aux);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_TotalPlus), (long)pMainTestState->data.totalPlus);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_TotalMinus), (long)pMainTestState->data.totalMinus);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 1, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_SensorCalibration)
    {
        sprintf(str, "%s [m3/h] [-]", UI_GetText(TXTID_Calibration));
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, str);

        sprintf(str, "%s:%ld.%ld", UI_GetText(TXTID_CalibrationPoint1), pMainTestState->data.calibrationPoint1 / 1000, pMainTestState->data.calibrationPoint1 % 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "%s:%ld.%ld", UI_GetText(TXTID_CalibrationPoint2), pMainTestState->data.calibrationPoint2 / 1000, pMainTestState->data.calibrationPoint2 % 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "%s:%ld.%ld", UI_GetText(TXTID_CalibrationPoint3), pMainTestState->data.calibrationPoint3 / 1000, pMainTestState->data.calibrationPoint3 % 1000);
        Write(0, MAIN_TEST_DATA_VALUE_LINE + 2, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_CalibrationData1), pMainTestState->data.calibrationData1);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_CalibrationData2), pMainTestState->data.calibrationData2);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 1, str);

        sprintf(str, "%s:%ld", UI_GetText(TXTID_CalibrationData3), pMainTestState->data.calibrationData3);
        Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + 2, str);
    }
    else if (pUIState->dataMainSubTestId == SUBTID_FREQUENCY)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Frequency));

        for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        {
            sprintf(str, "%f m3/h", pMainTestState->data.testFlows[i] / 1000.0);
            Write(0, MAIN_TEST_DATA_VALUE_LINE + i, str);

            sprintf(str, "%d Hz", pMainTestState->data.frequency[i]);
            Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + i, str);
        }
    }
    else if (pUIState->dataMainSubTestId == SUBTID_IOUT)
    {
        WriteCenter(MAIN_TEST_DATA_TITLE_LINE, UI_GetText(TXTID_Iout));

        for (unsigned char i = 0; i < TEST_FLOW_COUNT; i++)
        {
            sprintf(str, "%f m3/h", pMainTestState->data.testFlows[i] / 1000.0);
            Write(0, MAIN_TEST_DATA_VALUE_LINE + i, str);

            sprintf(str, "%d.%d mA", pMainTestState->data.iOut[i] / 1000, pMainTestState->data.iOut[i] % 1000);
            Write(NUMBER_OF_COLUMNS / 2, MAIN_TEST_DATA_VALUE_LINE + i, str);
        }
    }

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni prazdne obrazovky / vymazani obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowEmpty(void)
{
    SetActualScreen(Screen_ShowEmpty);

    WriteErase();
    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni zahajovaci obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowIntroduction(void)
{
    SetActualScreen(Screen_ShowIntroduction);

    WriteErase();
    WriteImage(0, 0, IMG_ARKON);
    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni menu obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowMenu(void)
{
    SetActualScreen(Screen_ShowMenu);

    WriteErase();

    WriteMainTopBar();
    WriteMainSecondTopBar();
    WriteMenuItems();
    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni edit obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowEdit(void)
{
    SetActualScreen(Screen_ShowEdit);

    WriteErase();

    const TUIState *pState = UI_GetState();

    WriteCenter(2, pState->editState.titleStr);
    WriteCenter(4, pState->editState.valueStr);

    // tento usek zjistuje, ktera cislica ma byt zabarvena, a zabarvi ji.
    int j     = 0;
    int Where = 0;
    while (j!=pState->editState.cursorPos)
    {
        Where+=(GetCharacterWidth(pState->editState.valueStr[j]));
        j+=1;
    }
    InvertLine( CENTR((char*)pState->editState.valueStr) + Where -1, 4, (GetCharacterWidth(pState->editState.valueStr[j])) +1 ); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni edit obrazovky pro typ enum.
//------------------------------------------------------------------------------
void Screen_ShowEditEnum(void)
{
    SetActualScreen(Screen_ShowEditEnum);

    WriteErase();

    const TUIState *pState = UI_GetState();

    WriteCenter(2, pState->editState.titleStr);
    WriteCenter(4, pState->editState.valueStr);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni info obrazovky.
//------------------------------------------------------------------------------
void Screen_ShowInfo(void)
{
    SetActualScreen(Screen_ShowInfo);

    WriteErase();

    const TUIState *pState = UI_GetState();

    WriteCenter(2, pState->infoState.titleStr);
    WriteCenter(4, pState->infoState.valueStr);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Meas2 C1.
//------------------------------------------------------------------------------
void Screen_ShowTestMeas2C1(void)
{
    SetActualScreen(Screen_ShowTestMeas2C1);

    WriteErase();
    WriteTestTopBar();

    char str[20];
    const TADTestState *pState = Test_Meas2C1_GetState();

    sprintf(str, "AD: %d", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %ld", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Avg: %ld", pState->adSampleAvg);
    Write(0, 3, str);

    sprintf(str, "FIO2PIN: %08x", FIO2PIN);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Meas2 C2.
//------------------------------------------------------------------------------
void Screen_ShowTestMeas2C2(void)
{
    SetActualScreen(Screen_ShowTestMeas2C2);

    WriteErase();
    WriteTestTopBar();

    char str[20];
    const TADTestState *pState = Test_Meas2C2_GetState();

    sprintf(str, "AD: %d", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %ld", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Avg: %ld", pState->adSampleAvg);
    Write(0, 3, str);

    sprintf(str, "FIO2PIN: %08x", FIO2PIN);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Meas1 E1.
//------------------------------------------------------------------------------
void Screen_ShowTestMeas1E1(void)
{
    SetActualScreen(Screen_ShowTestMeas1E1);

    WriteErase();
    WriteTestTopBar();

    char str[20];
    const TADTestState *pState = Test_Meas1E1_GetState();

    sprintf(str, "AD: %d", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %ld", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Avg: %ld", pState->adSampleAvg);
    Write(0, 3, str);

    sprintf(str, "FIO2PIN: %08x", FIO2PIN);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Meas1 Temp.
//------------------------------------------------------------------------------
void Screen_ShowTestMeas1Temp(void)
{
    SetActualScreen(Screen_ShowTestMeas1Temp);

    WriteErase();
    WriteTestTopBar();

    char str[20];
    const TADTestState *pState = Test_Meas1Temp_GetState();

    sprintf(str, "AD: %d", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %ld", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Avg: %ld", pState->adSampleAvg);
    Write(0, 3, str);

    sprintf(str, "FIO2PIN: %08x", FIO2PIN);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu mereni indukcnosti civek.
//------------------------------------------------------------------------------
void Screen_ShowTestCInductance(void)
{
    SetActualScreen(Screen_ShowTestCInductance);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TCInductanceTestState *pState = Test_CInductance_GetState();

    sprintf(str, "AD: %ld", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %d", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Cycles: %ld", pState->measureCount);
    Write(0, 3, str);

    sprintf(str, "AD state: %02x", pState->adState);
    Write(0, 4, str);

    sprintf(str, "Error: %s", pState->errText);
    Write(0, 5, str);

    sprintf(str, "L[uH]: %ld", pState->inductanceInHenry >= 0 ?
            (long)(pState->inductanceInHenry * 1000000) : -1);
    Write(0, 6, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu mereni buzeni.
//------------------------------------------------------------------------------
void Screen_ShowTestExcitationIn(void)
{
    SetActualScreen(Screen_ShowTestExcitationIn);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TExciteInTestState *pState = Test_ExcitationIn_GetState();

    sprintf(str, "L1 samples: %ld", pState->L1SampleState.adSampleCount);
    Write(0, 1, str);

    sprintf(str, "L1 avg: %ld", pState->L1SampleState.adSampleAvg);
    Write(0, 2, str);

    sprintf(str, "L2 samples: %ld", pState->L2SampleState.adSampleCount);
    Write(0, 3, str);

    sprintf(str, "L2 avg: %ld", pState->L2SampleState.adSampleAvg);
    Write(0, 4, str);

    sprintf(str, "L3 samples: %ld", pState->L3SampleState.adSampleCount);
    Write(0, 5, str);

    sprintf(str, "L3 avg: %ld", pState->L3SampleState.adSampleAvg);
    Write(0, 6, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu mereni frekvence.
//------------------------------------------------------------------------------
void Screen_ShowTestFrequency(void)
{
    SetActualScreen(Screen_ShowTestFrequency);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TFrequencyTestState *pState = Test_Frequency_GetState();

    sprintf(str, "Timer old: %08x", pState->timerSnapOld);
    Write(0, 1, str);

    sprintf(str, "Timer new: %08x", pState->timerSnapNew);
    Write(0, 2, str);

    sprintf(str, "Time count: %ld", pState->timeCount);
    Write(0, 3, str);

    sprintf(str, "Time[us]: %ld", (long)(pState->timeAvg * 1000000));
    Write(0, 4, str);

    sprintf(str, "f[Hz]: %ld", pState->frequency[0]);
    Write(0, 5, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu mereni Iout.
//------------------------------------------------------------------------------
void Screen_ShowTestIout(void)
{
    SetActualScreen(Screen_ShowTestIout);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TIoutTestState *pState = Test_Iout_GetState();

    sprintf(str, "AD: %d", pState->adLastSample);
    Write(0, 1, str);

    sprintf(str, "Samples: %d", pState->adSampleCount);
    Write(0, 2, str);

    sprintf(str, "Cycles: %ld", pState->measureCount);
    Write(0, 3, str);

    sprintf(str, "Error: %s", pState->errText);
    Write(0, 4, str);

    sprintf(str, "I[mA]: %ld", pState->currentInAmpere >= 0 ?
            (long)(pState->currentInAmpere[0] * 1000) : -1);
    Write(0, 5, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu MagX2 komunikace.
//------------------------------------------------------------------------------
void Screen_ShowTestMagX2Comm(void)
{
    SetActualScreen(Screen_ShowTestMagX2Comm);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TModbusTestState *pState = Test_MagX2Comm_GetState();

    sprintf(str, "Tx Frames: %ld", pState->txFrameCount);
    Write(0, 1, str);

    sprintf(str, "Rx Frames: %ld", pState->rxFrameCount);
    Write(0, 2, str);

    sprintf(str, "Rx Bytes: %ld", pState->rxByteCount);
    Write(0, 3, str);

    sprintf(str, "Rx Timeouts: %ld", pState->rxTimeoutCount);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Sensor Slave komunikace.
//------------------------------------------------------------------------------
void Screen_ShowTestSensorSlave(void)
{
    SetActualScreen(Screen_ShowTestSensorSlave);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TModbusTestState *pState = Test_SensorSlave_GetState();

    sprintf(str, "Rx Frames: %ld", pState->rxFrameCount);
    Write(0, 1, str);

    sprintf(str, "Rx Bytes: %ld", pState->rxByteCount);
    Write(0, 2, str);

    sprintf(str, "Rx Timeouts: %ld", pState->rxTimeoutCount);
    Write(0, 3, str);

    sprintf(str, "Tx Frames: %ld", pState->txFrameCount);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Sensor Master komunikace.
//------------------------------------------------------------------------------
void Screen_ShowTestSensorMaster(void)
{
    SetActualScreen(Screen_ShowTestSensorMaster);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TModbusTestState *pState = Test_SensorMaster_GetState();

    sprintf(str, "Tx Frames: %ld", pState->txFrameCount);
    Write(0, 1, str);

    sprintf(str, "Rx Frames: %ld", pState->rxFrameCount);
    Write(0, 2, str);

    sprintf(str, "Rx Bytes: %ld", pState->rxByteCount);
    Write(0, 3, str);

    sprintf(str, "Rx Timeouts: %ld", pState->rxTimeoutCount);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu Modbus komunikace.
//------------------------------------------------------------------------------
void Screen_ShowTestModbus(void)
{
    SetActualScreen(Screen_ShowTestModbus);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TModbusTestState *pState = Test_Modbus_GetState();

    sprintf(str, "Rx Frames: %ld", pState->rxFrameCount);
    Write(0, 1, str);

    sprintf(str, "Address: %ld", pState->address);
    Write(0, 2, str);

    sprintf(str, "Data: %ld", pState->data);
    Write(0, 3, str);

    WriteMainBottomBar();

    WriteNow();
}

//------------------------------------------------------------------------------
//  Zobrazeni obrazovky testu ADP5063.
//------------------------------------------------------------------------------
void Screen_ShowTestADP5063(void)
{
    SetActualScreen(Screen_ShowTestADP5063);

    WriteErase();
    WriteTestTopBar();

    char str[30];
    const TADP5063TestState *pState = Test_ADP5063_GetState();

    sprintf(str, "Error Count: %ld", pState->errCount);
    Write(0, 1, str);

    sprintf(str, "ChargerStatus1 [0B]: %02x", pState->regData_ChargerStatus1);
    Write(0, 2, str);

    sprintf(str, "ChargerStatus2 [0C]: %02x", pState->regData_ChargerStatus2);
    Write(0, 3, str);

    sprintf(str, "SYS EN Level: %d", pState->sysEnLevel);
    Write(0, 4, str);

    WriteMainBottomBar();

    WriteNow();
}
