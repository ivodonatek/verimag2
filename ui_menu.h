//******************************************************************************
//  Menu uzivatelskeho rozhrani.
//*****************************************************************************/

#ifndef UI_MENU_H_INCLUDED
#define UI_MENU_H_INCLUDED

#include "ui_texts.h"
#include "type.h"

// Identifikatory menu
typedef enum
{
    MENUID_Main,
    MENUID_TestHw,
    MENUID_MainTestData,
    MENUID_Setting,
    MENUID_Info,

} TUIMenuId;

// pocet menu
#define UI_MENU_COUNT   (MENUID_Info + 1)

// Typ - prototyp funkce, ktera je volana pro obsluhu menu.
typedef void (*TMenuHandler) (void);

// zadna funkce menu
#define NO_MENU_HANDLER   ((TMenuHandler)0)

// Polozka menu
typedef struct
{
    // ID zobrazovaneho textu
    TUITextId textId;

    // obsluha pro vstup do polozky
    TMenuHandler onEnter;

} TMenuItem;

// Menu
typedef struct
{
    // polozky
    TMenuItem *items;

    // pocet polozek
    unsigned int itemCount;

    // obsluha pro vystup z menu
    TMenuHandler onExit;

} TMenu;

// Stav menu
typedef struct
{
    // vlastni menu
    TMenu *menu;

    // poradi/index prvni zobrazovane polozky menu
    unsigned int firstMenuItem;

    // poradi/index vybrane polozky menu
    unsigned int selectedMenuItem;

} TMenuState;

//------------------------------------------------------------------------------
//  Vrati menu podle identifikatoru.
//------------------------------------------------------------------------------
const TMenu* UI_GetMenu(TUIMenuId id);

//------------------------------------------------------------------------------
//  Nastavi hlavni menu s predvybranou polozkou hlavniho testu
//  (postara se taky o tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetMainMenuWithMainTestItem(void);

#endif // UI_MENU_H_INCLUDED
