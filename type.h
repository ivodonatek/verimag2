/*
	LPCUSB, an USB device driver for LPC microcontrollers
	Copyright (C) 2006 Bertrik Sikken (bertrik@sikken.nl)

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	   notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	   notice, this list of conditions and the following disclaimer in the
	   documentation and/or other materials provided with the distribution.
	3. The name of the author may not be used to endorse or promote products
	   derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
	IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
	IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
	DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
	THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
	THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


/**
	@file
	primitive types used in the USB stack
 */


#ifndef _TYPE_H_
#define _TYPE_H_

#include"port.h"
//typedef unsigned char		BYTE;	/**< unsigned 8-bit */
typedef unsigned char		U8;		/**< unsigned 8-bit */
typedef unsigned short int	U16;	/**< unsigned 16-bit */
typedef unsigned int		U32;	/**< unsigned 32-bit */
//typedef unsigned short WORD;
//typedef unsigned long  DWORD;

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#ifndef BYTE_TYPEDEF
#define BYTE_TYPEDEF
typedef unsigned char  BYTE;
#endif

#ifndef USHORT_TYPEDEF
#define USHORT_TYPEDEF
typedef uint16_t USHORT;
#endif

#ifndef SHORT_TYPEDEF
#define SHORT_TYPEDEF
typedef int16_t SHORT;
#endif

#ifndef ULONG_TYPEDEF
#define ULONG_TYPEDEF
typedef uint32_t	ULONG;
#endif

#ifndef LONG_TYPEDEF
#define LONG_TYPEDEF
typedef int32_t	LONG;
#endif

#ifndef WORD_TYPEDEF
#define WORD_TYPEDEF
typedef unsigned short  WORD;
#endif

#ifndef DWORD_TYPEDEF
#define DWORD_TYPEDEF
typedef unsigned long  DWORD;
#endif

#ifndef CHAR_TYPEDEF
#define CHAR_TYPEDEF
typedef int8_t	CHAR;
#endif

#define	TRUE	1					/**< TRUE */
#define FALSE	0					/**< FALSE */

#ifndef NULL
#define NULL	((void*)0)			/**< NULL pointer */
#endif

/* some other useful macros */
#define MIN(x,y)	((x)<(y)?(x):(y))	/**< MIN */
#define MAX(x,y)	((x)>(y)?(x):(y))	/**< MAX */
#define EOF (-1)

#endif /* _TYPE_H_ */

