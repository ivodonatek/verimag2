#include "port.h"
#include "modbus.h"
#include "mb.h"
#include "mbport.h"
#include "maine.h"
#include "rtc.h"
#include "menu.h"
#include "math.h"
#include "delay.h"
#include "display.h"
#include "sbl_config.h"
#include "test.h"
#include "store_man.h"

volatile unsigned long modbus_datalogger_date = 0;
volatile unsigned int modbus_datalogger_time = 0;
volatile unsigned char modbus_read_datalogger_flag = 0;

// funkce, ktera je volana po prijmu Modbus dotazu.
static TModbusCommDataHandler modbusCommDataHandler;

void Modbus_init (void)
{
  unsigned int BaudRate = 0;
  unsigned char Parity = 0 ;

  BaudRate = 115200;
  // parity None, 1 stopbit
  //Parity = MB_PAR_NONE;
  // Even, 1 stopbit
  Parity = MB_PAR_EVEN;

  eMBErrorCode eStatus;
  eStatus = eMBDisable();
  eStatus = eMBInit( MB_RTU, 1, 0, BaudRate, Parity);
  eStatus = eMBEnable();

  modbusCommDataHandler = NO_MODBUS_COMM_DATA_HANDLER;
}

void Modbus_SetCommDataHandler(TModbusCommDataHandler handler)
{
    modbusCommDataHandler = handler;
}

eMBErrorCode eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode )
{
  eMBErrorCode    eStatus = MB_ENOERR;
  unsigned long TmpData;
  ///Online
  if ((usAddress>=MODBUS_ONLINE_OFFSET)&&(usAddress+usNRegs<= MODBUS_ONLINE_OFFSET+NUMBER_OF_ONLINE*2)) //Registry jsou 32bitove
  {
    if ( eMode==MB_REG_READ )
    {
      while (usNRegs)
      {
        switch ((usAddress-MODBUS_ONLINE_OFFSET)/2)//(online measuerement)
        {
          case MODBUS_MEAS_FLOW:
            TmpData=(long)(g_measurement.actual*1000);
            break;
          case MODBUS_MEAS_TOTAL_DIG      :
            TmpData=(unsigned long)(g_measurement.total/1000000.0);
            break;
          case MODBUS_MEAS_TOTAL_DEC      :
            TmpData=(unsigned long)(((g_measurement.total)%1000000)/1000);
            break;
          case MODBUS_MEAS_AUX_DIG        :
            TmpData=(unsigned long)(g_measurement.aux/1000000.0);
            break;
          case MODBUS_MEAS_AUX_DEC        :
            TmpData=(unsigned long)(((g_measurement.aux)%1000000)/1000);
            break;
          case MODBUS_MEAS_POS_DIG        :
            TmpData=(unsigned long)(g_measurement.flow_pos/1000000.0);
            break;
          case MODBUS_MEAS_POS_DEC        :
            TmpData=(unsigned long)(((g_measurement.flow_pos)%1000000)/1000);
            break;
          case MODBUS_MEAS_NEG_DIG   :
            TmpData=(unsigned long)(g_measurement.flow_neg/1000000.0);
            break;
          case MODBUS_MEAS_NEG_DEC   :
            TmpData=(unsigned long)(((g_measurement.flow_neg)%1000000)/1000);
            break;
          case MODBUS_MEAS_TEMP       :
            TmpData=(unsigned long)(g_measurement.temp*10);
            break;
          case MODBUS_MEAS_ERROR       :
            TmpData=(unsigned long)(actual_error);
            break;
          default:
            TmpData=0;
        }
        if ((usAddress-MODBUS_ONLINE_OFFSET)%2) //cteni vyssiho slova
        {
          *pucRegBuffer++ = ((long)TmpData>>24);
          *pucRegBuffer++ = ((long)TmpData>>16);
        }
        else
        {
          *pucRegBuffer++ = ((long)TmpData>>8);
          *pucRegBuffer++ =  (long)TmpData;
        }
        usNRegs--;
        usAddress++;
      }
    }
    else
    {
      eStatus = MB_ENOREG; //Pouze cteni
    }
  }
  // 1000 - 1020 nastaveni verifikatoru
  else if ( (usAddress >= 1000) && ( usAddress+usNRegs <= 1040 ))
  {
    switch ( eMode )
    {
      case MB_REG_READ:
      {
        while (usNRegs)
        {
          TmpData=RAM_Read_Data_Long(FLASH_FACTORY_START + ( ( (usAddress-1000) / 2 ) * 4) );
          if ((usAddress - 1000)%2) //cteni vyssiho slova
          {
            *pucRegBuffer++ = (UCHAR)(TmpData>>24);
            *pucRegBuffer++ = (UCHAR)(TmpData>>16);
          }
          else
          {
            *pucRegBuffer++ = (UCHAR)(TmpData>>8);
            *pucRegBuffer++ = (UCHAR)TmpData;

            if (modbusCommDataHandler != NO_MODBUS_COMM_DATA_HANDLER)
            {
                TModbusCommData modbusCommData;
                modbusCommData.address = usAddress;
                modbusCommData.data = TmpData;

                modbusCommDataHandler(&modbusCommData);
            }
          }
          usNRegs--;
          usAddress++;
        }
      }
      break;
      case MB_REG_WRITE:
      {
        unsigned long madata = 0;
        while (usNRegs)
        {
          if (usAddress == 1001)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<24;
            madata += ((unsigned long)(*pucRegBuffer++))<<16;
          }
          else if (usAddress == 1002)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<8;
            madata += *pucRegBuffer++;
            RAM_Write_Data_Long(madata,UNIT_NO);
            StoreMan_SaveSetting();
          }
          else if (usAddress == 1013)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<24;
            madata += ((unsigned long)(*pucRegBuffer++))<<16;
          }
          else if (usAddress == 1014)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<8;
            madata += *pucRegBuffer++;
            RAM_Write_Data_Long(madata,CALIB_DATE);
            StoreMan_SaveSetting();
          }
          else if (usAddress == 1021)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<24;
            madata += ((unsigned long)(*pucRegBuffer++))<<16;
          }
          else if (usAddress == 1022)
          {
            madata += ((unsigned long)(*pucRegBuffer++))<<8;
            madata += *pucRegBuffer++;
            RAM_Write_Data_Long(madata,TEST_ERASE);
          }
          usAddress++;
          usNRegs--;
        }
      }
      break;
    }
  }

  // testy adresa musi byt >= 1500
  else if (usAddress >= 1500)
  {
    switch ( eMode )
    {
      case MB_REG_READ:
      {
        while (usNRegs)
        {
          TmpData=RAM_Read_Data_Long(FLASH_TESTS_START + ( ( (usAddress-1500) / 2 ) * 4) );
          if ((usAddress - 1500)%2) //cteni vyssiho slova
          {
            *pucRegBuffer++ = (UCHAR)(TmpData>>24);
            *pucRegBuffer++ = (UCHAR)(TmpData>>16);
          }
          else
          {
            *pucRegBuffer++ = (UCHAR)(TmpData>>8);
            *pucRegBuffer++ = (UCHAR)TmpData;
          }
          usNRegs--;
          usAddress++;
        }
      }
      break;
      case MB_REG_WRITE:
      {
        eStatus = MB_ENOREG; //Pouze cteni
      }
      break;
    }
  }
  else
  {
    eStatus = MB_ENOREG;
  }
  return eStatus;
}

