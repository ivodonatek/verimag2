//******************************************************************************
//  Ovladani LT1944-1
//  (Dual Micropower Step-Up DC/DC Converter).
//  Changed to Vcc1 control (12.12.2017)
//*****************************************************************************/

#include "step_up.h"
#include "LPC23xx.h"

// konfigurace portu
#define VCC1_ON_IODIR IODIR1
#define VCC1_ON_IOSET IOSET1
#define VCC1_ON_IOCLR IOCLR1

#define VCC1_ON_IOPIN (1<<18)

#define VCC1_ON_INIT  {VCC1_ON_IODIR |= VCC1_ON_IOPIN;}

#define VCC1_ON_H {VCC1_ON_IOSET |= VCC1_ON_IOPIN;}
#define VCC1_ON_L {VCC1_ON_IOCLR |= VCC1_ON_IOPIN;}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void StepUp_Init(void)
{
    VCC1_ON_INIT;
    StepUp_Set(STPUP_OFF);
}

//------------------------------------------------------------------------------
//  Zapnuti/vypnuti Step-up.
//------------------------------------------------------------------------------
void StepUp_Set(TStepUpSetup setup)
{
    switch (setup)
    {
    case STPUP_ON:
        VCC1_ON_L;
        break;

    case STPUP_OFF:
        VCC1_ON_H;
        break;
    }
}
