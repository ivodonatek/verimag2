//******************************************************************************
//  Menu uzivatelskeho rozhrani.
//*****************************************************************************/

#include "ui_menu.h"
#include "ui.h"
#include "buttons.h"
#include "sensor_master.h"
#include "test.h"
#include "screen.h"
#include "rtc.h"
#include "rprintf.h"
#include "menu_tables.h"
#include "power_man.h"
#include "maine.h"

//------------------------------------------------------------------------------
//  Vrati index polozky menu podle Enter procedury
//------------------------------------------------------------------------------
static unsigned int GetMenuItemIndex(TUIMenuId menuId, TMenuHandler onEnter)
{
    const TMenu* pMenu = UI_GetMenu(menuId);

    for (unsigned int i = 0; i < pMenu->itemCount; i++)
    {
        if (pMenu->items[i].onEnter == onEnter)
            return i;
    }

    return 0;
}

//------------------------------------------------------------------------------
//  Menu - hlavni
//------------------------------------------------------------------------------

static void OnEnter_MainTest(void);
static void OnEnter_Debug(void);
static void OnEnter_TestHw(void);
static void OnEnter_PowerOff(void);
static void OnEnter_Setting(void);
static void OnEnter_Info(void);

static void OnExit_Main(void)
{
    UI_SetMenu(MENUID_Main, 0);
}

static void OnExit_MainTest(void)
{
    UI_InitDisplayButtons();
    Buttons_InitHandlers();

    Test_Main_WantStop();
}

static void OnEnter_MainTest(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_MainTest);

    Test_Main_Start();

    Screen_ShowMainTest();
}

static void OnExit_Debug(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_Debug));
}

static void OnEnter_Debug(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Debug);

    Screen_ShowDebug();
}

static void OnExit_PowerOff(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_PowerOff));
}

static void OnYes_PowerOff(void)
{
    PowerMan_SwitchOff();
}

static void OnEnter_PowerOff(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);
    UI_SetDisplayButton(UI_BTN_ENTER, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_PowerOff);
    Buttons_SetHandler(BTN_ENTER, OnYes_PowerOff);

    Screen_ShowPowerOff();
}

static void OnEnter_TestHw(void)
{
    UI_SetMenu(MENUID_TestHw, 0);
}

static void OnEnter_Setting(void)
{
    UI_SetMenu(MENUID_Setting, 0);
}

static void OnEnter_Info(void)
{
    UI_SetMenu(MENUID_Info, 0);
}

const TMenuItem MENU_ITEMS_Main[] =
{
    //{ TXTID_Debug, OnEnter_Debug },
    //{ TXTID_TestHw, OnEnter_TestHw },
    { TXTID_Info, OnEnter_Info },
    { TXTID_Setting, OnEnter_Setting },
    { TXTID_Test, OnEnter_MainTest },
    { TXTID_PowerOff, OnEnter_PowerOff },
};

const TMenu MENU_Main =
{
    MENU_ITEMS_Main,
    sizeof(MENU_ITEMS_Main) / sizeof(MENU_ITEMS_Main[0]),
    OnExit_Main
};

//------------------------------------------------------------------------------
//  Menu - Test hw
//------------------------------------------------------------------------------

static void OnEnter_TestMeas2C1(void);
static void OnEnter_TestMeas2C2(void);
static void OnEnter_TestMeas1E1(void);
static void OnEnter_TestMeas1Temp(void);
static void OnEnter_TestCInductance(void);
static void OnEnter_TestExcitationIn(void);
static void OnEnter_TestFrequency(void);
static void OnEnter_TestIout(void);
static void OnEnter_TestMagX2Comm(void);
static void OnEnter_TestSensorSlave(void);
static void OnEnter_TestSensorMaster(void);
static void OnEnter_TestModbus(void);
static void OnEnter_TestADP5063(void);

static void OnExit_TestHw(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_TestHw));
}

static void OnExit_TestMeas2C1(void)
{
    Test_Meas2C1_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestMeas2C1));
}

static void OnEnter_TestMeas2C1(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestMeas2C1);

    Test_Meas2C1_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestMeas2C1();
}

static void OnExit_TestMeas2C2(void)
{
    Test_Meas2C2_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestMeas2C2));
}

static void OnEnter_TestMeas2C2(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestMeas2C2);

    Test_Meas2C2_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestMeas2C2();
}

static void OnExit_TestMeas1E1(void)
{
    Test_Meas1E1_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestMeas1E1));
}

static void OnEnter_TestMeas1E1(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestMeas1E1);

    Test_Meas1E1_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestMeas1E1();
}

static void OnExit_TestMeas1Temp(void)
{
    Test_Meas1Temp_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestMeas1Temp));
}

static void OnEnter_TestMeas1Temp(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestMeas1Temp);

    Test_Meas1Temp_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestMeas1Temp();
}

static void OnExit_TestCInductance(void)
{
    Test_CInductance_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestCInductance));
}

static void OnEnter_TestCInductance(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestCInductance);

    Test_CInductance_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestCInductance();
}

static void OnExit_TestExcitationIn(void)
{
    Test_ExcitationIn_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestExcitationIn));
}

static void OnEnter_TestExcitationIn(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestExcitationIn);

    Test_ExcitationIn_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestExcitationIn();
}

static void OnExit_TestFrequency(void)
{
    Test_Frequency_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestFrequency));
}

static void OnEnter_TestFrequency(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestFrequency);

    Test_Frequency_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestFrequency();
}

static void OnExit_TestIout(void)
{
    Test_Iout_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestIout));
}

static void OnEnter_TestIout(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestIout);

    Test_Iout_Start(TEST_TYPE_UNLIMITED);

    Screen_ShowTestIout();
}

static void OnExit_TestMagX2Comm(void)
{
    Test_MagX2Comm_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestMagX2Comm));
}

static void OnEnter_TestMagX2Comm(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestMagX2Comm);

    Test_MagX2Comm_Start();

    Screen_ShowTestMagX2Comm();
}

static void OnExit_TestSensorSlave(void)
{
    Test_SensorSlave_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestSensorSlave));
}

static void OnEnter_TestSensorSlave(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestSensorSlave);

    Test_SensorSlave_Start();

    Screen_ShowTestSensorSlave();
}

static void OnExit_TestSensorMaster(void)
{
    Test_SensorMaster_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestSensorMaster));
}

static void OnEnter_TestSensorMaster(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestSensorMaster);

    Test_SensorMaster_Start();

    Screen_ShowTestSensorMaster();
}

static void OnExit_TestModbus(void)
{
    Test_Modbus_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestModbus));
}

static void OnEnter_TestModbus(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestModbus);

    Test_Modbus_Start();

    Screen_ShowTestModbus();
}

static void OnExit_TestADP5063(void)
{
    Test_ADP5063_Stop();
    UI_SetMenu(MENUID_TestHw, GetMenuItemIndex(MENUID_TestHw, OnEnter_TestADP5063));
}

static void OnEnter_TestADP5063(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TestADP5063);

    Test_ADP5063_Start();

    Screen_ShowTestADP5063();
}

const TMenuItem MENU_ITEMS_TestHw[] =
{
    /*{ TXTID_TestMeas2C1, OnEnter_TestMeas2C1 },
    { TXTID_TestMeas2C2, OnEnter_TestMeas2C2 },
    { TXTID_TestMeas1E1, OnEnter_TestMeas1E1 },
    { TXTID_TestMeas1Temp, OnEnter_TestMeas1Temp },
    { TXTID_TestCInductance, OnEnter_TestCInductance },
    { TXTID_TestExcitationIn, OnEnter_TestExcitationIn },*/
    { TXTID_TestFrequency, OnEnter_TestFrequency },
    { TXTID_TestIout, OnEnter_TestIout },
    { TXTID_TestMagX2Comm, OnEnter_TestMagX2Comm },
    { TXTID_TestSensorSlave, OnEnter_TestSensorSlave },
    { TXTID_TestSensorMaster, OnEnter_TestSensorMaster },
    { TXTID_TestModbus, OnEnter_TestModbus },
    { TXTID_TestADP5063, OnEnter_TestADP5063 },
};

const TMenu MENU_TestHw =
{
    MENU_ITEMS_TestHw,
    sizeof(MENU_ITEMS_TestHw) / sizeof(MENU_ITEMS_TestHw[0]),
    OnExit_TestHw
};

//------------------------------------------------------------------------------
//  Menu - Data hlavniho testu
//------------------------------------------------------------------------------

/*static void OnEnter_C1Rezistance(void);
static void OnEnter_C2Rezistance(void);
static void OnEnter_EInsulation(void);
static void OnEnter_TempSensor(void);
static void OnEnter_CInductance(void);
static void OnEnter_ExcitationIn(void);*/
static void OnEnter_Magx2UnitNo(void);
static void OnEnter_Magx2Firmware(void);
static void OnEnter_Magx2Measurement(void);
static void OnEnter_Magx2LowFlowCutoff(void);
static void OnEnter_Magx2Excitation(void);
static void OnEnter_Magx2CurrentLoopSetting(void);
static void OnEnter_Magx2FrequencyOutputSetting(void);
static void OnEnter_Magx2FlowSimulationSetting(void);
static void OnEnter_SensorUnitNo(void);
static void OnEnter_SensorFirmware(void);
static void OnEnter_SensorDiameter(void);
static void OnEnter_SensorFlowRange(void);
static void OnEnter_SensorErrorMin(void);
static void OnEnter_SensorOkMin(void);
static void OnEnter_SensorErrorCode(void);
static void OnEnter_SensorTotals(void);
static void OnEnter_SensorCalibration(void);
static void OnEnter_Frequency(void);
static void OnEnter_Iout(void);

static void OnExit_MainTestData(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_MainTest));
}

/*
static void OnExit_C1Rezistance(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_C1Rezistance));
}

static void OnEnter_C1Rezistance(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_C1Rezistance);

    UI_SetActualMainTestData(SUBTID_C1_REZISTANCE);

    Screen_ShowMainTestData();
}

static void OnExit_C2Rezistance(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_C2Rezistance));
}

static void OnEnter_C2Rezistance(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_C2Rezistance);

    UI_SetActualMainTestData(SUBTID_C2_REZISTANCE);

    Screen_ShowMainTestData();
}

static void OnExit_EInsulation(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_EInsulation));
}

static void OnEnter_EInsulation(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_EInsulation);

    UI_SetActualMainTestData(SUBTID_E_INSULATION);

    Screen_ShowMainTestData();
}

static void OnExit_TempSensor(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_TempSensor));
}

static void OnEnter_TempSensor(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_TempSensor);

    UI_SetActualMainTestData(SUBTID_TEMP_SENSOR);

    Screen_ShowMainTestData();
}

static void OnExit_CInductance(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_CInductance));
}

static void OnEnter_CInductance(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_CInductance);

    UI_SetActualMainTestData(SUBTID_C_INDUCTANCE);

    Screen_ShowMainTestData();
}

static void OnExit_ExcitationIn(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_ExcitationIn));
}

static void OnEnter_ExcitationIn(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_ExcitationIn);

    UI_SetActualMainTestData(SUBTID_EXCITATION_LEVELS);

    Screen_ShowMainTestData();
}*/

static void OnExit_Magx2UnitNo(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2UnitNo));
}

static void OnEnter_Magx2UnitNo(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2UnitNo);

    UI_SetActualMainTestData(SUBTID_Magx2UnitNo);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2Firmware(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2Firmware));
}

static void OnEnter_Magx2Firmware(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2Firmware);

    UI_SetActualMainTestData(SUBTID_Magx2Firmware);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2Measurement(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2Measurement));
}

static void OnEnter_Magx2Measurement(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2Measurement);

    UI_SetActualMainTestData(SUBTID_Magx2Measurement);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2LowFlowCutoff(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2LowFlowCutoff));
}

static void OnEnter_Magx2LowFlowCutoff(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2LowFlowCutoff);

    UI_SetActualMainTestData(SUBTID_Magx2LowFlowCutoff);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2Excitation(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2Excitation));
}

static void OnEnter_Magx2Excitation(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2Excitation);

    UI_SetActualMainTestData(SUBTID_Magx2Excitation);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2CurrentLoopSetting(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2CurrentLoopSetting));
}

static void OnEnter_Magx2CurrentLoopSetting(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2CurrentLoopSetting);

    UI_SetActualMainTestData(SUBTID_Magx2CurrentLoop);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2FrequencyOutputSetting(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2FrequencyOutputSetting));
}

static void OnEnter_Magx2FrequencyOutputSetting(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2FrequencyOutputSetting);

    UI_SetActualMainTestData(SUBTID_Magx2FrequencyOutput);

    Screen_ShowMainTestData();
}

static void OnExit_Magx2FlowSimulationSetting(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Magx2FlowSimulationSetting));
}

static void OnEnter_Magx2FlowSimulationSetting(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Magx2FlowSimulationSetting);

    UI_SetActualMainTestData(SUBTID_Magx2FlowSimulation);

    Screen_ShowMainTestData();
}

static void OnExit_SensorUnitNo(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorUnitNo));
}

static void OnEnter_SensorUnitNo(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorUnitNo);

    UI_SetActualMainTestData(SUBTID_SensorUnitNo);

    Screen_ShowMainTestData();
}

static void OnExit_SensorFirmware(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorFirmware));
}

static void OnEnter_SensorFirmware(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorFirmware);

    UI_SetActualMainTestData(SUBTID_SensorFirmware);

    Screen_ShowMainTestData();
}

static void OnExit_SensorDiameter(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorDiameter));
}

static void OnEnter_SensorDiameter(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorDiameter);

    UI_SetActualMainTestData(SUBTID_SensorDiameter);

    Screen_ShowMainTestData();
}

static void OnExit_SensorFlowRange(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorFlowRange));
}

static void OnEnter_SensorFlowRange(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorFlowRange);

    UI_SetActualMainTestData(SUBTID_SensorFlowRange);

    Screen_ShowMainTestData();
}

static void OnExit_SensorErrorMin(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorErrorMin));
}

static void OnEnter_SensorErrorMin(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorErrorMin);

    UI_SetActualMainTestData(SUBTID_SensorErrorMin);

    Screen_ShowMainTestData();
}

static void OnExit_SensorOkMin(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorOkMin));
}

static void OnEnter_SensorOkMin(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorOkMin);

    UI_SetActualMainTestData(SUBTID_SensorOkMin);

    Screen_ShowMainTestData();
}

static void OnExit_SensorErrorCode(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorErrorCode));
}

static void OnEnter_SensorErrorCode(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorErrorCode);

    UI_SetActualMainTestData(SUBTID_SensorErrorCode);

    Screen_ShowMainTestData();
}

static void OnExit_SensorTotals(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorTotals));
}

static void OnEnter_SensorTotals(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorTotals);

    UI_SetActualMainTestData(SUBTID_SensorTotals);

    Screen_ShowMainTestData();
}

static void OnExit_SensorCalibration(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_SensorCalibration));
}

static void OnEnter_SensorCalibration(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_SensorCalibration);

    UI_SetActualMainTestData(SUBTID_SensorCalibration);

    Screen_ShowMainTestData();
}

static void OnExit_Frequency(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Frequency));
}

static void OnEnter_Frequency(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Frequency);

    UI_SetActualMainTestData(SUBTID_FREQUENCY);

    Screen_ShowMainTestData();
}

static void OnExit_Iout(void)
{
    UI_SetMenu(MENUID_MainTestData, GetMenuItemIndex(MENUID_MainTestData, OnEnter_Iout));
}

static void OnEnter_Iout(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Iout);

    UI_SetActualMainTestData(SUBTID_IOUT);

    Screen_ShowMainTestData();
}

const TMenuItem MENU_ITEMS_MainTestData[] =
{
    /*{ TXTID_C1Rezistance, OnEnter_C1Rezistance },
    { TXTID_C2Rezistance, OnEnter_C2Rezistance },
    { TXTID_EInsulation, OnEnter_EInsulation },
    { TXTID_TempSensor, OnEnter_TempSensor },
    { TXTID_CInductance, OnEnter_CInductance },
    { TXTID_ExcitationLevels, OnEnter_ExcitationIn },*/
    { TXTID_Magx2UnitNo, OnEnter_Magx2UnitNo },
    { TXTID_Magx2Firmware, OnEnter_Magx2Firmware },
    { TXTID_Measurement, OnEnter_Magx2Measurement },
    { TXTID_LowFlowCutoff, OnEnter_Magx2LowFlowCutoff },
    { TXTID_Excitation, OnEnter_Magx2Excitation },
    { TXTID_CurrentLoopSetting, OnEnter_Magx2CurrentLoopSetting },
    { TXTID_FrequencyOutputSetting, OnEnter_Magx2FrequencyOutputSetting },
    { TXTID_FlowSimulationSetting, OnEnter_Magx2FlowSimulationSetting },
    { TXTID_SensorUnitNo, OnEnter_SensorUnitNo },
    { TXTID_SensorFirmware, OnEnter_SensorFirmware },
    { TXTID_Diameter, OnEnter_SensorDiameter },
    { TXTID_FlowRange, OnEnter_SensorFlowRange },
    { TXTID_ErrorMin, OnEnter_SensorErrorMin },
    { TXTID_OkMin, OnEnter_SensorOkMin },
    { TXTID_ErrorCode, OnEnter_SensorErrorCode },
    { TXTID_Totals, OnEnter_SensorTotals },
    { TXTID_Calibration, OnEnter_SensorCalibration },
    { TXTID_Frequency, OnEnter_Frequency },
    { TXTID_Iout, OnEnter_Iout },
};

const TMenu MENU_MainTestData =
{
    MENU_ITEMS_MainTestData,
    sizeof(MENU_ITEMS_MainTestData) / sizeof(MENU_ITEMS_MainTestData[0]),
    OnExit_MainTestData
};

//------------------------------------------------------------------------------
//  Menu - Nastaveni
//------------------------------------------------------------------------------

static void OnEnter_Date(void);
static void OnEnter_Time(void);
static void OnEnter_Backlight(void);

static void OnExit_Setting(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_Setting));
}

static void OnExit_Date(void)
{
    UI_SetMenu(MENUID_Setting, GetMenuItemIndex(MENUID_Setting, OnEnter_Date));
}

static void OnSave_Date(void)
{
    unsigned int year;
    unsigned char month, day;

    UI_GetEditDate(&year, &month, &day);
    RTC_set_year(year);
    RTC_set_month(month);
    RTC_set_day(day);

    OnExit_Date();
}

static void OnEnter_Date(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);
    UI_SetDisplayButton(UI_BTN_ENTER, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Date);
    Buttons_SetHandler(BTN_ENTER, OnSave_Date);

    const TDateTime *dt = RTC_GetDateTime();
    UI_SetEditDate(UI_GetText(TXTID_Date), dt->year, dt->month, dt->day);
}

static void OnExit_Time(void)
{
    UI_SetMenu(MENUID_Setting, GetMenuItemIndex(MENUID_Setting, OnEnter_Time));
}

static void OnSave_Time(void)
{
    unsigned char hour, minute, second;

    UI_GetEditTime(&hour, &minute, &second);
    RTC_set_hour(hour);
    RTC_set_minute(minute);
    RTC_set_second(second);

    OnExit_Time();
}

static void OnEnter_Time(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);
    UI_SetDisplayButton(UI_BTN_ENTER, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Time);
    Buttons_SetHandler(BTN_ENTER, OnSave_Time);

    const TDateTime *dt = RTC_GetDateTime();
    UI_SetEditTime(UI_GetText(TXTID_Time), dt->hour, dt->minute, dt->second);
}

static void OnExit_Backlight(void)
{
    UI_SetMenu(MENUID_Setting, GetMenuItemIndex(MENUID_Setting, OnEnter_Backlight));
}

static void OnSave_Backlight(void)
{
    RAM_Write_Data_Long(UI_GetEditEnum(), BACKLIGHT);

    if (RAM_Read_Data_Long(BACKLIGHT))
        DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
    else
        DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;

    OnExit_Backlight();
}

const TEnumItem ENUM_ITEMS_Backlight[] =
{
    { 0, TXTID_OFF },
    { 1, TXTID_ON },
};

static void OnEnter_Backlight(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);
    UI_SetDisplayButton(UI_BTN_ENTER, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Backlight);
    Buttons_SetHandler(BTN_ENTER, OnSave_Backlight);

    UI_SetEditEnum(UI_GetText(TXTID_Backlight), RAM_Read_Data_Long(BACKLIGHT),
                   ENUM_ITEMS_Backlight, sizeof(ENUM_ITEMS_Backlight) / sizeof(ENUM_ITEMS_Backlight[0]));
}

const TMenuItem MENU_ITEMS_Setting[] =
{
    { TXTID_Date, OnEnter_Date },
    { TXTID_Time, OnEnter_Time },
    { TXTID_Backlight, OnEnter_Backlight },
};

const TMenu MENU_Setting =
{
    MENU_ITEMS_Setting,
    sizeof(MENU_ITEMS_Setting) / sizeof(MENU_ITEMS_Setting[0]),
    OnExit_Setting
};

//------------------------------------------------------------------------------
//  Menu - Info
//------------------------------------------------------------------------------

static void OnEnter_UnitNo(void);
static void OnEnter_Firmware(void);
static void OnEnter_CalibrationDate(void);

static void OnExit_Info(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_Info));
}

static void OnExit_UnitNo(void)
{
    UI_SetMenu(MENUID_Info, GetMenuItemIndex(MENUID_Info, OnEnter_UnitNo));
}

static void OnEnter_UnitNo(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_UnitNo);

    char str[30];
    sprintf(str, "%ld", RAM_Read_Data_Long(UNIT_NO));
    UI_SetInfo(UI_GetText(TXTID_UnitNo), str);
}

static void OnExit_Firmware(void)
{
    UI_SetMenu(MENUID_Info, GetMenuItemIndex(MENUID_Info, OnEnter_Firmware));
}

static void OnEnter_Firmware(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_Firmware);

    char str[30];
    sprintf(str, "%d.%d", RAM_Read_Data_Long(FIRMWARE) / 100, RAM_Read_Data_Long(FIRMWARE) % 100);
    UI_SetInfo(UI_GetText(TXTID_Firmware), str);
}

static void OnExit_CalibrationDate(void)
{
    UI_SetMenu(MENUID_Info, GetMenuItemIndex(MENUID_Info, OnEnter_CalibrationDate));
}

static void OnEnter_CalibrationDate(void)
{
    UI_InitDisplayButtons();
    UI_SetDisplayButton(UI_BTN_ESC, TRUE);

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnExit_CalibrationDate);

    char str[30];
    unsigned long date = RAM_Read_Data_Long(CALIB_DATE);
    sprintf(str, "%d.%d.%d", RTC_GetDayFromBCD(date), RTC_GetMonthFromBCD(date), RTC_GetYearFromBCD(date));
    UI_SetInfo(UI_GetText(TXTID_CalibrationDate), str);
}

const TMenuItem MENU_ITEMS_Info[] =
{
    { TXTID_UnitNo, OnEnter_UnitNo },
    { TXTID_Firmware, OnEnter_Firmware },
    { TXTID_CalibrationDate, OnEnter_CalibrationDate },
};

const TMenu MENU_Info =
{
    MENU_ITEMS_Info,
    sizeof(MENU_ITEMS_Info) / sizeof(MENU_ITEMS_Info[0]),
    OnExit_Info
};

//------------------------------------------------------------------------------
//  Prevodni tabulka Identifikator -> menu
//------------------------------------------------------------------------------
const TMenu* ID_TO_MENU_TABLE[] =
{
    &MENU_Main,
    &MENU_TestHw,
    &MENU_MainTestData,
    &MENU_Setting,
    &MENU_Info,
};

//------------------------------------------------------------------------------
//  Vrati menu podle identifikatoru.
//------------------------------------------------------------------------------
const TMenu* UI_GetMenu(TUIMenuId id)
{
    return ID_TO_MENU_TABLE[id];
}

//------------------------------------------------------------------------------
//  Nastavi hlavni menu s predvybranou polozkou hlavniho testu
//  (postara se taky o tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetMainMenuWithMainTestItem(void)
{
    UI_SetMenu(MENUID_Main, GetMenuItemIndex(MENUID_Main, OnEnter_MainTest));
}
