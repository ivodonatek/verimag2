
extern void delay_us(void);
extern void delay_2us(void);
extern void delay_5us(void);
extern void delay_10us(void);
extern void delay_1ms(void);
extern void delay_10ms(void);
extern void delay_100ms(void);
extern void delay_seconds(unsigned int seconds);
