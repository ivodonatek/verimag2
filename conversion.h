//------------------------------------------------------------------------------
//  Prevody jednotek velicin.
//------------------------------------------------------------------------------

#ifndef CONVERSION_H_INCLUDED
#define CONVERSION_H_INCLUDED

//------------------------------------------------------------------------------
//  Konverze teploty v �C na Kelvin.
//  T(K) = T(C) + 273,15
//------------------------------------------------------------------------------
float Convert_CelsiusToKelvin(float tempCelsius);

//------------------------------------------------------------------------------
//  Konverze teploty v Kelvinech na �C.
//  T(C) = T(K) - 273,15
//------------------------------------------------------------------------------
float Convert_KelvinToCelsius(float tempKelvin);

//------------------------------------------------------------------------------
//  Konverze teploty v Kelvinech na �F.
//  T(F) = 1,8 � (T(K) - 273,15) + 32
//------------------------------------------------------------------------------
float Convert_KelvinToFahrenheit(float tempKelvin);

//------------------------------------------------------------------------------
//  Konverze tlaku v Pa (Pascal) na psi (Pounds per square inch).
//------------------------------------------------------------------------------
float Convert_PascalToPsi(float pressPascal);

//------------------------------------------------------------------------------
//  Konverze tlaku v psi (Pounds per square inch) na Pa (Pascal).
//------------------------------------------------------------------------------
float Convert_PsiToPascal(float pressPsi);

//------------------------------------------------------------------------------
//  Konverze tlaku v Pa (Pascal) na bar (Bar).
//------------------------------------------------------------------------------
float Convert_PascalToBar(float pressPascal);

//------------------------------------------------------------------------------
//  Konverze tlaku v bar (Bar) na Pa (Pascal).
//------------------------------------------------------------------------------
float Convert_BarToPascal(float pressBar);

#endif // CONVERSION_H_INCLUDED
