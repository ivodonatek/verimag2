//******************************************************************************
//  AD prevodnik
//*****************************************************************************/

#ifndef AD_H_INCLUDED
#define AD_H_INCLUDED

// AD kanal
typedef enum
{
    ADCH_MEAS_1,
    ADCH_MEAS_2,
    ADCH_EXCITATION_IN,

} TADChannel;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void AD_Init(void);

//------------------------------------------------------------------------------
//  Cteni AD dat.
//------------------------------------------------------------------------------
unsigned int AD_Read(TADChannel channel);

#endif // AD_H_INCLUDED
