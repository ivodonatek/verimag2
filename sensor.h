extern void sensor_communication(void);
void send_calibration(unsigned char id);
void recv_calibration(unsigned char id);
void request_data(void);
void send_ser_numb(unsigned long serial);
void recv_ser_numb(void);
unsigned char zpracovat_data(void);
void sensor_cisteni(unsigned char clean);
void sensor_buzeni(unsigned char power);
void sensor_frekvence(unsigned char frequnecy);
void sensor_stav_buzeni(void);
void sensor_stav_frekvence(void);
void recv_firmware(void);
//void odesli_user_hesla(void);
//void odesli_factory_heslo(void);
void recv_zero_constant(void);
void send_zero_constant(void);

///sensor 8
unsigned char inicializace_senzoru_senzor8(void );
void used_sensor8(void);
void send_totals_sensor8(void);
void request_totals_sensor8(void);
void send_calibration_sensor8(void);
void odesli_heslo_sensor8(void);
void recv_calibration_sensor8(void);
void zpracovat_data_sensor8(void);
void aktualizace_sensor8(void);
void co_sekunda_sensor8(void);
void request_data_sensor8(void);
void nastav_buzeni_sensor8(void);
void sensor_stav_buzeni_sensor8(void);
void recv_firmware_sensor8(void);
void delete_volume_sensor8(void);
void delete_positive_sensor8(void);
void delete_negative_sensor8(void);
void send_zero_flow_sensor8(void);
void send_zero_erase_sensor8(void);
void send_zero_constant_sensor8(long konstanta);
void recv_zero_constant_sensor8(void);
void send_unit_no_sensor8(void);
void recv_unit_no_sensor8(void);
void recv_diameter_sensor8(void);
void send_diameter_sensor8(void);
void recv_demo_sensor8(void);
void send_demo_sensor8(void);
void recv_simulated_flow_sensor8(void);
void send_simulated_flow_sensor8(void);
void delete_aux_sensor8(void);
void send_cisteni_sensor8(unsigned char power);
void recv_cisteni_sensor8(void);
void send_flow_range_sensor8(void);
void recv_flow_range_sensor8(void);
void send_air_constant_sensor8(void);
void recv_air_constant_sensor8(void);
void send_air_detector_sensor8(void);
void recv_air_detector_sensor8(void);
void send_measure_state_sensor8(void);
void recv_measure_state_sensor8(void);
void send_average_samples_sensor8(void);
void recv_average_samples_sensor8(void);
void send_low_flow_cut_sensor8(void);
void recv_low_flow_cut_sensor8(void);
void send_invert_flow_sensor8(void);
void recv_invert_flow_sensor8(void);
void send_start_delay_sensor8(void);
void recv_start_delay_sensor8(void);

///sensor 7
unsigned char inicializace_senzoru_senzor7(void);
void used_sensor7(void);
void co_sekunda_sensor7(void);
void send_calibration_sensor7(unsigned char id);
unsigned char sensor_cisteni_sensor7(unsigned char clean);
unsigned char sensor_buzeni_sensor7(unsigned char power);
unsigned char sensor_frekvence_sensor7(unsigned char frequenc);
unsigned char sensor_stav_buzeni_sensor7(void);
unsigned char request_data_sensor7(void);
unsigned char recv_calibration_sensor7(unsigned char id);
unsigned char send_ser_numb_sensor7(unsigned long serial);
unsigned char recv_ser_numb_sensor7(void);
unsigned char zpracovat_data_sensor7(void);
unsigned char recv_firmware_sensor7(void);
void send_zero_constant_sensor7(void);
void recv_zero_constant_sensor7(void);
unsigned char sensor_stav_frekvence_sensor7(void);

extern unsigned char pozice_bufferu;
long prutok;
unsigned long teplota;
unsigned long ep;
unsigned long actual_flow_s8;

extern volatile unsigned char flag_receive;
extern unsigned char flag_kalibrace_send;
extern unsigned char flag_kalibrace_recv;
extern volatile unsigned char flag_cekam_odpoved_od_sensor8;
extern unsigned char flag_cekam;
volatile unsigned char flag_modbus_ukon;
extern unsigned char recv_buffer[80];
extern unsigned char flag_second_graph;

#define DATA_ZE_SENZORU         0x01
#define KALIBRACE_VBODE1        0x02
#define KALIBRACE_VBODE2        0x03
#define KALIBRACE_VBODE3        0x04
#define KALIBRACNI_DATA1        0x05
#define KALIBRACNI_DATA2        0x06
#define KALIBRACNI_DATA3        0x07
#define ZAPIS_SERIOVEHO_CISLA   0x08
#define CTENI_SERIOVEHO_CISLA   0x09
#define ZAPNI_BUZENI            0x0A
#define NASTAV_FREKVENCI        0x0B
#define ZJISTI_STAV_BUZENI      0x0C
#define ZJISTI_FREKVENCI        0x0D
#define ZAPNI_CISTENI           0x0E
//modbus
#define CTENI_FIRMWARE          0x0F
#define ODESLI_HESLA            0x10
#define CTENI_ZERO_CONSTANT     0x11
#define ZAPIS_ZERO_CONSTANT     0x12
#define ZAPIS_KALIBRACI         0x13
#define ZJISTI_KALIBRACI        0x14
#define DELETE_VOLUME           0x15
#define DELETE_NEGATIVE         0x16
#define DELETE_POSITIVE         0x17
#define READ_DIAMETER           0x18
#define SEND_DIAMETER           0x19
#define READ_FLOW_RANGE         0x1A
#define SEND_FLOW_RANGE         0x1B
#define READ_MEASUREMENT        0x1C
#define SEND_MEASUREMENT        0x1D
#define READ_AIR_DETECTOR       0x1E
#define SEND_AIR_DETECTOR       0x1F
#define READ_AIR_CONSTANT       0x20
#define SEND_AIR_CONSTANT       0x21
#define READ_AVERAGE_SAMPLES    0x22
#define SEND_AVERAGE_SAMPLES    0x27
#define READ_LOW_FLOW_CUT       0x23
#define SEND_LOW_FLOW_CUT       0x24
#define READ_INVERT_FLOW        0x25
#define SEND_INVERT_FLOW        0x26
#define READ_CLEAN_POWER        0x29
#define SEND_CLEAN_POWER        0x2A
#define DELETE_AUX              0x2D
#define SEND_DEMO               0x2E
#define READ_DEMO               0x2F
#define SEND_SIMULATED_FLOW     0x30
#define READ_SIMULATED_FLOW     0x31
#define SEND_ZERO_FLOW          0x32
#define READ_ZERO_FLOW          0x33
#define SEND_ZERO_ERASE         0x34
#define READ_UNIT_NO            0x35
#define SEND_START_DELAY        0x36
#define READ_START_DELAY        0x37
#define SEND_UNIT_NO            0x38
#define SEND_TOTALS             0x39
#define READ_TOTALS             0x3A


#define SENSOR_ON               0xff
#define SENSOR_OFF              0x00

#define FREKVENCE_3HZ           0x00
#define FREKVENCE_6HZ           0x01

#define DEVICE_ID               0x0D7985CA

/// pro errory
#define ZADOST_ZAPIS_FREKVENCE  (1<<0)
#define ZADOST_ZAPIS_BUZENI     (1<<1)
#define ZADOST_STAV_BUZENI      (1<<2)
#define ZADOST_STAV_FREKVENCE   (1<<3)
#define ZADOST_ZAPIS_CISTENI    (1<<4)
#define AKTIVNI_ZADOST          (1<<5)
#define ZADOST_ZJISTI_KALIBRACI (1<<6)
#define ZADOST_ZAPIS_KALIBRACI  (1<<7)

///Error code
#define ERR_ILEGAL_FUNCTION         0x01
#define ERR_ILEGAL_ADRES            0x02
#define ERR_ILEGAL_VALUE            0x03
#define ERR_DEVICE_FAIL             0x04    //pri provadeni doslo k zavaznemu problem
#define ERR_CHECK                   0x05    //pozadavek prijat, bude trvat delsi dobu jeho vykonani
#define ERR_DEVICE_BUSY             0x06
#define ERR_BUFF_PARITY             0x08
#define ERR_GATEWAY_OVERRUN         0x0A
#define ERR_GATEWAY_NOT_RESPONSE    0x0B

#define ERR_ANSWER                  0x90

///function
#define READ_HOLDING_REGISTER       0x03
#define WRITE_MULTIPLE_REGISTER     0x10

///slave adresa sensoru
#define SLAVE_ID 0x01

///modbus adresy
#define MODBUS_KALIB_1          (4004-1)
#define MODBUS_KALIB_2          (4008-1)
#define MODBUS_KALIB_3          (4012-1)
#define MODBUS_HESLA            (2-1)
#define MODBUS_FLOW             (100-1)
#define MODBUS_ERROR_CODE       (120-1)
#define MODBUS_TOTAL2           (130-1)
#define MODBUS_DELETE_VOLUME    (3004-1)
#define MODBUS_DELETE_NEGATIVE  (3006-1)
#define MODBUS_DELETE_POSITIVE  (3008-1)
#define MODBUS_UNIT_NO          (1000-1)
#define MODBUS_ERROR_MIN        (1002-1)
#define MODBUS_OK_MIN           (1004-1)
#define MODBUS_FIRM_WARE        (1008-1)
#define MODBUS_DIAMETER         (4000-1)
#define MODBUS_FLOW_RANGE       (2012-1)
#define MODBUS_MEASUREMENT      (2000-1)
#define MODBUS_AIR_DETECTOR     (2002-1)
#define MODBUS_AIR_CONSTANT     (2004-1)
#define MODBUS_AVERAGE_SAMPLES  (2006-1)
#define MODBUS_LOW_FLOW_CUT     (2008-1)
#define MODBUS_INVERT_FLOW      (2010-1)
#define MODBUS_CLEAN_POWER      (2014-1)
#define MODBUS_DELETE_AUX       (2016-1)
#define MODBUS_DEMO             (3010-1)
#define MODBUS_SIMULATED_FLOW   (3012-1)
#define MODBUS_SET_UNIT         (4002-1)
#define MODBUS_ZERO_FLOW        (4016-1)
#define MODBUS_ZERO_CONST       (4018-1)
#define MODBUS_ZERO_ERASE       (4020-1)
#define MODBUS_EXCITATION_FREQ  (4022-1)
#define MODBUS_EXCITATION_POWER (4024-1)
#define MODBUS_START_DELAY      (2024-1)

///debugovani
//#define DEBUGGER    1


