//******************************************************************************
//  Nastaveni a prepinani mereni pomoci 4052
//  (Dual 4-channel analog multiplexer/demultiplexer).
//*****************************************************************************/

#ifndef SWITCH_H_INCLUDED
#define SWITCH_H_INCLUDED

// Nastaveni mereni
typedef enum
{
    S1_Q0,  // C1:0,5mA source, C2:GND, E1:, E2:, Temp:
    S1_Q1,  // C1:GND, C2:0,5mA source, E1:, E2:, Temp:
    S1_Q2,  // C1:, C2:, E1:0,5mA source, E2:GND, Temp:
    S1_Q3,  // C1:, C2:, E1:, E2:, Temp:0,5mA source

} TSwitch1Setup;

// Prepinani mereni
typedef enum
{
    S2_Q0,  // Meas1:, Meas2:
    S2_Q1,  // Meas1:Temp. Meas-S8, Meas2:
    S2_Q2,  // Meas1:E1, Meas2:C1
    S2_Q3,  // Meas1:E2, Meas2:C2

} TSwitch2Setup;

// Test
typedef enum
{
    SW_TEST1,  // Test 1, S1_Q0, S2_Q2, Meas2 (napeti na civce C1)
    SW_TEST2,  // Test 2, S1_Q1, S2_Q3, Meas2 (napeti na civce C2)
    SW_TEST3,  // Test 3, S1_Q2, S2_Q2, Meas1 (mozny zkrat)
    SW_TEST4,  // Test 4, S1_Q3, S2_Q1, Meas1 (Temp measurement - sensor 8)

} TSwitchTest;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Switch_Init(void);

//------------------------------------------------------------------------------
//  Nastaveni testu.
//------------------------------------------------------------------------------
void Switch_SetTest(TSwitchTest test);

//------------------------------------------------------------------------------
//  Odpojeni mericich obvodu.
//------------------------------------------------------------------------------
void Switch_Disconnect(void);

#endif // SWITCH_H_INCLUDED
