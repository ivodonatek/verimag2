#include "calibration.h"
#include "menu_tables.h"
#include "maine.h"

float Compute_Actual_Flow_From_AD_Value (long actual_AD_average)
{
  float a = 0;
  float b = 0;

  if (RAM_Read_Data_Long(CALIBRATION_POINT_THREE))
  {
    if ((actual_AD_average > (long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))) &&
        (actual_AD_average < (long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))))
    {
      // vypocet hodnoty a pro rovnici primky
      a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_TWO)-RAM_Read_Data_Long(CALIBRATION_POINT_THREE))/1000.0) /
            ((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))-(long)(RAM_Read_Data_Long(MEASURED_POINT_THREE)));
      // vypocet hodnoty b pro rovnici primky
      b  = (RAM_Read_Data_Long(CALIBRATION_POINT_TWO)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))*a);
    }
    else if ((actual_AD_average < (long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))) &&
             (actual_AD_average > (long)(RAM_Read_Data_Long(MEASURED_POINT_ONE))))
    {
      // vypocet hodnoty a pro rovnici primky
      a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_THREE)-RAM_Read_Data_Long(CALIBRATION_POINT_ONE))/1000.0) /
           ((long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))-(long)(RAM_Read_Data_Long(MEASURED_POINT_ONE)));
      // vypocet hodnoty b pro rovnici primky
      b  = (RAM_Read_Data_Long(CALIBRATION_POINT_THREE)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))*a);
    }
    //kalibrace v zapornem prutoku
    else if ((actual_AD_average < (long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))) &&
            (actual_AD_average > (long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))))
    {
      // vypocet hodnoty a pro rovnici primky
      a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_TWO)-RAM_Read_Data_Long(CALIBRATION_POINT_THREE))/1000.0) /
           ((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))-(long)(RAM_Read_Data_Long(MEASURED_POINT_THREE)));
      // vypocet hodnoty b pro rovnici primky
      b  = (RAM_Read_Data_Long(CALIBRATION_POINT_TWO)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))*a);
    }
    else if ((actual_AD_average > (long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))) &&
            (actual_AD_average < (long)(RAM_Read_Data_Long(MEASURED_POINT_ONE))))
    {
      // vypocet hodnoty a pro rovnici primky
      a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_THREE)-RAM_Read_Data_Long(CALIBRATION_POINT_ONE))/1000.0) /
           ((long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))-(long)(RAM_Read_Data_Long(MEASURED_POINT_ONE)));
      // vypocet hodnoty b pro rovnici primky
      b  = (RAM_Read_Data_Long(CALIBRATION_POINT_THREE)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_THREE))*a);
    }
    else
    {
      // vypocet hodnoty a pro rovnici primky
      a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_TWO)-RAM_Read_Data_Long(CALIBRATION_POINT_ONE))/1000.0) /
           ((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))-(long)(RAM_Read_Data_Long(MEASURED_POINT_ONE)));
      // vypocet hodnoty b pro rovnici primky
      b  = (RAM_Read_Data_Long(CALIBRATION_POINT_TWO)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))*a);
    }
  }
  else
  {
    // vypocet hodnoty a pro rovnici primky
    a  = ((RAM_Read_Data_Long(CALIBRATION_POINT_TWO)-RAM_Read_Data_Long(CALIBRATION_POINT_ONE))/1000.0) /
         ((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))-(long)(RAM_Read_Data_Long(MEASURED_POINT_ONE)));
    // vypocet hodnoty b pro rovnici primky
    b  = (RAM_Read_Data_Long(CALIBRATION_POINT_TWO)/1000.0)-((long)(RAM_Read_Data_Long(MEASURED_POINT_TWO))*a);
  }
  return a*actual_AD_average + b;
}
