//----------------------------------------------------------------------------
//  Ovladac LDC1000 (Inductance-to-Digital Converter).
//----------------------------------------------------------------------------

#ifndef LDC1000_H_INCLUDED
#define LDC1000_H_INCLUDED

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// LDC1000 registry
#define LDC1000_Device_ID_REG               (0x00)    /* Device ID */
#define LDC1000_RP_MAX_REG                  (0x01)    /* RP Maximum */
#define LDC1000_RP_MIN_REG                  (0x02)    /* RP Minimum */
#define LDC1000_Watchdog_Timer_Freq_REG     (0x03)    /* Min Sensor Frequency */
#define LDC1000_LDC_Config_REG              (0x04)    /* LDC Configuration */
#define LDC1000_Clock_Config_REG            (0x05)    /* Clock Configuration */

#define LDC1000_Power_Config_REG            (0x0B)    /* Power Configuration */
#define LDC1000_Status_REG                  (0x20)    /* Status */
#define LDC1000_Proximity_LSB_REG           (0x21)    /* Proximity Data [7:0] Data LSB */
#define LDC1000_Proximity_MSB_REG           (0x22)    /* Proximity Data [15:8] Data MSB */
#define LDC1000_FCOUNT_LSB_REG              (0x23)    /* Frequency Counter Data LSB */
#define LDC1000_FCOUNT_Mid_Byte_REG         (0x24)    /* Frequency Counter Data Mid-Byte */
#define LDC1000_FCOUNT_MSB_REG              (0x25)    /* Frequency Counter Data MSB */

// Bity LDC1000 registru
/* Status
7 OSC Status
1:Indicates sensor oscillation timeout. This can be caused by a sensor with
an RP below RP_MIN or setting Min Sensor Frequency too high.
0:Sensor oscillation timeout not detected

6 Data Ready
1:No new data available
0:Data is ready to be read

5 Wake-up
1:Wake-up disabled
0:Wake-up triggered. Proximity data is more than Threshold High value.

4 Comparator
1:Proximity data is less than Threshold Low value
0:Proximity data is more than Threshold High value
*/
#define LDC1000_Status_REG_Data_Ready_BIT       (1<<6)

//------------------------------------------------------------------------------
//  Configures the SPI port and initializes it.
//------------------------------------------------------------------------------
void LDC1000_SPISetup(void);

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr".
//------------------------------------------------------------------------------
void LDC1000_SPIWriteReg(uint8_t addr, uint8_t value);

//------------------------------------------------------------------------------
//  Reads a single register at address "addr" and returns the value read.
//------------------------------------------------------------------------------
uint8_t LDC1000_SPIReadReg(uint8_t addr);

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr". Reads a single
//  register at address "addr" and returns TRUE if the value read is the same
//  as in write operation.
//------------------------------------------------------------------------------
BOOL LDC1000_SPIWriteReadReg(uint8_t addr, uint8_t value);

//------------------------------------------------------------------------------
//  Writes values to multiple registers, the first register being
//  at address "addr".  First data byte is at "buffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//------------------------------------------------------------------------------
void LDC1000_SPIWriteRegs(uint8_t addr, uint8_t *buffer, uint8_t count);

//------------------------------------------------------------------------------
//  Reads multiple registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "buffer", until "count" registers have been read.
//------------------------------------------------------------------------------
void LDC1000_SPIReadRegs(uint8_t addr, uint8_t *buffer, uint8_t count);

//------------------------------------------------------------------------------
//  Writes values to multiple registers, the first register being
//  at address "addr".  First data byte is at "writeBuffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "readBuffer", until "count" registers have been read.
//  Returns TRUE if the values read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL LDC1000_SPIWriteReadRegs(uint8_t addr, uint8_t *writeBuffer,
                              uint8_t *readBuffer, uint8_t count);

#endif // LDC1000_H_INCLUDED
