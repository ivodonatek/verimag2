//----------------------------------------------------------------------------
//  Ovladac LDC1000 (Inductance-to-Digital Converter).
//----------------------------------------------------------------------------

#include <stdint.h>
#include "LPC23xx.h"
#include "LDC1000.h"

/* Power Control for Peripherals register (PCONP) SSP bits
    - The SSP1 interface power/clock enabled */
#define PCONP_SSP_MASK   0x400
#define PCONP_SSP_VALUE  0x400

/* Peripheral Clock Selection register (PCLKSELx)
    - Peripheral clock selection for SSP1 */
#define PCLKSEL PCLKSEL0

/* Peripheral Clock Selection register (PCLKSELx) SSP bits
    - PCLK_xyz = CCLK/4 */
#define PCLKSEL_SSP_MASK   0x300000
#define PCLKSEL_SSP_VALUE  0x0

/* SSPx Clock Prescale Register (SSPxCPSR) */
#define SSPCPSR SSP1CPSR

/* SSPx Clock Prescale Register (SSPxCPSR) value */
#define SSPCPSR_VALUE   30

/* Pin Function Select register (PINSELx) */
#define PINSEL PINSEL0

/* Pin Function Select register (PINSELx) SSP bits
    - GPIO: SSEL1,
    - SSP: SCK1, MISO1, MOSI1 */
#define PINSEL_SSP_MASK   0xFF000
#define PINSEL_SSP_VALUE  0xA8000

/* Pin Mode select register (PINMODEx) */
#define PINMODE PINMODE0

/* Pin Mode select register (PINMODEx) SSP bits
    - SSEL1: pull-up resistor enabled
    - SCK1: pull-up resistor enabled
    - MISO1: pull-up resistor enabled
    - MOSI1: pull-up resistor enabled */
#define PINMODE_SSP_MASK   0xFF000
#define PINMODE_SSP_VALUE  0x0

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) */
#define SSPIMSC SSP1IMSC

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) value
    - all interrupts disabled */
#define SSPIMSC_VALUE   0

/* Interrupt Enable Clear Register (VICIntEnClear) value
    - SSP1 */
#define VICIntEnClr_SSP_VALUE 0x800

/* SSPx Control Register 0 (SSPxCR0) */
#define SSPCR0 SSP1CR0

/* SSPx Control Register 0 (SSPxCR0) value
    - SCR=0
    - CPHA=1
    - CPOL=1
    - Frame Format SPI
    - Data Size 8 bit transfer */
#define SSPCR0_VALUE 0xC7

/* SSPx Control Register 1 (SSPxCR1) */
#define SSPCR1 SSP1CR1

/* SSPx Control Register 1 (SSPxCR1) value
    - The SSP controller acts as a master on the bus
    - The SSP controller is disabled
    - normal operation */
#define SSPCR1_VALUE 0x0

/* Enable SSPx */
#define SSP_ENABLE() (SSPCR1 = (SSPCR1 & 0xD) | 2)

/* Disable SSPx */
#define SSP_DISABLE() (SSPCR1 &= 0xD)

/* GPIO Port Direction control register */
#define IODIR   IODIR0

/* GPIO Port Direction control register SSP bits
    - SSEL1: output */
#define IODIR_SSP_MASK   0x40
#define IODIR_SSP_VALUE  0x40

/* GPIO Port SSEL pin mask */
#define SSEL_PIN_MASK   0x40

/* GPIO Port Output Set register */
#define IOSET   IOSET0

/* GPIO Port Output Clear register */
#define IOCLR   IOCLR0

/* SSEL pin high */
#define SSEL_PIN_HIGH() (IOSET = SSEL_PIN_MASK)

/* SSEL pin low */
#define SSEL_PIN_LOW() (IOCLR = SSEL_PIN_MASK)

/* SSPx Status Register (SSPxSR) */
#define SSPSR  SSP1SR

/* SSPx Busy */
#define SSP_BUSY() (SSPSR & 0x10)

/* SSPx Transmit FIFO Not Full */
#define SSP_TXBUF_READY() (SSPSR & 2)

/* SSPx Receive FIFO Empty */
#define SSP_RXBUF_EMPTY() (!(SSPSR & 4))

/* SSPx Data Register (SSPxDR) */
#define SSPDR  SSP1DR

// SPI definice
#define SPI_RA_MASK                               (0x7F)
#define SPI_READ_BIT                              (0x80)
#define SPI_WRITE_BIT                             (0x00)

//------------------------------------------------------------------------------
//  Clear SSP Rx FIFO buffer.
//------------------------------------------------------------------------------
static void SPIClearRxBuffer(void)
{
  uint8_t dummy;

  while (SSP_BUSY());
  while (!SSP_RXBUF_EMPTY())
  {
      dummy = SSPDR;
  }
}

//------------------------------------------------------------------------------
//  Configures the SPI port and initializes it.
//------------------------------------------------------------------------------
void LDC1000_SPISetup(void)
{
    SSP_DISABLE();

    PCONP = (PCONP & ~PCONP_SSP_MASK) | PCONP_SSP_VALUE;
    PCLKSEL = (PCLKSEL & ~PCLKSEL_SSP_MASK) | PCLKSEL_SSP_VALUE;
    SSPCPSR = SSPCPSR_VALUE;
    PINSEL = (PINSEL & ~PINSEL_SSP_MASK) | PINSEL_SSP_VALUE;
    PINMODE = (PINMODE & ~PINMODE_SSP_MASK) | PINMODE_SSP_VALUE;
    SSPIMSC = SSPIMSC_VALUE;
    VICIntEnClr = VICIntEnClr_SSP_VALUE;
    SSPCR0 = SSPCR0_VALUE;
    SSPCR1 = SSPCR1_VALUE;
    IODIR = (IODIR & ~IODIR_SSP_MASK) | IODIR_SSP_VALUE;

    SSEL_PIN_HIGH();
    SSP_ENABLE();
}

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr".
//------------------------------------------------------------------------------
void LDC1000_SPIWriteReg(uint8_t addr, uint8_t value)
{
  uint8_t inst;

  SSEL_PIN_LOW();                                                              // /CS enable

  inst = SPI_WRITE_BIT | (addr & SPI_RA_MASK);                                 // register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send register address

  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = value;                                                               // Send data value

  while (SSP_BUSY());                                                          // Wait for TX complete

  SSEL_PIN_HIGH();                                                             // /CS disable
}

//------------------------------------------------------------------------------
//  Reads a single register at address "addr" and returns the value read.
//------------------------------------------------------------------------------
uint8_t LDC1000_SPIReadReg(uint8_t addr)
{
  uint8_t value, inst;

  SSEL_PIN_LOW();                                                              // /CS enable

  inst = SPI_READ_BIT | (addr & SPI_RA_MASK);                                  // register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send register address

  while (SSP_BUSY());                                                          // Wait for TX complete
  SPIClearRxBuffer();                                                          // Clear Rx buffer before reading data
  SSPDR = 0;                                                                   // Dummy write so we can read data

  while (SSP_BUSY());                                                          // Wait for TX complete
  value = SSPDR;                                                               // Read data

  SSEL_PIN_HIGH();                                                             // /CS disable

  return value;
}

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr". Reads a single
//  register at address "addr" and returns TRUE if the value read is the same
//  as in write operation.
//------------------------------------------------------------------------------
BOOL LDC1000_SPIWriteReadReg(uint8_t addr, uint8_t value)
{
    LDC1000_SPIWriteReg(addr, value);
    return LDC1000_SPIReadReg(addr) == value ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Writes values to multiple registers, the first register being
//  at address "addr".  First data byte is at "buffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//------------------------------------------------------------------------------
void LDC1000_SPIWriteRegs(uint8_t addr, uint8_t *buffer, uint8_t count)
{
  uint8_t inst, i;

  SSEL_PIN_LOW();                                                              // /CS enable

  inst = SPI_WRITE_BIT | (addr & SPI_RA_MASK);                                 // lower register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  for (i = 0; i < count; i++)
  {
    while (!SSP_TXBUF_READY());                                                // Wait for TXBUF ready
    SSPDR = *(buffer+i);                                                       // Send data value
  }

  while (SSP_BUSY());                                                          // Wait for TX complete

  SSEL_PIN_HIGH();                                                             // /CS disable
}

//------------------------------------------------------------------------------
//  Reads multiple registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "buffer", until "count" registers have been read.
//------------------------------------------------------------------------------
void LDC1000_SPIReadRegs(uint8_t addr, uint8_t *buffer, uint8_t count)
{
  uint8_t i, inst;

  SSEL_PIN_LOW();                                                              // /CS enable

  inst = SPI_READ_BIT | (addr & SPI_RA_MASK);                                  // lower register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  while (SSP_BUSY());                                                          // Wait for TX complete
  SPIClearRxBuffer();                                                          // Clear Rx buffer before reading data

  for (i = 0; i < count; i++)
  {
    SSPDR = 0;                                                                 // Dummy write so we can read data

    while (SSP_BUSY());                                                        // Wait for TX complete
    *(buffer+i) = SSPDR;                                                       // Read data
  }

  SSEL_PIN_HIGH();                                                             // /CS disable
}

//------------------------------------------------------------------------------
//  Writes values to multiple registers, the first register being
//  at address "addr".  First data byte is at "writeBuffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "readBuffer", until "count" registers have been read.
//  Returns TRUE if the values read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL LDC1000_SPIWriteReadRegs(uint8_t addr, uint8_t *writeBuffer,
                              uint8_t *readBuffer, uint8_t count)
{
    // write
    LDC1000_SPIWriteRegs(addr, writeBuffer, count);

    // Read back for test
    LDC1000_SPIReadRegs(addr, readBuffer, count);

    // test if write/read values match
    for (uint8_t i = 0; i < count; i++)
    {
        if (writeBuffer[i] != readBuffer[i])
            return FALSE;
    }

    return TRUE;
}
