//******************************************************************************
//  This file contains functions that allow to access the synchronous 3-wire
//  serial interface of the ADS7822.
//*****************************************************************************/

#ifndef ADS7822_H_INCLUDED
#define ADS7822_H_INCLUDED

//------------------------------------------------------------------------------
//  Configures the interface and initializes it.
//------------------------------------------------------------------------------
void ADS7822_Setup(void);

//------------------------------------------------------------------------------
//  Get 12-bit sample from ADS7822. Returns -1 if sample error.
//------------------------------------------------------------------------------
int16_t ADS7822_GetSample(void);

#endif // ADS7822_H_INCLUDED
