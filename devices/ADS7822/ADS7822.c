//******************************************************************************
//  This file contains functions that allow to access the synchronous 3-wire
//  serial interface of the ADS7822.
//*****************************************************************************/

#include <stdint.h>
#include "LPC23xx.h"
#include "ADS7822.h"
#include "delay.h"

/* Pin Function Select register (PINSELx)
    - ADS7822 CS/SHDN Pin */
#define PINSEL_CS PINSEL0

/* Pin Function Select register (PINSELx) bits for ADS7822 CS/SHDN Pin
    - GPIO: P0.0 */
#define PINSEL_CS_MASK   0x00000003
#define PINSEL_CS_VALUE  0x0

/* Pin Function Select register (PINSELx)
    - ADS7822 DCLOCK Pin */
#define PINSEL_DCLOCK PINSEL3

/* Pin Function Select register (PINSELx) bits for ADS7822 DCLOCK Pin
    - GPIO: P1.28 */
#define PINSEL_DCLOCK_MASK   0x03000000
#define PINSEL_DCLOCK_VALUE  0x0

/* Pin Function Select register (PINSELx)
    - ADS7822 DOUT Pin */
#define PINSEL_DOUT PINSEL3

/* Pin Function Select register (PINSELx) bits for ADS7822 DOUT Pin
    - GPIO: P1.29 */
#define PINSEL_DOUT_MASK   0x0C000000
#define PINSEL_DOUT_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 CS/SHDN Pin */
#define PINMODE_CS PINMODE0

/* Pin Mode select register (PINMODEx) bits for ADS7822 CS/SHDN Pin
    - P0.0: pull-up resistor enabled */
#define PINMODE_CS_MASK   0x00000003
#define PINMODE_CS_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 DCLOCK Pin */
#define PINMODE_DCLOCK PINMODE3

/* Pin Mode select register (PINMODEx) bits for ADS7822 DCLOCK Pin
    - P1.28: pull-up resistor enabled */
#define PINMODE_DCLOCK_MASK   0x03000000
#define PINMODE_DCLOCK_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 DOUT Pin */
#define PINMODE_DOUT PINMODE3

/* Pin Mode select register (PINMODEx) bits for ADS7822 DOUT Pin
    - P1.29: pull-up resistor enabled */
#define PINMODE_DOUT_MASK   0x0C000000
#define PINMODE_DOUT_VALUE  0x0

/* GPIO Port registers for ADS7822 CS/SHDN Pin */
#define IODIR_CS   IODIR0
#define IOPIN_CS   IOPIN0
#define IOSET_CS   IOSET0
#define IOCLR_CS   IOCLR0

/* GPIO Port registers for ADS7822 DCLOCK Pin */
#define IODIR_DCLOCK   IODIR1
#define IOPIN_DCLOCK   IOPIN1
#define IOSET_DCLOCK   IOSET1
#define IOCLR_DCLOCK   IOCLR1

/* GPIO Port registers for ADS7822 DOUT Pin */
#define IODIR_DOUT   IODIR1
#define IOPIN_DOUT   IOPIN1
#define IOSET_DOUT   IOSET1
#define IOCLR_DOUT   IOCLR1

/* GPIO Port Pin masks for ADS7822 */
#define CS_PIN_MASK         (1 << 0)
#define DCLOCK_PIN_MASK     (1 << 28)
#define DOUT_PIN_MASK       (1 << 29)

/* GPIO Port Direction control register values for ADS7822 pins */
#define IODIR_CS_VALUE          (1 << 0)
#define IODIR_DCLOCK_VALUE      (1 << 28)
#define IODIR_DOUT_VALUE        (0 << 29)

/* ADS7822 CS/SHDN Pin high */
#define CS_PIN_HIGH() (IOSET_CS = CS_PIN_MASK)

/* ADS7822 CS/SHDN Pin low */
#define CS_PIN_LOW() (IOCLR_CS = CS_PIN_MASK)

/* ADS7822 DCLOCK Pin high */
#define DCLOCK_PIN_HIGH() (IOSET_DCLOCK = DCLOCK_PIN_MASK)

/* ADS7822 DCLOCK Pin low */
#define DCLOCK_PIN_LOW() (IOCLR_DCLOCK = DCLOCK_PIN_MASK)

/* ADS7822 DOUT Pin read */
#define DOUT_PIN_READ() (IOPIN_DOUT & DOUT_PIN_MASK)

/* ADS7822 DCLOCK Pin read */
#define DCLOCK_PIN_READ() (IOPIN_DCLOCK & DCLOCK_PIN_MASK)

/* ADS7822 CS Pin read */
#define CS_PIN_READ() (IOPIN_CS & CS_PIN_MASK)

/* ADS7822 CS falling to DCLOCK rising delay in microseconds */
#define DELAY_TSUCS_MICROSEC        100

/* ADS7822 half clock period in microseconds */
#define DELAY_HALF_CLOCK_MICROSEC   250

//------------------------------------------------------------------------------
//  Delay in microseconds.
//------------------------------------------------------------------------------
static void delay(uint8_t microseconds)
{
    uint8_t i;

    for (i = 0; i < microseconds; i++)
    {
        delay_us();
    }
}

//------------------------------------------------------------------------------
//  Drive clock period.
//------------------------------------------------------------------------------
static void clock_period(void)
{
    DCLOCK_PIN_HIGH();
    delay(DELAY_HALF_CLOCK_MICROSEC);
    DCLOCK_PIN_LOW();
    delay(DELAY_HALF_CLOCK_MICROSEC);
}

//------------------------------------------------------------------------------
//  Configures the interface and initializes it.
//------------------------------------------------------------------------------
void ADS7822_Setup(void)
{
    // ADS7822 CS/SHDN Pin
    PINSEL_CS = (PINSEL_CS & ~PINSEL_CS_MASK) | PINSEL_CS_VALUE;
    PINMODE_CS = (PINMODE_CS & ~PINMODE_CS_MASK) | PINMODE_CS_VALUE;
    IODIR_CS = (IODIR_CS & ~CS_PIN_MASK) | IODIR_CS_VALUE;
    CS_PIN_HIGH();

    // ADS7822 DCLOCK Pin
    PINSEL_DCLOCK = (PINSEL_DCLOCK & ~PINSEL_DCLOCK_MASK) | PINSEL_DCLOCK_VALUE;
    PINMODE_DCLOCK = (PINMODE_DCLOCK & ~PINMODE_DCLOCK_MASK) | PINMODE_DCLOCK_VALUE;
    IODIR_DCLOCK = (IODIR_DCLOCK & ~DCLOCK_PIN_MASK) | IODIR_DCLOCK_VALUE;
    DCLOCK_PIN_HIGH();

    // ADS7822 DOUT Pin
    PINSEL_DOUT = (PINSEL_DOUT & ~PINSEL_DOUT_MASK) | PINSEL_DOUT_VALUE;
    PINMODE_DOUT = (PINMODE_DOUT & ~PINMODE_DOUT_MASK) | PINMODE_DOUT_VALUE;
    IODIR_DOUT = (IODIR_DOUT & ~DOUT_PIN_MASK) | IODIR_DOUT_VALUE;
}

//------------------------------------------------------------------------------
//  Get 12-bit sample from ADS7822. Returns -1 if sample error.
//------------------------------------------------------------------------------
int16_t ADS7822_GetSample(void)
{
    uint8_t clocks = 0;          // pocitadlo clk period
    uint16_t sample = 0;         // hodnota AD prevodu

    // zacatek prenosu
    DCLOCK_PIN_LOW();            // CS falling to DCLOCK low max 0ns
    CS_PIN_LOW();
    delay(DELAY_TSUCS_MICROSEC);

    // detekce null bitu
    do
    {
        clock_period();
        clocks++;
        if (clocks > 5)
        {
            CS_PIN_HIGH();
            DCLOCK_PIN_HIGH();
            return -1;           // chyba
        }
    }
    while (DOUT_PIN_READ());

    // prijem bitu dat
    for (clocks = 0; clocks < 12; clocks++)
    {
        clock_period();

        sample <<= 1;
        if (DOUT_PIN_READ())
            sample |= 1;
    }

    // konec prenosu
    DCLOCK_PIN_HIGH();
    delay(DELAY_HALF_CLOCK_MICROSEC);
    CS_PIN_HIGH();

    return sample;
}
