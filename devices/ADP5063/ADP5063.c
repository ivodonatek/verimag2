//----------------------------------------------------------------------------
//  Ovladac ADP5063 (Linear LiFePO4 Battery Charger).
//----------------------------------------------------------------------------

#include <stdint.h>
#include "LPC23xx.h"
#include "ADP5063.h"
#include "delay.h"

/* Power Control for Peripherals register (PCONP) I2C bits
    - The I2C0 interface power/clock control bit. */
#define PCONP_I2C_MASK   0x80
#define PCONP_I2C_VALUE  0x80

/* Peripheral Clock Selection register (PCLKSELx)
    - Peripheral clock selection for I2C0 */
#define PCLKSEL PCLKSEL0

/* Peripheral Clock Selection register (PCLKSELx) I2C bits
    - PCLK_xyz = CCLK/4 */
#define PCLKSEL_I2C_MASK   0xC000
#define PCLKSEL_I2C_VALUE  0x0

/* Pin Function Select register (PINSELx) */
#define PINSEL PINSEL1

/* Pin Function Select register (PINSELx) I2C bits
    - P0.27: SDA0,
    - P0.28: SCL0 */
#define PINSEL_I2C_MASK   0x3C00000
#define PINSEL_I2C_VALUE  0x1400000

/* I2C Interface registers */
#define I2CONSET   I20CONSET
#define I2STAT     I20STAT
#define I2DAT      I20DAT
#define I2ADR      I20ADR
#define I2SCLH     I20SCLH
#define I2SCLL     I20SCLL
#define I2CONCLR   I20CONCLR

/* Enable I2C */
#define I2C_ENABLE() (I2CONSET = 0x40)

/* Disable I2C */
#define I2C_DISABLE() (I2CONCLR = 0x40)

/* Transmit a START condition or transmit a repeated START condition */
#define I2C_START_COND() (I2CONSET = 0x20)

/* Start flag Clear bit */
#define I2C_START_CLEAR() (I2CONCLR = 0x20)

/* Transmit a STOP condition */
#define I2C_STOP_COND() (I2CONSET = 0x10)

/* I2C state changed */
#define I2C_STATE_CHANGED() (I2CONSET & 0x08)

/* I2C state clear */
#define I2C_STATE_CLEAR() (I2CONCLR = 0x08)

/* I2C chip address in write mode */
#define I2C_CHIP_WRITE_ADDR   0x28

/* I2C chip address in read mode */
#define I2C_CHIP_READ_ADDR   0x29

/* GPIO Port Direction control register for SYS_EN */
#define SYS_EN_IODIR   FIO3DIR

/* GPIO Port SYS_EN pin mask */
#define SYS_EN_PIN_MASK   (1<<26)

/* GPIO Pins for SYS_EN */
#define SYS_EN_IOPIN   FIO3PIN

static unsigned long i2Stat = 0;

//------------------------------------------------------------------------------
//  Reset I2C rozhrani.
//------------------------------------------------------------------------------
static void I2C_Reset(void)
{
    I2C_DISABLE();
    I2C_ENABLE();
}

//------------------------------------------------------------------------------
//  Cekani na stav sbernice, vraci TRUE pokud ocekavany stav nastal, jinak FALSE
//  a podniknou se kroky k osetreni stavu.
//------------------------------------------------------------------------------
static BOOL I2C_Wait_State(unsigned char state)
{
    delay_1ms();

    if (!I2C_STATE_CHANGED())
    {
        i2Stat = I2STAT;
        I2C_Reset();
        return FALSE;
    }

    if (I2STAT != state)
    {
        i2Stat = I2STAT;
        I2C_STATE_CLEAR();
        I2C_Reset();
        return FALSE;
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//  Configures the IO ports and initializes it.
//------------------------------------------------------------------------------
void ADP5063_IOSetup(void)
{
    SYS_EN_IODIR |= SYS_EN_PIN_MASK;
}

//------------------------------------------------------------------------------
//  Configures the I2C port and initializes it.
//------------------------------------------------------------------------------
void ADP5063_I2CSetup(void)
{
    I2C_DISABLE();

    PCONP = (PCONP & ~PCONP_I2C_MASK) | PCONP_I2C_VALUE;
    PCLKSEL = (PCLKSEL & ~PCLKSEL_I2C_MASK) | PCLKSEL_I2C_VALUE;
    PINSEL = (PINSEL & ~PINSEL_I2C_MASK) | PINSEL_I2C_VALUE;

    // ~100kHz
    I2SCLH = 144;
    I2SCLL = 144;

    I2C_ENABLE();
}

//------------------------------------------------------------------------------
//  Get I2STAT
//------------------------------------------------------------------------------
unsigned long ADP5063_GetI2STAT(void)
{
    return i2Stat;
}

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr".
//------------------------------------------------------------------------------
BOOL ADP5063_I2CWriteReg(uint8_t addr, uint8_t value)
{
    I2C_START_COND();

    if (!I2C_Wait_State(0x08))
        return FALSE;

    I2DAT = I2C_CHIP_WRITE_ADDR;
    I2C_START_CLEAR();
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x18))
        return FALSE;

    I2DAT = addr;
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x28))
        return FALSE;

    I2DAT = value;
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x28))
        return FALSE;

    I2C_STATE_CLEAR();

    I2C_STOP_COND();

    delay_1ms();

    return TRUE;
}

//------------------------------------------------------------------------------
//  Reads a single register at address "addr" and returns the value read.
//  Returns -1 if error detected.
//------------------------------------------------------------------------------
int16_t ADP5063_I2CReadReg(uint8_t addr)
{
    I2C_START_COND();

    if (!I2C_Wait_State(0x08))
        return -1;

    I2DAT = I2C_CHIP_WRITE_ADDR;
    I2C_START_CLEAR();
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x18))
        return -1;

    I2DAT = addr;
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x28))
        return -1;

    I2C_STATE_CLEAR();

    I2C_START_COND();

    if (!I2C_Wait_State(0x10))
        return -1;

    I2DAT = I2C_CHIP_READ_ADDR;
    I2C_START_CLEAR();
    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x40))
        return -1;

    I2C_STATE_CLEAR();

    if (!I2C_Wait_State(0x58))
        return -1;

    unsigned char rcvByte = I2DAT;
    I2C_STATE_CLEAR();

    I2C_STOP_COND();

    delay_1ms();

    return rcvByte;
}

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr". Reads a single
//  register at address "addr" and returns TRUE if the value read is the same
//  as in write operation.
//------------------------------------------------------------------------------
BOOL ADP5063_I2CWriteReadReg(uint8_t addr, uint8_t value)
{
    if (!ADP5063_I2CWriteReg(addr, value))
        return FALSE;

    return ADP5063_I2CReadReg(addr) == value ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Get SYS_EN output level.
//------------------------------------------------------------------------------
BOOL ADP5063_GetSysEnLevel(void)
{
    return (SYS_EN_IOPIN & SYS_EN_PIN_MASK) ? TRUE : FALSE;
}
