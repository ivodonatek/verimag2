//******************************************************************************
//  This file contains functions that allow to access the SPI
//  interface of the LMP90100.
//*****************************************************************************/

#ifndef TI_LMP90100_SPI_H_INCLUDED
#define TI_LMP90100_SPI_H_INCLUDED

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

//------------------------------------------------------------------------------
//  Configures the SPI port and initializes it.
//------------------------------------------------------------------------------
void TI_LMP90100_SPISetup(void);

//------------------------------------------------------------------------------
//  Writes "value" to a single configuration register at address "addr". If
//  "addr" lies within the same segment as "*pURA", it takes 1 less transaction
//  to write the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPIWriteReg(uint8_t addr, uint8_t value, uint8_t *pURA);

//------------------------------------------------------------------------------
//  Reads a single configuration register at address "addr" and returns the
//  value read. If "addr" lies within the same segment as "*pURA", it takes 1
//  less transaction to read the value.
//------------------------------------------------------------------------------
uint8_t TI_LMP90100_SPIReadReg(uint8_t addr, uint8_t *pURA);

//------------------------------------------------------------------------------
//  Writes "value" to a single configuration register at address "addr". If
//  "addr" lies within the same segment as "*pURA", it takes 1 less transaction
//  to write the value. Reads a single configuration register at address "addr"
//  and returns TRUE if the value read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL TI_LMP90100_SPIWriteReadReg(uint8_t addr, uint8_t value, uint8_t *pURA);

//------------------------------------------------------------------------------
//  Writes values to multiple configuration registers, the first register being
//  at address "addr".  First data byte is at "buffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  If "addr" lies within the same segment as "*pURA", it takes 1 less
//  transaction to write the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPINormalStreamWriteReg(uint8_t addr, uint8_t *buffer,
                                         uint8_t count, uint8_t *pURA);

//------------------------------------------------------------------------------
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "buffer", until "count" registers have been read. If "addr" lies within
//  the same segment as "*pURA", it takes 1 less transaction to read the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPINormalStreamReadReg(uint8_t addr, uint8_t *buffer,
                                        uint8_t count, uint8_t *pURA);

//------------------------------------------------------------------------------
//  Writes values to multiple configuration registers, the first register being
//  at address "addr".  First data byte is at "writeBuffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  If "addr" lies within the same segment as "*pURA", it takes 1 less
//  transaction to write the value.
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "readBuffer", until "count" registers have been read. If "addr" lies within
//  the same segment as "*pURA", it takes 1 less transaction to read the value.
//  Returns TRUE if the values read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL TI_LMP90100_SPINormalStreamWriteReadReg(uint8_t addr, uint8_t *writeBuffer,
                                         uint8_t *readBuffer, uint8_t count,
                                         uint8_t *pURA);

#endif // TI_LMP90100_SPI_H_INCLUDED
