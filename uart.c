/*****************************************************************************
 *   uart.c:  UART API file for NXP LPC23xx/24xx Family Microprocessors
 *
 *   Copyright(C) 2006, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2006.07.12  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/

// modified by Martin Thomas
// - const BYTE in send

#include "LPC23xx.h"                        /* LPC23xx/24xx definitions */
#include "type.h"
#include "target.h"
#include "irq.h"
#include "uart.h"
#include "sensor.h"
#include "maine.h"
#include "timer.h"
#include "delay.h"
#include <string.h>

#define BUFSIZE UART_BUFSIZE




DWORD Sensor_init( DWORD baudrate )
{
  DWORD Fdiv;
	PCONP |= (1<<25);      //set power on UART3
	PINSEL9 |= (1<<24) | (1<<25) | (1<<26) | (1<<27);   //nastaveni TxD3 a RxD3 na pinz 4.28,4.29
  U3LCR = 0x83;   //8bits,no Paritz,1 stop, !!! DLAB=0 !!!!

	Fdiv = ( Fpclk / 16 ) / baudrate;	//baud rate
  U3FCR = 0x07;   //FIFO Enable and restart tx and rx

  U3DLM = Fdiv / 256;
  U3DLL = Fdiv % 256;

  IODIR0 |= (1<<4);    //P0.4 jako vystupni pro ovladani smeru dat 485

  ///preruseni
  VICVectAddr29 = (unsigned long) sensor_communication;   //set interrupt vector
  VICIntEnable |= (1<<29);            //enable UART3 enable
  U3LCR = 0x03;                       //DLAB=0
  U3IER=1;

  return 0;
}

void Send_sensor(const BYTE *BufferPtr, DWORD Length )
{
  DATA_SEND; // prepnuti smeru RS485
	while ( Length != 0 )
  {
    while ( !(U3LSR & (1<<5) ) ); // Wait until Transmitter Holding Register is not Empty
    U3THR = *BufferPtr;
    BufferPtr++;
    Length--;
  }

  while ( !(U3LSR & (1<<6) ) );
  DATA_RECEIVE; // prepnuti smeru RS485

  if( flag_sensor8)
  {
    enable_timer2();            // zapni casovac pro cekani na odpoved
    flag_aktivni_zadost=1;      //nuluje se pri dobrem prijmu nebo pri vyprseni casovace
  }
  pozice_bufferu=0;
  return;
}

unsigned char Read_sensor()
{
 return U3RBR;
}
/******************************************************************************
**                            End Of File
******************************************************************************/
