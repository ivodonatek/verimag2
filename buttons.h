//******************************************************************************
//  Management tlacitek.
//*****************************************************************************/

#ifndef BUTTONS_H_INCLUDED
#define BUTTONS_H_INCLUDED

#include "type.h"

// Scan kody tlacitek
typedef enum
{
    BTC_NONE = 0xFF,
    BTC_DOWN = 2,
    BTC_UP = 4,
    BTC_LEFT = 3,
    BTC_RIGHT = 1,
    BTC_ESC = 5,
    BTC_ENTER = 0

} TButtonScanCode;

// max. scan kod
#define BUTTON_SCAN_CODE_MAX   (5)

// Tlacitka
typedef enum
{
    BTN_ESC = 0,
    BTN_LEFT = 1,
    BTN_UP = 2,
    BTN_DOWN = 3,
    BTN_RIGHT = 4,
    BTN_ENTER = 5

} TButton;

// pocet tlacitek
#define BUTTONS_COUNT   (BTN_ENTER + 1)

// Typ - prototyp funkce, ktera je volana po stisku tlacitka.
typedef void (*TButtonHandler) (void);

// zadna funkce po stisku tlacitka
#define NO_BUTTON_HANDLER   ((TButtonHandler)0)

// Typ - funkce pro volani po stisku kazdeho tlacitka.
typedef TButtonHandler TButtonHandlers[BUTTONS_COUNT];

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Buttons_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha tlacitek, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void Buttons_Task(void);

//------------------------------------------------------------------------------
//  Inicializace (vymazani) vsech obsluznych rutin stisku tlacitek.
//------------------------------------------------------------------------------
void Buttons_InitHandlers(void);

//------------------------------------------------------------------------------
//  Nastaveni obsluzne rutiny stisku tlacitka.
//------------------------------------------------------------------------------
void Buttons_SetHandler(TButton button, TButtonHandler handler);

//------------------------------------------------------------------------------
//  Nastaveni obsluznych rutin stisku tlacitek.
//------------------------------------------------------------------------------
void Buttons_SetHandlers(const TButtonHandlers handlers);

//------------------------------------------------------------------------------
//  Vrati, zda je nastavena nejaka obsluzna rutina pro dane tlacitko.
//------------------------------------------------------------------------------
BOOL Buttons_IsHandlerSet(TButton button);

//------------------------------------------------------------------------------
//  Vrati scan kod tlacitek.
//------------------------------------------------------------------------------
TButtonScanCode Buttons_GetScanCode(void);

#endif // BUTTONS_H_INCLUDED
