//------------------------------------------------------------------------------
//  Prevody jednotek velicin.
//------------------------------------------------------------------------------

#include "conversion.h"

//------------------------------------------------------------------------------
//  Konverze teploty v �C na Kelvin.
//  T(K) = T(C) + 273,15
//------------------------------------------------------------------------------
float Convert_CelsiusToKelvin(float tempCelsius)
{
    return tempCelsius + 273.15f;
}

//------------------------------------------------------------------------------
//  Konverze teploty v Kelvinech na �C.
//  T(C) = T(K) - 273,15
//------------------------------------------------------------------------------
float Convert_KelvinToCelsius(float tempKelvin)
{
    return tempKelvin - 273.15f;
}

//------------------------------------------------------------------------------
//  Konverze teploty v Kelvinech na �F.
//  T(F) = 1,8 � (T(K) - 273,15) + 32
//------------------------------------------------------------------------------
float Convert_KelvinToFahrenheit(float tempKelvin)
{
    return 1.8f * (tempKelvin - 273.15f) + 32;
}

//------------------------------------------------------------------------------
//  Konverze tlaku v Pa (Pascal) na psi (Pounds per square inch).
//------------------------------------------------------------------------------
float Convert_PascalToPsi(float pressPascal)
{
    return ((0.0254 * 0.0254) / 4.4482) * pressPascal;
}

//------------------------------------------------------------------------------
//  Konverze tlaku v psi (Pounds per square inch) na Pa (Pascal).
//------------------------------------------------------------------------------
float Convert_PsiToPascal(float pressPsi)
{
    return (4.4482 / (0.0254 * 0.0254)) * pressPsi;
}

//------------------------------------------------------------------------------
//  Konverze tlaku v Pa (Pascal) na bar (Bar).
//------------------------------------------------------------------------------
float Convert_PascalToBar(float pressPascal)
{
    return 0.00001 * pressPascal;
}

//------------------------------------------------------------------------------
//  Konverze tlaku v bar (Bar) na Pa (Pascal).
//------------------------------------------------------------------------------
float Convert_BarToPascal(float pressBar)
{
    return 100000 * pressBar;
}
