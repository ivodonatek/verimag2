//******************************************************************************
//  Management uloh.
//*****************************************************************************/

#ifndef TASK_MAN_H_INCLUDED
#define TASK_MAN_H_INCLUDED

// Typ - prototyp ulohy.
typedef void (*TTaskProcedure) (void);

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void TaskMan_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne. Volaji se zaregistrovane ulohy.
//------------------------------------------------------------------------------
void TaskMan_Task(void);

//------------------------------------------------------------------------------
//  Registrace ulohy.
//------------------------------------------------------------------------------
void TaskMan_Register(TTaskProcedure task);

//------------------------------------------------------------------------------
//  Odregistrace ulohy.
//------------------------------------------------------------------------------
void TaskMan_Unregister(TTaskProcedure task);

#endif // TASK_MAN_H_INCLUDED
