#ifndef _INTEGER
#define _INTEGER

#include <stdint.h>
#include "port.h"
#include "type.h"


/* modified by Martin Thomas to avoid redefiniton
   for BYTE, WORD and DWORD */

/* These types are assumed as 16-bit or larger integer */
typedef int32_t		INT;
typedef uint32_t	UINT;

/* These types are assumed as 8-bit integer */

#ifndef CHAR_TYPEDEF
#define CHAR_TYPEDEF
typedef int8_t	CHAR;
#endif

//typedef int8_t		CHAR;
//typedef uint8_t	UCHAR;

#ifndef BYTE_TYPEDEF
#define BYTE_TYPEDEF
typedef uint8_t	BYTE;
#endif

#ifndef USHORT_TYPEDEF
#define USHORT_TYPEDEF
typedef uint16_t	USHORT;
#endif

//typedef uint16_t USHORT;
#ifndef SHORT_TYPEDEF
#define SHORT_TYPEDEF
typedef int16_t	SHORT;
#endif
//typedef int16_t SHORT;

#ifndef ULONG_TYPEDEF
#define ULONG_TYPEDEF
typedef uint32_t	ULONG;
#endif

#ifndef LONG_TYPEDEF
#define LONG_TYPEDEF
typedef int32_t	LONG;
#endif

/* These types are assumed as 16-bit integer */
//typedef int16_t	SHORT;
//typedef uint16_t	USHORT;
#ifndef WORD_TYPEDEF
#define WORD_TYPEDEF
typedef uint16_t	WORD;
#endif

/* These types are assumed as 32-bit integer */
//typedef int32_t		LONG;
//typedef uint32_t	ULONG;
#ifndef DWORD_TYPEDEF
#define DWORD_TYPEDEF
typedef uint32_t	DWORD;
#endif
/*
#if 1
// mthomas
#ifndef FALSE
typedef enum { FALSE = 0, TRUE } BOOL;
#endif
#else
// Boolean type
typedef enum { FALSE = 0, TRUE } BOOL;
#endif
*/
#endif

