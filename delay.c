#include "delay.h"
#include "target.h"

void delay_us()
{
  for(unsigned int q=0;q<5;q++)
  {
    asm("nop");
  }
}

void delay_2us()
{
  for(unsigned int q=0;q<12;q++)
  {
    asm("nop");
  }
}

void delay_5us()
{
  for(unsigned int q=0;q<41;q++)
  {
    asm("nop");
  }
}

void delay_10us()
{
  for(unsigned int q=0;q<89;q++)
  {
    asm("nop");
  }
}

void delay_1ms(void)
{
  for(unsigned int q=0;q<6430;q++)
  {
    asm("nop");
  }
}

void delay_10ms(void)
{
  for(unsigned int q=0;q<64300;q++)
  {
    asm("nop");
  }
}

void delay_100ms()
{
  for(unsigned int q=0;q<11;q++)
    delay_10ms();
}

void delay_seconds(unsigned int seconds)
{
  for(unsigned int i=0;i<seconds;i++)
    for(unsigned int j=0;j<10;j++)
        delay_100ms();
}
