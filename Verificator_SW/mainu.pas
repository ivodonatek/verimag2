unit mainU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Menus, EditBtn, DBGrids, DbCtrls, ComCtrls, RTTIGrids, TAGraph,
  LR_Class, LR_DBSet, LR_Desgn, SerialPort, ModBusSerial, HMILabel, PLCBlock,
  PLCBlockElement, INIFiles, db, dbf, IBConnection, sqldb, LR_DSet;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button7: TButton;
    Button9: TButton;
    CheckBox2: TCheckBox;
    ComboBox14: TComboBox;
    ComboBox9: TComboBox;
    Datasource1: TDatasource;
    Datasource2: TDatasource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBNavigator1: TDBNavigator;
    frDBDataSet1: TfrDBDataSet;
    frDesigner1: TfrDesigner;
    frReport1: TfrReport;
    HMILabel1: THMILabel;
    HMILabel2: THMILabel;
    HMILabel5: THMILabel;
    DBConnection: TIBConnection;
    Image1: TImage;
    Label1: TLabel;
    Label25: TLabel;
    Label3: TLabel;
    LabelComStat2: TLabel;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    PLCBEDelete: TPLCBlockElement;
    PLCBEVeri_Cal: TPLCBlockElement;
    PLCBEBacklit: TPLCBlockElement;
    PLCBEAdvancedMode: TPLCBlockElement;
    PLCBEDate: TPLCBlockElement;
    PLCBETest10_flowRange: TPLCBlockElement;
    PLCBETest11_cp1: TPLCBlockElement;
    PLCBETest12_cp2: TPLCBlockElement;
    PLCBETest13_cp3: TPLCBlockElement;
    PLCBETest14_cd1: TPLCBlockElement;
    PLCBETest15_cd2: TPLCBlockElement;
    PLCBETest16_cd3: TPLCBlockElement;
    PLCBETest17_total: TPLCBlockElement;
    PLCBETest18_totalPlus: TPLCBlockElement;
    PLCBETest19_totalMinus: TPLCBlockElement;
    PLCBETest26_reserved1: TPLCBlockElement;
    PLCBETest27_reserved2: TPLCBlockElement;
    PLCBETest28_reserved3: TPLCBlockElement;
    PLCBETest29_reserved4: TPLCBlockElement;
    PLCBETest30_reserved5: TPLCBlockElement;
    PLCBETest2_date: TPLCBlockElement;
    PLCBETest20_aux: TPLCBlockElement;
    PLCBETest21_testFlows01: TPLCBlockElement;
    PLCBETest22_testFlows2_frequency0: TPLCBlockElement;
    PLCBETest23_frequency12: TPLCBlockElement;
    PLCBETest24_iOut01: TPLCBlockElement;
    PLCBETest25_iOut2_fiResult: TPLCBlockElement;
    PLCBETest3_magx2UnitNo: TPLCBlockElement;
    PLCBETest4_magx2Firmware_diameter: TPLCBlockElement;
    PLCBETest5_testResult_sensorFirmware: TPLCBlockElement;
    PLCBETest6_sensorUnitNo: TPLCBlockElement;
    PLCBETest7_errorMin: TPLCBlockElement;
    PLCBETest8_okMin: TPLCBlockElement;
    PLCBETest9_errorCode: TPLCBlockElement;
    PLCBETestCount: TPLCBlockElement;
    PLCBETestWidth: TPLCBlockElement;
    PLCBETime: TPLCBlockElement;
    PLCBETest1_id: TPLCBlockElement;
    SQLQuery1: TSQLQuery;
    SQLQuery2: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    SQLTransaction2: TSQLTransaction;
    StatusBar1: TStatusBar;
    User: TPLCBlock;
    ModBusRTUDriver1: TModBusRTUDriver;
    PLCBESerialNumber: TPLCBlockElement;
    PLCBEContrast: TPLCBlockElement;
    PLCBEFWVersion: TPLCBlockElement;
    SerialPortDriver1: TSerialPortDriver;
    TIPropertyGrid1: TTIPropertyGrid;
    Test: TPLCBlock;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure ComboBox14Change(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure SerialPortDriver1CommPortClosed(Sender: TObject);
    procedure SerialPortDriver1CommPortOpened(Sender: TObject);
    procedure SerialPortDriver1CommPortOpenError(Sender: TObject);

  private
    { private declarations }
    function TestResultToStr(testResult : integer) : string;
    function IntAsFloat(intValue : integer) : real;

  public
    { public declarations }
  end; 

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

//**********************************************************************************
function TForm1.TestResultToStr(testResult : integer) : string;
begin
  case testResult of
    0: Result := 'Pass';
    4: Result := 'Fail';
    else Result := 'N/A';
  end;
end;

//**********************************************************************************
function TForm1.IntAsFloat(intValue : integer) : real;
var
    MyArray: array [0..3] of Byte;
    singleValue: Single;
begin
    // Move the integer into the array
    Move(intValue, MyArray, 4);

    // Move the array into the single
    Move(MyArray, singleValue, 4);

    Result:= singleValue;
end;

//*************************** Read from Verimag ******************************************
procedure TForm1.Button1Click(Sender: TObject);
var
    i:integer=0;
    pom:integer;
begin
  Button1.Enabled:=false;
  try
  SQLTransaction1.Active:=false;
  SQLQuery1.SQL.Text:= 'select * from Tests';
  SQLTransaction1.Active:=true;
  SQLQuery1.Open;
  for i:=0 to trunc(PLCBETestCount.value)-1 do
  begin
    test.memaddress:=1499+i*60;
    test.read;                                             //201401
    SQLQuery1.Append;
    SQLQuery1.FieldByName('UID').AsFloat := PLCBETest1_id.Value + 1000000*PLCBESerialNumber.value;         //todo: fix
    SQLQuery1.FieldByName('METER_TESTDATE').AsString := floattostr(PLCBETest2_date.Value);
    SQLQuery1.FieldByName('VERIFICATOR_CALIBRATION').AsString := floattostr(PLCBEVeri_Cal.Value);
    SQLQuery1.FieldByName('VERIFICATOR_SN').AsFloat := PLCBESerialNumber.value;
    SQLQuery1.FieldByName('VERIFICATOR_FW').AsInteger := ((trunc(PLCBEFWVersion.Value)) AND $ffff);
    SQLQuery1.FieldByName('METER_MAGX2_SN').AsFloat := PLCBETest3_magx2UnitNo.value;
    pom := ((trunc(PLCBETest4_magx2Firmware_diameter.Value)) AND $ffff);
    SQLQuery1.FieldByName('METER_MAGX2_FW').AsString := Format('%0:2.2d.%1:2.2d', [pom div 100, pom mod 100]);
    SQLQuery1.FieldByName('METER_SENSOR_SN').AsFloat := PLCBETest6_sensorUnitNo.value;
    pom := ((trunc(PLCBETest5_testResult_sensorFirmware.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('METER_SENSOR_FW').AsString := Format('%0:2.2d.%1:2.2d', [pom div 100, pom mod 100]);
    SQLQuery1.FieldByName('METER_DIAMETER').AsInteger := ((trunc(PLCBETest4_magx2Firmware_diameter.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('METER_FLOW_RANGE').AsFloat := ((trunc(PLCBETest10_flowRange.Value)) AND $ffff) / 1000;
    SQLQuery1.FieldByName('METER_ERROR_MIN').AsFloat := PLCBETest7_errorMin.Value;
    SQLQuery1.FieldByName('METER_OK_MIN').AsFloat := PLCBETest8_okMin.Value;
    pom := trunc(PLCBETest9_errorCode.value);
    SQLQuery1.FieldByName('METER_ERROR_CODE').AsString := Format('%0:8.8x', [pom]);
    SQLQuery1.FieldByName('METER_CP1').AsFloat := PLCBETest11_cp1.Value / 1000;
    SQLQuery1.FieldByName('METER_CP2').AsFloat := PLCBETest12_cp2.Value / 1000;
    SQLQuery1.FieldByName('METER_CP3').AsFloat := PLCBETest13_cp3.Value / 1000;
    SQLQuery1.FieldByName('METER_CD1').AsFloat := PLCBETest14_cd1.Value;
    SQLQuery1.FieldByName('METER_CD2').AsFloat := PLCBETest15_cd2.Value;
    SQLQuery1.FieldByName('METER_CD3').AsFloat := PLCBETest16_cd3.Value;
    SQLQuery1.FieldByName('METER_TOTAL').AsFloat := IntAsFloat(trunc(PLCBETest17_total.Value));
    SQLQuery1.FieldByName('METER_TOTAL_PLUS').AsFloat := IntAsFloat(trunc(PLCBETest18_totalPlus.Value));
    SQLQuery1.FieldByName('METER_TOTAL_MINUS').AsFloat := IntAsFloat(trunc(PLCBETest19_totalMinus.Value));
    SQLQuery1.FieldByName('METER_AUX').AsFloat := IntAsFloat(trunc(PLCBETest20_aux.Value));

    SQLQuery1.FieldByName('TEST_FLOW1').AsFloat := 0.001 * (trunc(PLCBETest21_testFlows01.Value) AND $ffff);
    SQLQuery1.FieldByName('TEST_FLOW2').AsFloat := 0.001 * ((trunc(PLCBETest21_testFlows01.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('TEST_FLOW3').AsFloat := 0.001 * (trunc(PLCBETest22_testFlows2_frequency0.Value) AND $ffff);
    SQLQuery1.FieldByName('TEST_FREQ1').AsFloat := ((trunc(PLCBETest22_testFlows2_frequency0.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('TEST_FREQ2').AsFloat := ((trunc(PLCBETest23_frequency12.Value)) AND $ffff);
    SQLQuery1.FieldByName('TEST_FREQ3').AsFloat := ((trunc(PLCBETest23_frequency12.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('TEST_IOUT1').AsFloat := 0.001 * (trunc(PLCBETest24_iOut01.Value) AND $ffff);
    SQLQuery1.FieldByName('TEST_IOUT2').AsFloat := 0.001 * ((trunc(PLCBETest24_iOut01.Value) div $10000) AND $ffff);
    SQLQuery1.FieldByName('TEST_IOUT3').AsFloat := 0.001 * (trunc(PLCBETest25_iOut2_fiResult.Value) AND $ffff);

    SQLQuery1.FieldByName('TEST_C1_REZISTANCE').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_C2_REZISTANCE').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_L1').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_L2').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_L3').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_SHORT').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_TEMP').AsFloat := 0;
    SQLQuery1.FieldByName('TEST_INDUCTANCE').AsFloat := 0;

    SQLQuery1.FieldByName('TEST_RESERVED1').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED2').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED3').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED4').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED5').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED6').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED7').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_RESERVED8').AsString := 'N/A';

    SQLQuery1.FieldByName('TEST_ERRORCODE_RESULT').AsString := TestResultToStr((trunc(PLCBETest5_testResult_sensorFirmware.Value) div $100) AND $ff);
    SQLQuery1.FieldByName('TEST_FREQ_RESULT').AsString := TestResultToStr((trunc(PLCBETest25_iOut2_fiResult.Value) div $10000) AND $ff);
    SQLQuery1.FieldByName('TEST_IOUT_RESULT').AsString := TestResultToStr((trunc(PLCBETest25_iOut2_fiResult.Value) div $1000000) AND $ff);

    SQLQuery1.FieldByName('TEST_C_RESULT').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_L_RESULT').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_S_RESULT').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_T_RESULT').AsString := 'N/A';
    SQLQuery1.FieldByName('TEST_I_RESULT').AsString := 'N/A';

    pom := (trunc(PLCBETest5_testResult_sensorFirmware.Value) AND $ff);
    if pom=0 then
      SQLQuery1.FieldByName('TEST_RESULT').AsString := 'The electromagnetic flowmeter is measuring within +-1% of the factory calibration values.'
    else
      SQLQuery1.FieldByName('TEST_RESULT').AsString := 'Verification failed! The flowmeter may not be within the +-1% of the factory calibration values.';

    SQLQuery1.Post;
  end;
  SQLQuery1.ApplyUpdates;
  SQLTransaction1.Commit;
  SQLTransaction1.Active:=false;
  SQLQuery1.SQL.Text:= 'select UID,METER_TESTDATE,METER_MAGX2_SN,METER_DIAMETER,TEST_RESULT from Tests';
  DBConnection.Connected:= True;
  SQLTransaction1.Active:=true;
  SQLQuery1.Open;
  PLCBEDelete.value:=1;
  PLCBEDelete.write;
  PLCBEDelete.value:=0;
  User.read;
  //showmessage(inttostr(i)+':   from '+inttostr(trunc(PLCBETestCount.value)) );
  except
    showmessage('VeriMAG reading failed!');
  end;
end;

//*************************** Open *******************************************************
procedure TForm1.Button2Click(Sender: TObject);
begin
  SQLQuery1.Close;
  SQLQuery1.SQL.Text:= 'select * from Tests';
  DBConnection.Connected:= True;
  SQLTransaction1.Active:= True;
  SQLQuery1.Open;
end;

//*************************** Connect ****************************************************
procedure TForm1.Button3Click(Sender: TObject);
begin
  Button3.enabled:=false;
  try
    SerialPortDriver1.comport:=combobox9.text;
    SerialPortDriver1.active:=true;
  except
    showmessage('Port Open Failed');
  end;
end;

//**************************** Disconnect ************************************************
procedure TForm1.Button4Click(Sender: TObject);
begin
  serialPortDriver1.active:=false;
  Button3.enabled:=true;
end;

//*************************** Save *******************************************************
procedure TForm1.Button5Click(Sender: TObject);
begin
   try
    if SQLTransaction1.Active then
    // Only if we are within a started transaction;
    // otherwise you get "Operation cannot be performed on an inactive dataset"
    begin
      SQLQuery1.ApplyUpdates; //Pass user-generated changes back to database...
      SQLTransaction1.Commit; //... and commit them using the transaction.
      //SQLTransaction1.Active now is false
      SQLQuery1.SQL.Text:= 'select UID,METER_TESTDATE,METER_MAGX2_SN,METER_DIAMETER,TEST_RESULT from Tests';
      DBConnection.Connected:= True;
      SQLTransaction1.Active:=true;
      SQLQuery1.Open;
    end;
  except
  on E: EDatabaseError do
    begin
      MessageDlg('Error', 'A database error has occurred. Technical error message: ' +
        E.Message, mtError, [mbOK], 0);
    end;
  end;
end;

//*************************** Print ******************************************************
procedure TForm1.Button7Click(Sender: TObject);
begin
  if DBGrid1.SelectedRows.Count > 0 then begin
     SQLQuery2.Close;
     DBGrid1.DataSource.DataSet.GotoBookmark(Tbookmark(DBGrid1.SelectedRows[0]));
     // showmessage((format('%s',[DbGrid1.columns[0].field.asString])));
     // showmessage(DbGrid1.columns[0].field.asString);

     SQLQuery2.SQL.Text:= 'select * from Tests where UID='+DbGrid1.columns[0].field.asString;     //where uid=
     DBConnection.Connected:= True;
     SQLTransaction1.Active:= True;
     SQLQuery2.Open;

     frReport1.LoadFromFile('report_template.lrf');
     frReport1.ShowReport;
  end;
end;

{datetime decode
procedure TForm1.Button8Click(Sender: TObject);
Var YY,MM,DD : Word;
    H,M,S,MS: Word;
Begin
 DeCodeDate (Date,YY,MM,DD);
 DecodeTime(Time,H,M,S,MS);
 PLCBEDate.value:=(((yy div 1000)*$10000000+((yy mod 1000) div 100)*$1000000+((yy mod 100) div 10)*$100000+(yy mod 10)*$10000)+
 (mm div 10)*$1000 + (mm mod 10)*$100 + (dd div 10)*$10 + (dd mod 10));
 showmessage(inttostr((h div 10)*$100000+(h mod 10)*$10000));
 PLCBETime.value:=(h div 10)*$100000+(h mod 10)*$10000+(m div 10)*$1000+(m mod 10)*$100+(s div 10)*$10+(s mod 10);

end;
 }

//*************************** Create *****************************************************
procedure TForm1.FormCreate(Sender: TObject);
var //i:byte;
    INI:TINIFile;
begin
  INI := TINIFile.Create('Verificator.ini');
  try
  if INI.ReadString('Settings','Initialized','0') <> '1' then
  begin
    showmessage('Failed to load INI file, creating a new one');
    INI.WriteString('Settings','Initialized','1');
    INI.WriteString('Settings','Master_Mode','1');
  end  else
  begin
    if strtoint(INI.ReadString('Settings','Master_Mode','0')) = 3695 then
    begin
      Checkbox2.visible:=true;
      ComboBox14.visible:=true;
      Button5.visible:=true;
      DBNavigator1.Visible:=true;
      DBGrid1.Options:=DBGrid1.Options + [dgEditing];
      SQLQuery1.SQL.Text:= 'select UID,METER_TESTDATE,METER_MAGX2_SN,METER_DIAMETER,TEST_RESULT from Tests';
      DBGrid2.Options:=DBGrid2.Options + [dgEditing];
    end;
  end;
  finally
    INI.free;
  end;
  try
    SQLQuery1.Close;
    SQLQuery1.SQL.Text:= 'select UID,METER_TESTDATE,METER_MAGX2_SN,METER_DIAMETER,TEST_RESULT from Tests';
    //SQLQuery1.SQL.Text:= 'select * from Tests';
    DBConnection.Connected:= True;
    SQLTransaction1.Active:= True;
    SQLQuery1.Open;
  except
    Showmessage('Internal database failure');
    ComboBox9.Enabled:=false;
    Button9.Enabled:=false;
    Button3.Enabled:=false;
    Button4.Enabled:=false;
    Button1.Enabled:=false;
    Button7.Enabled:=false;
    DBGrid1.enabled:=false;
    DBGrid2.enabled:=false;
  end;
end;

//************************** Refresh *****************************************************
procedure TForm1.Button9Click(Sender: TObject);
var i:byte;
begin
  SerialPortDriver1.active:=false;
  combobox9.items.Clear;

  Button9.Enabled:=false;
  Button3.Enabled:=false;
  Button4.Enabled:=false;
  Button1.Enabled:=false;

  StatusBar1.Panels.Items[0].Text:='Refreshing...';
  for i:=1 to 8 do
  begin
    try
     application.processmessages();
     StatusBar1.Panels.Items[0].Text:='Refreshing... COM'+inttostr(i);
     combobox9.items.add('COM'+inttostr(i));
     SerialPortDriver1.Comport:='COM'+inttostr(i);
     SerialPortDriver1.active:=true;
     combobox9.itemindex:=-1;
     SerialPortDriver1.active:=false;
    except
     combobox9.items.delete(combobox9.items.count-1)
    end;
  end;
  StatusBar1.Panels.Items[0].Text:='';

  if combobox9.items.count>0 then begin
    combobox9.itemindex:=0;
    Button3.Enabled:=true;
  end;

  {
  try
     SerialPortDriver1.Comport:=combobox9.text;
     SerialPortDriver1.active:=true;
  except
    Showmessage('Port failed to open');
  end;

  if serialportdriver1.active then Button3.Enabled:=false else Button3.Enabled:=true;
  }

  Button4.Enabled:=true;
  Button9.Enabled:=true;
end;

//************************** Troubleshooting *********************************************
procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  if checkbox2.checked then
  begin
    form1.Width:=form1.width+TIPropertyGrid1.width;
    TIPropertyGrid1.visible:=true;
  end;
  if checkbox2.checked=false then
  begin
    form1.Width:=form1.width-TIPropertyGrid1.width;
    TIPropertyGrid1.visible:=false;
  end;
  case combobox14.itemindex of
    0:  TIPropertyGrid1.tiobject:=serialportdriver1;
    1:  TIPropertyGrid1.tiobject:=user;
  end;
end;

procedure TForm1.ComboBox14Change(Sender: TObject);
begin
    case ComboBox14.itemindex of
    0:  TIPropertyGrid1.tiobject:=serialportdriver1;
    1:  TIPropertyGrid1.tiobject:=user;
  end;
end;

//************************** Grid Click **************************************************
procedure TForm1.DBGrid1CellClick(Column: TColumn);
begin
  SQLQuery2.Close;
  try
    DBGrid1.DataSource.DataSet.GotoBookmark(Tbookmark(DBGrid1.SelectedRows[0]));
 // showmessage((format('%s',[DbGrid1.columns[0].field.asString])));
 // showmessage(DbGrid1.columns[0].field.asString);
  finally
    SQLQuery2.SQL.Text:= 'select * from Tests where UID='+DbGrid1.columns[0].field.asString;     //where uid=
    DBConnection.Connected:= True;
    SQLTransaction1.Active:= True;
    SQLQuery2.Open;
  end;
end;

//************************** COM Closed **************************************************
procedure TForm1.SerialPortDriver1CommPortClosed(Sender: TObject);
begin
  StatusBar1.Panels.Items[0].Text:='Port Closed';
  Button1.Enabled:=false;
end;

//************************** COM Open ****************************************************
procedure TForm1.SerialPortDriver1CommPortOpened(Sender: TObject);
var LastCommReadsOK:Cardinal;
begin
  LastCommReadsOK:=User.CommReadsOK;
  User.read;
  StatusBar1.Panels.Items[0].Text:='Port Open';
  if LastCommReadsOK<>User.CommReadsOK then Button1.Enabled:=true;
end;

//************************** COM Error ***************************************************
procedure TForm1.SerialPortDriver1CommPortOpenError(Sender: TObject);
begin
 showmessage(serialportdriver1.lastoserrormessage);
 StatusBar1.Panels.Items[0].Text:='Port Closed';
 Button1.Enabled:=false;
end;

//************************** Form Destroy ************************************************
procedure TForm1.FormDestroy(Sender: TObject);
var INI:TINIFile;
begin
  serialPortDriver1.active:=false;
  INI := TINIFile.Create('Verificator.ini');
  try
   // INI.WriteString('Settings','LastID','1');
  finally
    INI.free;
  end;
  SQLQuery1.Close;
  SQLTransaction1.Active:= False;
  DBConnection.Connected:= False;

end;

//************************** Menu About **************************************************
procedure TForm1.MenuItem1Click(Sender: TObject);
begin
  ShowMessage('VeriMAG read SW v.2.0' + sLineBreak + 'www.arkon.co.uk');
end;

//************************** Menu Exit ***************************************************
procedure TForm1.MenuItem2Click(Sender: TObject);
begin
  application.Terminate;
end;




end.

