; -- VeriMAG --

[Setup]
AppName=VeriMAG
AppVerName=VeriMAG 2.0.0.0
AppPublisher=Arkon Flow Systems, s.r.o.
AppVersion=2.0.0.0
VersionInfoVersion=2.0.0.0
DefaultDirName={pf}\Arkon Flow Systems\VeriMAG\
DefaultGroupName=Arkon Flow Systems\VeriMAG\
UninstallDisplayIcon={app}\VeriMAG\
Compression=lzma
SolidCompression=yes

[Files]
Source: DATABASE.DBF; DestDir: {app}\
Source: fbclient.dll; DestDir: {app}\
Source: fbembed.dll; DestDir: {app}\
Source: firebird.msg; DestDir: {app}\
Source: ib_util.dll; DestDir: {app}\
Source: icudt30.dll; DestDir: {app}\
Source: icuin30.dll; DestDir: {app}\
Source: icuuc30.dll; DestDir: {app}\
Source: Microsoft.VC80.CRT.manifest; DestDir: {app}\
Source: msvcp80.dll; DestDir: {app}\
Source: msvcr80.dll; DestDir: {app}\
Source: Veri_SW.exe; DestDir: {app}\
Source: Veri_SW.ico; DestDir: {app}\
Source: Verificator.ini; DestDir: {app}\
Source: VeriMAG_USB_VCOM.inf; DestDir: {app}\
      
[Icons]
Name: {group}\VeriMAG; Filename: {app}\Veri_SW.exe
Name: {commondesktop}\VeriMAG; Filename: {app}\Veri_SW.exe

[Run]
Filename: {sys}\rundll32.exe; Parameters: setupapi,InstallHinfSection Uninstall 132 {app}\VeriMAG_USB_VCOM.inf; Description: Install USB driver; Flags: postinstall nowait skipifsilent unchecked
Filename: {app}\Veri_SW.exe; Description: Launch application; Flags: postinstall nowait skipifsilent unchecked
