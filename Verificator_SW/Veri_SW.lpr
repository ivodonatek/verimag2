program Veri_SW;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, runtimetypeinfocontrols, pascalscada, zcomponent,
  mainU, tachartlazaruspkg, memdslaz, dbflaz, printer4lazarus, lazreport, Graph,
  PrintU;

{$R *.res}

begin
  Application.Title:='Verificator';
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.

