//******************************************************************************
//  Management uzivatelskeho rozhrani.
//*****************************************************************************/

#ifndef UI_H_INCLUDED
#define UI_H_INCLUDED

#include "type.h"
#include "ui_menu.h"
#include "test.h"

// Pojmenovani druhu UI tlacitek zobrazenych na displeji
typedef enum
{
    UI_BTN_ESC = 0,
    UI_BTN_LEFT = 1,
    UI_BTN_UP = 2,
    UI_BTN_DOWN = 3,
    UI_BTN_RIGHT = 4,
    UI_BTN_ENTER = 5,
    UI_BTN_MENU = 6

} TUIButton;

// pocet UI tlacitek
#define UI_BUTTONS_COUNT   (UI_BTN_MENU + 1)

// zobrazena UI tlacitka
typedef BOOL TDisplayButtons[UI_BUTTONS_COUNT];

// pocet bytu editacniho pole
#define UI_EDIT_CHAR_COUNT   30

// Typ - prototyp funkce, ktera je volana pro obsluhu editace hodnoty.
typedef void (*TEditHandler) (TUIButton uiButton);

// zadna funkce pro obsluhu editace hodnoty.
#define NO_EDIT_HANDLER   ((TEditHandler)0)

// Polozka vyctove hodnoty
typedef struct
{
    // hodnota
    unsigned long value;

    // ID zobrazovaneho textu
    TUITextId textId;

} TEnumItem;

// Stav editace
typedef struct
{
    // nazev editovane hodnoty
    char *titleStr;

    // retezec editovane hodnoty
    char valueStr[UI_EDIT_CHAR_COUNT];

    // -poradi/index kurzoru v retezci hodnoty
    // -aktualni polozka vyctoveho typu
    unsigned char cursorPos;

    // editacni obsluha
    TEditHandler editHandler;

} TEditState;

// Stav info
typedef struct
{
    // nazev info hodnoty
    char *titleStr;

    // retezec info hodnoty
    char valueStr[UI_EDIT_CHAR_COUNT];

} TInfoState;

// Stav UI
typedef struct
{
    // zobrazena UI tlacitka
    TDisplayButtons displayButtons;

    // Stav menu
    TMenuState menuState;

    // aktualne prohlizena data hlavniho testu
    TMainSubTestId dataMainSubTestId;

    // Stav editace
    TEditState editState;

    // Stav infa
    TInfoState infoState;

} TUIState;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void UI_Init(void);

//------------------------------------------------------------------------------
//  Vrati stav UI.
//------------------------------------------------------------------------------
const TUIState* UI_GetState(void);

//------------------------------------------------------------------------------
//  Inicializace zobrazeni UI tlacitek.
//------------------------------------------------------------------------------
void UI_InitDisplayButtons(void);

//------------------------------------------------------------------------------
//  Nastaveni zobrazeni UI tlacitka.
//------------------------------------------------------------------------------
void UI_SetDisplayButton(TUIButton displayButton, BOOL display);

//------------------------------------------------------------------------------
//  Nastaveni zobrazeni UI tlacitek.
//------------------------------------------------------------------------------
void UI_SetDisplayButtons(const TDisplayButtons displayButtons);

//------------------------------------------------------------------------------
//  Nastaveni menu podle ID (postara se taky o tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetMenu(TUIMenuId id, unsigned int selectedMenuItem);

//------------------------------------------------------------------------------
//  Nastaveni aktualne prohlizenych dat hlavniho testu.
//------------------------------------------------------------------------------
void UI_SetActualMainTestData(TMainSubTestId id);

//------------------------------------------------------------------------------
//  Nastaveni editace casu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetEditTime(const char *title, unsigned char hour, unsigned char minute, unsigned char second);

//------------------------------------------------------------------------------
//  Vraci editovany cas.
//------------------------------------------------------------------------------
void UI_GetEditTime(unsigned char *hour, unsigned char *minute, unsigned char *second);

//------------------------------------------------------------------------------
//  Nastaveni editace datumu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetEditDate(const char *title, unsigned int year, unsigned char month, unsigned char day);

//------------------------------------------------------------------------------
//  Vraci editovany datum.
//------------------------------------------------------------------------------
void UI_GetEditDate(unsigned int *year, unsigned char *month, unsigned char *day);

//------------------------------------------------------------------------------
//  Nastaveni editace vyctoveho typu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetEditEnum(const char *title, unsigned long value, const TEnumItem *items, unsigned char itemCount);

//------------------------------------------------------------------------------
//  Vraci editovanou hodnotu vyctoveho typu.
//------------------------------------------------------------------------------
unsigned long UI_GetEditEnum(void);

//------------------------------------------------------------------------------
//  Nastaveni info (postara se taky o displej).
//------------------------------------------------------------------------------
void UI_SetInfo(const char *title, const char *value);

#endif // UI_H_INCLUDED
