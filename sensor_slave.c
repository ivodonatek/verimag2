//******************************************************************************
//  Komunikace s MagX2 - VeriMag je v pozici slave na sbernici, simuluje senzor.
//*****************************************************************************/

#include "sensor_master.h"
#include "sensor_slave.h"
#include "LPC23xx.h"
#include "target.h"
#include "type.h"
#include "rtc.h"
#include "sensor.h"
#include "mbcrc.h"

#define BAUD_RATE           9600   //baud rate
#define RCV_BUFFER_LENGTH   80     //delka prijimaciho buferu
#define RCV_TIMEOUT         2      //timeout prijmu [s]

#define DATA_RECEIVE        FIO2CLR|=(1<<2)
#define DATA_SEND           FIO2SET|=(1<<2)

// Fw module state
static struct
{
    // stav komunikace
    TSensorSlaveCommStatus commStatus;

    // prijimaci bufer
    unsigned char rcvBuffer[RCV_BUFFER_LENGTH];

    // pocet prijatych bytu v prijimacim buferu
    unsigned char rcvBufferByteCount;

    // prijimaci casovac
    unsigned long rcvTimer;

    // funkce, ktera je volana po prijmu ramce nebo timeoutu
    TSensorSlaveDataHandler sensorSlaveDataRxHandler;

    // funkce, ktera je volana po odeslani ramce
    TSensorSlaveDataHandler sensorSlaveDataTxHandler;

} state;

//------------------------------------------------------------------------------
//  Start prijimaciho casovace
//------------------------------------------------------------------------------
static void StartRcvTimer(void)
{
    state.rcvTimer = RTC_GetSysSeconds();
}

//------------------------------------------------------------------------------
//  Zjisteni prijimaciho timeoutu
//------------------------------------------------------------------------------
static unsigned char IsRcvTimeout(void)
{
    return RTC_IsSysSecondTimeout(state.rcvTimer, RCV_TIMEOUT);
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prijmu
//------------------------------------------------------------------------------
static void ProcessRcvTimeout(void)
{
    state.commStatus = SS_CMSTA_NONE;
    unsigned char byteCount = state.rcvBufferByteCount;
    state.rcvBufferByteCount = 0;
    StartRcvTimer();
    state.commStatus = SS_CMSTA_RX;

    if (state.sensorSlaveDataRxHandler != NO_SENSOR_SLAVE_DATA_HANDLER)
    {
        TCommData commData;
        commData.commResult = CRES_TMR;
        commData.buffer = state.rcvBuffer;
        commData.bufferSize = byteCount;

        state.sensorSlaveDataRxHandler(&commData);
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
static void ProcessRcvData(void)
{
    if (state.rcvBufferByteCount < 2)
        return;

    switch (state.rcvBuffer[1])
    {
        case WRITE_MULTIPLE_REGISTER:
            if (state.rcvBufferByteCount >= 7)
            {
                if (state.rcvBufferByteCount == (state.rcvBuffer[6] + 9))
                {
                    state.commStatus = SS_CMSTA_WAIT;
                    if (state.sensorSlaveDataRxHandler != NO_SENSOR_SLAVE_DATA_HANDLER)
                    {
                        TCommData commData;
                        commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                        commData.buffer = state.rcvBuffer;
                        commData.bufferSize = state.rcvBufferByteCount;

                        state.sensorSlaveDataRxHandler(&commData);
                    }
                }
            }
            break;

        case READ_HOLDING_REGISTER:
            if (state.rcvBufferByteCount == 8)
            {
                state.commStatus = SS_CMSTA_WAIT;
                if (state.sensorSlaveDataRxHandler != NO_SENSOR_SLAVE_DATA_HANDLER)
                {
                    TCommData commData;
                    commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                    commData.buffer = state.rcvBuffer;
                    commData.bufferSize = state.rcvBufferByteCount;

                    state.sensorSlaveDataRxHandler(&commData);
                }
            }
            break;

        default:
            break;
    }
}

//------------------------------------------------------------------------------
//  Vyzvednuti bytu z UART1
//------------------------------------------------------------------------------
static unsigned char ReadUartByte(void)
{
    return U1RBR;
}

//------------------------------------------------------------------------------
//  Obsluha preruseni od UART1
//------------------------------------------------------------------------------
static void OnRxUartHandler(void)
{
    unsigned char rxByte = ReadUartByte();

    if (state.commStatus == SS_CMSTA_RX)
    {
        if (state.rcvBufferByteCount < RCV_BUFFER_LENGTH)
        {
            state.rcvBuffer[state.rcvBufferByteCount] = rxByte;
            state.rcvBufferByteCount++;
        }
    }

    VICVectAddr = 0;
}

//------------------------------------------------------------------------------
//  Odeslani dat na sbernici.
//------------------------------------------------------------------------------
static void SendData(const unsigned char *dataBuffer, unsigned int dataCount)
{
    state.commStatus = SS_CMSTA_TX;

    DATA_SEND; // prepnuti smeru RS485

    while (dataCount != 0)
    {
        while (!(U1LSR & (1<<5))); // Wait until Transmitter Holding Register is not Empty
        U1THR = *dataBuffer;
        dataBuffer++;
        dataCount--;
    }

    while (!(U1LSR & (1<<6)));

    DATA_RECEIVE; // prepnuti smeru RS485

    state.rcvBufferByteCount = 0;
    StartRcvTimer();
    state.commStatus = SS_CMSTA_RX;
}

//------------------------------------------------------------------------------
//  Funkce, ktera je volana po prijmu odpovedi ze senzoru (sensor masteru)
//------------------------------------------------------------------------------
static void SensorMasterDataHandler(const TCommData *commData)
{
    if ((commData->commResult == CRES_OK) && (SensorMaster_GetRcvFrameByteCount() > 0))
    {
        if ((state.commStatus == SS_CMSTA_RX) && (state.rcvBufferByteCount == 0))
        {
            SendData(SensorMaster_GetRcvFrame(), SensorMaster_GetRcvFrameByteCount());
            if (state.sensorSlaveDataTxHandler != NO_SENSOR_SLAVE_DATA_HANDLER)
            {
                TCommData txData;
                txData.commResult = CRES_OK;
                txData.buffer = SensorMaster_GetRcvFrame();
                txData.bufferSize = SensorMaster_GetRcvFrameByteCount();

                state.sensorSlaveDataTxHandler(&txData);
            }
        }
    }
}

//------------------------------------------------------------------------------
//  Inicializace hw komunikacniho rozhrani.
//------------------------------------------------------------------------------
static void MagX2CommHwInit(void)
{
    unsigned long Fdiv;
    PCONP |= (1<<4);      //set power on UART1
    PINSEL4 |= (1<<1) | (1<<3);   //nastaveni TxD1 a RxD1 na piny 2.0,2.1
    U1LCR = 0x83;   //8bits,no Parity,1 stop, !!! DLAB=0 !!!!

    Fdiv = ( Fpclk / 16 ) / BAUD_RATE;	//baud rate
    U1FCR = 0x07;   //FIFO Enable and restart tx and rx

    U1DLM = Fdiv / 256;
    U1DLL = Fdiv % 256;

    FIO2DIR |= (1<<2);    //P2.2 jako vystupni pro ovladani smeru dat 485
    DATA_RECEIVE;

    // preruseni
    VICVectAddr7 = (unsigned long) OnRxUartHandler;   //set interrupt vector
    VICIntEnable |= (1<<7);            // UART1 enable
    U1LCR = 0x03;                       //DLAB=0
    U1IER = 1;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void SensorSlave_Init(void)
{
    state.commStatus = SS_CMSTA_NONE;
    state.rcvBufferByteCount = 0;
    StartRcvTimer();
    MagX2CommHwInit();
    state.commStatus = SS_CMSTA_RX;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void SensorSlave_Task(void)
{
    if (state.commStatus == SS_CMSTA_RX)
    {
        if (IsRcvTimeout())
            ProcessRcvTimeout();
        else
            ProcessRcvData();
    }
    else if (state.commStatus == SS_CMSTA_WAIT)
    {
        if (SensorMaster_GetCommStatus() == CMSTA_NONE)
        {
            SensorMaster_SendData(state.rcvBuffer, state.rcvBufferByteCount, SensorMasterDataHandler);
            state.rcvBufferByteCount = 0;
            StartRcvTimer();
            state.commStatus = SS_CMSTA_RX;
        }
    }
}

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TSensorSlaveCommStatus SensorSlave_GetCommStatus(void)
{
    return state.commStatus;
}

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* SensorSlave_GetRcvFrame(void)
{
    return state.rcvBuffer;
}

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int SensorSlave_GetRcvFrameByteCount(void)
{
    return state.rcvBufferByteCount;
}

//------------------------------------------------------------------------------
//  Nastaveni handleru prijatych ramcu.
//------------------------------------------------------------------------------
void SensorSlave_SetRxHandler(TSensorSlaveDataHandler handler)
{
    state.sensorSlaveDataRxHandler = handler;
}

//------------------------------------------------------------------------------
//  Nastaveni handleru odeslanych ramcu.
//------------------------------------------------------------------------------
void SensorSlave_SetTxHandler(TSensorSlaveDataHandler handler)
{
    state.sensorSlaveDataTxHandler = handler;
}
