//******************************************************************************
//  Komunikace s MagX2 - VeriMag je v pozici mastera na seriove sbernici.
//*****************************************************************************/

#ifndef MAGX2_COMM_H_INCLUDED
#define MAGX2_COMM_H_INCLUDED

// Typ - prototyp funkce, ktera je volana po prijmu odpovedi na dotaz.
typedef void (*TMagX2CommDataHandler) (const TCommData *commData);

// zadna funkce po prijmu odpovedi
#define NO_MAGX2_COMM_DATA_HANDLER   ((TMagX2CommDataHandler)0)

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void MagX2Comm_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MagX2Comm_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TCommStatus MagX2Comm_GetCommStatus(void);

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* MagX2Comm_GetRcvFrame(void);

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int MagX2Comm_GetRcvFrameByteCount(void);

//------------------------------------------------------------------------------
//  Stop prijmu.
//------------------------------------------------------------------------------
void MagX2Comm_StopReceiving(void);

//------------------------------------------------------------------------------
//  Parsing typu unsigned long MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUlong(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis hesel MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WritePasswords(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Cteni firmware MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFirmware(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing firmware MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseFirmware(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni cisla MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadUnitNo(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing cisla MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUnitNo(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni chybovych minut MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadErrorMin(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing chybovych minut MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseErrorMin(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni ok minut MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadOkMin(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing ok minut MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseOkMin(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni stavu mereni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadMeasurement(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing stavu mereni MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseMeasurement(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis parametru stavu mereni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteMeasurement(TMagX2CommDataHandler handler, unsigned long setting);

//------------------------------------------------------------------------------
//  Cteni stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadLowFlowCutoff(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseLowFlowCutoff(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis parametru stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteLowFlowCutoff(TMagX2CommDataHandler handler, unsigned long setting);

//------------------------------------------------------------------------------
//  Cteni stavu buzeni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExcitation(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing stavu buzeni MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseExcitation(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis parametru stavu buzeni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteExcitation(TMagX2CommDataHandler handler, unsigned long setting);

//------------------------------------------------------------------------------
//  Cteni nastaveni proudove smycky MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadCurrentLoopSetting(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing nastaveni proudove smycky MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseCurrentLoopSetting(const unsigned char *buffer, TCurrentLoopSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru proudove smycky MagX2 - Set.
//------------------------------------------------------------------------------
void MagX2Comm_WriteCurrentLoopSetting_Set(TMagX2CommDataHandler handler, const TCurrentLoopSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru proudove smycky MagX2 - MinMax.
//------------------------------------------------------------------------------
void MagX2Comm_WriteCurrentLoopSetting_MinMax(TMagX2CommDataHandler handler, const TCurrentLoopSetting *setting);

//------------------------------------------------------------------------------
//  Cteni nastaveni frekvencniho vystupu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFrequencyOutputSetting(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing nastaveni frekvencniho vystupu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFrequencyOutputSetting(const unsigned char *buffer, TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - Set.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_Set(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - MinMax.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_MinMax(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - Duty cycle.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Cteni nastaveni simulovaneho toku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowSimulationSetting(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing nastaveni simulovaneho toku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFlowSimulationSetting(const unsigned char *buffer, TFlowSimulationSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru simulovaneho toku MagX2 - Demo.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFlowSimulationSetting_Demo(TMagX2CommDataHandler handler, const TFlowSimulationSetting *setting);

//------------------------------------------------------------------------------
//  Zapis parametru simulovaneho toku MagX2 - Flow.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFlowSimulationSetting_Flow(TMagX2CommDataHandler handler, const TFlowSimulationSetting *setting);

#endif // MAGX2_COMM_H_INCLUDED
