//******************************************************************************
//  AD prevodnik
//*****************************************************************************/

#include "ad.h"
#include "LPC23xx.h"

// konfigurace portu
#define MEAS_1_ADPIN (1<<2)
#define MEAS_2_ADPIN (1<<1)
#define EXCITATION_IN_ADPIN (1<<0)

#define MEAS_1_AD0DR AD0DR2
#define MEAS_2_AD0DR AD0DR1
#define EXCITATION_IN_AD0DR AD0DR0

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void AD_Init(void)
{
    // ADC power on
    PCONP |= (1<<12);
    AD0CR &= ~(1<<21);

    // select clock for ADC
    PCLKSEL0 |= (1 << 25);
    PCLKSEL0 |= (1 << 24);

    // AD0.0 - AD0.3
    PINSEL1 = (PINSEL1 & ~(0x3 << 14)) | (0x1 << 14);
    PINSEL1 = (PINSEL1 & ~(0x3 << 16)) | (0x1 << 16);
    PINSEL1 = (PINSEL1 & ~(0x3 << 18)) | (0x1 << 18);
    PINSEL1 = (PINSEL1 & ~(0x3 << 20)) | (0x1 << 20);

    AD0CR &= ~(1<<0);
    AD0CR &= ~(1<<1);
    AD0CR &= ~(1<<2);
    AD0CR &= ~(1<<3);
    AD0CR &= ~(1<<4);
    AD0CR &= ~(1<<5);
    AD0CR &= ~(1<<6);
    AD0CR &= ~(1<<7);

    AD0CR |= (1<<21);
}

//------------------------------------------------------------------------------
//  Cteni AD dat.
//------------------------------------------------------------------------------
unsigned int AD_Read(TADChannel channel)
{
    unsigned int adResult = 0xFFFF;

    AD0CR &= 0xFFFFFF00;

    switch (channel)
    {
    case ADCH_MEAS_1:
        AD0CR |= (1 << 24) | MEAS_1_ADPIN;
        while ((MEAS_1_AD0DR & (1<<31)) == 0);
        AD0CR &= 0xF8FFFFFF;
        adResult = (MEAS_1_AD0DR >> 6) & 0x3ff;
        break;

    case ADCH_MEAS_2:
        AD0CR |= (1 << 24) | MEAS_2_ADPIN;
        while ((MEAS_2_AD0DR & (1<<31)) == 0);
        AD0CR &= 0xF8FFFFFF;
        adResult = (MEAS_2_AD0DR >> 6) & 0x3ff;
        break;

    case ADCH_EXCITATION_IN:
        AD0CR |= (1 << 24) | EXCITATION_IN_ADPIN;
        while ((EXCITATION_IN_AD0DR & (1<<31)) == 0);
        AD0CR &= 0xF8FFFFFF;
        adResult = (EXCITATION_IN_AD0DR >> 6) & 0x3ff;
        break;
    }

    return adResult;
}
