//******************************************************************************
//  Management nabijeni/uspavani/probouzeni.
//*****************************************************************************/

#ifndef POWER_MAN_H_INCLUDED
#define POWER_MAN_H_INCLUDED

// Stav baterie
typedef enum
{
    BATS_NO_BATTERY = 0,
    BATS_LOW_BATTERY = 1,
    BATS_MEDIUM_BATTERY = 2,
    BATS_HIGH_BATTERY = 3,
    BATS_CHARGING = 4,

} TBatteryStatus;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void PowerMan_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void PowerMan_Task(void);

//------------------------------------------------------------------------------
//  Vypnout.
//------------------------------------------------------------------------------
void PowerMan_SwitchOff(void);

//------------------------------------------------------------------------------
//  Vrati stav baterie.
//------------------------------------------------------------------------------
TBatteryStatus PowerMan_GetBatteryStatus(void);

#endif // POWER_MAN_H_INCLUDED
