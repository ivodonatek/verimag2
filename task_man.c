//******************************************************************************
//  Management uloh.
//*****************************************************************************/

#include "task_man.h"

// zadna uloha
#define NO_TASK_PROCEDURE   ((TTaskProcedure)0)

// max. pocet uloh
#define MAX_TASK_COUNT   5

// Fw module state
static struct
{
    // ulohy
    TTaskProcedure tasks[MAX_TASK_COUNT];

} state;

//------------------------------------------------------------------------------
//  Inicializace buferu uloh.
//------------------------------------------------------------------------------
static void InitTaskBuffer(void)
{
    for (unsigned int i = 0; i < MAX_TASK_COUNT; i++)
    {
        state.tasks[i] = NO_TASK_PROCEDURE;
    }
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void TaskMan_Init(void)
{
    InitTaskBuffer();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne. Volaji se zaregistrovane ulohy.
//------------------------------------------------------------------------------
void TaskMan_Task(void)
{
    for (unsigned int i = 0; i < MAX_TASK_COUNT; i++)
    {
        if (state.tasks[i] != NO_TASK_PROCEDURE)
            state.tasks[i]();
    }
}

//------------------------------------------------------------------------------
//  Registrace ulohy.
//------------------------------------------------------------------------------
void TaskMan_Register(TTaskProcedure task)
{
    for (unsigned int i = 0; i < MAX_TASK_COUNT; i++)
    {
        if (state.tasks[i] == NO_TASK_PROCEDURE)
        {
            state.tasks[i] = task;
            return;
        }
    }
}

//------------------------------------------------------------------------------
//  Odregistrace ulohy.
//------------------------------------------------------------------------------
void TaskMan_Unregister(TTaskProcedure task)
{
    for (unsigned int i = 0; i < MAX_TASK_COUNT; i++)
    {
        if (state.tasks[i] == task)
            state.tasks[i] = NO_TASK_PROCEDURE;
    }
}
