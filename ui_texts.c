//******************************************************************************
//  Texty uzivatelskeho rozhrani.
//*****************************************************************************/

#include "ui_texts.h"

//------------------------------------------------------------------------------
//  texty
//------------------------------------------------------------------------------
const char TXT_TestID[] = "Test ID";
const char TXT_TestMeas2C1[] = "Test Meas2 C1";
const char TXT_TestMeas2C2[] = "Test Meas2 C2";
const char TXT_TestMeas1E1[] = "Test Meas1 E1";
const char TXT_TestMeas1Temp[] = "Test Meas1 Temp";
const char TXT_TestCInductance[] = "Test C Inductance";
const char TXT_TestExcitationIn[] = "Test Excitation In";
const char TXT_TestFrequency[] = "Test Frequency";
const char TXT_TestIout[] = "Test Iout";
const char TXT_TestMagX2Comm[] = "Test MagX2 Comm";
const char TXT_TestSensorSlave[] = "Test Sensor Slave";
const char TXT_TestSensorMaster[] = "Test Sensor Master";
const char TXT_TestModbus[] = "Test Modbus";
const char TXT_TestADP5063[] = "Test ADP5063";
const char TXT_NA[] = "N/A";
const char TXT_CalibrationDate[] = "Calibration Date";
const char TXT_ON[] = "ON";
const char TXT_OFF[] = "OFF";
const char TXT_Backlight[] = "Backlight";
const char TXT_UnitNo[] = "Unit No.";
const char TXT_Firmware[] = "Firmware";
const char TXT_Info[] = "Info";
const char TXT_Date[] = "Date";
const char TXT_Time[] = "Time";
const char TXT_Setting[] = "Setting";
const char TXT_PowerOff[] = "POWER OFF";
const char TXT_PowerOffQuestion[] = "TURN DEVICE OFF?";
const char TXT_Error[] = "Error";
const char TXT_Timeout[] = "Timeout";
const char TXT_EmptyString[] = "";
const char TXT_TestHw[] = "Test Hardware";
const char TXT_Debug[] = "Debug";
const char TXT_Test[] = "Start Test";
const char TXT_Progress[] = "PROGRESS";
const char TXT_TestComplete[] = "TEST COMPLETE";
const char TXT_C1Rezistance[] = "C1 Rezistance";
const char TXT_C2Rezistance[] = "C2 Rezistance";
const char TXT_EInsulation[] = "Electrodes Insulation";
const char TXT_TempSensor[] = "Temp Sensor";
const char TXT_CInductance[] = "Coil Inductance";
const char TXT_ExcitationLevels[] = "Excitation Levels";
const char TXT_Frequency[] = "Frequency";
const char TXT_Iout[] = "Output Current";
const char TXT_Magx2UnitNo[] = "MagX2 Unit No.";
const char TXT_Magx2Firmware[] = "Magx2 Firmware";
const char TXT_SensorUnitNo[] = "Sensor Unit No.";
const char TXT_SensorFirmware[] = "Sensor Firmware";
const char TXT_Diameter[] = "Diameter";
const char TXT_FlowRange[] = "Flow Qn";
const char TXT_ErrorMin[] = "Error Min";
const char TXT_OkMin[] = "OK Min";
const char TXT_ErrorCode[] = "Error Code";
const char TXT_Total[] = "Total";
const char TXT_TotalPlus[] = "Total+";
const char TXT_TotalMinus[] = "Total-";
const char TXT_Aux[] = "Aux";
const char TXT_CalibrationPoint1[] = "CP1";
const char TXT_CalibrationPoint2[] = "CP2";
const char TXT_CalibrationPoint3[] = "CP3";
const char TXT_CalibrationData1[] = "CD1";
const char TXT_CalibrationData2[] = "CD2";
const char TXT_CalibrationData3[] = "CD3";
const char TXT_Totals[] = "Totalizers";
const char TXT_Calibration[] = "Calibration";
const char TXT_CurrentLoopSetting[] = "Current Loop Setting";
const char TXT_Magx2Passwords[] = "Magx2 Passwords";
const char TXT_FrequencyOutputSetting[] = "Frequency Output Setting";
const char TXT_Measurement[] = "Measurement";
const char TXT_Excitation[] = "Excitation";
const char TXT_FlowSimulationSetting[] = "Flow Simulation Setting";
const char TXT_Pass[] = "Pass";
const char TXT_Fail[] = "Fail";
const char TXT_PrepareSector[] = "Prepare Sector";
const char TXT_WriteData[] = "WriteData";
const char TXT_CompareData[] = "Compare Data";
const char TXT_Unsupported[] = "Unsupported";
const char TXT_LowFlowCutoff[] = "Low Flow Cutoff";
const char TXT_StoppingTest[] = "Stopping Test...";
const char TXT_TestStopped[] = "Test Stopped";

//------------------------------------------------------------------------------
//  Prevodni tabulka Identifikator -> text
//------------------------------------------------------------------------------
const char* ID_TO_TEXT_TABLE[] =
{
    TXT_TestID,
    TXT_TestMeas2C1,
    TXT_TestMeas2C2,
    TXT_TestMeas1E1,
    TXT_TestMeas1Temp,
    TXT_TestCInductance,
    TXT_TestExcitationIn,
    TXT_TestFrequency,
    TXT_TestIout,
    TXT_TestMagX2Comm,
    TXT_TestSensorSlave,
    TXT_TestSensorMaster,
    TXT_TestModbus,
    TXT_TestADP5063,
    TXT_NA,
    TXT_CalibrationDate,
    TXT_ON,
    TXT_OFF,
    TXT_Backlight,
    TXT_UnitNo,
    TXT_Firmware,
    TXT_Info,
    TXT_Date,
    TXT_Time,
    TXT_Setting,
    TXT_PowerOff,
    TXT_PowerOffQuestion,
    TXT_Error,
    TXT_Timeout,
    TXT_EmptyString,
    TXT_TestHw,
    TXT_Debug,
    TXT_Test,
    TXT_Progress,
    TXT_TestComplete,
    TXT_C1Rezistance,
    TXT_C2Rezistance,
    TXT_EInsulation,
    TXT_TempSensor,
    TXT_CInductance,
    TXT_ExcitationLevels,
    TXT_Frequency,
    TXT_Iout,
    TXT_Magx2UnitNo,
    TXT_Magx2Firmware,
    TXT_SensorUnitNo,
    TXT_SensorFirmware,
    TXT_Diameter,
    TXT_FlowRange,
    TXT_ErrorMin,
    TXT_OkMin,
    TXT_ErrorCode,
    TXT_Total,
    TXT_TotalPlus,
    TXT_TotalMinus,
    TXT_Aux,
    TXT_CalibrationPoint1,
    TXT_CalibrationPoint2,
    TXT_CalibrationPoint3,
    TXT_CalibrationData1,
    TXT_CalibrationData2,
    TXT_CalibrationData3,
    TXT_Totals,
    TXT_Calibration,
    TXT_CurrentLoopSetting,
    TXT_Magx2Passwords,
    TXT_FrequencyOutputSetting,
    TXT_Measurement,
    TXT_Excitation,
    TXT_FlowSimulationSetting,
    TXT_Pass,
    TXT_Fail,
    TXT_PrepareSector,
    TXT_WriteData,
    TXT_CompareData,
    TXT_Unsupported,
    TXT_LowFlowCutoff,
    TXT_StoppingTest,
    TXT_TestStopped,
};

//------------------------------------------------------------------------------
//  Vrati text podle identifikatoru.
//------------------------------------------------------------------------------
const char* UI_GetText(TUITextId id)
{
    return ID_TO_TEXT_TABLE[id];
}
