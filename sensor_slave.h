//******************************************************************************
//  Komunikace s MagX2 - VeriMag je v pozici slave na sbernici, simuluje senzor.
//  Prijima ramce z MagX2 a preposila je senzoru, odpovedi senzoru preposila
//  na MagX2.
//*****************************************************************************/

#ifndef SENSOR_SLAVE_H_INCLUDED
#define SENSOR_SLAVE_H_INCLUDED

// stav komunikace
typedef enum
{
    SS_CMSTA_NONE, // nic se nedeje
    SS_CMSTA_TX, // vysilani
    SS_CMSTA_RX, // prijem
    SS_CMSTA_WAIT, // cekani na volnou linku na senzor

} TSensorSlaveCommStatus;

// Typ - prototyp funkce, ktera je volana po prijmu/vyslani zpravy nebo timeoutu.
typedef void (*TSensorSlaveDataHandler) (const TCommData *commData);

// zadna funkce po prijmu zpravy nebo timeoutu.
#define NO_SENSOR_SLAVE_DATA_HANDLER   ((TSensorSlaveDataHandler)0)

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void SensorSlave_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void SensorSlave_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TSensorSlaveCommStatus SensorSlave_GetCommStatus(void);

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* SensorSlave_GetRcvFrame(void);

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int SensorSlave_GetRcvFrameByteCount(void);

//------------------------------------------------------------------------------
//  Nastaveni handleru prijatych ramcu.
//------------------------------------------------------------------------------
void SensorSlave_SetRxHandler(TSensorSlaveDataHandler handler);

//------------------------------------------------------------------------------
//  Nastaveni handleru odeslanych ramcu.
//------------------------------------------------------------------------------
void SensorSlave_SetTxHandler(TSensorSlaveDataHandler handler);

#endif // SENSOR_SLAVE_H_INCLUDED
