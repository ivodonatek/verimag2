

/*/---------------------------------------------------------------------------------------------------------------------/
///                 /                   /                   /                       /                   /               /
///     ID SLAVE    /   FUNCTION CODE   /   START ADRESS    /   NUM OF REGISTER     /   NUM OF BYTES    /     VALUES    /
///      1 BYTE     /      1 BYTE       /      2 BYTES      /       2 BYTES         /       1 BYTE      /   2*n BYTES   /
///                 /                   /                   /                       /                   /               /
///--------------------------------------------------------------------------------------------------------------------*/

///zapis vice registru, parametr - pocatecni adresa, pocet registru,pole s datama
int write_multiple_register(unsigned char id,unsigned int adress,unsigned int count_reg,unsigned char bytes,unsigned char *data)
{
  unsigned char grr[10]; //debugger

  unsigned int velikost = 9+(unsigned int)bytes;
  unsigned char sendbuffer[velikost];
  unsigned char *buff = sendbuffer;
  unsigned int n = 0;

  *(buff+n)=id;                           //id slave
  n++;

  *(buff+n)=WRITE_MULTIPLE_REGISTER;      //function code
  n++;

  *(buff+n)=(unsigned char)(adress>>8);   //start adress
  n++;
  *(buff+n)=(unsigned char)adress;
  n++;

  *(buff+n)=(unsigned char)(count_reg>>8);//pocet registru
  n++;
  *(buff+n)=(unsigned char)count_reg;
  n++;

  *(buff+n)=bytes;                        //pocet bytu
  n++;

  unsigned int bajty=(unsigned int)bytes; //doplneni dat
  while(bajty)
  {
    *(buff+n)=*(data++);
    n++;
    bajty--;
  }

  unsigned int crcecko=usMBCRC16(buff,velikost-2);;   //kontrolni soucet
  *(buff+n)=(unsigned char)(crcecko);
  n++;

  *(buff+n)=(unsigned char)(crcecko>>8);
  n++;


  Send_sensor(buff,velikost);             //odeslani do sensoru
  UART3_int_enable();                     //povoleni preruseni od UART3 - po prijmu se zakaze

  WriteLine(0,"pred");
  ShowLine(1);

  enable_timer0();
  unsigned char nic;
  while( (timeout==0) && (SensorDataReady==0))
  {
    nic=1;
    WriteLine(0,"ve while");
    ShowLine(4);
  }

  WriteLine(0,"po podmince");
  ShowLine(1);

  if( (timeout) && (SensorDataReady==0))
  {
    WriteLine(0,"timeout");
    ShowLine(1);
    timeout=0;
    disable_timer0();
    return 0;
  }
  else
  {
    disable_timer0();
    WriteLine(0,"prijem");
    ShowLine(1);
    timeout=0;
    unsigned char prijem[5];//maximi pocet co muze nastat
    unsigned int k=0;
    ///nastavit casovac
    unsigned int j;
    while( (U3LSR&0x01) && !(timeout))  //pokud je plnej buffer a nevyprsel timeout
    {
      prijem[k]=Read_sensor();
      k++;
      for(j=0;j<30000;j++)asm("nop"); ///pauza, doladit delku
      ///znovu nastaveni casovace
    }

    if(timeout) //pokud vyprsel timeout
    {
      WriteLine(0,"timeout po prijmu");
      ShowLine(0);
      return 0;
    }
    switch(prijem[1])
    {
      case WRITE_MULTIPLE_REGISTER:
        EraseLineBuffer();
        WriteLine(0,"funkce ok");
        ShowLine(0);
        break;

      case ERR_ANSWER:
        WriteLine(0,"chyba");
        ShowLine(0);
        return 0;
        break;

      default:
        WriteLine(0,"kod funkce je blbost");
        ShowLine(0);
        return 0;
        break;
    }
  }
  return 1;
}
/*/---------------------------------------------------------------------------------/
///                 /                   /                   /                       /
///     ID SLAVE    /   FUNCTION CODE   /   START ADRESS    /   NUM OF REGISTER     /
///      1 BYTE     /      1 BYTE       /      2 BYTES      /       2 BYTES         /
///                 /                   /                   /                       /
///--------------------------------------------------------------------------------*/
int read_holding_register(unsigned char id,unsigned int adress,unsigned int count_reg)
{
  unsigned int velikost=8;
  unsigned char sendbuffer[velikost];
  unsigned char *buff=sendbuffer;
  unsigned int n=0;

  *(buff+n)=id;                           //id slave
  n++;

  *(buff+n)=READ_HOLDING_REGISTER;      //function code
  n++;

  *(buff+n)=(unsigned char)(adress>>8);   //start adress
  n++;
  *(buff+n)=(unsigned char)adress;
  n++;

  *(buff+n)=(unsigned char)(count_reg<<8);//pocet registru
  // *(buff+n)=0x00;//pocet registru
  n++;
  *(buff+n)=(unsigned char)count_reg;
  n++;

  unsigned int crcecko=usMBCRC16(buff,velikost-2);;   //kontrolni soucet

  *(buff+n)=(unsigned char)(crcecko);     //naplneni crcka
  n++;

  *(buff+n)=(unsigned char)(crcecko>>8);
  n++;

  Send_sensor(buff,velikost);             //odeslani pozadavku

  UART3_int_enable();

 ///prijem

  enable_timer0();
  while( (timeout==0) && (SensorDataReady==0))
      {
      WriteLine(0,"ve while");        ///kdyz je nop tak to nejde
      ShowLine(3);
      }

  if( (timeout) && (SensorDataReady==0))
      {
      WriteLine(0,"timeout");
      ShowLine(4);
      timeout=0;
      disable_timer0();
      return 0;
      }
  else
  {
    EraseLineBuffer();
    WriteLine(0,"prijem");
    ShowLine(5);
    timeout=0;
    disable_timer0();

    unsigned char prijem[3+count_reg*2];
    unsigned int k=0;
    ///nastavit casovac
    unsigned int j;
    while( (U3LSR&0x01) && !(timeout))///pridat jeste vyprseni casovace (zatim jen plnej buffer)
    {
      prijem[k]=Read_sensor();
      k++;
      for(j=0;j<30000;j++)asm("nop"); ///pauza, doladit delku
      ///znovu nastaveni casovace
    }

    if(timeout) //pokud vyprsel timeout
    {
      EraseLineBuffer();
      WriteLine(0,"timeout po prijmu");
      ShowLine(4);
      return 0;
    }
    switch(prijem[1])
    {
      case READ_HOLDING_REGISTER:
          EraseLineBuffer();
          WriteLine(0,"Funkce OK");
          ShowLine(6);
          break;

      case ERR_ANSWER:
          EraseLineBuffer();
          WriteLine(0,"chyba");
          ShowLine(4);
          return 0;
          break;

      default:
          EraseLineBuffer();
          WriteLine(0,"kod funkce je blbost");
          ShowLine(4);
          return 0;
          break;
    }
    unsigned int size=prijem[2];
    if( (k-5)!=size )
    {
      WriteLine(0,"prisla jina velikost");
      ShowLine(4);
      return 0;
    }
    else
    {
      unsigned char *registry;
      registry=prijem[3];
    }
  }
  ///return registry;
  return 1;
}

unsigned char receive_from_sensor(unsigned int pocet)
{

}
