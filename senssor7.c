void used_sensor7()
{

        ///*************************************************************************************
        ///                                                                                  ///
        ///                           HLAVNI SMYCKA                                          ///
        ///                                                                                  ///
        ///*************************************************************************************

                while(1)
                {
                    ( void )eMBPoll(  ); // modbus pool
                    //DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;

                    ///prevzeti actual error do erroru pro datalogger
                    actual_error_datalogger |= actual_error;
                    /// jednou za sekundu
                    if (flag_RTC) // jednou za sekundu
                    {
                        if(backlight_second>0)
                        {
                            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
                            backlight_second--;
                        }
                        else
                        {
                            DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;
                        }
                        flag_RTC = 0;
                        co_sekunda();
                        write_to_RAM();
                        control(SHOW);     // prekresleni displeje
                    }

                    /// zmena nastaveni modbus
                    if ((ChangeCommunicationParam)&&(modbus_transmitt_state == 1))
                    {
                        modbus_transmitt_state = 0;
            #ifdef  RTS_ENABLE
                        //while (RTS_PORT&(1<<RTS_PIN))
                        //while((IOPIN0&(1<<10))==(1<<10))
                            asm volatile ("nop");//cekej na odvysilani cele zpravy
            #endif
                        Modbus_init();
                        ChangeCommunicationParam=false;
                    }

                    if(flag_receive)        //pokud se vyvolalo preruseni prijmu od sensoru 7.1
                        {
                        if(zpracovat_data())        //zpracuj prijata data
                            flag_cekam=0;   //flag cekam se nastavi po odeslani pozadavku
                        }

                    if (flag_tlacitko || flag_timer1)
                    {
                        flag_tlacitko = 0;
                        flag_timer1 = 0;
                        backlight_second=BACKLIGHT_SEC;
                        if (pressed_key != 0xFF)              // bylo tlacitko stisknuto?
                        {
                            switch (pressed_key)                           // ktere tlacitko bylo stisknuto
                            {
                            case 16 : // UP
                                if (!key_lock) control(UP);
                                last_key = UP;
                                enable_timer1();
                                break;
                            case 2 : // DOWN
                                if (!key_lock) control(DOWN);
                                last_key = DOWN;
                                enable_timer1();
                                break;
                            case 1 : // RIGHT
                                if (!key_lock) control(RIGHT);
                                last_key = RIGHT;
                                enable_timer1();
                                break;
                            case 0 : // ENTER
                                if (g_mode == MEASUREMENT)
                                {
                                    RTC_read(); // aktualizace casove struktury - kvuli kontrole zamku klaves
                                    if ( (DateTime.second-key_ESC_time_sec < 2) || ( (key_ESC_time_sec == 59) && (DateTime.second == 00) ) ) // pokud je ESC a Enter stisteny do dvou sekund
                                    {
                                        if ((last_key == ESC) && (key_lock == 0)) key_lock = 1; // pokud bylo posledni tlacitko ESC a ted je ENTER tak se zamkne klavesnice
                                        else if ( (last_key == ESC) && (key_lock == 1) )
                                        {
                                            key_lock = 0; // pokud byla zamknuta klavesnice tak se odemkne
                                            key_dummy = 1; // vynechani klavesy ENTER
                                        }
                                    }
                                }
                                if (!(key_lock) && !(key_dummy)) control(ENTR);
                                key_dummy = 0;
                                last_key = ENTR;
                                enable_timer1();
                                break;
                            case 17 : // ESC
                                if (!key_lock) control(ESC);
                                last_key = ESC;
                                if (g_mode == MEASUREMENT)
                                {
                                    RTC_read(); // aktualizace casove struktury
                                    key_ESC_time_sec = DateTime.second;
                                }
                                enable_timer1();
                                break;
                            case 3 : // LEFT
                                if (!key_lock) control(LEFT);
                                last_key = LEFT;
                                enable_timer1();
                                break;
                            default:
                                break;
                            }
                            pressed_key = 0xff;
                        }
                        else                                                        // tlacitko bylo uvolneno
                        {
                            disable_timer1();
                        }
                    }
                    if (modbus_read_datalogger_flag)
                    {
                        modbus_read_datalogger_flag = 0;
                        read_datalogger(modbus_datalogger_date, modbus_datalogger_time, 1);
                    }

                }
        return 0;

    }

    void co_sekunda()
    {
        ///kontrola cyclovani totaliseru
        if( ++flag_totaliser_cycling >=3)
        {
            if( !(RAM_Read_Data_Long( TOTALIZER_CYCLING)) ) //pokud je aktivni cyklovacni totalizeru
            {
                g_measurement.act_rot_meas = (g_measurement.act_rot_meas < (MEAS_COUNT-1)) ? (g_measurement.act_rot_meas+1) : 0;
            }
            flag_totaliser_cycling=0;//vynulovani pocitadla;
        }

        ///kontrola zapisu do flashky
        if( --flag_zapsat_flash == 0 )
        {
            flag_zapsat_flash=SEC_TO_WRITE_FLASH;
            write_data_to_flash();
        }

        ///kontrola cisteni
        if( ((--flag_cisteni_cas)==0) && (flag_cisteni>0) ) //jestlize je cas roven nule a je stale jeste zaple cisteni
        {
            flag_cisteni=0;
            sensor_cisteni(SENSOR_OFF);
        }

        ///kontrola error min,Ok min
        if(actual_error)
        {
             error_min+=(1.0/60.0);
        }
        else
        {
            ok_min+=(1.0/60.0);
        }

        /// zpracovani prijmu dat
        if((!flag_aktivni_zadost) )     //pokud je aktivni zadost pro neco jineho nez requeest data, pak jsou request data odstavena a nemuze se tedy nastavit flag cekam
        {
            if(!(flag_cekam))   //pokud neni nastaven flag vynuluje se error a pocitadlo spatnych prijmu ****JEN PRO AKT.PRUTOK
            {
                actual_error &= ~ERR_SENSOR;
                pocet_spatnych_prijmu=0;
            }
            else    //pokud je nastaven, neprisla posledni odpoved od senzoru
            {
                prutok=0;

                pocet_spatnych_prijmu++;
                if(pocet_spatnych_prijmu>=5)    //pokud je pocet spaynych prijmu vetsi jako 5 tak error
                {
                    #ifdef DEBUGGER
                    EraseLineBuffer();
                    WriteLine(0,"Timeout");
                    ShowLine(ERR_LINE);
                    #endif
                    //aktivovat error
                    actual_error|=ERR_SENSOR;
                }
            }

            ///zadost o nova data
            request_data();
        }
        flag_zapis_prutok=1;        //pokyn k upadtovani dataloggeru a totaliseru


        if(flag_sensor_zadosti & AKTIVNI_ZADOST)    ///neprisla odpoved na cisteni civek,cteni ci zapisu frekvence,cteni ci zapisu buzeni
        {
            flag_aktivni_zadost=1;

            if( pocet_pokusu_zadost_sensor>3)   //pocet pokusu presahl 3
            {
                flag_aktivni_zadost=4;
                flag_sensor_zadosti &= ~AKTIVNI_ZADOST;
                actual_error |= ERR_SENSOR;             //nastavit flag v hlavni smycce
            }
            else if( flag_sensor_zadosti & ZADOST_STAV_BUZENI )
            {
                sensor_stav_buzeni();
            }
            else if( flag_sensor_zadosti & ZADOST_STAV_FREKVENCE)
            {
                sensor_stav_frekvence();
            }
            else if( flag_sensor_zadosti & ZADOST_ZAPIS_BUZENI)
            {
                if( RAM_Read_Data_Long(EXCITATION))
                    sensor_buzeni(SENSOR_OFF);
                else
                    sensor_buzeni(SENSOR_ON);
            }
            else if( flag_sensor_zadosti & ZADOST_ZAPIS_FREKVENCE)
            {
                if( RAM_Read_Data_Long(EXCITATION_FREQUENCY))
                    sensor_frekvence(FREKVENCE_6HZ);
                else
                    sensor_frekvence(FREKVENCE_3HZ);
            }
            else if( flag_sensor_zadosti & ZADOST_ZAPIS_CISTENI)
            {
                if( aktivni_cisteni)
                    sensor_cisteni(SENSOR_ON);
                else
                    sensor_cisteni(SENSOR_OFF);
            }

            pocet_pokusu_zadost_sensor++;
        }
        else if(flag_kalibrace_send > 0)     ///pokud nedojde ke spravnemu prijmu, nastavvi se prislusny bit ve flagu a je tedy podminka splnena
        {
                if(pocet_pokusu_send>=4)    //pokud je pocet pokusu vetsi tak vyhalsim error
                {
                    actual_error |= ERR_SENSOR;
                    flag_kalibrace_send=0;
                    flag_aktivni_zadost=0;
                }
                else                        //jinak se znovu provede zadost o kalib data
                {
                    flag_aktivni_zadost=1;          //odstaveni funkce requestdata() kvuli uvolneni linky pro komunikaci
                    if( flag_kalibrace_send & 0x01)
                        {
                            send_calibration(1);
                        }
                    else if( flag_kalibrace_send & 0x02)
                        {
                            send_calibration(2);
                        }
                    else if( flag_kalibrace_send & 0x04)
                        {
                            send_calibration(3);
                        }
                    pocet_pokusu_send++;
                }
        }
        ///to same jako kalibrace_send vyse, akorat pro zadost o kalibraci
        else if(flag_kalibrace_recv > 0)     //pokud je error
        {
                if(pocet_pokusu_recv>=4)
                {
                    actual_error |= ERR_SENSOR;
                    flag_kalibrace_recv=0;
                    flag_aktivni_zadost=0;
                }
                else                //znovu zadost
                {
                    flag_aktivni_zadost=1;          //odstaveni funkce requestdata()
                    if( flag_kalibrace_recv & 0x01)
                        {
                            recv_calibration(1);
                        }
                    else if( flag_kalibrace_recv & 0x02)
                        {
                            recv_calibration(2);
                        }
                    else if( flag_kalibrace_recv & 0x04)
                        {
                            recv_calibration(3);
                        }
                    pocet_pokusu_recv++;
                }

        }
        else                //pokud je vse v poradku
        {
            flag_aktivni_zadost=0;
            //actual_error &= ~ERR_SENSOR;
            pocet_pokusu_recv=0;
            pocet_pokusu_send=0;
            pocet_pokusu_zadost_sensor=0;
        }

        if(flag_zapis_prutok)        //byla prijata a zpracovana data a data budou zapsana do tatlizeru a pole
        {
            flag_zapis_prutok=0;
            ///zapis novych dat
            ///posun prvku v poli
                for(unsigned int j=SAMPLES_MAX-1;j>0;j--)
                    {
                     act_adc[j]=act_adc[j-1];
                    }

            ///zapsani aktualniho prutoku na 0.pozici
            act_adc[0]=prutok;        //kdyz prijdou data tak se zapisi, kdyz byl timeout zapise se nula

            if( actual_error & ERR_SENSOR )
            {
                if( RAM_Read_Data_Long ( DEMO))     //neni demo
                {
                    adc_average=0;
                    g_measurement.actual=0;
                }
                else                                //je demo
                    g_measurement.actual=((float)RAM_Read_Data_Long(SIMULATED_FLOW))/1000.0;

            }
            else
            {
                ///vypocet prumerne hodnoty
                adc_average=0;
                    for(unsigned int k=0;k<RAM_Read_Data_Long( AVERAGE_SAMPLES);k++)
                        {
                        adc_average+=act_adc[k];
                        }
                adc_average/=((long)(RAM_Read_Data_Long(AVERAGE_SAMPLES)));
                ///vypocet aktualniho prutoku y prumerne hodnoty

                if( RAM_Read_Data_Long( DEMO))  //OFF -> 0=ON,1=OFF
                {
                    if(flag_start>0)            //10 sekund po startu se plni nulama
                    {
                        g_measurement.actual=0;
                        flag_start--;
                    }
                    else
                    {
                    ///g_measurement.actual=Compute_Actual_Flow_From_AD_Value(adc_average - RAM_Read_Data_Long(ZERO_FLOW_CONSTANT) );
                    g_measurement.actual=Compute_Actual_Flow_From_AD_Value(adc_average - RAM_Read_Data_Long(ZERO_FLOW_CONSTANT) );
                    }
                }
                else    //JE DEMO
                    {
                    g_measurement.actual=((float)RAM_Read_Data_Long(SIMULATED_FLOW))/1000.0;
                    }
            }

            ///low float cutoff
            if( RAM_Read_Data_Long(LOW_FLOW_CUTOFF) != 5)
            {
            float pomocny_prutok=0;
                if(g_measurement.actual < 0)      //pokud je zaporny prutok
                {
                    pomocny_prutok=g_measurement.actual;  //ulozi se do promenne
                    g_measurement.actual/=(-1.0);               //a prutok se invertuje
                }

                switch ( RAM_Read_Data_Long( LOW_FLOW_CUTOFF))
                {
                    case 0 :
                    {
                        // pokud je prutok mensi jak 0,5% FLOW_RANGE tak se prutok nastavi na 0
                        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.000005))
                            g_measurement.actual = 0;
                        break;
                    }
                    case 1 :
                    {
                        // pokud je prutok mensi jak 1% FLOW_RANGE tak se prutok nastavi na 0
                        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00001))
                            g_measurement.actual = 0;
                        break;
                    }
                    case 2 :
                    {
                        // pokud je prutok mensi jak 2% FLOW_RANGE tak se prutok nastavi na 0
                        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00002))
                            g_measurement.actual = 0;
                        break;
                    }
                    case 3 :
                    {
                        // pokud je prutok mensi jak 5% FLOW_RANGE tak se prutok nastavi na 0
                        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00005))
                            g_measurement.actual = 0;
                        break;
                    }
                    case 4 :
                    {
                        // pokud je prutok mensi jak 10% FLOW_RANGE tak se prutok nastavi na 0
                        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.0001))
                            g_measurement.actual = 0;
                        break;
                    }
                }

                if( (pomocny_prutok != 0) && (g_measurement.actual!=0) ) //pokud je naplena promenna a neni nulovy prutok
                    g_measurement.actual=pomocny_prutok;
            }

            ///kontrola INVERT FLOW
            if( RAM_Read_Data_Long(INVERT_FLOW) ) //invert flow
                g_measurement.actual=g_measurement.actual*(-1.0);

            ///zapis do pole EP a TEPLOTA
            for(unsigned char r=VELIKOST_POLE_TEPLOTA_EP-1;r>0;r--)
                {
                 pole_ep[r]=pole_ep[r-1];
                 pole_teplota[r]=pole_teplota[r-1];
            }
            pole_ep[0]=ep;
            pole_teplota[0]=teplota;

            ///vypocet average teploty a ep
            if(flag_start>0)    //start delay
            {
            teplota_avg=8523000;
            ep_avg=7000000;
            }
            else                //po start delay
            {
                teplota_avg=0;
                ep_avg=0;
                    for(unsigned char r=0;r<VELIKOST_POLE_TEPLOTA_EP;r++)
                    {
                        teplota_avg+=pole_teplota[r];
                        ep_avg+=pole_ep[r];
                    }
            teplota_avg/=VELIKOST_POLE_TEPLOTA_EP;
            ep_avg/=VELIKOST_POLE_TEPLOTA_EP;
            }
            ///kontrola empty pipe
            if ((ep_avg > (RAM_Read_Data_Long(AIR_CONSTANT)*16777)) && (RAM_Read_Data_Long(AIR_DETECTOR) == 0))             // je stav empty pipe
                actual_error |= ERR_EMPTY_PIPE;
            else if((ep < (RAM_Read_Data_Long(AIR_CONSTANT)*16777)) || (RAM_Read_Data_Long(AIR_DETECTOR) == 1))       // neni stav empty pipe
                actual_error &= ~ERR_EMPTY_PIPE;

            g_measurement.temp = (0.0001939237*teplota_avg - 1430.9631545)/10;       //ve stupnich celsia


            ///inkrementace totaliseru pokud je measurement ON
            if (!RAM_Read_Data_Long(MEASUREMENT_STATE))
            {
                if(g_measurement.actual>0)
                    {
                     g_measurement.total+=(g_measurement.actual)/3600;      //3600 -> m3/h na m3/s
                     g_measurement.flow_pos+=(g_measurement.actual)/3600;
                     g_measurement.aux+=(g_measurement.actual)/3600;
                }
                else
                    {
                     g_measurement.total+= (((g_measurement.actual)*(-1))/3600);
                     g_measurement.flow_neg+= (((g_measurement.actual)*(-1))/3600);
                }
            }

            ///zkouska jestli se ma zapsat do dataloggru
            if( ((--second_to_datalogger) == 0) && (flag_datalogger) )
            {
                write_datalogger();

                switch(RAM_Read_Data_Long(DATALOGGER_INTERVAL))
                {
                    case 0: //off
                        flag_datalogger=0;
                        second_to_datalogger=0;
                    break;
                    case 1: //1 minuta
                        flag_datalogger=1;
                        second_to_datalogger=60;
                    break;
                    case 2: //5 minut
                        flag_datalogger=1;
                        second_to_datalogger=300;
                    break;
                    case 3: //10 minut
                        flag_datalogger=1;
                        second_to_datalogger=600;
                    break;
                    case 4: //15 minut
                        flag_datalogger=1;
                        second_to_datalogger=900;
                    break;
                    case 5: //30 minuit
                        flag_datalogger=1;
                        second_to_datalogger=1800;
                    break;
                    case 6: //1 hodina
                        flag_datalogger=1;
                        second_to_datalogger=3600;
                    break;
                    case 7: //2 hodiny
                        flag_datalogger=1;
                        second_to_datalogger=7200;
                    break;
                    case 8: //6 hodin
                        flag_datalogger=1;
                        second_to_datalogger=21600;
                    break;
                    case 9: //12 hodin
                        flag_datalogger=1;
                        second_to_datalogger=43200;
                    break;
                    case 10:    //1 den
                        flag_datalogger=1;
                        second_to_datalogger=86400;
                    default:
                        flag_datalogger=0;
                        second_to_datalogger=0;
                }
            }
    }

        ///odeslani dat do BINu
        Send_Float(FLOW,g_measurement.actual);
        delay_10ms();
        Send_Long(ERR,(long)( actual_error ));
        delay_10ms();

        ///========nastaveni vystupniho modulu
        switch(flag_outputs_modules)
        {
            case IOUT_MODUL:        //pokud je aktivovan IOUT
                I_Out(g_measurement.actual);    //posle aktualni prutok do funkce
            break;
            case VOUT_MODUL:        //pokud je aktivovan VOUT
                V_Out(g_measurement.actual);
            break;

            default:
            break;
        }

    }
