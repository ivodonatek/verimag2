//******************************************************************************
//  Management uzivatelskeho rozhrani.
//*****************************************************************************/

#include "ui.h"
#include "buttons.h"
#include "screen.h"
#include "rprintf.h"

// Stav UI
static TUIState uiState;

// polozky vyctoveho typu
static TEnumItem *enumItems;

// pocet polozek vyctoveho typu
static unsigned char enumItemCount;

static void UpdateMenuButtons(void);
static void UpdateEditButtons(void);

//------------------------------------------------------------------------------
//  Test, zda je znak v mezich
//------------------------------------------------------------------------------
/*static BOOL IsCharInLimit(char ch, char min, char max)
{
    if ((min <= ch) && (ch <= max))
        return TRUE;
    else
        return FALSE;
}*/

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void UI_Init(void)
{
    UI_InitDisplayButtons();
    uiState.dataMainSubTestId = 0;

    uiState.editState.cursorPos = 0;
    uiState.editState.editHandler = NO_EDIT_HANDLER;
    uiState.editState.titleStr = NULL;
    uiState.editState.valueStr[0] = 0;

    uiState.infoState.titleStr = NULL;
    uiState.infoState.valueStr[0] = 0;
}

//------------------------------------------------------------------------------
//  Vrati stav UI.
//------------------------------------------------------------------------------
const TUIState* UI_GetState(void)
{
    return &uiState;
}

//------------------------------------------------------------------------------
//  Inicializace zobrazeni UI tlacitek.
//------------------------------------------------------------------------------
void UI_InitDisplayButtons(void)
{
    for (unsigned int i = 0; i < UI_BUTTONS_COUNT; i++)
    {
        uiState.displayButtons[i] = FALSE;
    }
}

//------------------------------------------------------------------------------
//  Nastaveni zobrazeni UI tlacitka.
//------------------------------------------------------------------------------
void UI_SetDisplayButton(TUIButton displayButton, BOOL display)
{
    uiState.displayButtons[displayButton] = display;
}

//------------------------------------------------------------------------------
//  Nastaveni zobrazeni UI tlacitek.
//------------------------------------------------------------------------------
void UI_SetDisplayButtons(const TDisplayButtons displayButtons)
{
    for (unsigned int i = 0; i < UI_BUTTONS_COUNT; i++)
    {
        uiState.displayButtons[i] = displayButtons[i];
    }
}

//------------------------------------------------------------------------------
//  Obsluha menu tlacitka ESC.
//------------------------------------------------------------------------------
static void OnMenuButtonESC(void)
{
    uiState.menuState.menu->onExit();
}

//------------------------------------------------------------------------------
//  Obsluha menu tlacitka ENTER.
//------------------------------------------------------------------------------
static void OnMenuButtonENTER(void)
{
    unsigned int selectedMenuItem = uiState.menuState.selectedMenuItem;
    uiState.menuState.menu->items[selectedMenuItem].onEnter();
}

//------------------------------------------------------------------------------
//  Obsluha menu tlacitka UP.
//------------------------------------------------------------------------------
static void OnMenuButtonUP(void)
{
    uiState.menuState.selectedMenuItem--;

    if (uiState.menuState.selectedMenuItem < uiState.menuState.firstMenuItem)
        uiState.menuState.firstMenuItem = uiState.menuState.selectedMenuItem;

    UpdateMenuButtons();

    Screen_ShowMenu();
}

//------------------------------------------------------------------------------
//  Obsluha menu tlacitka DOWN.
//------------------------------------------------------------------------------
static void OnMenuButtonDOWN(void)
{
    uiState.menuState.selectedMenuItem++;

    if ((uiState.menuState.selectedMenuItem - uiState.menuState.firstMenuItem + 1) > SCREEN_MENU_ITEM_COUNT)
        uiState.menuState.firstMenuItem = uiState.menuState.selectedMenuItem - SCREEN_MENU_ITEM_COUNT + 1;

    UpdateMenuButtons();

    Screen_ShowMenu();
}

//------------------------------------------------------------------------------
//  Aktualizace menu tlacitek.
//------------------------------------------------------------------------------
static void UpdateMenuButtons(void)
{
    UI_InitDisplayButtons();
    uiState.displayButtons[UI_BTN_ESC] = TRUE;
    uiState.displayButtons[UI_BTN_ENTER] = TRUE;
    if (uiState.menuState.selectedMenuItem != 0)
        uiState.displayButtons[UI_BTN_UP] = TRUE;
    if ((uiState.menuState.selectedMenuItem + 1) < uiState.menuState.menu->itemCount)
        uiState.displayButtons[UI_BTN_DOWN] = TRUE;

    Buttons_InitHandlers();
    Buttons_SetHandler(BTN_ESC, OnMenuButtonESC);
    Buttons_SetHandler(BTN_ENTER, OnMenuButtonENTER);
    if (uiState.displayButtons[UI_BTN_UP])
        Buttons_SetHandler(BTN_UP, OnMenuButtonUP);
    if (uiState.displayButtons[UI_BTN_DOWN])
        Buttons_SetHandler(BTN_DOWN, OnMenuButtonDOWN);
}

//------------------------------------------------------------------------------
//  Obsluha edit tlacitka UP.
//------------------------------------------------------------------------------
static void OnEditButtonUP(void)
{
    if (uiState.editState.editHandler != NO_EDIT_HANDLER)
        uiState.editState.editHandler(UI_BTN_UP);
}

//------------------------------------------------------------------------------
//  Obsluha edit tlacitka DOWN.
//------------------------------------------------------------------------------
static void OnEditButtonDOWN(void)
{
    if (uiState.editState.editHandler != NO_EDIT_HANDLER)
        uiState.editState.editHandler(UI_BTN_DOWN);
}

//------------------------------------------------------------------------------
//  Obsluha edit tlacitka LEFT.
//------------------------------------------------------------------------------
static void OnEditButtonLEFT(void)
{
    if (uiState.editState.editHandler != NO_EDIT_HANDLER)
        uiState.editState.editHandler(UI_BTN_LEFT);
}

//------------------------------------------------------------------------------
//  Obsluha edit tlacitka RIGHT.
//------------------------------------------------------------------------------
static void OnEditButtonRIGHT(void)
{
    if (uiState.editState.editHandler != NO_EDIT_HANDLER)
        uiState.editState.editHandler(UI_BTN_RIGHT);
}

//------------------------------------------------------------------------------
//  Aktualizace editacnich tlacitek.
//------------------------------------------------------------------------------
static void UpdateEditButtons(void)
{
    uiState.displayButtons[UI_BTN_UP] = TRUE;
    uiState.displayButtons[UI_BTN_DOWN] = TRUE;
    if (uiState.editState.cursorPos != 0)
        uiState.displayButtons[UI_BTN_LEFT] = TRUE;
    else
        uiState.displayButtons[UI_BTN_LEFT] = FALSE;
    if (uiState.editState.valueStr[uiState.editState.cursorPos + 1] != 0)
        uiState.displayButtons[UI_BTN_RIGHT] = TRUE;
    else
        uiState.displayButtons[UI_BTN_RIGHT] = FALSE;

    Buttons_SetHandler(BTN_UP, OnEditButtonUP);
    Buttons_SetHandler(BTN_DOWN, OnEditButtonDOWN);
    if (uiState.displayButtons[UI_BTN_LEFT])
        Buttons_SetHandler(BTN_LEFT, OnEditButtonLEFT);
    else
        Buttons_SetHandler(BTN_LEFT, NO_BUTTON_HANDLER);
    if (uiState.displayButtons[UI_BTN_RIGHT])
        Buttons_SetHandler(BTN_RIGHT, OnEditButtonRIGHT);
    else
        Buttons_SetHandler(BTN_RIGHT, NO_BUTTON_HANDLER);
}

//------------------------------------------------------------------------------
//  Aktualizace editacnich tlacitek pro vyctovy typ.
//------------------------------------------------------------------------------
static void UpdateEditEnumButtons(void)
{
    uiState.displayButtons[UI_BTN_UP] = TRUE;
    uiState.displayButtons[UI_BTN_DOWN] = TRUE;

    Buttons_SetHandler(BTN_UP, OnEditButtonUP);
    Buttons_SetHandler(BTN_DOWN, OnEditButtonDOWN);
}

//------------------------------------------------------------------------------
//  Nastaveni menu podle ID (postara se taky o tlacitka a displej).
//------------------------------------------------------------------------------
void UI_SetMenu(TUIMenuId id, unsigned int selectedMenuItem)
{
    uiState.menuState.menu = UI_GetMenu(id);
    uiState.menuState.firstMenuItem = 0;
    uiState.menuState.selectedMenuItem = selectedMenuItem;

    if ((uiState.menuState.selectedMenuItem - uiState.menuState.firstMenuItem + 1) > SCREEN_MENU_ITEM_COUNT)
        uiState.menuState.firstMenuItem = uiState.menuState.selectedMenuItem - SCREEN_MENU_ITEM_COUNT + 1;

    UpdateMenuButtons();

    Screen_ShowMenu();
}

//------------------------------------------------------------------------------
//  Nastaveni aktualne prohlizenych dat hlavniho testu.
//------------------------------------------------------------------------------
void UI_SetActualMainTestData(TMainSubTestId id)
{
    uiState.dataMainSubTestId = id;
}

//------------------------------------------------------------------------------
//  Nastaveni editace casu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------

static void OnEditTime(TUIButton uiButton)
{
    char maxChar = '9';
    char minChar = '0';
    if (uiState.editState.cursorPos == 0)
    {
        if (uiState.editState.valueStr[1] > '3')
            maxChar = '1';
        else
            maxChar = '2';
    }
    else if (uiState.editState.cursorPos == 1)
    {
        if (uiState.editState.valueStr[0] > '1')
            maxChar = '3';
    }
    else if (uiState.editState.cursorPos == 3)
    {
        maxChar = '5';
    }
    else if (uiState.editState.cursorPos == 6)
    {
        maxChar = '5';
    }

    switch (uiButton)
    {
    case UI_BTN_LEFT:
        {
            if (uiState.editState.cursorPos > 0)
                uiState.editState.cursorPos--;

            if ((uiState.editState.cursorPos == 2) || (uiState.editState.cursorPos == 5))
                uiState.editState.cursorPos--;
        }
        break;

    case UI_BTN_RIGHT:
        {
            if (uiState.editState.cursorPos < 7)
                uiState.editState.cursorPos++;

            if ((uiState.editState.cursorPos == 2) || (uiState.editState.cursorPos == 5))
                uiState.editState.cursorPos++;
        }
        break;

    case UI_BTN_UP:
        {
            uiState.editState.valueStr[uiState.editState.cursorPos]++;
            if (uiState.editState.valueStr[uiState.editState.cursorPos] > maxChar)
                uiState.editState.valueStr[uiState.editState.cursorPos] = minChar;
        }
        break;

    case UI_BTN_DOWN:
        {
            uiState.editState.valueStr[uiState.editState.cursorPos]--;
            if (uiState.editState.valueStr[uiState.editState.cursorPos] < minChar)
                uiState.editState.valueStr[uiState.editState.cursorPos] = maxChar;
        }
        break;

    default:
        break;
    }

    UpdateEditButtons();
    Screen_ShowEdit();
}

void UI_SetEditTime(const char *title, unsigned char hour, unsigned char minute, unsigned char second)
{
    uiState.editState.cursorPos = 0;
    uiState.editState.editHandler = OnEditTime;
    uiState.editState.titleStr = (char *)title;
    sprintf(uiState.editState.valueStr, "%02d:%02d:%02d", hour, minute, second);

    UpdateEditButtons();

    Screen_ShowEdit();
}

//------------------------------------------------------------------------------
//  Vraci editovany cas.
//------------------------------------------------------------------------------
void UI_GetEditTime(unsigned char *hour, unsigned char *minute, unsigned char *second)
{
    *hour = 10 * (uiState.editState.valueStr[0] - '0') + (uiState.editState.valueStr[1] - '0');
    *minute = 10 * (uiState.editState.valueStr[3] - '0') + (uiState.editState.valueStr[4] - '0');
    *second = 10 * (uiState.editState.valueStr[6] - '0') + (uiState.editState.valueStr[7] - '0');
}

//------------------------------------------------------------------------------
//  Nastaveni editace datumu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------

static void OnEditDate(TUIButton uiButton)
{
    char maxChar = '9';
    char minChar = '0';
    if (uiState.editState.cursorPos == 0)
    {
        if (uiState.editState.valueStr[1] > '1')
            maxChar = '2';
        else
            maxChar = '3';

        if (uiState.editState.valueStr[1] == '0')
            minChar = '1';
    }
    else if (uiState.editState.cursorPos == 1)
    {
        if (uiState.editState.valueStr[0] > '2')
            maxChar = '1';

        if (uiState.editState.valueStr[0] == '0')
            minChar = '1';
    }
    else if (uiState.editState.cursorPos == 3)
    {
        if (uiState.editState.valueStr[4] > '2')
            maxChar = '0';
        else
            maxChar = '1';

        if (uiState.editState.valueStr[4] == '0')
            minChar = '1';
    }
    else if (uiState.editState.cursorPos == 4)
    {
        if (uiState.editState.valueStr[3] > '0')
            maxChar = '2';

        if (uiState.editState.valueStr[3] == '0')
            minChar = '1';
    }

    switch (uiButton)
    {
    case UI_BTN_LEFT:
        {
            if (uiState.editState.cursorPos > 0)
                uiState.editState.cursorPos--;

            if ((uiState.editState.cursorPos == 2) || (uiState.editState.cursorPos == 5))
                uiState.editState.cursorPos--;
        }
        break;

    case UI_BTN_RIGHT:
        {
            if (uiState.editState.cursorPos < 9)
                uiState.editState.cursorPos++;

            if ((uiState.editState.cursorPos == 2) || (uiState.editState.cursorPos == 5))
                uiState.editState.cursorPos++;
        }
        break;

    case UI_BTN_UP:
        {
            uiState.editState.valueStr[uiState.editState.cursorPos]++;
            if (uiState.editState.valueStr[uiState.editState.cursorPos] > maxChar)
                uiState.editState.valueStr[uiState.editState.cursorPos] = minChar;
        }
        break;

    case UI_BTN_DOWN:
        {
            uiState.editState.valueStr[uiState.editState.cursorPos]--;
            if (uiState.editState.valueStr[uiState.editState.cursorPos] < minChar)
                uiState.editState.valueStr[uiState.editState.cursorPos] = maxChar;
        }
        break;

    default:
        break;
    }

    UpdateEditButtons();
    Screen_ShowEdit();
}

void UI_SetEditDate(const char *title, unsigned int year, unsigned char month, unsigned char day)
{
    uiState.editState.cursorPos = 0;
    uiState.editState.editHandler = OnEditDate;
    uiState.editState.titleStr = (char *)title;
    sprintf(uiState.editState.valueStr, "%02d.%02d.%04d", day, month, year);

    UpdateEditButtons();

    Screen_ShowEdit();
}

//------------------------------------------------------------------------------
//  Vraci editovany datum.
//------------------------------------------------------------------------------
void UI_GetEditDate(unsigned int *year, unsigned char *month, unsigned char *day)
{
    *year = 1000 * (uiState.editState.valueStr[6] - '0') +
            100 * (uiState.editState.valueStr[7] - '0') +
            10 * (uiState.editState.valueStr[8] - '0') +
            (uiState.editState.valueStr[9] - '0');
    *month = 10 * (uiState.editState.valueStr[3] - '0') + (uiState.editState.valueStr[4] - '0');
    *day = 10 * (uiState.editState.valueStr[0] - '0') + (uiState.editState.valueStr[1] - '0');
}

//------------------------------------------------------------------------------
//  Nastaveni editace vyctoveho typu (postara se taky o editacni tlacitka a displej).
//------------------------------------------------------------------------------

static unsigned char GetEnumCursorPos(unsigned long value, const TEnumItem *items, unsigned char itemCount)
{
    for (unsigned char i = 0; i < itemCount; i++)
    {
        if (items[i].value == value)
            return i;
    }

    return itemCount - 1;
}

static const char* GetEnumText(unsigned char itemPos, TEnumItem *items)
{
    return UI_GetText(items[itemPos].textId);
}

static void OnEditEnum(TUIButton uiButton)
{
    switch (uiButton)
    {
    case UI_BTN_UP:
        {
            uiState.editState.cursorPos++;
            if (uiState.editState.cursorPos >= enumItemCount)
                uiState.editState.cursorPos = 0;
        }
        break;

    case UI_BTN_DOWN:
        {
            uiState.editState.cursorPos--;
            if (uiState.editState.cursorPos >= enumItemCount)
                uiState.editState.cursorPos = enumItemCount - 1;
        }
        break;

    default:
        break;
    }

    sprintf(uiState.editState.valueStr, "%s", GetEnumText(uiState.editState.cursorPos, enumItems));

    UpdateEditEnumButtons();
    Screen_ShowEditEnum();
}

void UI_SetEditEnum(const char *title, unsigned long value, const TEnumItem *items, unsigned char itemCount)
{
    uiState.editState.cursorPos = GetEnumCursorPos(value, items, itemCount);
    uiState.editState.editHandler = OnEditEnum;
    uiState.editState.titleStr = (char *)title;
    enumItems = (TEnumItem *)items;
    enumItemCount = itemCount;
    sprintf(uiState.editState.valueStr, "%s", GetEnumText(uiState.editState.cursorPos, items));

    UpdateEditEnumButtons();

    Screen_ShowEditEnum();
}

//------------------------------------------------------------------------------
//  Vraci editovanou hodnotu vyctoveho typu.
//------------------------------------------------------------------------------
unsigned long UI_GetEditEnum(void)
{
    return enumItems[uiState.editState.cursorPos].value;
}

//------------------------------------------------------------------------------
//  Nastaveni info (postara se taky o displej).
//------------------------------------------------------------------------------
void UI_SetInfo(const char *title, const char *value)
{
    uiState.infoState.titleStr = (char *)title;
    sprintf(uiState.infoState.valueStr, "%s", value);

    Screen_ShowInfo();
}
