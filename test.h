//******************************************************************************
//  Testy.
//*****************************************************************************/

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

#include "type.h"
#include "sensor_master.h"

// pocet testovacich prutoku
#define TEST_FLOW_COUNT     3

// Identifikatory dilcich testu hlavniho testu
typedef enum
{
    /*SUBTID_C1_REZISTANCE,
    SUBTID_C2_REZISTANCE,
    SUBTID_E_INSULATION,
    SUBTID_TEMP_SENSOR,
    SUBTID_C_INDUCTANCE,
    SUBTID_EXCITATION_LEVELS,*/
    SUBTID_Magx2Passwords,
    SUBTID_Magx2UnitNo,
    SUBTID_Magx2Firmware,
    SUBTID_Magx2Measurement,
    SUBTID_Magx2LowFlowCutoff,
    SUBTID_Magx2Excitation,
    SUBTID_Magx2CurrentLoop,
    SUBTID_Magx2FrequencyOutput,
    SUBTID_Magx2FlowSimulation,
    SUBTID_SensorUnitNo,
    SUBTID_SensorFirmware,
    SUBTID_SensorDiameter,
    SUBTID_SensorFlowRange,
    SUBTID_SensorErrorMin,
    SUBTID_SensorOkMin,
    SUBTID_SensorErrorCode,
    SUBTID_SensorTotals,
    SUBTID_SensorCalibration,
    SUBTID_SetTestMagx2Measurement,
    SUBTID_SetTestMagx2LowFlowCutoff,
    SUBTID_SetTestMagx2Excitation,
    SUBTID_SetTestMagx2CurrentLoop,
    SUBTID_SetTestMagx2FrequencyOutput,
    SUBTID_FREQUENCY,
    SUBTID_IOUT,
    SUBTID_SetOriginalMagx2Measurement,
    SUBTID_SetOriginalMagx2LowFlowCutoff,
    SUBTID_SetOriginalMagx2Excitation,
    SUBTID_SetOriginalMagx2CurrentLoop,
    SUBTID_SetOriginalMagx2FrequencyOutput,
    SUBTID_SetOriginalMagx2FlowSimulation,

    SUBTID_End

} TMainSubTestId;

// pocet dilcich testu hlavniho testu
#define MAIN_SUB_TEST_COUNT   (SUBTID_End)

// Stav testu
typedef enum
{
    TEST_STOPPED, // zastaven
    TEST_RUNNING, // bezi
    TEST_COMPLETE // kompletni

} TTestState;

// Vysledek testu
typedef enum
{
    TRES_OK,
    TRES_ERROR,
    TRES_TIMEOUT,
    TRES_UNKNOWN,
    TRES_FAIL,
    TRES_UNSUPPORTED,

} TTestResult;

// Typ testu
typedef enum
{
    // explicitne ukonceny prikazem zastaveni testu (pak stav TEST_STOPPED)
    TEST_TYPE_UNLIMITED,

    // automaticke ukonceni testu (pak stav TEST_COMPLETE)
    TEST_TYPE_LIMITED

} TTestType;

// Nastaveni proudove smycky
typedef struct
{
    unsigned long currentSet; // Setting - Signal
    unsigned long flowMin; // prutok
    unsigned long flowMax;
    unsigned long currentMin; // proud
    unsigned long currentMax;

} TCurrentLoopSetting;

// Nastaveni frekvencniho vystupu
typedef struct
{
    unsigned long frequencySet; // Setting - Signal
    unsigned long flowMin; // prutok
    unsigned long flowMax;
    unsigned long frequencyMin; // frekvence
    unsigned long frequencyMax;
    unsigned long dutyCycle;

} TFrequencyOutputSetting;

// Nastaveni simulace toku
typedef struct
{
    unsigned long demo;
    unsigned long simulatedFlow;

} TFlowSimulationSetting;

// Testovaci nastaveni
typedef struct
{
    // Stav mereni
    unsigned long measurement;

    // Stav potlaceni nizkeho prutoku
    unsigned long lowFlowCutoff;

    // Stav buzeni
    unsigned long excitation;

    // Nastaveni proudove smycky
    TCurrentLoopSetting currentLoopSetting;

    // Nastaveni frekvencniho vystupu
    TFrequencyOutputSetting frequencyOutputSetting;

    // Nastaveni simulace toku
    TFlowSimulationSetting flowSimulationSetting;

} TTestSetting;

// Data hlavniho testu
typedef struct
{
    // Vysledek testu
    TTestResult testResult;

    unsigned long date;
    unsigned long id;

    unsigned long magx2UnitNo;
    unsigned int magx2Firmware;

    unsigned long sensorUnitNo;
    unsigned int sensorFirmware;
    unsigned int diameter;
    unsigned int flowRange;
    unsigned long errorMin;
    unsigned long okMin;

    unsigned long errorCode;

    // errorCode test result
    TTestResult errorCodeTestResult;

    float total;
    float totalPlus;
    float totalMinus;
    float aux;

    unsigned long calibrationPoint1;
    unsigned long calibrationPoint2;
    unsigned long calibrationPoint3;
    unsigned long calibrationData1;
    unsigned long calibrationData2;
    unsigned long calibrationData3;

    // rezistance C1
    unsigned int c1Rezistance;

    // rezistance C2
    unsigned int c2Rezistance;

    // Electrodes insulation
    unsigned int eInsulation;

    // temp sensor
    unsigned int tempSensor;

    // coil inductance
    unsigned long cInductance;

    // excitation levels
    unsigned int L1;
    unsigned int L2;
    unsigned int L3;

    // testovaci prutoky
    unsigned int testFlows[TEST_FLOW_COUNT];

    // frequency
    unsigned int frequency[TEST_FLOW_COUNT];

    // frequency test result
    TTestResult frequencyTestResult;

    // Iout
    unsigned int iOut[TEST_FLOW_COUNT];

    // Iout test result
    TTestResult iOutTestResult;

    // puvodni nastaveni pred testem
    TTestSetting originalTestSetting;

} TMainTestData;

// Stav hlavniho testu
typedef struct
{
    // Stav testu
    TTestState testState;

    // Vysledek testu
    TTestResult testResult;

    // Pozadavek na ukonceni testu
    BOOL requestToStop;

    // text pripadne chyby
    char *errText;

    // aktualni dilci test
    TMainSubTestId subTestId;

    // aktualni prispevek k celkovemu trvani testu
    unsigned long actualProgressSteps;

    // celkovy prispevek trvani testu
    unsigned long allProgressSteps;

    // pokrok testu [%]
    unsigned char progressPercent;

    // posledni systemovy cas
    unsigned long lastSysTime;

    // Data
    TMainTestData data;

} TMainTestState;

// Stav testu s AD prevodnikem
typedef struct
{
    // Stav testu
    TTestState testState;

    // Typ testu
    TTestType testType;

    // posledni vzorek z AD
    unsigned int adLastSample;

    // suma vzorku z AD
    unsigned long adSampleSum;

    // pocet vzorku
    unsigned long adSampleCount;

    // prumer z mereneho poctu vzorku
    unsigned long adSampleAvg;

} TADTestState;

// Stav testu mereni indukcnosti civek
typedef struct
{
    // Stav testu
    TTestState testState;

    // Typ testu
    TTestType testType;

    // stav AD prevodniku
    unsigned char adState;

    // text pripadne chyby
    char *errText;

    // posledni vzorek z AD
    unsigned long adLastSample;

    // pocet vzorku
    unsigned char adSampleCount;

    // pocet mericich cyklu
    unsigned long measureCount;

    // zmerena hodnota indukcnosti [H]
    float inductanceInHenry;

} TCInductanceTestState;

// Stav AD vzorku
typedef struct
{
    // suma vzorku z AD
    unsigned long adSampleSum;

    // pocet vzorku
    unsigned long adSampleCount;

    // prumer z mereneho poctu vzorku
    unsigned long adSampleAvg;

} TADSampleState;

// Stav testu urovni buzeni
typedef struct
{
    // Stav testu
    TTestState testState;

    // Typ testu
    TTestType testType;

    // vzorky L1
    TADSampleState L1SampleState;

    // vzorky L2
    TADSampleState L2SampleState;

    // vzorky L3
    TADSampleState L3SampleState;

} TExciteInTestState;

// Stav testu mereni frekvence
typedef struct
{
    // Stav testu
    TTestState testState;

    // Typ testu
    TTestType testType;

    // Vysledek testu
    TTestResult testResult;

    // predchozi snimek Timeru po hrane signalu
    unsigned long timerSnapOld;

    // novy snimek Timeru po hrane signalu
    unsigned long timerSnapNew;

    // suma casu mezi hranami signalu
    float timeSum;

    // pocet casu mezi hranami signalu
    unsigned long timeCount;

    // prumer casu mezi hranami signalu
    float timeAvg;

    // systemovy cas posledni udalosti
    unsigned long lastEventSysTime;

    // aktualni index testovaciho prutoku
    unsigned char testFlowIndex;

    // pocet timeoutu zapisu
    unsigned char timeoutCount;

    // frekvence
    unsigned long frequency[TEST_FLOW_COUNT];

} TFrequencyTestState;

// Stav testu mereni Iout
typedef struct
{
    // Stav testu
    TTestState testState;

    // Typ testu
    TTestType testType;

    // Vysledek testu
    TTestResult testResult;

    // text pripadne chyby
    char *errText;

    // posledni vzorek z AD
    unsigned int adLastSample;

    // pocet vzorku
    unsigned char adSampleCount;

    // pocet mericich cyklu
    unsigned long measureCount;

    // systemovy cas
    unsigned long sysTime;

    // aktualni index testovaciho prutoku
    unsigned char testFlowIndex;

    // pocet timeoutu zapisu
    unsigned char timeoutCount;

    // zmerena hodnota proudu [A]
    float currentInAmpere[TEST_FLOW_COUNT];

} TIoutTestState;

// Stav Modbus testu
typedef struct
{
    // pocet odeslanych ramcu
    unsigned long txFrameCount;

    // pocet prijatych ramcu
    unsigned long rxFrameCount;

    // pocet timeoutu prijmu
    unsigned long rxTimeoutCount;

    // pocet prijatych bytu
    unsigned long rxByteCount;

    // Modbus adresa
    unsigned int address;

    // data
    unsigned long data;

} TModbusTestState;

// Stav testu ADP5063
typedef struct
{
    // pocet chyb cteni/zapisu
    unsigned long errCount;

    // data registru Charger Status 1
    unsigned char regData_ChargerStatus1;

    // data registru Charger Status 2
    unsigned char regData_ChargerStatus2;

    // stav SYS_EN
    unsigned char sysEnLevel;

} TADP5063TestState;

// Stav testu vycteni hodnoty pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // vyctena hodnota
    unsigned long readValue;

    // pocet timeoutu cteni
    unsigned char timeoutCount;

} TReadValueCommTestState;

// Stav testu vycteni/zapisu hodnoty pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // vyctena/zapisovana hodnota
    unsigned long value;

    // pocet timeoutu cteni/zapisu
    unsigned char timeoutCount;

} TValueCommTestState;

// Stav testu zapisu hodnoty pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // pocet timeoutu zapisu
    unsigned char timeoutCount;

} TWriteValueCommTestState;

// Stav testu vycteni hodnot totalizeru pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // vyctena hodnota
    TSensorTotals readTotals;

    // pocet timeoutu cteni
    unsigned char timeoutCount;

} TReadTotalsCommTestState;

// Stav testu vycteni hodnot kalibrace pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // vyctena hodnota
    TSensorCalibration readCalibration;

    // pocet timeoutu cteni
    unsigned char timeoutCount;

} TReadCalibrationCommTestState;

// Stav testu vycteni/zapisu hodnot nastaveni proudove smycky pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // hodnota
    TCurrentLoopSetting setting;

    // pocet timeoutu cteni/zapisu
    unsigned char timeoutCount;

    // systemovy cas
    unsigned long sysTime;

} TCurrentLoopSettingCommTestState;

// Stav testu vycteni/zapisu hodnot nastaveni frekvencniho vystupu pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // hodnota
    TFrequencyOutputSetting setting;

    // pocet timeoutu cteni/zapisu
    unsigned char timeoutCount;

    // systemovy cas
    unsigned long sysTime;

} TFrequencyOutputSettingCommTestState;

// Stav testu vycteni/zapisu hodnot nastaveni simulovaneho toku pres komunikacni port
typedef struct
{
    // Stav testu
    TTestState testState;

    // hodnota
    TFlowSimulationSetting setting;

    // pocet timeoutu cteni/zapisu
    unsigned char timeoutCount;

} TFlowSimulationSettingCommTestState;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void Test_Init(void);

//------------------------------------------------------------------------------
//  Spusteni hlavniho testu.
//------------------------------------------------------------------------------
void Test_Main_Start(void);

//------------------------------------------------------------------------------
//  Zadost o zastaveni hlavniho testu.
//------------------------------------------------------------------------------
void Test_Main_WantStop(void);

//------------------------------------------------------------------------------
//  Vrati stav hlavniho testu.
//------------------------------------------------------------------------------
const TMainTestState* Test_Main_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Meas2 C1.
//------------------------------------------------------------------------------
void Test_Meas2C1_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu Meas2 C1.
//------------------------------------------------------------------------------
void Test_Meas2C1_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Meas2 C1.
//------------------------------------------------------------------------------
const TADTestState* Test_Meas2C1_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Meas2 C2.
//------------------------------------------------------------------------------
void Test_Meas2C2_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu Meas2 C2.
//------------------------------------------------------------------------------
void Test_Meas2C2_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Meas2 C2.
//------------------------------------------------------------------------------
const TADTestState* Test_Meas2C2_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Meas1 E1.
//------------------------------------------------------------------------------
void Test_Meas1E1_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu Meas1 E1.
//------------------------------------------------------------------------------
void Test_Meas1E1_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Meas1 E1.
//------------------------------------------------------------------------------
const TADTestState* Test_Meas1E1_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Meas1 Temp.
//------------------------------------------------------------------------------
void Test_Meas1Temp_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu Meas1 Temp.
//------------------------------------------------------------------------------
void Test_Meas1Temp_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Meas1 Temp.
//------------------------------------------------------------------------------
const TADTestState* Test_Meas1Temp_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu mereni indukcnosti civek.
//------------------------------------------------------------------------------
void Test_CInductance_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu mereni indukcnosti civek.
//------------------------------------------------------------------------------
void Test_CInductance_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu mereni indukcnosti civek.
//------------------------------------------------------------------------------
const TCInductanceTestState* Test_CInductance_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu mereni urovni buzeni.
//------------------------------------------------------------------------------
void Test_ExcitationIn_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu mereni urovni buzeni.
//------------------------------------------------------------------------------
void Test_ExcitationIn_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu mereni urovni buzeni.
//------------------------------------------------------------------------------
const TExciteInTestState* Test_ExcitationIn_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu mereni frekvence.
//------------------------------------------------------------------------------
void Test_Frequency_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu mereni frekvence.
//------------------------------------------------------------------------------
void Test_Frequency_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu mereni frekvence.
//------------------------------------------------------------------------------
const TFrequencyTestState* Test_Frequency_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu mereni Iout.
//------------------------------------------------------------------------------
void Test_Iout_Start(TTestType testType);

//------------------------------------------------------------------------------
//  Zastaveni testu mereni Iout.
//------------------------------------------------------------------------------
void Test_Iout_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu mereni Iout.
//------------------------------------------------------------------------------
const TIoutTestState* Test_Iout_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu MagX2 komunikace.
//------------------------------------------------------------------------------
void Test_MagX2Comm_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni testu MagX2 komunikace.
//------------------------------------------------------------------------------
void Test_MagX2Comm_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu MagX2 komunikace.
//------------------------------------------------------------------------------
const TModbusTestState* Test_MagX2Comm_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Sensor Slave komunikace.
//------------------------------------------------------------------------------
void Test_SensorSlave_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni testu Sensor Slave komunikace.
//------------------------------------------------------------------------------
void Test_SensorSlave_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Sensor Slave komunikace.
//------------------------------------------------------------------------------
const TModbusTestState* Test_SensorSlave_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni testu Sensor Master komunikace.
//------------------------------------------------------------------------------
void Test_SensorMaster_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni testu Sensor Master komunikace.
//------------------------------------------------------------------------------
void Test_SensorMaster_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav testu Sensor Master komunikace.
//------------------------------------------------------------------------------
const TModbusTestState* Test_SensorMaster_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni Modbus testu.
//------------------------------------------------------------------------------
void Test_Modbus_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Modbus testu.
//------------------------------------------------------------------------------
void Test_Modbus_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Modbus testu.
//------------------------------------------------------------------------------
const TModbusTestState* Test_Modbus_GetState(void);

//------------------------------------------------------------------------------
//  Spusteni ADP5063 testu.
//------------------------------------------------------------------------------
void Test_ADP5063_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni ADP5063 testu.
//------------------------------------------------------------------------------
void Test_ADP5063_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav ADP5063 testu.
//------------------------------------------------------------------------------
const TADP5063TestState* Test_ADP5063_GetState(void);

//------------------------------------------------------------------------------
//  Zapis hesel magx2.
//------------------------------------------------------------------------------
void Test_WriteMagx2Passwords_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Zapisu hesel magx2.
//------------------------------------------------------------------------------
void Test_WriteMagx2Passwords_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Zapisu hesel magx2.
//------------------------------------------------------------------------------
const TWriteValueCommTestState* Test_WriteMagx2Passwords_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni magx2UnitNo.
//------------------------------------------------------------------------------
void Test_ReadMagx2UnitNo_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni magx2UnitNo.
//------------------------------------------------------------------------------
void Test_ReadMagx2UnitNo_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni magx2UnitNo.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadMagx2UnitNo_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni magx2Firmware.
//------------------------------------------------------------------------------
void Test_ReadMagx2Firmware_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni magx2Firmware.
//------------------------------------------------------------------------------
void Test_ReadMagx2Firmware_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni magx2Firmware.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadMagx2Firmware_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu mereni.
//------------------------------------------------------------------------------
void Test_ReadMagx2Measurement_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni stavu mereni.
//------------------------------------------------------------------------------
void Test_ReadMagx2Measurement_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni stavu mereni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_ReadMagx2Measurement_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_ReadMagx2LowFlowCutoff_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_ReadMagx2LowFlowCutoff_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_ReadMagx2LowFlowCutoff_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni stavu buzeni.
//------------------------------------------------------------------------------
void Test_ReadMagx2Excitation_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni stavu buzeni.
//------------------------------------------------------------------------------
void Test_ReadMagx2Excitation_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni stavu buzeni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_ReadMagx2Excitation_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni proudove smycky.
//------------------------------------------------------------------------------
void Test_ReadMagx2CurrentLoopSetting_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni proudove smycky.
//------------------------------------------------------------------------------
void Test_ReadMagx2CurrentLoopSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni proudove smycky.
//------------------------------------------------------------------------------
const TCurrentLoopSettingCommTestState* Test_ReadMagx2CurrentLoopSetting_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_ReadMagx2FrequencyOutputSetting_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_ReadMagx2FrequencyOutputSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni frekvencniho vystupu.
//------------------------------------------------------------------------------
const TFrequencyOutputSettingCommTestState* Test_ReadMagx2FrequencyOutputSetting_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni nastaveni simulovaneho toku.
//------------------------------------------------------------------------------
void Test_ReadMagx2FlowSimulationSetting_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni nastaveni simulovaneho toku.
//------------------------------------------------------------------------------
void Test_ReadMagx2FlowSimulationSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni nastaveni simulovaneho toku.
//------------------------------------------------------------------------------
const TFlowSimulationSettingCommTestState* Test_ReadMagx2FlowSimulationSetting_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni sensorUnitNo.
//------------------------------------------------------------------------------
void Test_ReadSensorUnitNo_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni sensorUnitNo.
//------------------------------------------------------------------------------
void Test_ReadSensorUnitNo_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni sensorUnitNo.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorUnitNo_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni sensorFirmware.
//------------------------------------------------------------------------------
void Test_ReadSensorFirmware_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni sensorFirmware.
//------------------------------------------------------------------------------
void Test_ReadSensorFirmware_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni sensorFirmware.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorFirmware_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni diameter.
//------------------------------------------------------------------------------
void Test_ReadSensorDiameter_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni diameter.
//------------------------------------------------------------------------------
void Test_ReadSensorDiameter_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni diameter.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorDiameter_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni flowRange.
//------------------------------------------------------------------------------
void Test_ReadSensorFlowRange_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni flowRange.
//------------------------------------------------------------------------------
void Test_ReadSensorFlowRange_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni flowRange.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorFlowRange_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni errorMin.
//------------------------------------------------------------------------------
void Test_ReadSensorErrorMin_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni errorMin.
//------------------------------------------------------------------------------
void Test_ReadSensorErrorMin_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni errorMin.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorErrorMin_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni okMin.
//------------------------------------------------------------------------------
void Test_ReadSensorOkMin_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni okMin.
//------------------------------------------------------------------------------
void Test_ReadSensorOkMin_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni okMin.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorOkMin_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni errorCode.
//------------------------------------------------------------------------------
void Test_ReadSensorErrorCode_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni errorCode.
//------------------------------------------------------------------------------
void Test_ReadSensorErrorCode_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni errorCode.
//------------------------------------------------------------------------------
const TReadValueCommTestState* Test_ReadSensorErrorCode_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni totals.
//------------------------------------------------------------------------------
void Test_ReadSensorTotals_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni totals.
//------------------------------------------------------------------------------
void Test_ReadSensorTotals_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni totals.
//------------------------------------------------------------------------------
const TReadTotalsCommTestState* Test_ReadSensorTotals_GetState(void);

//------------------------------------------------------------------------------
//  Vycteni calibration.
//------------------------------------------------------------------------------
void Test_ReadSensorCalibration_Start(void);

//------------------------------------------------------------------------------
//  Zastaveni Vycteni calibration.
//------------------------------------------------------------------------------
void Test_ReadSensorCalibration_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Vycteni calibration.
//------------------------------------------------------------------------------
const TReadCalibrationCommTestState* Test_ReadSensorCalibration_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu mereni.
//------------------------------------------------------------------------------
void Test_SetTestMagx2Measurement_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni testovacich parametru stavu mereni.
//------------------------------------------------------------------------------
void Test_SetTestMagx2Measurement_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni testovacich parametru stavu mereni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetTestMagx2Measurement_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_SetTestMagx2LowFlowCutoff_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni testovacich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_SetTestMagx2LowFlowCutoff_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni testovacich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetTestMagx2LowFlowCutoff_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru stavu buzeni.
//------------------------------------------------------------------------------
void Test_SetTestMagx2Excitation_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni testovacich parametru stavu buzeni.
//------------------------------------------------------------------------------
void Test_SetTestMagx2Excitation_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni testovacich parametru stavu buzeni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetTestMagx2Excitation_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru proudove smycky.
//------------------------------------------------------------------------------
void Test_SetTestMagx2CurrentLoopSetting_Start(const TCurrentLoopSetting *setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni testovacich parametru proudove smycky.
//------------------------------------------------------------------------------
void Test_SetTestMagx2CurrentLoopSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni testovacich parametru proudove smycky.
//------------------------------------------------------------------------------
const TCurrentLoopSettingCommTestState* Test_SetTestMagx2CurrentLoopSetting_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni testovacich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_SetTestMagx2FrequencyOutputSetting_Start(const TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni testovacich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_SetTestMagx2FrequencyOutputSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni testovacich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
const TFrequencyOutputSettingCommTestState* Test_SetTestMagx2FrequencyOutputSetting_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu mereni.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2Measurement_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru stavu mereni.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2Measurement_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru stavu mereni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetOriginalMagx2Measurement_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2LowFlowCutoff_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2LowFlowCutoff_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru stavu potlaceni nizkeho prutoku.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetOriginalMagx2LowFlowCutoff_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru stavu buzeni.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2Excitation_Start(unsigned long setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru stavu buzeni.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2Excitation_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru stavu buzeni.
//------------------------------------------------------------------------------
const TValueCommTestState* Test_SetOriginalMagx2Excitation_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru proudove smycky.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2CurrentLoopSetting_Start(const TCurrentLoopSetting *setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru proudove smycky.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2CurrentLoopSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru proudove smycky.
//------------------------------------------------------------------------------
const TCurrentLoopSettingCommTestState* Test_SetOriginalMagx2CurrentLoopSetting_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2FrequencyOutputSetting_Start(const TFrequencyOutputSetting *setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2FrequencyOutputSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru frekvencniho vystupu.
//------------------------------------------------------------------------------
const TFrequencyOutputSettingCommTestState* Test_SetOriginalMagx2FrequencyOutputSetting_GetState(void);

//------------------------------------------------------------------------------
//  Nastaveni puvodnich parametru simulovaneho toku.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2FlowSimulationSetting_Start(const TFlowSimulationSetting *setting);

//------------------------------------------------------------------------------
//  Zastaveni Nastaveni puvodnich parametru simulovaneho toku.
//------------------------------------------------------------------------------
void Test_SetOriginalMagx2FlowSimulationSetting_Stop(void);

//------------------------------------------------------------------------------
//  Vrati stav Nastaveni puvodnich parametru simulovaneho toku.
//------------------------------------------------------------------------------
const TFlowSimulationSettingCommTestState* Test_SetOriginalMagx2FlowSimulationSetting_GetState(void);

//------------------------------------------------------------------------------
//  Vrati text vysledku testu.
//------------------------------------------------------------------------------
const char* Test_GetResultText(TTestResult testResult);

#endif // TEST_H_INCLUDED
