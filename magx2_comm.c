//******************************************************************************
//  Komunikace s MagX2 - VeriMag je v pozici mastera na seriove sbernici.
//*****************************************************************************/

#include "test.h"
#include "sensor_master.h"
#include "magx2_comm.h"
#include "LPC23xx.h"
#include "target.h"
#include "type.h"
#include "rtc.h"
#include "sensor.h"
#include "mbcrc.h"

#define BAUD_RATE           19200   //baud rate
#define RCV_BUFFER_LENGTH   80     //delka prijimaciho buferu
#define RCV_TIMEOUT         2      //timeout prijmu [s]

///modbus adresy
#define MAGX2_MODBUS_HESLA                          (2-1)
#define MAGX2_MODBUS_UNIT_NO                        (1000-1)
#define MAGX2_MODBUS_ERROR_MIN                      (1004-1)
#define MAGX2_MODBUS_OK_MIN                         (1006-1)
#define MAGX2_MODBUS_FIRM_WARE                      (1012-1)
#define MAGX2_MODBUS_CURRENT_LOOP                   (2010-1)
#define MAGX2_MODBUS_CURRENT_LOOP_SET               (2010-1)
#define MAGX2_MODBUS_CURRENT_LOOP_MINMAX            (2012-1)
#define MAGX2_MODBUS_FREQUENCY_OUTPUT               (2060-1)
#define MAGX2_MODBUS_FREQUENCY_OUTPUT_SET           (2060-1)
#define MAGX2_MODBUS_FREQUENCY_OUTPUT_MINMAX        (2062-1)
#define MAGX2_MODBUS_FREQUENCY_OUTPUT_DUTYCYCLE     (2070-1)
#define MAGX2_MODBUS_FLOW_SIMULATION                (3010-1)
#define MAGX2_MODBUS_FLOW_SIMULATION_DEMO           (3010-1)
#define MAGX2_MODBUS_FLOW_SIMULATION_FLOW           (3012-1)
#define MAGX2_MODBUS_MEASUREMENT_STATE              (3016-1)
#define MAGX2_MODBUS_LOW_FLOW_CUTOFF                (3030-1)
#define MAGX2_MODBUS_EXCITATION                     (4024-1)

// Fw module state
static struct
{
    // stav komunikace
    TCommStatus commStatus;

    // prijimaci bufer
    unsigned char rcvBuffer[RCV_BUFFER_LENGTH];

    // pocet prijatych bytu v prijimacim buferu
    unsigned char rcvBufferByteCount;

    // prijimaci casovac
    unsigned long rcvTimer;

    // funkce, ktera je volana po prijmu odpovedi na dotaz
    TMagX2CommDataHandler magX2CommDataHandler;

} state;

//------------------------------------------------------------------------------
//  Start prijimaciho casovace
//------------------------------------------------------------------------------
static void StartRcvTimer(void)
{
    state.rcvTimer = RTC_GetSysSeconds();
}

//------------------------------------------------------------------------------
//  Zjisteni prijimaciho timeoutu
//------------------------------------------------------------------------------
static unsigned char IsRcvTimeout(void)
{
    return RTC_IsSysSecondTimeout(state.rcvTimer, RCV_TIMEOUT);
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prijmu
//------------------------------------------------------------------------------
static void ProcessRcvTimeout(void)
{
    state.commStatus = CMSTA_NONE;
    if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
    {
        TCommData commData;
        commData.commResult = CRES_TMR;
        commData.buffer = (unsigned char *)0;
        commData.bufferSize = 0;

        state.magX2CommDataHandler(&commData);
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
static void ProcessRcvData(void)
{
    if (state.rcvBufferByteCount < 2)
        return;

    switch (state.rcvBuffer[1])
    {
        case WRITE_MULTIPLE_REGISTER:
            if (state.rcvBufferByteCount >= 8)
            {
                state.commStatus = CMSTA_NONE;
                if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
                {
                    TCommData commData;
                    commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                    commData.buffer = (unsigned char *)0;
                    commData.bufferSize = 0;

                    state.magX2CommDataHandler(&commData);
                }
            }
            break;

        case READ_HOLDING_REGISTER:
            if (state.rcvBufferByteCount >= 3)
            {
                if (state.rcvBufferByteCount == (state.rcvBuffer[2] + 5))
                {
                    state.commStatus = CMSTA_NONE;
                    if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
                    {
                        TCommData commData;
                        commData.commResult = (usMBCRC16(state.rcvBuffer, state.rcvBufferByteCount) == 0) ? CRES_OK : CRES_ERR;
                        commData.buffer = &state.rcvBuffer[3];
                        commData.bufferSize = state.rcvBuffer[2];

                        state.magX2CommDataHandler(&commData);
                    }
                }
            }
            break;

        default:
            break;
    }
}

//------------------------------------------------------------------------------
//  Vyzvednuti bytu z UART0
//------------------------------------------------------------------------------
static unsigned char ReadUartByte(void)
{
    return U0RBR;
}

//------------------------------------------------------------------------------
//  Obsluha preruseni od UART0
//------------------------------------------------------------------------------
static void OnRxUartHandler(void)
{
    unsigned char rxByte = ReadUartByte();

    if (state.commStatus == CMSTA_RX)
    {
        if (state.rcvBufferByteCount < RCV_BUFFER_LENGTH)
        {
            state.rcvBuffer[state.rcvBufferByteCount] = rxByte;
            state.rcvBufferByteCount++;
        }
    }

    VICVectAddr = 0;
}

//------------------------------------------------------------------------------
//  Odeslani dat na sbernici
//------------------------------------------------------------------------------
static void SendData(const unsigned char *dataBuffer, unsigned int dataCount, TMagX2CommDataHandler handler)
{
    state.commStatus = CMSTA_TX;

    while (dataCount != 0)
    {
        while (!(U0LSR & (1<<5))); // Wait until Transmitter Holding Register is not Empty
        U0THR = *dataBuffer;
        dataBuffer++;
        dataCount--;
    }

    while (!(U0LSR & (1<<6)));

    state.commStatus = CMSTA_RX;
    state.rcvBufferByteCount = 0;
    state.magX2CommDataHandler = handler;

    StartRcvTimer();
}

//------------------------------------------------------------------------------
//  Cteni holding DWORDu MagX2.
//------------------------------------------------------------------------------
static void ReadHoldingDWord(unsigned int address, TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(address >> 8);
    buff[3]=(unsigned char)(address);

    buff[4]=0x00;
    buff[5]=0x02;       //zadost musi byt delitelna dvema

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Inicializace hw komunikacniho rozhrani.
//------------------------------------------------------------------------------
static void MagX2CommHwInit(void)
{
    unsigned long Fdiv;
    PCONP |= (1<<3);      //set power on UART0
    PINSEL0 |= (1<<4) | (1<<6);   //nastaveni TxD0 a RxD0 na piny 0.2,0.3
    U0LCR = 0x83;   //8bits,no Parity,1 stop, !!! DLAB=0 !!!!

    Fdiv = ( Fpclk / 16 ) / BAUD_RATE;	//baud rate
    U0FCR = 0x07;   //FIFO Enable and restart tx and rx

    U0DLM = Fdiv / 256;
    U0DLL = Fdiv % 256;

    // preruseni
    VICVectAddr6 = (unsigned long) OnRxUartHandler;   //set interrupt vector
    VICIntEnable |= (1<<6);            // UART0 enable
    U0LCR = 0x03;                       //DLAB=0
    U0IER = 1;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void MagX2Comm_Init(void)
{
    state.commStatus = CMSTA_NONE;
    state.rcvBufferByteCount = 0;
    state.magX2CommDataHandler = NO_MAGX2_COMM_DATA_HANDLER;
    MagX2CommHwInit();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MagX2Comm_Task(void)
{
    if (state.commStatus == CMSTA_RX)
    {
        if (IsRcvTimeout())
            ProcessRcvTimeout();
        else
            ProcessRcvData();
    }
}

//------------------------------------------------------------------------------
//  Vrati stav komunikace.
//------------------------------------------------------------------------------
TCommStatus MagX2Comm_GetCommStatus(void)
{
    return state.commStatus;
}

//------------------------------------------------------------------------------
//  Vrati ramec v prijimacim buferu.
//------------------------------------------------------------------------------
const unsigned char* MagX2Comm_GetRcvFrame(void)
{
    return state.rcvBuffer;
}

//------------------------------------------------------------------------------
//  Vrati pocet bytu ramce v prijimacim buferu.
//------------------------------------------------------------------------------
unsigned int MagX2Comm_GetRcvFrameByteCount(void)
{
    return state.rcvBufferByteCount;
}

//------------------------------------------------------------------------------
//  Stop prijmu.
//------------------------------------------------------------------------------
void MagX2Comm_StopReceiving(void)
{
    state.commStatus = CMSTA_NONE;
}

//------------------------------------------------------------------------------
//  Parsing typu unsigned long MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUlong(const unsigned char *buffer)
{
    unsigned char pos=0;
    unsigned long help=0;

    help=(unsigned long)buffer[pos++]<<8;
    help+=(unsigned long)buffer[pos++];
    help+=(unsigned long)buffer[pos++]<<24;
    help+=(unsigned long)buffer[pos++]<<16;

    return help;
}

//------------------------------------------------------------------------------
//  Zapis hesel MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WritePasswords(TMagX2CommDataHandler handler)
{
    unsigned char buff[21];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)( MODBUS_HESLA >> 8);
    buff[3]=(unsigned char)( MODBUS_HESLA);

    buff[4]=0x00;   //pocet registru
    buff[5]=6;

    buff[6]=12;   //pocet bytu

    unsigned long heslo=1111;        //user

    buff[7]=(unsigned char)(heslo >> 8);
    buff[8]=(unsigned char)(heslo);
    buff[9]=(unsigned char)(heslo >> 24);
    buff[10]=(unsigned char)(heslo >> 16);

    heslo=242628;        //service

    buff[11]=(unsigned char)(heslo >> 8);
    buff[12]=(unsigned char)(heslo);
    buff[13]=(unsigned char)(heslo >> 24);
    buff[14]=(unsigned char)(heslo >> 16);

    heslo=82624224;        //factory

    buff[15]=(unsigned char)(heslo >> 8);
    buff[16]=(unsigned char)(heslo);
    buff[17]=(unsigned char)(heslo >> 24);
    buff[18]=(unsigned char)(heslo >> 16);

    unsigned int crcecko=usMBCRC16(buff,19);;   //kontrolni soucet
    buff[19]=(unsigned char)(crcecko);
    buff[20]=(unsigned char)(crcecko>>8);

    SendData(buff, 21, handler);
}

//------------------------------------------------------------------------------
//  Cteni firmware MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFirmware(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_FIRM_WARE, handler);
}

//------------------------------------------------------------------------------
//  Parsing firmware MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseFirmware(const unsigned char *buffer)
{
    unsigned long fw_version = 0; // fw verze0
    fw_version=(unsigned long)buffer[1];
    fw_version+=(unsigned int)buffer[0]<<8;
    return fw_version;
}

//------------------------------------------------------------------------------
//  Cteni cisla MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadUnitNo(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_UNIT_NO, handler);
}

//------------------------------------------------------------------------------
//  Parsing cisla MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUnitNo(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni chybovych minut MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadErrorMin(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_ERROR_MIN, handler);
}

//------------------------------------------------------------------------------
//  Parsing chybovych minut MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseErrorMin(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni ok minut MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadOkMin(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_OK_MIN, handler);
}

//------------------------------------------------------------------------------
//  Parsing ok minut MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseOkMin(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni stavu mereni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadMeasurement(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEASUREMENT_STATE, handler);
}

//------------------------------------------------------------------------------
//  Parsing stavu mereni MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseMeasurement(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Zapis parametru stavu mereni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteMeasurement(TMagX2CommDataHandler handler, unsigned long setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_MEASUREMENT_STATE >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_MEASUREMENT_STATE);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Cteni stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadLowFlowCutoff(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_LOW_FLOW_CUTOFF, handler);
}

//------------------------------------------------------------------------------
//  Parsing stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseLowFlowCutoff(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Zapis parametru stavu potlaceni nizkeho prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteLowFlowCutoff(TMagX2CommDataHandler handler, unsigned long setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_LOW_FLOW_CUTOFF >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_LOW_FLOW_CUTOFF);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Cteni stavu buzeni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExcitation(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_EXCITATION, handler);
}

//------------------------------------------------------------------------------
//  Parsing stavu buzeni MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseExcitation(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Zapis parametru stavu buzeni MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WriteExcitation(TMagX2CommDataHandler handler, unsigned long setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_EXCITATION >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_EXCITATION);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Cteni nastaveni proudove smycky MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadCurrentLoopSetting(TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP);

    buff[4]=0x00;
    buff[5]=10;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing nastaveni proudove smycky MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseCurrentLoopSetting(const unsigned char *buffer, TCurrentLoopSetting *setting)
{
    unsigned char pos=0;
    unsigned long data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->currentSet = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->flowMin = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->flowMax = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->currentMin = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->currentMax = data;
}

//------------------------------------------------------------------------------
//  Zapis parametru proudove smycky MagX2 - Set.
//------------------------------------------------------------------------------
void MagX2Comm_WriteCurrentLoopSetting_Set(TMagX2CommDataHandler handler, const TCurrentLoopSetting *setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP_SET >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP_SET);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting->currentSet;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Zapis parametru proudove smycky MagX2 - MinMax.
//------------------------------------------------------------------------------
void MagX2Comm_WriteCurrentLoopSetting_MinMax(TMagX2CommDataHandler handler, const TCurrentLoopSetting *setting)
{
    unsigned char buff[25];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP_MINMAX >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_CURRENT_LOOP_MINMAX);

    buff[4]=0x00;   //pocet registru
    buff[5]=8;

    buff[6]=16;   //pocet bytu

    unsigned long data=setting->flowMin;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    data=setting->flowMax;

    buff[11]=(unsigned char)(data >> 8);
    buff[12]=(unsigned char)(data);
    buff[13]=(unsigned char)(data >> 24);
    buff[14]=(unsigned char)(data >> 16);

    data=setting->currentMin;

    buff[15]=(unsigned char)(data >> 8);
    buff[16]=(unsigned char)(data);
    buff[17]=(unsigned char)(data >> 24);
    buff[18]=(unsigned char)(data >> 16);

    data=setting->currentMax;

    buff[19]=(unsigned char)(data >> 8);
    buff[20]=(unsigned char)(data);
    buff[21]=(unsigned char)(data >> 24);
    buff[22]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,23);;   //kontrolni soucet
    buff[23]=(unsigned char)(crcecko);
    buff[24]=(unsigned char)(crcecko>>8);

    SendData(buff, 25, handler);
}

//------------------------------------------------------------------------------
//  Cteni nastaveni frekvencniho vystupu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFrequencyOutputSetting(TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT);

    buff[4]=0x00;
    buff[5]=12;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing nastaveni frekvencniho vystupu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFrequencyOutputSetting(const unsigned char *buffer, TFrequencyOutputSetting *setting)
{
    unsigned char pos=0;
    unsigned long data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->frequencySet = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->flowMin = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->flowMax = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->frequencyMin = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->frequencyMax = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->dutyCycle = data;
}

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - Set.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_Set(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_SET >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_SET);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting->frequencySet;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - MinMax.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_MinMax(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting)
{
    unsigned char buff[25];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_MINMAX >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_MINMAX);

    buff[4]=0x00;   //pocet registru
    buff[5]=8;

    buff[6]=16;   //pocet bytu

    unsigned long data=setting->flowMin;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    data=setting->flowMax;

    buff[11]=(unsigned char)(data >> 8);
    buff[12]=(unsigned char)(data);
    buff[13]=(unsigned char)(data >> 24);
    buff[14]=(unsigned char)(data >> 16);

    data=setting->frequencyMin;

    buff[15]=(unsigned char)(data >> 8);
    buff[16]=(unsigned char)(data);
    buff[17]=(unsigned char)(data >> 24);
    buff[18]=(unsigned char)(data >> 16);

    data=setting->frequencyMax;

    buff[19]=(unsigned char)(data >> 8);
    buff[20]=(unsigned char)(data);
    buff[21]=(unsigned char)(data >> 24);
    buff[22]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,23);   //kontrolni soucet
    buff[23]=(unsigned char)(crcecko);
    buff[24]=(unsigned char)(crcecko>>8);

    SendData(buff, 25, handler);
}

//------------------------------------------------------------------------------
//  Zapis parametru frekvencniho vystupu MagX2 - Duty cycle.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFrequencyOutputSetting_DutyCycle(TMagX2CommDataHandler handler, const TFrequencyOutputSetting *setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_DUTYCYCLE >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FREQUENCY_OUTPUT_DUTYCYCLE);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting->dutyCycle;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Cteni nastaveni simulovaneho toku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowSimulationSetting(TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION);

    buff[4]=0x00;
    buff[5]=4;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing nastaveni simulovaneho toku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFlowSimulationSetting(const unsigned char *buffer, TFlowSimulationSetting *setting)
{
    unsigned char pos=0;
    unsigned long data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->demo = data;

    data=0;
    data=(unsigned long)buffer[pos++]<<8;
    data+=(unsigned long)buffer[pos++];
    data+=(unsigned long)buffer[pos++]<<24;
    data+=(unsigned long)buffer[pos++]<<16;

    setting->simulatedFlow = data;
}

//------------------------------------------------------------------------------
//  Zapis parametru simulovaneho toku MagX2 - Demo.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFlowSimulationSetting_Demo(TMagX2CommDataHandler handler, const TFlowSimulationSetting *setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION_DEMO >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION_DEMO);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting->demo;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Zapis parametru simulovaneho toku MagX2 - Flow.
//------------------------------------------------------------------------------
void MagX2Comm_WriteFlowSimulationSetting_Flow(TMagX2CommDataHandler handler, const TFlowSimulationSetting *setting)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION_FLOW >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_FLOW_SIMULATION_FLOW);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long data=setting->simulatedFlow;

    buff[7]=(unsigned char)(data >> 8);
    buff[8]=(unsigned char)(data);
    buff[9]=(unsigned char)(data >> 24);
    buff[10]=(unsigned char)(data >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}
