/****************************************************************************
***  images.c    ************************************************************
*****************************************************************************
*  - Tento modul obsahuje obrazky                                           *
*                                                                           *
*                                                                           *
****************************************************************************/
#ifndef images
#define images

/* Offsety k obrazkum
*  - Offsety je potreba prepocitat, vzdy, pokud odebereme nejaky obrazek.
*  - offset = offset predchoziho + (sirka*vyska)/8 predchoziho + 2.  2=prvni dva byty udavajici rozmery
*/
/*                                //  res   | leng.=res/8+2 | offset next leng+predchozi
#define IMG_ARKON        (0)    // 132x64 |  1058         | 1058
#define IMG_TROJUHELNIK  (1058) //  80x40 |  402          | 1460
#define IMG_OK_RAMECEK   (1460) //  64x32 |  258          | 1718
#define IMG_ERR_RAMECEK  (1718) //  64x32 |  258          | 1976
#define IMG_TEMP         (1976) //  16x24 |   50          | 2026
#define IMG_CHACK_IN_WIN (2026) //  15x16 |   32          | 2058
#define IMG_CHACK        (2058) //  15x16 |   32          | 2090
#define IMG_CROSH_IN_WIN (2090) //  15x16 |   32          | 2122
#define IMG_CROSH        (2122) //  15x16 |   32          | 2154
*/
#define IMG_ARKON        (0)    // 128x64 |  1026         | 1026
#define IMG_TROJUHELNIK  (1026) //  80x40 |  402          | 1428
#define IMG_OK_RAMECEK   (1428) //  64x32 |  258          | 1686
#define IMG_ERR_RAMECEK  (1686) //  64x32 |  258          | 1944
#define IMG_TEMP         (1944) //  16x24 |   50          | 1994
#define IMG_CHACK_IN_WIN (1994) //  15x16 |   32          | 2026
#define IMG_CHACK        (2026) //  15x16 |   32          | 2058
#define IMG_CROSH_IN_WIN (2058) //  15x16 |   32          | 2090
#define IMG_CROSH        (2090) //  15x16 |   32          | 2122
#define IMG_KEY_HELP     (2122) // 128*64 |  1026         | 3148

extern const unsigned char P_Images[];

#endif //images
