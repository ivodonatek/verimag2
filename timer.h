
///timer1 jako timenout pro opakovany stitk klavesy
void init_timer1(void);
void disable_timer1(void);
void enable_timer1(void);
void timer1_overflow(void);
void init_timer2(void);
void disable_timer2(void);
void enable_timer2(void);
void timer2_overflow(void);
void init_timer3(void);
void enable_timer3(void);
void disable_timer3(void);
void timer3_overflow(void);
