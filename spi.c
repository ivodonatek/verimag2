#include "lpc23xx.h"
#include "spi.h"


void SPI_DisplayInit(void)
{
  PCONP |= (1 << 21);
  IODIR1 |= 0x03700000;
  IOSET1 |= 0x03700000;
  PINSEL3 |= 0x0003CF00; // configure SPI0 pins
  SSP0CR0  = 0x00C7; // CPHA=1, CPOL=1, master mode, MSB first, interrupt enabled
  SSP0CPSR = 0x2;
  SSP0CR1 = 0x0002;
}

void SPI_PulseInit(void) // pulse module
{
  PCONP |= (1 << 10);
  PINSEL0 |= 0x0008A000; // configure SPI0 pins
  SSP1CR0  = 0x0087; // CPHA=1, CPOL=0, master mode, MSB first, interrupt enabled
  SSP1CPSR = 30;  // delicka pro clock
  SSP1CR1 = 0x0002;
  IODIR0 &= ~0x100; // P0.8 set to input
  IOCLR0 = 0x100; // P0.8 set to pull down
  PINMODE0 |= 0x20000; // P0.8 set to high impedance
}

void SPI_DisplayTransmit(unsigned char cData)
{
  SSP0DR = 0x0000 + cData;
  while ((SSP0SR & 0x10)) ; // wait for end of transfer
}

void SPI_PulseTransmit(unsigned char cData) // pulse module
{
  SSP1DR = 0x0000 + cData;
  while ((SSP1SR & 0x10)) ; // wait for end of transfer
}
