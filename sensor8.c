#include "sensor.h"
#include "LPC23xx.h"
#include "rprintf.h"
#include "menu_tables.h"
#include "maine.h"
#include "display.h"
#include "menu.h"
#include "timer.h"
#include "rtc.h"
#include "modbus_bin.h"
#include "type.h"
#include "modbus.h"
#include "delay.h"
#include "mb.h"
#include "iout.h"
#include "mbcrc.h"
#include "uart.h"
#include "math.h"
#include "fw_check.h"

static unsigned long sensor_unit_no_read = 0; // vyctene seriove cislo senzoru
static BOOL backup_sensor_unit_no = FALSE; // true = porizovat zalohu serioveho cisla senzoru

volatile unsigned char flag_cekam_odpoved_od_sensor8=0;
extern float Convert_Flow(float in, unsigned char typ);

void used_sensor8()
{
  ///nastaveni sensoru na rychlost asi vetsi nez 9600
  flag_sensor7=0;
  flag_sensor8=1;
  RTC_init();
  RTC_set_interrupt();
  init_timer2();

  if( !inicializace_senzoru_senzor8() )
  {
    actual_error |= ERR_SENSOR;
  }

  backup_sensor_unit_no = TRUE;
  RAM_Write_Data_Long(sensor_unit_no_read, SENSOR_UNIT_NO);

  init_timer3();
  enable_timer3();


  ///*************************************************************************************
  ///                                                                                  ///
  ///                           HLAVNI SMYCKA                                          ///
  ///                                                                                  ///
  ///*************************************************************************************

  while(1)
  {
    FwCheck_Task(FWCHECK_Step); // dalsi krok testu firmware
    EventLogTask();

    if( !flag_cekam_odpoved_od_sensor8)     //pokud komunikuje sensor s transmittrem nekontroluje se poyadavaek od PC
      ( void )eMBPoll(  ); // modbus pool

    ///prevzeti actual error do erroru pro datalogger
    actual_error_datalogger |= actual_error;
    /// jednou za sekundu
    if (flag_RTC) // jednou za sekundu
    {
      flag_RTC = 0;
      co_sekunda_sensor8();
      write_to_RAM();

      if(backlight_second>0)
      {
        DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
        backlight_second--;
      }
      else
      {
        if ( !RAM_Read_Data_Long(BACKLIGHT) ) // pokud je nastaveno na 10 seconds tak se podsviceni vypne
          DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;
      }
    }

    if((flag_timeout_timer3)  ) //TIMER pro zadost na senzor
    {
      flag_timeout_timer3=0;

      if (pulse_version == PULSE_V2) // aktualizace 3x za sekundu
      {
        Send_Float(FLOW_FAST,g_measurement.actual);
        delay_1ms();
        Send_Long(ERR,(long)( actual_error ));
      }

      if (!(flag_cekam_odpoved_od_sensor8))
        aktualizace_sensor8();

      control(SHOW);     // prekresleni displeje
    }

    if( flag_timeout_timer2)
    {
      flag_timeout_timer2=0;
      flag_cekam_odpoved_od_sensor8=0;

      if( pocet_pokusu_send++>4)
      {
        pocet_pokusu_send=5;
        flag_aktivni_zadost=0;
        actual_error |= ERR_SENSOR;
      }
      else
      {
        switch( flag_modbus_ukon )
        {
            case DATA_ZE_SENZORU:
                //request_data_sensor8(); stejne si porad zada
            break;

            case ZAPIS_KALIBRACI:
                send_calibration_sensor8();
            break;

            case ZJISTI_KALIBRACI:
                recv_calibration_sensor8();
            break;

            case ZJISTI_STAV_BUZENI:
                sensor_stav_buzeni_sensor8();
            break;

            case ZAPNI_BUZENI:
                nastav_buzeni_sensor8();
            break;

            case ODESLI_HESLA:
                odesli_heslo_sensor8();
            break;

            case READ_ZERO_FLOW:
                recv_zero_constant_sensor8();
            break;

            case DELETE_VOLUME:
                delete_volume_sensor8();
            break;

            case DELETE_POSITIVE:
                delete_positive_sensor8();
            break;

            case DELETE_NEGATIVE:
                delete_negative_sensor8();
            break;

            case SEND_ZERO_FLOW:
                send_zero_flow_sensor8();
            break;

            case SEND_ZERO_ERASE:
                send_zero_erase_sensor8();
            break;

            case ZAPIS_ZERO_CONSTANT:
                ///send_zero_constant_sensor8(long konstanta);
            break;

            case CTENI_ZERO_CONSTANT:
                recv_zero_constant_sensor8();
            break;

            case SEND_UNIT_NO:
                send_unit_no_sensor8();

            case READ_UNIT_NO:
                recv_unit_no_sensor8();
            break;

            case READ_DIAMETER:
                recv_diameter_sensor8();
            break;

            case SEND_DIAMETER:
                send_diameter_sensor8();
            break;

            case READ_DEMO:
                recv_demo_sensor8();
            break;

            case SEND_DEMO:
                send_demo_sensor8();
            break;

            case READ_SIMULATED_FLOW:
                recv_simulated_flow_sensor8();
            break;

            case SEND_SIMULATED_FLOW:
                send_simulated_flow_sensor8();
            break;

            case DELETE_AUX:
                delete_aux_sensor8();
            break;

            case SEND_CLEAN_POWER:
                //send_cisteni_sensor8(SENSOR_OFF);
            break;

            case READ_CLEAN_POWER:
                recv_cisteni_sensor8();
            break;

            case SEND_FLOW_RANGE:
                send_flow_range_sensor8();
            break;

            case READ_FLOW_RANGE:
                recv_flow_range_sensor8();
            break;

            case SEND_AIR_CONSTANT:
                send_air_constant_sensor8();
            break;

            case READ_AIR_CONSTANT:
                recv_air_constant_sensor8();
            break;

            case SEND_AIR_DETECTOR:
                send_air_detector_sensor8();
            break;

            case READ_AIR_DETECTOR:
                recv_air_detector_sensor8();
            break;

            case SEND_MEASUREMENT:
                send_measure_state_sensor8();
            break;

            case READ_MEASUREMENT:
                recv_measure_state_sensor8();
            break;

            case SEND_AVERAGE_SAMPLES:
                send_average_samples_sensor8();
            break;

            case READ_AVERAGE_SAMPLES:
                recv_average_samples_sensor8();
            break;

            case SEND_LOW_FLOW_CUT:
                send_low_flow_cut_sensor8();
            break;

            case READ_LOW_FLOW_CUT:
                recv_low_flow_cut_sensor8();
            break;

            case SEND_INVERT_FLOW:
                send_invert_flow_sensor8();
            break;

            case READ_INVERT_FLOW:
                recv_invert_flow_sensor8();
            break;

            case READ_START_DELAY:
                recv_start_delay_sensor8();
            break;

            case SEND_START_DELAY:
                send_start_delay_sensor8();
            break;

            case SEND_TOTALS:
                send_totals_sensor8();
            break;

            default:

            break;
        }
      }
      ///znovu poslani zadosti
    }

    /// zmena nastaveni modbus
    if ((ChangeCommunicationParam)&&(modbus_transmitt_state == 1))
    {
      modbus_transmitt_state = 0;
      #ifdef  RTS_ENABLE
      //while (RTS_PORT&(1<<RTS_PIN))
      //while((IOPIN0&(1<<10))==(1<<10))
        asm volatile ("nop");//cekej na odvysilani cele zpravy
      #endif
      Modbus_init();
      ChangeCommunicationParam=false;
    }

    if(flag_receive)        //pokud se vyvolalo preruseni prijmu od sensoru 8
    {
        flag_receive=0;
        zpracovat_data_sensor8();
    }

    obsluha_tlacitek();
  }
}

void aktualizace_sensor8()
{
  ///zapis novych dat
  ///posun prvku v poli
  for(unsigned int j=SAMPLES_MAX-1;j>0;j--)
  {
    act_adc[j]=act_adc[j-1];
  }

  ///zapsani aktualniho prutoku na 0.pozici
  act_adc[0]=prutok;        //kdyz prijdou data tak se zapisi, kdyz byl timeout zapise se nula

  if( actual_error & ERR_SENSOR )
  {
    if( RAM_Read_Data_Long ( DEMO))     //neni demo
    {
      adc_average=0;
      g_measurement.actual=0;
    }
    else                                //je demo
      g_measurement.actual=((float)RAM_Read_Data_Long(SIMULATED_FLOW))/1000.0;
  }
  else
  {
    if(flag_start>0)            //10 sekund po startu se plni nulama
    {
        g_measurement.actual=0;
        flag_start--;
    }
    else
    {
        if( RAM_Read_Data_Long ( DEMO))     //neni demo
        {
            /// g_measurement.actual je naplneno v prijimani od modbusu
        }
        else                                //je demo
        {
            g_measurement.actual=((float)RAM_Read_Data_Long(SIMULATED_FLOW))/1000.0;
        }
    }
  }

  if( !flag_aktivni_zadost)
    request_data_sensor8();
}

void co_sekunda_sensor8()
{
  ///kontrola cyclovani totaliseru
  if( ++flag_totaliser_cycling >=3)
  {
    if( !(RAM_Read_Data_Long( TOTALIZER_CYCLING)) ) //pokud je aktivni cyklovacni totalizeru
    {
      ShiftForwardDispMeasurement();
    }
    flag_totaliser_cycling=0;//vynulovani pocitadla;
  }

  ///kontrola zapisu do flashky
  if( --flag_zapsat_flash == 0 )
  {
    flag_zapsat_flash=SEC_TO_WRITE_FLASH;
    write_data_to_flash();
  }

  ///kontrola cisteni
  if( ((--flag_cisteni_cas)==0) && (flag_cisteni>0) ) //jestlize je cas roven nule a je stale jeste zaple cisteni
  {
    flag_cisteni=0;
    RAM_Write_Data_Long(0,CLEAN_POWER);
    sensor_cisteni(SENSOR_OFF);
  }

  if( flag_start)
  {
    ///event error
    last_sending_error_gsm=actual_error;
    flag_start--;
  }

  // Lukas oprava
  ///kontrola error min,Ok min
  if(actual_error)
  {
    error_sec++;
    if (error_sec >= 60)
    {
      error_sec -= 60;
      error_min++;
    }
  }
  else
  {
    ok_sec++;
    if (ok_sec >= 60)
    {
      ok_sec -= 60;
      ok_min++;
    }
  }

  // aktualizace hodnot pro graf
  if (++flag_second_graph > 1)
  {
    flag_second_graph = 0;
    // posun cely graficky buffer doleva
    for(int i = 0; i < GRAPH_WIDTH-1; i++)
    {
      graphHistory[i].top = graphHistory[i+1].top;
      graphHistory[i].center = graphHistory[i+1].center;
      graphHistory[i].bottom = graphHistory[i+1].bottom;
    }
    // nacti novou hodnotu prutoku do grafu
    long long pomocna = 0;
    pomocna = Convert_Flow(g_measurement.actual,RAM_Read_Data_Long(UNIT_FLOW))*1000;
    int percent = pomocna / (RAM_Read_Data_Long(FLOW_RANGE) / 100.0);

    if(percent < 0)
      percent = 0;
    if(percent > 100)
      percent = 100;

    int pixels = 0.17 * percent;

    graphHistory[GRAPH_WIDTH-1].top = 0;
    graphHistory[GRAPH_WIDTH-1].center = 0;
    graphHistory[GRAPH_WIDTH-1].bottom = 4;

    if (pixels <= 5)
    {
      for(int i = 0; i < pixels; i++)
        graphHistory[GRAPH_WIDTH-1].bottom |= (1<<(i+3));
    }
    else if (pixels > 5 && pixels <= 13)
    {
      graphHistory[GRAPH_WIDTH-1].bottom = 252;
      for(int i = 0; i < pixels-5; i++)
        graphHistory[GRAPH_WIDTH-1].center |= (1<<i);
    }
    else if(pixels > 13)
    {
      graphHistory[GRAPH_WIDTH-1].bottom = 252;
      graphHistory[GRAPH_WIDTH-1].center = 255;
      for(int i = 0; i < pixels-13; i++)
        graphHistory[GRAPH_WIDTH-1].top |= (1<<i);
    }
  }

  ///zkouska jestli se ma zapsat do dataloggru
  if( ((--second_to_datalogger) == 0) && (flag_datalogger) )
  {
    write_datalogger();

    switch(RAM_Read_Data_Long(DATALOGGER_INTERVAL))
    {
        case 0: //off
            flag_datalogger=0;
            second_to_datalogger=0;
        break;
        case 1: //1 minuta
            flag_datalogger=1;
            second_to_datalogger=60;
        break;
        case 2: //5 minut
            flag_datalogger=1;
            second_to_datalogger=300;
        break;
        case 3: //10 minut
            flag_datalogger=1;
            second_to_datalogger=600;
        break;
        case 4: //15 minut
            flag_datalogger=1;
            second_to_datalogger=900;
        break;
        case 5: //30 minuit
            flag_datalogger=1;
            second_to_datalogger=1800;
        break;
        case 6: //1 hodina
            flag_datalogger=1;
            second_to_datalogger=3600;
        break;
        case 7: //2 hodiny
            flag_datalogger=1;
            second_to_datalogger=7200;
        break;
        case 8: //6 hodin
            flag_datalogger=1;
            second_to_datalogger=21600;
        break;
        case 9: //12 hodin
            flag_datalogger=1;
            second_to_datalogger=43200;
        break;
        case 10:    //1 den
            flag_datalogger=1;
            second_to_datalogger=86400;
        break;
        default:
            flag_datalogger=0;
            second_to_datalogger=0;
        break;
    }
  }

  ///odeslani dat do BINu - pokud je novejsi verze pulse, tak se vola 3x za sekundu na jinem miste v kodu sensor8.c
  if (pulse_version == PULSE_V1) // aktualizace 1x za sekundu
  {
    Send_Float(FLOW,g_measurement.actual);
    delay_1ms();
    Send_Long(ERR,(long)( actual_error ));
  }

  ///========nastaveni vystupniho modulu
  switch(flag_outputs_modules)
  {
    case IOUT_MODUL:        //pokud je aktivovan IOUT
      I_Out(g_measurement.actual);    //posle aktualni prutok do funkce
    break;
    case VOUT_MODUL:        //pokud je aktivovan VOUT
      V_Out(g_measurement.actual);
    break;

    default:
    break;
  }

  ExternalMeasurements(); // externi mereni

  if(flag_start>0)
    flag_start--;
}

void send_totals_sensor8(void)
{
  unsigned char buff[41];
  unsigned char pos=0;
  buff[pos++]=SLAVE_ID;

  buff[pos++]=WRITE_MULTIPLE_REGISTER;

  buff[pos++]=(unsigned int)MODBUS_TOTAL2 >> 8;
  buff[pos++]=(unsigned char)MODBUS_TOTAL2;

  buff[pos++]=0x00;   //pocet registru 2B - 4x4=16
  buff[pos++]=16;

  buff[pos++]=32;   //pocet bytu    4x8=32

  unsigned long dig=(unsigned long)(g_measurement.total/1000.0);
  unsigned long dec=(unsigned long)(g_measurement.total%1000);

  buff[pos++]=(unsigned char) (dig >> 8);
  buff[pos++]=(unsigned char) (dig);
  buff[pos++]=(unsigned char) (dig >> 24);
  buff[pos++]=(unsigned char) (dig >> 16);

  buff[pos++]=(unsigned char) (dec >> 8);
  buff[pos++]=(unsigned char) (dec);
  buff[pos++]=(unsigned char) (dec >> 24);
  buff[pos++]=(unsigned char) (dec >> 16);

  dig=(unsigned long)(g_measurement.flow_pos/1000.0);
  dec=(unsigned long)(g_measurement.flow_pos%1000);

  buff[pos++]=(unsigned char) (dig >> 8);
  buff[pos++]=(unsigned char) (dig);
  buff[pos++]=(unsigned char) (dig >> 24);
  buff[pos++]=(unsigned char) (dig >> 16);

  buff[pos++]=(unsigned char) (dec >> 8);
  buff[pos++]=(unsigned char) (dec);
  buff[pos++]=(unsigned char) (dec >> 24);
  buff[pos++]=(unsigned char) (dec >> 16);

  dig=(unsigned long)(g_measurement.flow_neg/1000.0);
  dec=(unsigned long)(g_measurement.flow_neg%1000);

  buff[pos++]=(unsigned char) (dig >> 8);
  buff[pos++]=(unsigned char) (dig);
  buff[pos++]=(unsigned char) (dig >> 24);
  buff[pos++]=(unsigned char) (dig >> 16);

  buff[pos++]=(unsigned char) (dec >> 8);
  buff[pos++]=(unsigned char) (dec);
  buff[pos++]=(unsigned char) (dec >> 24);
  buff[pos++]=(unsigned char) (dec >> 16);

  dig=(unsigned long)(g_measurement.aux/1000.0);
  dec=(unsigned long)(g_measurement.aux%1000);

  buff[pos++]=(unsigned char) (dig >> 8);
  buff[pos++]=(unsigned char) (dig);
  buff[pos++]=(unsigned char) (dig >> 24);
  buff[pos++]=(unsigned char) (dig >> 16);

  buff[pos++]=(unsigned char) (dec >> 8);
  buff[pos++]=(unsigned char) (dec);
  buff[pos++]=(unsigned char) (dec >> 24);
  buff[pos++]=(unsigned char) (dec >> 16);

  unsigned int crcecko=usMBCRC16(buff,39);;   //kontrolni soucet
  buff[pos++]=(unsigned char)(crcecko);
  buff[pos++]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,41);
  flag_modbus_ukon=SEND_TOTALS;
  flag_cekam_odpoved_od_sensor8=1;
}

void send_calibration_sensor8(void)
{
  unsigned char buff[33];
  unsigned char pos=0;
  buff[pos++]=SLAVE_ID;

  buff[pos++]=WRITE_MULTIPLE_REGISTER;

  buff[pos++]=(unsigned int)MODBUS_KALIB_1 >> 8;
  buff[pos++]=(unsigned char)MODBUS_KALIB_1;

  buff[pos++]=0x00;   //pocet regiustru 2B - 3x4=12
  buff[pos++]=0x0C;

  buff[pos++]=0x18;   //pocet bytu    3x8=24

  unsigned long calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_ONE);
  unsigned long meas_data=RAM_Read_Data_Long(MEASURED_POINT_ONE);

  buff[pos++]=(unsigned char) (calib_data >> 8);
  buff[pos++]=(unsigned char) (calib_data );
  buff[pos++]=(unsigned char) (calib_data >> 24);
  buff[pos++]=(unsigned char) (calib_data >> 16);

  buff[pos++]=(unsigned char) (meas_data >> 8);
  buff[pos++]=(unsigned char) (meas_data) ;
  buff[pos++]=(unsigned char) (meas_data >> 24);
  buff[pos++]=(unsigned char) (meas_data >> 16);


  calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_TWO);
  meas_data=RAM_Read_Data_Long(MEASURED_POINT_TWO);

  buff[pos++]=(unsigned char) (calib_data >> 8);
  buff[pos++]=(unsigned char) (calib_data );
  buff[pos++]=(unsigned char) (calib_data >> 24);
  buff[pos++]=(unsigned char) (calib_data >> 16);

  buff[pos++]=(unsigned char) (meas_data >> 8);
  buff[pos++]=(unsigned char) (meas_data) ;
  buff[pos++]=(unsigned char) (meas_data >> 24);
  buff[pos++]=(unsigned char) (meas_data >> 16);

  calib_data=RAM_Read_Data_Long(CALIBRATION_POINT_THREE);
  meas_data=RAM_Read_Data_Long(MEASURED_POINT_THREE);

  buff[pos++]=(unsigned char) (calib_data >> 8);
  buff[pos++]=(unsigned char) (calib_data );
  buff[pos++]=(unsigned char) (calib_data >> 24);
  buff[pos++]=(unsigned char) (calib_data >> 16);

  buff[pos++]=(unsigned char) (meas_data >> 8);
  buff[pos++]=(unsigned char) (meas_data) ;
  buff[pos++]=(unsigned char) (meas_data >> 24);
  buff[pos++]=(unsigned char) (meas_data >> 16);

  unsigned int crcecko=usMBCRC16(buff,31);;   //kontrolni soucet
  buff[pos++]=(unsigned char)(crcecko);
  buff[pos++]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,33);
  flag_modbus_ukon=ZAPIS_KALIBRACI;
  flag_sensor_zadosti |= ZADOST_ZAPIS_KALIBRACI;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_calibration_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_KALIB_1 >> 8);
  buff[3]=(unsigned char)(MODBUS_KALIB_1);

  buff[4]=0x00;   //pocet 4 registry, 1 reg je zapsan jako 2-byty
  buff[5]=0x0C;

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,8);
  flag_modbus_ukon=ZJISTI_KALIBRACI;
  flag_sensor_zadosti |= ZADOST_ZJISTI_KALIBRACI;
  flag_cekam_odpoved_od_sensor8=1;
}

void odesli_heslo_sensor8()
{
  unsigned char buff[21];
  buff[0]=SLAVE_ID;
  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)( MODBUS_HESLA >> 8);
  buff[3]=(unsigned char)( MODBUS_HESLA);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x06;

  buff[6]=0x0C;   //pocet bytu 4

  unsigned int heslo=1111;        //user

  buff[7]=(unsigned char)(heslo >> 8);
  buff[8]=(unsigned char)(heslo);
  buff[9]=(unsigned char)(heslo >> 24);
  buff[10]=(unsigned char)(heslo >> 16);

  heslo=5831;                     //service

  buff[11]=(unsigned char)(heslo >> 8);
  buff[12]=(unsigned char)(heslo);
  buff[13]=(unsigned char)(heslo >> 24);
  buff[14]=(unsigned char)(heslo >> 16);

  heslo=7367;

  buff[15]=(unsigned char)(heslo >> 8);
  buff[16]=(unsigned char)(heslo);
  buff[17]=(unsigned char)(heslo >> 24);
  buff[18]=(unsigned char)(heslo >> 16);

  unsigned int crcecko=usMBCRC16(buff,19);;   //kontrolni soucet
  buff[19]=(unsigned char)(crcecko);
  buff[20]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,21);
  flag_modbus_ukon = ODESLI_HESLA;
  flag_cekam_odpoved_od_sensor8=1;
}

void request_data_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_FLOW >> 8);
  buff[3]=(unsigned char)(MODBUS_FLOW);

  buff[4]=0x00;
  buff[5]=0x1E;

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=DATA_ZE_SENZORU;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void request_totals_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_TOTAL2 >> 8);
  buff[3]=(unsigned char)(MODBUS_TOTAL2);

  buff[4]=0x00;
  buff[5]=16;

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_TOTALS;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void sensor_stav_buzeni_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_EXCITATION_FREQ >> 8);
  buff[3]=(unsigned char)(MODBUS_EXCITATION_FREQ);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=ZJISTI_STAV_BUZENI;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void nastav_buzeni_sensor8()
{
  unsigned char buff[13];
  unsigned char pos=0;
  buff[pos++]=SLAVE_ID;
  buff[pos++]=WRITE_MULTIPLE_REGISTER;

  buff[pos++]=(unsigned char)( MODBUS_EXCITATION_FREQ >> 8);
  buff[pos++]=(unsigned char)( MODBUS_EXCITATION_FREQ);

  buff[pos++]=0x00;   //pocet registru 2B
  buff[pos++]=0x04;

  buff[pos++]=0x08;   //pocet bytu 4

  unsigned int help=RAM_Read_Data_Long(EXCITATION_FREQUENCY);

  buff[pos++]=(unsigned char)(help >> 8);
  buff[pos++]=(unsigned char)(help);
  buff[pos++]=(unsigned char)(help >> 24);
  buff[pos++]=(unsigned char)(help >> 16);

  help=RAM_Read_Data_Long(EXCITATION);

  buff[pos++]=(unsigned char)(help >> 8);
  buff[pos++]=(unsigned char)(help);
  buff[pos++]=(unsigned char)(help >> 24);
  buff[pos++]=(unsigned char)(help >> 16);

  unsigned int crcecko=usMBCRC16(buff,15);;   //kontrolni soucet
  buff[pos++]=(unsigned char)(crcecko);
  buff[pos++]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=ZAPNI_BUZENI;
  Send_sensor(buff,17);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_zero_constant_sensor8(long konstanta)
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char) ((MODBUS_ZERO_CONST) >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_ZERO_CONST);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  buff[7]=(unsigned char)(konstanta >> 8);
  buff[8]=(unsigned char)(konstanta);
  buff[9]=(unsigned char)(konstanta >> 24);
  buff[10]=(unsigned char)(konstanta >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = ZAPIS_ZERO_CONSTANT;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_zero_constant_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_ZERO_CONST >> 8);
  buff[3]=(unsigned char)(MODBUS_ZERO_CONST);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=CTENI_ZERO_CONSTANT;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_zero_flow_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_ZERO_FLOW >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_ZERO_FLOW);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_ZERO_FLOW;
  flag_cekam_odpoved_od_sensor8=1;
}

void send_zero_erase_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_ZERO_ERASE >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_ZERO_ERASE);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_ZERO_ERASE;
  flag_cekam_odpoved_od_sensor8=1;
}

void send_diameter_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DIAMETER >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DIAMETER);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(DIAMETER);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_DIAMETER;
  flag_cekam_odpoved_od_sensor8=1;
}

void send_unit_no_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_SET_UNIT >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_SET_UNIT);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(SENSOR_UNIT_NO);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_UNIT_NO;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_diameter_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_DIAMETER >> 8);
  buff[3]=(unsigned char)(MODBUS_DIAMETER);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_DIAMETER;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_firmware_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_FIRM_WARE >> 8);
  buff[3]=(unsigned char)(MODBUS_FIRM_WARE);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=CTENI_FIRMWARE;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_unit_no_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_UNIT_NO >> 8);
  buff[3]=(unsigned char)(MODBUS_UNIT_NO);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_UNIT_NO;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_demo_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_DEMO >> 8);
  buff[3]=(unsigned char)(MODBUS_DEMO);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_DEMO;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_demo_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DEMO >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DEMO);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(DEMO);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_DEMO;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_simulated_flow_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_SIMULATED_FLOW >> 8);
  buff[3]=(unsigned char)(MODBUS_SIMULATED_FLOW);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_SIMULATED_FLOW;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_simulated_flow_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_SIMULATED_FLOW >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_SIMULATED_FLOW);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(SIMULATED_FLOW);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_SIMULATED_FLOW;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_cisteni_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_CLEAN_POWER >> 8);
  buff[3]=(unsigned char)(MODBUS_CLEAN_POWER);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_CLEAN_POWER;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_cisteni_sensor8(unsigned char power)
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_CLEAN_POWER >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_CLEAN_POWER);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc;
  if( power==SENSOR_OFF)
      pomoc=0;
  else
      pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_CLEAN_POWER;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_flow_range_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_FLOW_RANGE >> 8);
  buff[3]=(unsigned char)(MODBUS_FLOW_RANGE);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_FLOW_RANGE;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_flow_range_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_FLOW_RANGE >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_FLOW_RANGE);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(FLOW_RANGE);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_FLOW_RANGE;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_air_detector_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_AIR_DETECTOR >> 8);
  buff[3]=(unsigned char)(MODBUS_AIR_DETECTOR);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_AIR_DETECTOR;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_air_detector_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_AIR_DETECTOR >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_AIR_DETECTOR);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(AIR_DETECTOR);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_AIR_DETECTOR;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_air_constant_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_AIR_CONSTANT >> 8);
  buff[3]=(unsigned char)(MODBUS_AIR_CONSTANT);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_AIR_CONSTANT;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_air_constant_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_AIR_CONSTANT >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_AIR_CONSTANT);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(AIR_CONSTANT);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_AIR_CONSTANT;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_measure_state_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_MEASUREMENT >> 8);
  buff[3]=(unsigned char)(MODBUS_MEASUREMENT);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_MEASUREMENT;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_measure_state_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_MEASUREMENT >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_MEASUREMENT);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(MEASUREMENT_STATE);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_MEASUREMENT;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_average_samples_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_AVERAGE_SAMPLES >> 8);
  buff[3]=(unsigned char)(MODBUS_AVERAGE_SAMPLES);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_AVERAGE_SAMPLES;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_average_samples_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_AVERAGE_SAMPLES >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_AVERAGE_SAMPLES);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(AVERAGE_SAMPLES);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_AVERAGE_SAMPLES;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_low_flow_cut_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_LOW_FLOW_CUT >> 8);
  buff[3]=(unsigned char)(MODBUS_LOW_FLOW_CUT);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_LOW_FLOW_CUT;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_low_flow_cut_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_LOW_FLOW_CUT >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_LOW_FLOW_CUT);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(LOW_FLOW_CUTOFF);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_LOW_FLOW_CUT;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_invert_flow_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_INVERT_FLOW >> 8);
  buff[3]=(unsigned char)(MODBUS_INVERT_FLOW);

  buff[4]=0x00;
  buff[5]=0x04;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_INVERT_FLOW;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_invert_flow_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_INVERT_FLOW >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_INVERT_FLOW);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(INVERT_FLOW);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_INVERT_FLOW;
  flag_cekam_odpoved_od_sensor8=1;
}

void recv_start_delay_sensor8()
{
  unsigned char buff[8];
  buff[0]=SLAVE_ID;

  buff[1]=READ_HOLDING_REGISTER;

  buff[2]=(unsigned char)(MODBUS_START_DELAY >> 8);
  buff[3]=(unsigned char)(MODBUS_START_DELAY);

  buff[4]=0x00;
  buff[5]=0x02;       //zadost msui byt delitelna dvema

  unsigned int crcecko=usMBCRC16(buff,6);;   //kontrolni soucet
  buff[6]=(unsigned char)(crcecko);
  buff[7]=(unsigned char)(crcecko>>8);

  flag_modbus_ukon=READ_START_DELAY;
  Send_sensor(buff,8);
  flag_cekam_odpoved_od_sensor8=1;
}

void send_start_delay_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_START_DELAY >> 8);
  buff[3]=(unsigned char)  (MODBUS_START_DELAY);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=RAM_Read_Data_Long(START_DELAY);

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = SEND_START_DELAY;
  flag_cekam_odpoved_od_sensor8=1;
}

void delete_aux_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DELETE_AUX >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DELETE_AUX);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = DELETE_AUX;
  flag_cekam_odpoved_od_sensor8=1;
  if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
    g_measurement.aux = 0;
}

void delete_volume_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DELETE_VOLUME >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DELETE_VOLUME);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = DELETE_VOLUME;
  flag_cekam_odpoved_od_sensor8=1;
  if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
      g_measurement.total = 0;
}

void delete_negative_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DELETE_NEGATIVE >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DELETE_NEGATIVE);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = DELETE_NEGATIVE;
  flag_cekam_odpoved_od_sensor8=1;
  if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
    g_measurement.flow_neg = 0;
}

void delete_positive_sensor8()
{
  unsigned char buff[13];
  buff[0]=SLAVE_ID;

  buff[1]=WRITE_MULTIPLE_REGISTER;

  buff[2]=(unsigned char)  (MODBUS_DELETE_POSITIVE >> 8);    //factory je posunuto o 4 dal nez zacatek hesel
  buff[3]=(unsigned char)  (MODBUS_DELETE_POSITIVE);

  buff[4]=0x00;   //pocet registru 2B
  buff[5]=0x02;

  buff[6]=0x04;   //pocet bytu je 4

  unsigned long pomoc=1;

  buff[7]=(unsigned char)(pomoc >> 8);
  buff[8]=(unsigned char)(pomoc);
  buff[9]=(unsigned char)(pomoc >> 24);
  buff[10]=(unsigned char)(pomoc >> 16);

  unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
  buff[11]=(unsigned char)(crcecko);
  buff[12]=(unsigned char)(crcecko>>8);

  Send_sensor(buff,13);
  flag_modbus_ukon = DELETE_POSITIVE;
  flag_cekam_odpoved_od_sensor8=1;
  if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
    g_measurement.flow_pos = 0;
}

void zpracovat_data_sensor8()
{
  flag_cekam_odpoved_od_sensor8=0;
  disable_timer2();
  switch(flag_modbus_ukon)
  {
      case CTENI_FIRMWARE:
          sensor_fw_version=(unsigned long)recv_buffer[4];
          sensor_fw_version+=(unsigned int)recv_buffer[3]<<8;
          flag_aktivni_zadost=0;
      break;

      case ZAPIS_KALIBRACI:
          ///jen asi zkontrolovat pocet bytu
          flag_sensor_zadosti &= ~ZADOST_ZAPIS_KALIBRACI;
          recv_calibration_sensor8();
      break;

      case ZJISTI_KALIBRACI:
          {
          unsigned char pos=3;
          unsigned long cdata=0;
          cdata=(unsigned long)recv_buffer[pos++]<<8;
          cdata+=(unsigned long)recv_buffer[pos++];
          cdata+=(unsigned long)recv_buffer[pos++]<<24;
          cdata+=(unsigned long)recv_buffer[pos++]<<16;

          unsigned long mdata=0;
          mdata=(unsigned long)recv_buffer[pos++]<<8;
          mdata+=(unsigned long)recv_buffer[pos++];
          mdata+=(unsigned long)recv_buffer[pos++]<<24;
          mdata+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(cdata,CALIBRATION_POINT_ONE);
          RAM_Write_Data_Long(mdata,MEASURED_POINT_ONE);

          cdata=0;
          cdata=(unsigned long)recv_buffer[pos++]<<8;
          cdata+=(unsigned long)recv_buffer[pos++];
          cdata+=(unsigned long)recv_buffer[pos++]<<24;
          cdata+=(unsigned long)recv_buffer[pos++]<<16;

          mdata=0;
          mdata=(unsigned long)recv_buffer[pos++]<<8;
          mdata+=(unsigned long)recv_buffer[pos++];
          mdata+=(unsigned long)recv_buffer[pos++]<<24;
          mdata+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(cdata,CALIBRATION_POINT_TWO);
          RAM_Write_Data_Long(mdata,MEASURED_POINT_TWO);

          cdata=0;
          cdata=(unsigned long)recv_buffer[pos++]<<8;
          cdata+=(unsigned long)recv_buffer[pos++];
          cdata+=(unsigned long)recv_buffer[pos++]<<24;
          cdata+=(unsigned long)recv_buffer[pos++]<<16;

          mdata=0;
          mdata=(unsigned long)recv_buffer[pos++]<<8;
          mdata+=(unsigned long)recv_buffer[pos++];
          mdata+=(unsigned long)recv_buffer[pos++]<<24;
          mdata+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(cdata,CALIBRATION_POINT_THREE);
          RAM_Write_Data_Long(mdata,MEASURED_POINT_THREE);
          }
          flag_sensor_zadosti &= ~ZADOST_ZJISTI_KALIBRACI;
          flag_aktivni_zadost=0;
      break;

      case ODESLI_HESLA:

      break;

      case DATA_ZE_SENZORU:
      {
          unsigned long pomocna;
          unsigned char pos=3;
          pomocna=0;  //aktualni prutok
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.actual=((float)pomocna)/1000.0;

          pomocna=0;  //smer prutoku
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          unsigned char sign = 0;
          if(pomocna)
          {
              g_measurement.actual*=-1.0;
              sign = 1;
          }

          pomocna=0;      //total dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {
              if (!RAM_Read_Data_Long(MEASUREMENT_STATE))
              {
                  g_measurement.total += fabs(g_measurement.actual / 36.0 * 3332.0);
                  if (g_measurement.total > 999999999000000)
                      g_measurement.total = 0;
              }
          }
          else if (sensor_fw_version < 3003)
          {
              g_measurement.total=pomocna;
              g_measurement.total*=1000000.0;
          }

          pomocna=0;      //total dec
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {}
          else if (sensor_fw_version < 3003)
              g_measurement.total+=(pomocna)*1000;

          pomocna=0;      //total + dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {
              if (!RAM_Read_Data_Long(MEASUREMENT_STATE)) // pokud je zapnute mereni
              {
                  if (!sign) // pokud je kladny prutok
                  {
                      g_measurement.flow_pos += fabs(g_measurement.actual / 36.0 * 3332.0);
                      if (g_measurement.flow_pos > 999999999000000)
                          g_measurement.flow_pos = 0;
                  }
              }
          }
          else if (sensor_fw_version < 3003)
          {
              g_measurement.flow_pos=pomocna;
              g_measurement.flow_pos*=1000000.0;
          }

          pomocna=0;      //total + deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {}
          else if (sensor_fw_version < 3003)
              g_measurement.flow_pos+=(pomocna)*1000;

          pomocna=0;      //total - dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {
              if (!RAM_Read_Data_Long(MEASUREMENT_STATE)) // pokud je zapnute mereni
              {
                  if (sign) // pokud je zaporny prutok
                  {
                      g_measurement.flow_neg += fabs(g_measurement.actual / 36.0 * 3332.0);
                      if (g_measurement.flow_neg > 999999999000000)
                          g_measurement.flow_neg = 0;
                  }
              }
          }
          else if (sensor_fw_version < 3003)
          {
              g_measurement.flow_neg=pomocna;
              g_measurement.flow_neg*=1000000.0;
          }

          pomocna=0;      //total - deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {}
          else if (sensor_fw_version < 3003)
              g_measurement.flow_neg+=(pomocna)*1000;

          pomocna=0;      //aux dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {
              if (!RAM_Read_Data_Long(MEASUREMENT_STATE)) // pokud je zapnute mereni
              {
                  if (!sign) // pokud je kladny prutok
                  {
                      g_measurement.aux += fabs(g_measurement.actual / 36.0 * 3332.0);
                      if (g_measurement.aux > 999999999000000)
                          g_measurement.aux = 0;
                  }
              }
          }
          else if (sensor_fw_version < 3003)
          {
              g_measurement.aux=pomocna;
              g_measurement.aux*=1000000.0;
          }

          pomocna=0;      //aux deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          if ((sensor_fw_version < 3001) || (sensor_fw_version > 4000))
          {}
          else if (sensor_fw_version < 3003)
              g_measurement.aux+=(pomocna)*1000;

          pomocna=0;      //error code
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          actual_error &= ~ERR_EMPTY_PIPE & ~ERR_OVERLOAD & ~ERR_ADC & ~ERR_EXCITATION & ~ERR_TEMPERATURE & ~ERR_OVERLOAD2;
          actual_error |= pomocna;

          pomocna=0;      //actual adc
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          prutok=pomocna; ///actual adc

          pomocna=0;      //average adc
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          adc_average=pomocna;

          pomocna=0;      //emoty pipe
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          ep_avg=pomocna;

          pomocna=0;      //teplota
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          teplota_avg=pomocna;
          g_measurement.temp=((signed long)(pomocna))/10.0;

          flag_aktivni_zadost=0;

          if ((sensor_fw_version >= 3003) && (sensor_fw_version <= 4000))
            request_totals_sensor8();
      }
      break;

      case READ_TOTALS:
      {
          unsigned long pomocna;
          unsigned char pos=3;

          pomocna=0;      //total dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.total=pomocna;
          g_measurement.total*=1000.0;

          pomocna=0;      //total dec
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.total+=(pomocna);

          pomocna=0;      //total + dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.flow_pos=pomocna;
          g_measurement.flow_pos*=1000.0;

          pomocna=0;      //total + deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.flow_pos+=(pomocna);

          pomocna=0;      //total - dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.flow_neg=pomocna;
          g_measurement.flow_neg*=1000.0;

          pomocna=0;      //total - deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.flow_neg+=(pomocna);

          pomocna=0;      //aux dig
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.aux=pomocna;
          g_measurement.aux*=1000.0;

          pomocna=0;      //aux deg
          pomocna=(unsigned long)recv_buffer[pos++]<<8;
          pomocna+=(unsigned long)recv_buffer[pos++];
          pomocna+=(unsigned long)recv_buffer[pos++]<<24;
          pomocna+=(unsigned long)recv_buffer[pos++]<<16;

          g_measurement.aux+=(pomocna);

          flag_aktivni_zadost=0;
      }
      break;

      case ZJISTI_STAV_BUZENI:
      {
          unsigned char pos=3;
          unsigned long zero=0;

          zero=(unsigned long)recv_buffer[pos++]<<8;
          zero+=(unsigned long)recv_buffer[pos++];
          zero+=(unsigned long)recv_buffer[pos++]<<24;
          zero+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(zero,EXCITATION_FREQUENCY);

          zero=0;
          zero=(unsigned long)recv_buffer[pos++]<<8;
          zero+=(unsigned long)recv_buffer[pos++];
          zero+=(unsigned long)recv_buffer[pos++]<<24;
          zero+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(zero,EXCITATION);
          flag_aktivni_zadost=0;
      }
      break;

      case ZAPNI_BUZENI:
          sensor_stav_buzeni_sensor8();
      break;

      case CTENI_ZERO_CONSTANT:
      {
          unsigned char pos=3;
          unsigned long zero=0;

          zero=(unsigned long)recv_buffer[pos++]<<8;
          zero+=(unsigned long)recv_buffer[pos++];
          zero+=(unsigned long)recv_buffer[pos++]<<24;
          zero+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(zero,ZERO_FLOW_CONSTANT);
          flag_aktivni_zadost=0;
      }
      break;

      case ZAPIS_ZERO_CONSTANT:
          recv_zero_constant();

      case READ_UNIT_NO:
      {
          unsigned char pos=3;
          sensor_unit_no_read = 0;

          sensor_unit_no_read=(unsigned long)recv_buffer[pos++]<<8;
          sensor_unit_no_read+=(unsigned long)recv_buffer[pos++];
          sensor_unit_no_read+=(unsigned long)recv_buffer[pos++]<<24;
          sensor_unit_no_read+=(unsigned long)recv_buffer[pos++]<<16;

          if (backup_sensor_unit_no)
            RAM_Write_Data_Long(sensor_unit_no_read, SENSOR_UNIT_NO);

          flag_aktivni_zadost=0;
      }
      break;

      case READ_MEASUREMENT:
      {
          unsigned char pos=3;
          unsigned long unit=0;

          unit=(unsigned long)recv_buffer[pos++]<<8;
          unit+=(unsigned long)recv_buffer[pos++];
          unit+=(unsigned long)recv_buffer[pos++]<<24;
          unit+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(unit,MEASUREMENT_STATE);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_DIAMETER:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,DIAMETER);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_LOW_FLOW_CUT:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,LOW_FLOW_CUTOFF);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_INVERT_FLOW:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,INVERT_FLOW);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_AVERAGE_SAMPLES:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,AVERAGE_SAMPLES);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_FLOW_RANGE:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,FLOW_RANGE);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_AIR_CONSTANT:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,AIR_CONSTANT);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_AIR_DETECTOR:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,AIR_DETECTOR);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_CLEAN_POWER:
      {
          unsigned char pos=3;
          unsigned long unit=0;

          unit=(unsigned long)recv_buffer[pos++]<<8;
          unit+=(unsigned long)recv_buffer[pos++];
          unit+=(unsigned long)recv_buffer[pos++]<<24;
          unit+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(unit,CLEAN_POWER);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_DEMO:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,DEMO);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_SIMULATED_FLOW:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,SIMULATED_FLOW);
          flag_aktivni_zadost=0;
      }
      break;

      case READ_START_DELAY:
      {
          unsigned char pos=3;
          unsigned long help=0;

          help=(unsigned long)recv_buffer[pos++]<<8;
          help+=(unsigned long)recv_buffer[pos++];
          help+=(unsigned long)recv_buffer[pos++]<<24;
          help+=(unsigned long)recv_buffer[pos++]<<16;

          RAM_Write_Data_Long(help,START_DELAY);
          flag_aktivni_zadost=0;
      }
      break;

      case SEND_ZERO_FLOW:
          recv_zero_constant();   //musi se naplnit ramka s aktualni hodnotou ze sens8
      break;

      case SEND_AIR_CONSTANT:
          recv_air_constant_sensor8();
      break;

      case SEND_LOW_FLOW_CUT:
          recv_low_flow_cut_sensor8();
      break;

      case SEND_INVERT_FLOW:
          recv_invert_flow_sensor8();
      break;

      case SEND_AVERAGE_SAMPLES:
          recv_average_samples_sensor8();
      break;

      case SEND_AIR_DETECTOR:
          recv_air_detector_sensor8();
      break;

      case SEND_MEASUREMENT:
          recv_measure_state_sensor8();
      break;

      case SEND_CLEAN_POWER:
          recv_cisteni_sensor8();
      break;

      case SEND_FLOW_RANGE:
          recv_flow_range_sensor8();
      break;

      case SEND_DIAMETER:
          recv_diameter_sensor8();
      break;

      case SEND_DEMO:
          recv_demo_sensor8();
      break;

      case SEND_SIMULATED_FLOW:
          recv_simulated_flow_sensor8();
      break;

      case SEND_ZERO_ERASE:
          recv_zero_constant();
      break;

      case SEND_START_DELAY:
          recv_start_delay_sensor8();
      break;

      case SEND_UNIT_NO:
          recv_unit_no_sensor8();
      break;

      case DELETE_AUX:
      case DELETE_VOLUME:
      case DELETE_NEGATIVE:
      case DELETE_POSITIVE:
          flag_aktivni_zadost=0;
      break;

      case SEND_TOTALS:
          flag_aktivni_zadost=0;
      break;

      default:
      break;
  }
  actual_error &= ~ERR_SENSOR;
  flag_receive=0;
  //flag_aktivni_zadost=0;
  //flag_modbus_ukon=0;
}

unsigned char inicializace_senzoru_senzor8()
{
  /**
  Nacte se ze sensoru nasledujici:

  hesla
  kalibrace
  measure state
  air detector
  air constant
  average samples
  low flow cut off
  invert flow
  flow range
  clean power
  demo
  simulated flow
  diameter
  unit sensor number
  zero constant
  excitaiton freq
  excitation
  pripadne se do senzoru zapisou zalohovane totalizery
  */

  odesli_heslo_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_firmware_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_calibration_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_measure_state_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_air_detector_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_air_constant_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_average_samples_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_low_flow_cut_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_invert_flow_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_flow_range_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_air_constant_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_demo_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_simulated_flow_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_diameter_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_unit_no_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  recv_zero_constant_sensor8();
  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  sensor_stav_buzeni();    //excitaion frequency a power excitation v jednom
    while( !flag_receive)
    {
      if( flag_timeout_timer2 )
        return 0;
    }
    flag_receive=0;
    zpracovat_data_sensor8();

  recv_start_delay_sensor8();
    while( !flag_receive)
    {
      if( flag_timeout_timer2 )
        return 0;
    }
    flag_receive=0;
    zpracovat_data_sensor8();

  switch ( RAM_Read_Data_Long(CLEAN_POWER))
  {
    case 0:
        send_cisteni_sensor8(SENSOR_OFF);
        flag_cisteni=0;
    break;
    case 1:
        send_cisteni_sensor8(SENSOR_ON);
        flag_cisteni_cas=64800;
        flag_cisteni=1;
    break;
    case 2:
        send_cisteni_sensor8(SENSOR_ON);
        flag_cisteni_cas=RAM_Read_Data_Long(CLEAN_TIME);
        flag_cisteni=1;
    break;

    default:
        send_cisteni_sensor8(SENSOR_OFF);
        flag_cisteni=0;
    break;
  }

  while( !flag_receive)
  {
    if( flag_timeout_timer2 )
      return 0;
  }
  flag_receive=0;
  zpracovat_data_sensor8();

  if (sensor_fw_version >= 3003)
  {
      if (sensor_unit_no_read == RAM_Read_Data_Long(SENSOR_UNIT_NO))
      {
          send_totals_sensor8();
          while( !flag_receive)
          {
              if( flag_timeout_timer2 )
                  return 0;
          }
          flag_receive=0;
          zpracovat_data_sensor8();
      }
  }

  return 1;
}
